package com.bokyu.fireanddestroy;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.bokyu.buildhouse.AdManager;
import com.bokyu.fireanddestroy.jobs.JobManager;
import com.bokyu.fireanddestroy.listeners.RewardAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.ArrayList;
import java.util.List;

public class AdManagerAndroid implements AdManager, RewardedVideoAdListener {

    List<AdView> _menuAdViews = new ArrayList();
    AdView _playAdView = null;
    private InterstitialAd _interstitialAd;
    private RewardedVideoAd _rewardedVideoAd;
    private JobManager.Token _token;
    private AdRequest _adRequest;
    private LinearLayout _layoutMenu = null;
    private LinearLayout _layoutPlay = null;
    private AndroidLauncher _context;

    public AdManagerAndroid() {
    }

    private AdView createAdView(Context context, String unitId, AdSize size) {
        AdView adView = new AdView(context);
        adView.setAdSize(size);
        adView.setAdUnitId(unitId);

        adView.setVisibility(View.VISIBLE);
        adView.bringToFront();
        adView.loadAd(_adRequest);
        adView.setAdListener(
                new AdListener() {

                    public void onAdClosed() {
                        Log.d("adlog", "adClosed");
                    }

                    public void onAdFailedToLoad(int var1) {
                        Log.d("adlog", "failed to load " + var1);
                    }

                    public void onAdLeftApplication() {
                        Log.d("adlog", "onAdLeftapplication");
                    }

                    public void onAdOpened() {
                        Log.d("adlog", "onAdOpened");
                    }

                    public void onAdLoaded() {
                        Log.d("adlog", "onAdLoaded");
                    }

                    public void onAdClicked() {
                        Log.d("adlog", "onAdClicked");
                    }

                    public void onAdImpression() {
                        Log.d("adlog", "onAdImpression");
                    }
                }
        );
        return adView;
    }

    public void initializeAds(AndroidLauncher context) {
        _context = context;
        _adRequest = new AdRequest.Builder()
                .addTestDevice("CDF155B0E6524332239BB0D38451635A")
                .addTestDevice("FDBC638B9EB66CA8957B2D998BF4EE99")
                .addTestDevice("F17AD9DAA7CFD267F3EBECFE7F84D08E")
                .addTestDevice("DCE864DD6774BD86C7DDE3CB41D98ABB")
                .build();


        _menuAdViews.add(createAdView(context, "ca-app-pub-2500234658671976/5952509305", AdSize.LARGE_BANNER));
        _playAdView = createAdView(context, "ca-app-pub-2500234658671976/6096458462", AdSize.BANNER);
//        _menuAdViews.add(createAdView(context, "ca-app-pub-2500234658671976/7272565498"));
//        _menuAdViews.add(createAdView(context, "ca-app-pub-2500234658671976/1828667125"));

        _interstitialAd = new InterstitialAd(context);
        _interstitialAd.setAdUnitId("ca-app-pub-2500234658671976/4680817905");
        _interstitialAd.setAdListener(
                new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        // Code to be executed when an ad finishes loading.
                        System.out.println("loaded");
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // Code to be executed when an ad request fails.
                        if ( _token != null ) {
                            _token.finished();
                        }
                    }

                    @Override
                    public void onAdOpened() {
                        // Code to be executed when the ad is displayed.
                    }

                    @Override
                    public void onAdLeftApplication() {
                        // Code to be executed when the user has left the app.
                    }

                    @Override
                    public void onAdClosed() {
                        // Code to be executed when when the interstitial ad is closed.
                        _interstitialAd.loadAd(_adRequest);
                        if ( _token != null ) {
                            _token.finished();
                        }
                    }
                }
        );
        _interstitialAd.loadAd(_adRequest);

        _rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context);
        _rewardedVideoAd.setRewardedVideoAdListener(this);

        loadRewardAd();
    }

    public void reloadMenuAd() {
        for ( AdView adView : _menuAdViews) {
            adView.loadAd(_adRequest);
        }
    }

    public void showInterstitial(final JobManager.Token token) {
        _token = token;
        _context.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        if ( _interstitialAd.isLoaded() ) {
                            _interstitialAd.show();
                        } else {
                            if ( _interstitialAd.isLoading() == false ) {
                                _interstitialAd.loadAd(_adRequest);
                            }
                            if ( token != null ) token.finished();
                        }

                    }
                }
        );
    }

    private void loadRewardAd() {
                        _rewardedVideoAd.loadAd("ca-app-pub-2500234658671976/1147108442", _adRequest);
    }

    private RewardAdListener _rewardListener;
    @Override
    public void showReward(RewardAdListener listener) {

        _rewardListener = listener;

        if ( _rewardFreePass ) {
            listener.rewardSuccess();
        } else {
            _context.runOnUiThread(
                    new Runnable() {
                        @Override
                        public void run() {
                            if (_rewardedVideoAd.isLoaded()) {
                                _rewardedVideoAd.show();
                            } else {
                                loadRewardAd();
                            }
                        }
                    }
            );
        }

    }

    @Override
    public void setMenuAdVisibility(final boolean menuVisible, final boolean playVisible)
    {
        _context.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        _layoutMenu.setVisibility(menuVisible ? View.VISIBLE : View.GONE);
                        _layoutPlay.setVisibility(playVisible ? View.VISIBLE : View.GONE);
//                        if ( menuVisible ) {
//                            reloadMenuAd();
//                        }
//                        if ( playVisible ) {
//                            _playAdView.loadAd(_adRequest);
//                        }
                    }
                }
        );
    }

    public void addToLayout(LinearLayout layoutMenu, LinearLayout layoutPlay) {
        _layoutMenu = layoutMenu;
        _layoutPlay = layoutPlay;

        LinearLayout.LayoutParams adParams =
                new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
        for ( AdView adView : _menuAdViews) {
            layoutMenu.addView(adView, adParams);
        }

        layoutPlay.addView(_playAdView, adParams);
    }

    @Override
    public void onRewarded(RewardItem reward) {
        if ( _rewardListener != null ) {
            _rewardListener.rewardSuccess();
        }
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        if ( _rewardListener != null ) {
            _rewardListener.rewardFailed();
        }
    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardAd();
        if ( _token != null ) {
            _token.finished();
        }
    }

    boolean _rewardFreePass = false;
    @Override
    public void onRewardedVideoAdFailedToLoad(int errorCode) {
        System.out.println("reward video load failed : " + errorCode);
        if ( errorCode == 3 ) {
            _rewardFreePass = true;
        }
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        _rewardFreePass = false;
    }

    @Override
    public void onRewardedVideoAdOpened() {
    }

    @Override
    public void onRewardedVideoStarted() {
    }

    @Override
    public void onRewardedVideoCompleted() {
    }
}
