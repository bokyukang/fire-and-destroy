package com.bokyu.fireanddestroy.localdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.bokyu.fireanddestroy.localdb.tables.BuildingGlyphsTable;
import com.bokyu.fireanddestroy.localdb.tables.BuildingModelsTable;
import com.bokyu.fireanddestroy.localdb.tables.BuildingsTable;
import com.bokyu.fireanddestroy.localdb.tables.GlyphsTable;
import com.bokyu.fireanddestroy.localdb.tables.VillagesTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by bokyu on 3/30/2016.
 */
public class SqliteManager {

    static public class SqlTable {
        protected Map<String, Column> mColumns = new HashMap();
        public enum DataType
        {
            NULL,
            INTEGER,
            REAL,
            TEXT,
            BLOB
        }
        static public class Column {
            public Column(DataType dataType, boolean isUnique, boolean isAutoIncrement, boolean isPrimaryKey, boolean isKey) {
                mDataType = dataType;
                mIsUnique = isUnique;
                mIsPrimaryKey = isPrimaryKey;
                mIsKey = isKey;
                mIsAutoincrement = isAutoIncrement;
            }
            public DataType mDataType = DataType.NULL;
            public boolean mIsUnique = false;
            public boolean mIsPrimaryKey = false;
            public boolean mIsKey = false;
            public boolean mIsAutoincrement = false;
        }
        public String getCreateString() {
            String stmt = "CREATE TABLE " + getName() + "(";
            boolean first = true;
            for ( String columnName : mColumns.keySet() ) {
                Column column = mColumns.get(columnName);
                if ( first ) {
                    first = false;
                } else {
                    stmt += ", ";
                }
                stmt += columnName;
                switch ( column.mDataType ) {
                    case NULL:
                        stmt += " NULL";
                        break;
                    case INTEGER:
                        stmt += " INTEGER";
                        break;
                    case REAL:
                        stmt += " REAL";
                        break;
                    case TEXT:
                        stmt += " TEXT";
                        break;
                    case BLOB:
                        stmt += " BLOB";
                        break;
                }
                if ( column.mIsPrimaryKey ) {
                    stmt += " PRIMARY KEY";
                } else if ( column.mIsUnique ) {
                    stmt += " UNIQUE";
                }
                if ( column.mIsAutoincrement ) {
                    stmt += " AUTOINCREMENT";
                }
            }
            stmt += ");";
            for ( String columnName : mColumns.keySet() ) {
                Column column = mColumns.get(columnName);
                if ( column.mIsKey ) {
                    stmt += "CREATE INDEX " + getName() + "_" + columnName + " ON "
                            + getName() + " (" + columnName + ");";
                }
            }
            Log.d("create statement", stmt);
            return stmt;
        }

        public String getName() { return ""; }
    }

    public class MyHelper extends SQLiteOpenHelper {
        public MyHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            for ( SqlTable table : mTables.values() )
            {
                db.execSQL(table.getCreateString());
            }
            copyFromInitialDb();
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            for ( String tableName : mTables.keySet() ) {
                db.execSQL("DROP TABLE IF EXISTS " + tableName);
            }
            onCreate(db);
        }

    }


    static private SqliteManager mInstance = null;
    static public String DB_NAME = "buildhouse";
    static public int DB_VERSION = 16;

    /* member variables */
    private Map<String, SqlTable> mTables = new HashMap();
    private MyHelper mHelper;
    private Context mContext = null;


    static public SqliteManager instance() {
        if ( mInstance == null ) {
            mInstance = new SqliteManager();
        }

        return mInstance;
    }

    // should be called menually
    public void init(Context context) {
        mContext = context;
        mTables.put( BuildingGlyphsTable.Entry.TABLE_NAME, new BuildingGlyphsTable() );
        mTables.put( BuildingModelsTable.Entry.TABLE_NAME, new BuildingModelsTable() );
        mTables.put(BuildingsTable.Entry.TABLE_NAME, new BuildingsTable());
        mTables.put(VillagesTable.Entry.TABLE_NAME, new VillagesTable());
        mTables.put(GlyphsTable.Entry.TABLE_NAME, new GlyphsTable());

        mHelper = new MyHelper(context);
    }

    public MyHelper getHelper() { return mHelper; }

    public void copyFromInitialDb() {
        try {
            InputStream inputStream = mContext.getAssets().open("initial.db");
            File dbPath = mContext.getDatabasePath(DB_NAME);
            OutputStream outputStream = new FileOutputStream(dbPath);
            if (dbPath.exists() == false) {
                dbPath.getParentFile().mkdirs();
                dbPath.createNewFile();
            }

            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, len);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    public void copyToInitialDb() {
        try {
            File filesDir = mContext.getExternalFilesDir("initial");
            File outFile = new File(filesDir.getPath(), "initial.txt");
            if ( outFile.exists() == false ) {
                outFile.getParentFile().mkdirs();
                outFile.createNewFile();
            }
            outFile.setWritable(true);
            OutputStream outputStream = new FileOutputStream(outFile);
            File dbPath = mContext.getDatabasePath(DB_NAME);
            InputStream inputStream = new FileInputStream(dbPath);

            byte[] buffer = new byte[1024];
            int len;
            while ( (len = inputStream.read(buffer)) > 0 ) {
                outputStream.write(buffer, 0, len);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();
        } catch ( IOException exception ) {
            exception.printStackTrace();
        }
    }

}
