package com.bokyu.fireanddestroy.localdb;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bk on 2017-02-02.
 */

public class WhereClause {
    private Map<String, String> _fields = new HashMap();

    public void setField(String name, String value) {
        _fields.put(name, "'" + value + "'");
    }
    public void setField(String name, float value) {
        _fields.put(name,  Float.toString(value));
    }
    public void setField(String name, int value) {
        _fields.put(name,  Integer.toString(value));
    }

    public void clear() { _fields.clear(); }

    public String getString() {
        String result = "";
        boolean isFirst = true;
        for ( String key : _fields.keySet() )
        {
            if (!isFirst) {
                result += " and ";
            }

            String value = _fields.get(key);

            result += key + " = " + value;

            isFirst = false;
        }
        return result;
    }
}
