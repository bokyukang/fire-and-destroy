package com.bokyu.fireanddestroy.localdb.tables;

import android.provider.BaseColumns;

import com.bokyu.fireanddestroy.localdb.SqliteManager;


/**
 * Created by bk on 2016-12-21.
 */

public class BuildingGlyphsTable extends SqliteManager.SqlTable {
    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "building_glyphs";
        public static final String CN_ACCOUNT_ID = "account_id";
        public static final String CN_VILLAGE_ID = "village_id";
        public static final String CN_TYPE = "type";
        public static final String CN_DIRECTION = "direction";
        public static final String CN_DIAGONAL = "diagonal";
        public static final String CN_ELEVATION = "elevation";
        public static final String CN_X = "x";
        public static final String CN_Y = "y";
        public static final String CN_RESOURCE_ID = "resource_id";
        public static final String CN_TEX_ID = "tex_id";
        public static final String CN_BUILDING_ID = "building_id";
    }

    public BuildingGlyphsTable() {
        mColumns.put(Entry.CN_ACCOUNT_ID, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_TYPE, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_DIAGONAL, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_DIRECTION, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_ELEVATION, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_X, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_Y, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_RESOURCE_ID, new Column(DataType.TEXT, false, false, false, false));
        mColumns.put(Entry.CN_TEX_ID, new Column(DataType.TEXT, false, false, false, false));
        mColumns.put(Entry.CN_BUILDING_ID, new Column(DataType.INTEGER, false, false, false, true));
    }

    @Override
    public String getName() {
        return Entry.TABLE_NAME;
    }
}
