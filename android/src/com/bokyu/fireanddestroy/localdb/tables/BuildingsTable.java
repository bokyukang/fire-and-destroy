package com.bokyu.fireanddestroy.localdb.tables;

import android.provider.BaseColumns;

import com.bokyu.fireanddestroy.localdb.SqliteManager;


/**
 * Created by bk on 2017-01-12.
 */

public class BuildingsTable extends SqliteManager.SqlTable {
    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "buildings";
        public static final String CN_ACCOUNT_ID = "account_id";
        public static final String CN_VILLAGE_ID = "village_id";
        public static final String CN_X = "x";
        public static final String CN_Y = "y";
        public static final String CN_ROTATION = "rotation";
        public static final String CN_BUILDING_ID = "building_id";
        public static final String CN_BUILDING_MODEL_ID = "building_model_id";
    }

    public BuildingsTable() {
        mColumns.put(Entry.CN_ACCOUNT_ID, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_BUILDING_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_BUILDING_MODEL_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_X, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_Y, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_ROTATION, new Column(DataType.INTEGER, false, false, false, false));
    }

    @Override
    public String getName() { return Entry.TABLE_NAME; }
}
