package com.bokyu.fireanddestroy.localdb.tables;

import android.provider.BaseColumns;

import com.bokyu.fireanddestroy.localdb.SqliteManager;


/**
 * Created by bk on 2016-12-21.
 */

public class BuildingModelsTable extends SqliteManager.SqlTable {
    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "buildingmodels";
        public static final String CN_ACCOUNT_ID = "account_id";
        public static final String CN_VILLAGE_ID = "village_id";
        public static final String CN_NAME = "name";
        public static final String CN_BUILDING_MODEL_ID = "building_model_id";
    }

    public BuildingModelsTable() {
        mColumns.put(Entry.CN_ACCOUNT_ID, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_NAME, new Column(DataType.TEXT, false, false, false, false));
        mColumns.put(Entry.CN_BUILDING_MODEL_ID, new Column(DataType.INTEGER, false, false, false, true));
    }

    @Override
    public String getName() { return Entry.TABLE_NAME; }
}
