package com.bokyu.fireanddestroy.localdb.tables;

import android.provider.BaseColumns;

import com.bokyu.fireanddestroy.localdb.SqliteManager;


/**
 * Created by bk on 2017-02-02.
 */

public class VillagesTable extends SqliteManager.SqlTable {
    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "villages";
        public static final String CN_ACCOUNT_ID = "account_id";
        public static final String CN_VILLAGE_ID = "village_id";
        public static final String CN_VILLAGE_NAME = "village_name";
        public static final String CN_TERREIN = "terrein";
    }

    public VillagesTable() {
        mColumns.put(Entry.CN_ACCOUNT_ID, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_NAME, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_TERREIN, new Column(DataType.TEXT, false, false, false, false));
    }

    @Override
    public String getName() { return Entry.TABLE_NAME; }
}
