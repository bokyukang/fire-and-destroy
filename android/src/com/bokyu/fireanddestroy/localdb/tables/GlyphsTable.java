package com.bokyu.fireanddestroy.localdb.tables;

import android.provider.BaseColumns;

import com.bokyu.fireanddestroy.localdb.SqliteManager;


/**
 * Created by bk on 2017-02-15.
 */

public class GlyphsTable extends SqliteManager.SqlTable {
    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "glyphs";
        public static final String CN_ACCOUNT_ID = "account_id";
        public static final String CN_VILLAGE_ID = "village_id";
        public static final String CN_TYPE = "type";
        public static final String CN_DIRECTION = "direction";
        public static final String CN_X = "x";
        public static final String CN_Y = "y";
        public static final String CN_RESOURCE_ID = "resource_id";
    }

    public GlyphsTable() {
        mColumns.put(Entry.CN_ACCOUNT_ID, new Column(DataType.TEXT, false, false, false, true));
        mColumns.put(Entry.CN_VILLAGE_ID, new Column(DataType.INTEGER, false, false, false, true));
        mColumns.put(Entry.CN_TYPE, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_DIRECTION, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_X, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_Y, new Column(DataType.INTEGER, false, false, false, false));
        mColumns.put(Entry.CN_RESOURCE_ID, new Column(DataType.TEXT, false, false, false, false));
    }

    @Override
    public String getName() {
        return Entry.TABLE_NAME;
    }
}
