package com.bokyu.fireanddestroy.localdb;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.data.GlyphDbData;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.fireanddestroy.localdb.tables.BuildingGlyphsTable;
import com.bokyu.fireanddestroy.localdb.tables.BuildingModelsTable;
import com.bokyu.fireanddestroy.localdb.tables.BuildingsTable;
import com.bokyu.fireanddestroy.localdb.tables.GlyphsTable;
import com.bokyu.fireanddestroy.localdb.tables.VillagesTable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by bk on 2016-12-20.
 */

public class DbManagerAndroid implements DbManager {

    final String PREF_LEVEL = "level";

    private Context _context = null;
    private SQLiteDatabase _db = null;
    private SharedPreferences _sharedPreferences = null;

    public DbManagerAndroid(Context context) {
        _context = context;
    }

    @Override
    public void init() {
        SqliteManager.instance().init(_context);
        _db = SqliteManager.instance().getHelper().getWritableDatabase();
        _sharedPreferences = _context.getSharedPreferences("",0);
    }

    @Override
    public boolean saveClearedLevel(int level)
    {
        _sharedPreferences.edit().putInt(PREF_LEVEL, level).apply();
        _sharedPreferences.edit().commit();
        return true;
    }

    @Override
    public int getClearedLevel()
    {
        return _sharedPreferences.getInt(PREF_LEVEL, 0);
    }

    @Override
    public boolean saveBuildingModel(Building building){
        ContentValues contentValues = new ContentValues();
        int nextId = getNextBuildingModelId();
        contentValues.put(BuildingModelsTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        contentValues.put(BuildingModelsTable.Entry.CN_VILLAGE_ID, Account.instance().getVillageId());
        contentValues.put(BuildingModelsTable.Entry.CN_NAME, building.getName());
        contentValues.put(BuildingModelsTable.Entry.CN_BUILDING_MODEL_ID, nextId);
        building._modelId = nextId;
        if ( _db.insert(BuildingModelsTable.Entry.TABLE_NAME, null, contentValues) == -1 ) {
            return false;
        }

        for (MyGlyph glyph : building.getGlyphs())
        {
            if (!saveBuildingGlyph((BuildingGlyph)glyph, nextId) ) return false;
        }
        return true;
    }

    public boolean deleteBuildingModel(int id) {
        if ( _db.delete(BuildingModelsTable.Entry.TABLE_NAME,
                BuildingModelsTable.Entry.CN_BUILDING_MODEL_ID + "=" + id + " and "
                        + BuildingModelsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and "
                        + BuildingModelsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId()
                 , null) == -1 ) {
            return false;
        }
        if ( _db.delete(BuildingGlyphsTable.Entry.TABLE_NAME,
                BuildingGlyphsTable.Entry.CN_BUILDING_ID + "=" + id + " and account_id = '"
            + Account.instance().getId() + "' and village_id = " + Account.instance().getVillageId(), null) == -1 ) {
            return false;
        }
        return true;
    }

    private int getNextBuildingModelId() {
        String sql = "select max(" + BuildingModelsTable.Entry.CN_BUILDING_MODEL_ID + ") from "
            + BuildingModelsTable.Entry.TABLE_NAME + " where " + BuildingModelsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and "
                + BuildingModelsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        int nextId = 0;
        if ( cursor.moveToFirst() )
        {
            nextId = cursor.getInt(0) + 1;
        }
        cursor.close();
        return nextId;
    }

    private int getNextBuildingId() {
        String sql = "select max(" + BuildingsTable.Entry.CN_BUILDING_ID + ") from "
                + BuildingsTable.Entry.TABLE_NAME + " where " + BuildingsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and "
                + BuildingsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        int nextId = 0;
        if ( cursor.moveToFirst() )
        {
            nextId = cursor.getInt(0) + 1;
        }
        cursor.close();
        return nextId;
    }

    private boolean saveBuildingGlyph(BuildingGlyph buildingGlyph, int buildingId){
        ContentValues contentValues = new ContentValues();
        contentValues.put(BuildingGlyphsTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        contentValues.put(BuildingGlyphsTable.Entry.CN_VILLAGE_ID, Account.instance().getVillageId());
        contentValues.put(BuildingGlyphsTable.Entry.CN_TYPE, buildingGlyph.getType().ordinal());
        contentValues.put(BuildingGlyphsTable.Entry.CN_DIRECTION, buildingGlyph.getDIrection().ordinal());
        contentValues.put(BuildingGlyphsTable.Entry.CN_DIAGONAL, buildingGlyph._isDiagonal);
        contentValues.put(BuildingGlyphsTable.Entry.CN_ELEVATION, buildingGlyph.getElevation());
        contentValues.put(BuildingGlyphsTable.Entry.CN_RESOURCE_ID, buildingGlyph.getResourceId());
        contentValues.put(BuildingGlyphsTable.Entry.CN_TEX_ID, buildingGlyph.getTextureId());
        contentValues.put(BuildingGlyphsTable.Entry.CN_BUILDING_ID, buildingId);
        contentValues.put(BuildingGlyphsTable.Entry.CN_X, buildingGlyph.getPositionIndex()._x);
        contentValues.put(BuildingGlyphsTable.Entry.CN_Y, buildingGlyph.getPositionIndex()._y);

        if ( _db.insert(BuildingGlyphsTable.Entry.TABLE_NAME, null, contentValues) == -1 ) {
            return false;
        }
        return true;
    }

    public boolean saveGlyph(GlyphDbData data) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GlyphsTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        contentValues.put(GlyphsTable.Entry.CN_VILLAGE_ID, Account.instance().getVillageId());
        contentValues.put(GlyphsTable.Entry.CN_RESOURCE_ID, data._resourceId);
        contentValues.put(GlyphsTable.Entry.CN_DIRECTION, data._direction.ordinal());
        contentValues.put(GlyphsTable.Entry.CN_X, data._x);
        contentValues.put(GlyphsTable.Entry.CN_Y, data._y);
        contentValues.put(GlyphsTable.Entry.CN_TYPE, data._type.ordinal());
        if ( _db.insert(GlyphsTable.Entry.TABLE_NAME, null, contentValues) == -1 ) {
            return false;
        }
        return true;
    }

    public Set<GlyphDbData> loadGlyphs() {
        Set<GlyphDbData> glyphs = new HashSet();
        String sql  = "select " + GlyphsTable.Entry.CN_X + ", " + GlyphsTable.Entry.CN_Y + ", "
                + GlyphsTable.Entry.CN_DIRECTION + ", " + GlyphsTable.Entry.CN_TYPE + ", "
                + GlyphsTable.Entry.CN_RESOURCE_ID + " from " +
                GlyphsTable.Entry.TABLE_NAME + " where " +
                BuildingModelsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and " +
                BuildingModelsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        if ( cursor.moveToFirst() ) {
            do {
                int x = cursor.getInt(cursor.getColumnIndex(GlyphsTable.Entry.CN_X));
                int y = cursor.getInt(cursor.getColumnIndex(GlyphsTable.Entry.CN_Y));
                int direction = cursor.getInt(cursor.getColumnIndex(GlyphsTable.Entry.CN_DIRECTION));
                int type = cursor.getInt(cursor.getColumnIndex(GlyphsTable.Entry.CN_TYPE));
                String resourceId = cursor.getString(cursor.getColumnIndex(GlyphsTable.Entry.CN_RESOURCE_ID));

                GlyphDbData newData = new GlyphDbData();
                newData._x = x;
                newData._y = y;
                newData._direction = Constants.DIRECTION.values()[direction];
                newData._type = MyGlyph.TYPE.values()[type];
                newData._resourceId = resourceId;

                glyphs.add(newData);
            } while (cursor.moveToNext());
        }

        return glyphs;
    }

    public void close() {
        _db.close();
    }

    @Override
    public Set<Building> loadBuildingModels() {
        Set<Building> buildings = new HashSet();
        String sql = "select " + BuildingModelsTable.Entry.CN_NAME + ", " +
                BuildingModelsTable.Entry.CN_BUILDING_MODEL_ID + " from " +
                BuildingModelsTable.Entry.TABLE_NAME + " where " +
                BuildingModelsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and " +
                BuildingModelsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        if ( cursor.moveToFirst() )
        {
            do {
                String name = cursor.getString( cursor.getColumnIndex(BuildingModelsTable.Entry.CN_NAME) );
                int buildingId = cursor.getInt( cursor.getColumnIndex(BuildingModelsTable.Entry.CN_BUILDING_MODEL_ID));
                Building building = new Building();
                building.setName(name);
                building._modelId = buildingId;

                Set<BuildingGlyph> glyphs = loadBuildingGlyphs(buildingId);
                for ( BuildingGlyph glyph : glyphs ) building.addGlyph(glyph);

                buildings.add(building);
            } while( cursor.moveToNext());
        }

        cursor.close();
        return buildings;
    }

    Set<BuildingGlyph> loadBuildingGlyphs(int buildingId) {
        String sql = "select " + BuildingGlyphsTable.Entry.CN_TYPE + ", "
                    + BuildingGlyphsTable.Entry.CN_DIRECTION + ", "
                    + BuildingGlyphsTable.Entry.CN_DIAGONAL + ", "
                    + BuildingGlyphsTable.Entry.CN_ELEVATION + ", "
                    + BuildingGlyphsTable.Entry.CN_RESOURCE_ID + ", "
                    + BuildingGlyphsTable.Entry.CN_TEX_ID + ", "
                    + BuildingGlyphsTable.Entry.CN_X + ", "
                    + BuildingGlyphsTable.Entry.CN_Y + " from "
                    + BuildingGlyphsTable.Entry.TABLE_NAME + " where "
                    + BuildingGlyphsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and "
                    + BuildingGlyphsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId()
                    + " and " + BuildingGlyphsTable.Entry.CN_BUILDING_ID + " = " + buildingId;
        Cursor cursor = _db.rawQuery(sql, null);
        Set<BuildingGlyph> glyphs = new HashSet();

        if (cursor.moveToFirst()) {
            do {
                int type = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_TYPE));
                int direction = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_DIRECTION));
                int elevation = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_ELEVATION));
                int idxX = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_X));
                int idxY = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_Y));
                boolean isDiagonal = cursor.getInt( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_DIAGONAL)) == 0 ? false : true;
                String resourceId = cursor.getString( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_RESOURCE_ID));
                String texId = cursor.getString( cursor.getColumnIndex(BuildingGlyphsTable.Entry.CN_TEX_ID));
                BuildingGlyph buildingGlyph = BuildingsManager.instance().generateGlyph(
                        BuildingGlyph.TYPE.values()[type], resourceId, texId);
                if ( buildingGlyph == null ) continue;
                buildingGlyph.setTextureId(texId);
                buildingGlyph.setDirection(Constants.DIRECTION.values()[direction]);
                buildingGlyph.setElevation(elevation);
                buildingGlyph.setPositionIndexes(idxX,idxY);
                buildingGlyph.updatePosition();
                buildingGlyph._isDiagonal = isDiagonal;
                glyphs.add(buildingGlyph);
            } while( cursor.moveToNext());
        }
        return glyphs;
    }

    @Override
    public boolean saveTerrein(String encodedTerrein) {

        createVillageIfNot();

        ContentValues contentValues = new ContentValues();
        contentValues.put(VillagesTable.Entry.CN_TERREIN, encodedTerrein);

        if (_db.update(VillagesTable.Entry.TABLE_NAME, contentValues, VillagesTable.Entry.CN_ACCOUNT_ID + " = '" +
            Account.instance().getId() + "' and " + VillagesTable.Entry.CN_VILLAGE_ID + " = " +
                Account.instance().getVillageId(), null
        ) == -1 ) {
            return false;
        }
        return true;
    }

    public String loadTerrein() {
        String sql = "select " + VillagesTable.Entry.CN_TERREIN + " from " + VillagesTable.Entry.TABLE_NAME
                 + " where " + VillagesTable.Entry.CN_ACCOUNT_ID + " = '" +
            Account.instance().getId() + "' and " + VillagesTable.Entry.CN_VILLAGE_ID + " = " +
                Account.instance().getVillageId();

        Cursor cursor = _db.rawQuery(sql, null);
        String result = "";
        if ( cursor.moveToFirst() ) {
            do {
                result = cursor.getString(0);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return result;
    }

    public void createVillageIfNot() {
        String sql = "select " + VillagesTable.Entry.CN_TERREIN + " from " + VillagesTable.Entry.TABLE_NAME
                + " where " + VillagesTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and "
                + VillagesTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        int count = cursor.getCount();
        cursor.close();
        if ( count == 0 ) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(VillagesTable.Entry.CN_TERREIN, "");
            contentValues.put(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
            contentValues.put(VillagesTable.Entry.CN_VILLAGE_ID, Account.instance().getVillageId());

            _db.insert(VillagesTable.Entry.TABLE_NAME, null, contentValues);
        }
    }

    public boolean saveBuilding(Building building){
        ContentValues contentValues = new ContentValues();
        contentValues.put(BuildingsTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        contentValues.put(BuildingsTable.Entry.CN_VILLAGE_ID, Account.instance().getVillageId());
        contentValues.put(BuildingsTable.Entry.CN_BUILDING_MODEL_ID, building._modelId);
        contentValues.put(BuildingsTable.Entry.CN_X, building.getPosition()._x);
        contentValues.put(BuildingsTable.Entry.CN_Y, building.getPosition()._y);
        contentValues.put(BuildingsTable.Entry.CN_ROTATION, building.getRotation());
        int nextId = getNextBuildingId();
        contentValues.put(BuildingsTable.Entry.CN_BUILDING_ID, nextId);
        if ( _db.insert(BuildingsTable.Entry.TABLE_NAME, null, contentValues) == -1 ) {
            building.setId(nextId);
            return false;
        }
        return true;
    }

    public boolean deleteBuilding(int id) {
        if ( _db.delete(BuildingsTable.Entry.TABLE_NAME,
                BuildingsTable.Entry.CN_BUILDING_ID + "=" + id + " and " +
                        BuildingsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and " +
                        BuildingsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId()
                , null) == -1 ) {
            return false;
        }
        return true;
    }

    @Override
    public Set<Building> loadBuildings() {
        Set<Building> buildings = new HashSet();
        String sql = "select " + BuildingsTable.Entry.CN_BUILDING_ID + ", " +
                BuildingsTable.Entry.CN_BUILDING_MODEL_ID + ", " +
                BuildingsTable.Entry.CN_ROTATION + ", " +
                BuildingsTable.Entry.CN_X + ", " +
                BuildingsTable.Entry.CN_Y + " from " +
                BuildingsTable.Entry.TABLE_NAME + " where " +
                BuildingsTable.Entry.CN_ACCOUNT_ID + " = '" + Account.instance().getId() + "' and " +
                BuildingsTable.Entry.CN_VILLAGE_ID + " = " + Account.instance().getVillageId();
        Cursor cursor = _db.rawQuery(sql, null);
        if ( cursor.moveToFirst() )
        {
            do {
                int buildingId = cursor.getInt( cursor.getColumnIndex(BuildingsTable.Entry.CN_BUILDING_ID) );
                int buildingModelId = cursor.getInt( cursor.getColumnIndex(BuildingsTable.Entry.CN_BUILDING_MODEL_ID));
                int rotation = cursor.getInt( cursor.getColumnIndex(BuildingsTable.Entry.CN_ROTATION));
                int x = cursor.getInt( cursor.getColumnIndex(BuildingsTable.Entry.CN_X));
                int y = cursor.getInt( cursor.getColumnIndex(BuildingsTable.Entry.CN_Y));
                Building building = new Building();
                building._modelId = buildingModelId;
                building.setId(buildingId);
                building.getPosition()._x = x;
                building.getPosition()._y = y;

                buildings.add(building);

                /* add glyphs */
                Set<BuildingGlyph> glyphs = loadBuildingGlyphs(buildingModelId);
                for ( BuildingGlyph glyph : glyphs ) building.addGlyph(glyph);

                building.rotate(rotation) ;
            } while( cursor.moveToNext());
        }

        cursor.close();
        return buildings;
    }

    public List<Village> getVillages() {
        WhereClause wc = new WhereClause();
        wc.setField(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        String sql = "select " + VillagesTable.Entry.CN_VILLAGE_ID + ", "
                + VillagesTable.Entry.CN_VILLAGE_NAME + " from "
                + VillagesTable.Entry.TABLE_NAME + " where "
                + wc.getString();

        List<Village> villages = new ArrayList();
        Cursor cursor = _db.rawQuery(sql, null);
        if ( cursor.moveToFirst() )
        {
            do {
                Village village = new Village( cursor.getInt(0), cursor.getString(1) );
                villages.add(village);
            } while( cursor.moveToNext() );
        }
        cursor.close();
        return villages;
    }

    public void renameVillage(String name, int villageId) {
        WhereClause wc = new WhereClause();
        wc.setField(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        wc.setField(VillagesTable.Entry.CN_VILLAGE_ID, villageId);
        ContentValues cv = new ContentValues();
        cv.put(VillagesTable.Entry.CN_VILLAGE_NAME, name);

        _db.update(VillagesTable.Entry.TABLE_NAME, cv, wc.getString(), null);
    }

    public int addVillage(String name) {
        WhereClause wc = new WhereClause();
        wc.setField(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        String sql = "select max(" + VillagesTable.Entry.CN_VILLAGE_ID + ") "
                + " from " + VillagesTable.Entry.TABLE_NAME + " where " + wc.getString();
        Cursor cursor = _db.rawQuery(sql, null);

        int maxId = 0;
        if ( cursor.getCount() != 0 )
        {
            if ( cursor.moveToFirst() ) {
                maxId = cursor.getInt(0);
            }
        }
        cursor.close();

        int newId = maxId + 1;

        ContentValues cv = new ContentValues();
        cv.put(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        cv.put(VillagesTable.Entry.CN_VILLAGE_ID, newId);
        cv.put(VillagesTable.Entry.CN_VILLAGE_NAME, name);

        _db.insert(VillagesTable.Entry.TABLE_NAME, null, cv);

        return newId;
    }

    public int removeVillage(int villageId) {
        WhereClause wc = new WhereClause();
        wc.setField(VillagesTable.Entry.CN_ACCOUNT_ID, Account.instance().getId());
        wc.setField(VillagesTable.Entry.CN_VILLAGE_ID, villageId);

        int countVil =   _db.delete(VillagesTable.Entry.TABLE_NAME, wc.getString(), null );
        int countModel = _db.delete(BuildingModelsTable.Entry.TABLE_NAME, wc.getString(), null);
        int countGlyph = _db.delete(BuildingGlyphsTable.Entry.TABLE_NAME, wc.getString(), null);
        int countBd =    _db.delete(BuildingsTable.Entry.TABLE_NAME, wc.getString(), null);

        Log.d("db", "deleted Village ( " + countVil + ", " + countModel + ", " + countGlyph + ", " + countBd);

        return countVil;
    }

    public void copyToInitialDb() {
       SqliteManager.instance().copyToInitialDb();
    }


}
