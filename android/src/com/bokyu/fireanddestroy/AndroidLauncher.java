package com.bokyu.fireanddestroy;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.MobileAds;

import static android.widget.LinearLayout.VERTICAL;

public class AndroidLauncher extends AndroidApplication {

	static private int WRITE_REQUEST_CODE = 10;
	public AdManagerAndroid _adManager = null;

	@Override
	protected void onCreate (Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		MobileAds.initialize(this, "ca-app-pub-2500234658671976~1269769084");

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		RelativeLayout relativeLayout = new RelativeLayout(this);
		config.useGyroscope = true;

		// create gameplay view
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

		_adManager = new AdManagerAndroid();
		_adManager.initializeAds(this);
		View gameView = initializeForView(new FireAndDestroy(new MediaterAndroid(this)), config);

		relativeLayout.addView(gameView);

		LinearLayout menuAdLayout = new LinearLayout(this);
		menuAdLayout.setOrientation(VERTICAL);
		RelativeLayout.LayoutParams relParams =
				new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		relParams.leftMargin = 10;
		menuAdLayout.setPadding(10,10,10,10);
		menuAdLayout.setBackgroundColor(Color.TRANSPARENT);
		relativeLayout.addView(menuAdLayout, relParams);

		LinearLayout playAdLayout = new LinearLayout(this);
		menuAdLayout.setOrientation(VERTICAL);
		relParams =
				new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
						RelativeLayout.LayoutParams.WRAP_CONTENT);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		relParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		playAdLayout.setPadding(0,0,0,0);
		playAdLayout.setBackgroundColor(Color.TRANSPARENT);
		relativeLayout.addView(playAdLayout, relParams);

		_adManager.addToLayout(menuAdLayout, playAdLayout);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(relativeLayout);

//		grantPermission();

	}

	private void grantPermission() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
				this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQUEST_CODE);
			}
		}
	}

	public void onGameLoaded() {
		setTheme(R.style.GdxTheme);
	}

	@Override
	public void onBackPressed() {
		AlertDialog dialog = new AlertDialog.Builder(this)
				.setTitle("EXIT")
				.setMessage("DO YOU WANT TO QUIT?")
				.setPositiveButton("YES", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				})
				.setNegativeButton("NO", null)
                .setIcon(R.drawable.door)
				.show();
	}

	@Override
	public void onDestroy() {
		android.os.Process.killProcess(android.os.Process.myPid());
		super.onDestroy();
	}
}
