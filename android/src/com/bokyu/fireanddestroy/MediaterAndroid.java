package com.bokyu.fireanddestroy;

import com.bokyu.buildhouse.AdManager;
import com.bokyu.buildhouse.Mediater;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.fireanddestroy.localdb.DbManagerAndroid;


/**
 * Created by bk on 2017-02-22.
 */

public class MediaterAndroid implements Mediater {
    protected DbManager _dbManager = null;
    protected AndroidLauncher _context = null;

    public MediaterAndroid(AndroidLauncher context) {
        _context = context;
    }

    public DbManager getDbManager() {
        if ( _dbManager == null ) _dbManager = new DbManagerAndroid(_context);
        return _dbManager;
    }

    public AdManager getAdManager() {
        return _context._adManager;
    }

    public void gameLoaded() {
        _context.onGameLoaded();
    }
}
