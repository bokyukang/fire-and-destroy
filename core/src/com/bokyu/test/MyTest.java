package com.bokyu.test;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.ObjectsManager;
import com.bokyu.fireanddestroy.SoundManager;
import com.bokyu.fireanddestroy.bullet.BulletWorld;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MyTest extends ApplicationAdapter {
    ModelBatch _modelBatch = null;
    DirectionalLight _light;
    PerspectiveCamera _camera;
    Environment _environment;
    InputMultiplexer _multiplexer;
    BulletWorld _bulletWorld;

    Model _model;
    ModelInstance _modelInstance;
    Stage _stage = null;

    @Override
    public void create () {


        final float width = Gdx.graphics.getWidth();
        final float height = Gdx.graphics.getHeight();
        EventBus.getDefault().register(this);

        Bullet.init();

        SoundManager.instance().init();

        _bulletWorld = new BulletWorld();
        ObjectsManager.instance().init(_bulletWorld);

        _modelBatch = new ModelBatch();
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, .7f, .7f, .7f, 1f));
        _light =  new DirectionalLight();
        _light.set(.8f, .8f, .8f, .5f, 1f, -.7f);
        _camera = new PerspectiveCamera(67f, 3f, 3f * height / width);
        _camera.near = 0.1f;
        _camera.far = 2000f;
        _camera.position.set(0,4,15);
        _camera.lookAt(0,0,0);
        _camera.update();
        _environment.add(_light);

//        _bulletWorld.add("sea",0,0,0);

//        G3dModelLoader loader = new G3dModelLoader(new UBJsonReader());
//        _model = loader.loadModel(Gdx.files.internal("character/head.g3db"));
//        _modelInstance = new ModelInstance(_model);
//        _modelInstance.transform.idt();
//        EnemyCharacter enemyCharacter = new EnemyCharacter(1);
//        _bulletWorld.add( enemyCharacter );

        _stage = new Stage();
        _multiplexer = new InputMultiplexer( _stage);
        Gdx.input.setInputProcessor(_multiplexer);

        Table table = new Table();
        table.setX(400);
        table.setY(100);
        Button button = new Button(UIManager.instance().getSkin(UIManager.SKINS.LGDXS), "oval5");
        button.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        EventBus.getDefault().post(new EventButtonClick(EventButtonClick.BTN_NAMES.TEST, EventButtonClick.TYPE.SELECT));
                        if ( event.getTarget().getParent().getParent().getStage() != null ) {
                            event.getTarget().getParent().getParent().remove();
                        }
                    }
                });
        table.add(button).width(300);
        _stage.addActor( table );

    }

    protected void beginRender () {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(70/255f, 70/255f, 70/255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void render()
    {
        float delta = Gdx.graphics.getDeltaTime();

        beginRender();

        animate(delta);
        _bulletWorld.update();
        _modelBatch.begin(_camera);
        _bulletWorld.render(_modelBatch, _environment);
        _modelBatch.end();

        _stage.act();
        _stage.draw();
//        _modelBatch.begin(_camera);
//        _modelBatch.render(_modelInstance, _environment);
//        _modelBatch.end();
    }

    Vector3 _upVec = new Vector3(0,1,0);
    private void animate(float delta) {
//        _modelInstance.transform.rotate(_upVec, delta * 200f);
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case TEST:
                SoundManager.instance().playSound(SoundManager.SOUND_NAMES.DEMOLITION);
                break;

        }
    }
}
