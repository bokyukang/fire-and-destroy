package com.bokyu.fireanddestroy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.bokyu.buildhouse.Mediater;
import com.bokyu.buildhouse.data.BallData;
import com.bokyu.buildhouse.data.BallDataContainer;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.ResourceManager;
import com.bokyu.buildhouse.events.EventBuildingSelected;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.buildhouse.screen.ScreenConstruct;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.dialog.UIAlert;
import com.bokyu.buildhouse.ui.dialog.UIConfirm;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.events.EventCommand;
import com.bokyu.fireanddestroy.events.EventGameLoaded;
import com.bokyu.fireanddestroy.events.EventUnlockLevel;
import com.bokyu.fireanddestroy.jobs.JobEvent;
import com.bokyu.fireanddestroy.jobs.JobManager;
import com.bokyu.fireanddestroy.jobs.JobShowAd;
import com.bokyu.fireanddestroy.level.Level;
import com.bokyu.fireanddestroy.level.LevelManager;
import com.bokyu.fireanddestroy.listeners.RewardAdListener;
import com.bokyu.fireanddestroy.states.GameState;
import com.bokyu.fireanddestroy.states.GameStateMenu;
import com.bokyu.fireanddestroy.states.GameStatePlay;
import com.bokyu.fireanddestroy.ui.TextPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.bokyu.fireanddestroy.jobs.JobShowAd.TYPE.INTER;

public class FireAndDestroy extends ApplicationAdapter implements RewardAdListener {

	private Stack<GameState> _gameState = new Stack();
	Mediater _factory;
	Set<Disposable> _dispoaables = new HashSet();

	public FireAndDestroy(Mediater factory) {
		_factory = factory;
	}

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);


		ResourceManager.instance().loadResources();
		SoundManager.instance().init();
	    GameStatePlay.instance().init(_factory);
		GameStateMenu.instance().init(_factory);
		ScreenConstruct.instance().init();
		BuildingsManager.instance().init();

		_dispoaables.add(SoundManager.instance());

	    pushGameState(GameStateMenu.instance());
//		pushGameState(GameStatePlay.instance());

		EventBus.getDefault().register(this);

		DbManager dbManager = _factory.getDbManager();
		dbManager.init();
		LevelManager.instance().init(dbManager);
		GameManager.instance().init(dbManager);
		Configuration.instance().setFactory(_factory);
		BuildingsManager.instance().updateBuildingsFromDb();

		_ballDatas.add(BallDataContainer.instance()._ballDataMap.get("black_ball"));
		_ballDatas.add(BallDataContainer.instance()._ballDataMap.get("black_ball"));
		_ballDatas.add(BallDataContainer.instance()._ballDataMap.get("red_ball"));
		_ballDatas.add(BallDataContainer.instance()._ballDataMap.get("green_ball"));

		EventBus.getDefault().post(new EventGameLoaded());
		_factory.gameLoaded();
	}

	@Override
	public void render () {

		float delta = Gdx.graphics.getDeltaTime();
		AnimaterManager.instance().animate(delta);
		if ( _gameState.size() > 0) {
			_gameState.peek().render(delta);
		}
	}



	@Override
	public void dispose () {
		EventBus.getDefault().unregister(this);
		for (Disposable disposable : _dispoaables ) {
			disposable.dispose();
		}
	}

	Lock _gameStateLock = new ReentrantLock();

	public void pushGameState(GameState gs) {
		_gameStateLock.lock();
		try {
			if (_gameState.size() > 0) {
				if (_gameState.peek() == gs)
					return;
				_gameState.peek().onExit();
			}
			_gameState.push(gs);
		} finally {
			_gameStateLock.unlock();
		}
		gs.onEnter();

	}

	public GameState popGameState() {
		GameState gs = null;
		_gameStateLock.lock();
		try {
			if (_gameState.size() > 0) {
				gs = _gameState.pop();
				gs.onExit();
			}
			if (_gameState.size() > 0) _gameState.peek().onEnter();
		}finally {
			_gameStateLock.unlock();
		}
		return gs;
	}

	@Subscribe
	public void onEvent(EventButtonClick e) {
		switch ( e._name ) {
			case OPEN_BUILDING_EDITOR:
				pushGameState(ScreenConstruct.instance());
				break;
			case SCREEN_GO_BACK:
				JobEvent jobEvent = new JobEvent(new EventCommand(EventCommand.COMMAND.GO_BACK));
				JobManager.instance()
						.addJob(jobEvent);
				break;
            case PLAY_NEXT_LEVEL:
            	Level level = LevelManager.instance().getLevel(
            			LevelManager.instance().getClearedLevel() + 1
				);
            	if ( level != null && level._levelData.needs_unlock) {
					UIConfirm.instance().show("ad",
							"to unlock this level, you need to view an ad.",
							getStage(),
							new EventUnlockLevel(level));
				} else {
//                    _factory.getAdManager().showInterstitial( null );
//                    EventBus.getDefault().post(new EventCommand(EventCommand.COMMAND.NEXT_LEVEL));
					JobShowAd jobShowAd = new JobShowAd(_factory.getAdManager(), INTER);
					jobEvent = new JobEvent(new EventCommand(EventCommand.COMMAND.NEXT_LEVEL));
					JobManager.instance()
							.addJob(jobShowAd)
							.addJob(jobEvent);
				}
                break;
            case START_FROM_BEGINNING:
                break;
			case TEST:
				EventBus.getDefault().post(
						new EventButtonClick(
								EventButtonClick.BTN_NAMES.SCREEN_GO_BACK,
								EventButtonClick.TYPE.SELECT));
				break;
		}
	}

	private Stage getStage() {
	    _gameStateLock.lock();
	    try {
			if (_gameState.size() > 0) {
				return _gameState.peek().getStage();
			}
		} finally {
	    	_gameStateLock.unlock();
		}
		return null;
	}


	@Subscribe
	public void onEvent(EventCommand e ){
		switch ( e._command ) {
			case GO_BACK:
				popGameState();
				break;
			case NEXT_LEVEL:
				pushGameState(GameStatePlay.instance());
				int totalLevels = LevelManager.instance().getTotalLevelCount();
				int clearedLevel = LevelManager.instance().getClearedLevel();
				if ( clearedLevel + 1 <= totalLevels ) {
					GameStatePlay.instance().startLevel(clearedLevel +1);
				} else {
					TextPopup.showText("all levels cleared", 5f, UIManager.SKINS.CRAFTCULAR, "title", TextPopup.MOVE_TYPE.FROM_TOP, getStage(), 1.5f, null);
				}
				break;
			case START_FROM_BEGINNING:
				JobShowAd jobShowAd = new JobShowAd(_factory.getAdManager(), INTER);
				JobEvent jobEvent = new JobEvent(new EventCommand(EventCommand.COMMAND.START_FROM_BEGINNING_PHASE2));
				JobManager.instance()
						.addJob(jobShowAd)
						.addJob(jobEvent);
				break;
			case START_FROM_BEGINNING_PHASE2:
				GameManager.instance().startFromLevelOne();
				pushGameState(GameStatePlay.instance());
				GameStatePlay.instance().startLevel(1);
				break;
		}

	}

	private List<BallData> _ballDatas = new ArrayList();

	@Subscribe
	public void onEvent(EventBuildingSelected e) {
		pushGameState(GameStatePlay.instance());
		ArrayList<Building> buildings = new ArrayList();
		buildings.add(e._selectedBuildingData);
		GameStatePlay.instance().addBuilding( buildings);
		GameStatePlay.instance().setBalls(_ballDatas);
		GameStatePlay.instance().start(new Vector3(0,3,10), true);
		GameStatePlay.instance().setCurLevel(null);
	}

	@Subscribe
	public void onEvent(EventUnlockLevel event) {
		_tobeUnlockedLevel = event._level;
		_factory.getAdManager().showReward(this);
	}
	Level _tobeUnlockedLevel = null;

	public void rewardSuccess() {
		if ( _tobeUnlockedLevel != null ) {
			_tobeUnlockedLevel._levelData.needs_unlock = false;
			UIAlert.instance().show("reward", "next level unlocked", getStage());
			UIAlert.instance().align(Align.right);
		}
	}

	public void rewardFailed() {
		UIAlert.instance().show("reward", "unlock failed", getStage());
	}
}
