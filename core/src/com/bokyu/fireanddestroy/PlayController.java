package com.bokyu.fireanddestroy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.bokyu.buildhouse.ui.scene2d.Scene2DManager;
import com.bokyu.fireanddestroy.events.EventKeyDown;
import com.bokyu.fireanddestroy.events.EventTap;
import com.bokyu.fireanddestroy.events.EventTouch;
import com.bokyu.fireanddestroy.listeners.TouchDownUpListener;
import com.bokyu.fireanddestroy.listeners.TouchDragListener;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;
import java.util.Set;

public class PlayController implements InputProcessor, GestureDetector.GestureListener {

    PerspectiveCamera _camera;
    CameraInputController _cameraInputController;
    Stage _stage;
    Scene2DManager _scene2dManager;
    InputMultiplexer _multiplexer;
    Set<TouchDragListener> _dragListeners = new HashSet();
    Set<TouchDownUpListener> _downupListeners = new HashSet();

    public void init(Scene2DManager scene2DManager, PerspectiveCamera camera, Stage stage) {
        _scene2dManager = scene2DManager;
        _camera = camera;
        _stage = stage;
        _cameraInputController = new CameraInputController(camera);
        _multiplexer = new InputMultiplexer(_stage,_scene2dManager.getStage(),  this, _cameraInputController, new GestureDetector(this));
    }

    public void onEnter() {
        Gdx.input.setInputProcessor(_multiplexer);
    }

    public void onExit() { }

    public void addDragListener(TouchDragListener listener) { _dragListeners.add(listener ); }
    public void removeDragListener(TouchDragListener listener) { _dragListeners.remove(listener); }
    public void addDownupListener(TouchDownUpListener listener) { _downupListeners.add(listener ); }
    public void removeDownupListener(TouchDownUpListener listener) { _downupListeners.remove(listener); }

    @Override
    public boolean keyDown (int keycode) {
        if ( keycode == Input.Keys.SPACE ) {
            EventBus.getDefault().post(new EventKeyDown(keycode));
        }
        return true;
    }

    @Override
    public boolean keyTyped (char character) {
        return true;
    }

    @Override
    public boolean keyUp (int keycode) {
        return true;
    }

    @Override
    public boolean touchDown (int x, int y, int pointer, int button) {
        System.out.println("touchDown");
        EventBus.getDefault().post(new EventTouch(x,y,pointer,true));
        for ( TouchDownUpListener l : _downupListeners ) {
            l.touchDownUp(x,y,pointer, true);
        }
        return true;
    }

    @Override
    public boolean touchDragged (int x, int y, int pointer) {
        System.out.println("touch Drag");
        for ( TouchDragListener l : _dragListeners ) {
            l.touchDragged(x,y,pointer);
        }
        return true;
    }

    @Override
    public boolean touchUp (int x, int y, int pointer, int button) {
        EventBus.getDefault().post(new EventTouch(x,y,pointer,false));
        for ( TouchDownUpListener l : _downupListeners ) {
            l.touchDownUp(x,y,pointer, false);
        }
        return true;
    }

    @Override
    public boolean mouseMoved (int x, int y) {
        return true;
    }

    @Override
    public boolean scrolled (int amount) {
        return true;
    }

    @Override
    public boolean touchDown (float x, float y, int pointer, int button) {
        return true;
    }

    @Override
    public boolean tap (float x, float y, int count, int button) {
        EventBus.getDefault().post(new EventTap());
        return true;
    }

    @Override
    public boolean longPress (float x, float y) {
        return true;
    }

    @Override
    public boolean fling (float velocityX, float velocityY, int button) {
        return true;
    }

    @Override
    public boolean pan (float x, float y, float deltaX, float deltaY) {
        return true;
    }

    @Override
    public boolean panStop (float x, float y, int pointer, int button) {
        return true;
    }

    @Override
    public boolean zoom (float originalDistance, float currentDistance) {
        return true;
    }

    @Override
    public boolean pinch (Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
        return true;
    }

    @Override
    public void pinchStop () {
    }
}
