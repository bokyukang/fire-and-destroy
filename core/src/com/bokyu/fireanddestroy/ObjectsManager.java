package com.bokyu.fireanddestroy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.ContactListener;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btSphereShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.UBJsonReader;
import com.bokyu.fireanddestroy.bullet.BulletConstructor;
import com.bokyu.fireanddestroy.bullet.BulletEntity;
import com.bokyu.fireanddestroy.bullet.BulletWorld;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ObjectsManager {
    ModelBuilder _modelBuilder = new ModelBuilder();
    Array<Disposable> _disposables = new Array<Disposable>();
    BulletWorld _bulletWorld;
    private Map<String, Model> _models = new HashMap();
    static private ObjectsManager _instance = null;


    private ObjectsManager() {}

    static public ObjectsManager instance() {
        if ( _instance == null ) _instance = new ObjectsManager();
        return _instance;
    }


    public void init(BulletWorld bulletWorld) {
        _bulletWorld = bulletWorld;
        createTestModels();
    }

    public Model getModel(String name) {
        if ( _models.containsKey(name) ) return _models.get(name);
        return null;
    }

    public void saveModel(String name, Model model) {
        _models.put(name, model);
    }

    private void createPlane(MeshPartBuilder meshBuilder, float xmin, float xmax, float zmin, float zmax, Material material) {

        meshBuilder.rect(
                xmax, 0f, zmin,
                xmin, 0f, zmin,
                xmin, 0f, zmax,
                xmax, 0f, zmax
                , 0, 1, 0
        );
    }
    private void createTestModels() {
        // Create some simple models
        float SEA_WIDTH = 160;
        final Texture groundTexture = new Texture(Gdx.files.internal("textures/brick_8.gif"));
        _disposables.add(groundTexture);
        TextureAttribute textureAttribute = TextureAttribute.createDiffuse(groundTexture);
        textureAttribute.scaleU = 20f;
        textureAttribute.scaleV = 20f;
        groundTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        Material material = new Material(textureAttribute);

        int attr = VertexAttributes.Usage.Position |
                VertexAttributes.Usage.Normal|
                VertexAttributes.Usage.TextureCoordinates;
        _modelBuilder.begin();
        createPlane(
                _modelBuilder.part( "1", GL20.GL_TRIANGLES, attr, material),
                -SEA_WIDTH, 0, -SEA_WIDTH, 0, material);
        _modelBuilder.node();
        createPlane(_modelBuilder.part( "2", GL20.GL_TRIANGLES, attr, material),
                -SEA_WIDTH, 0, 0, SEA_WIDTH, material);
        _modelBuilder.node();
        createPlane(_modelBuilder.part( "3", GL20.GL_TRIANGLES, attr, material),
                0, SEA_WIDTH, -SEA_WIDTH, 0, material);
        _modelBuilder.node();
        createPlane(_modelBuilder.part( "4", GL20.GL_TRIANGLES, attr, material),
                0, SEA_WIDTH, 0, SEA_WIDTH, material);
        Model groundModel = _modelBuilder.end();
//        final Model groundModel = _modelBuilder.createCylinder(40, 20,40, 10,
//                new Material(ColorAttribute.createDiffuse(Color.WHITE), ColorAttribute.createSpecular(Color.WHITE), FloatAttribute
//                        .createShininess(16f)), VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
//        _disposables.add(groundModel);
        btCollisionShape groundShape = Bullet.obtainStaticNodeShape(groundModel.nodes);
        _disposables.add(groundShape);
        final Model boxModel = _modelBuilder.createBox(1f, 1f, 1f, new Material(ColorAttribute.createDiffuse(Color.WHITE),
                ColorAttribute.createSpecular(Color.WHITE), FloatAttribute.createShininess(64f)), VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        _disposables.add(boxModel);
        final Model playerModel =_modelBuilder.createCapsule(1, 2, 5, new Material(ColorAttribute.createDiffuse(Color.BROWN),
                ColorAttribute.createSpecular(Color.WHITE), FloatAttribute.createShininess(64f)), VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        _disposables.add(playerModel);

        final Model wallModel = _modelBuilder.createBox(40f, 7f, 1f, new Material(ColorAttribute.createDiffuse(Color.WHITE),
                ColorAttribute.createSpecular(Color.WHITE), FloatAttribute.createShininess(64f)), VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        _disposables.add(wallModel);

        int ballDiv = 15;
        final Model blackBallModel = _modelBuilder.createSphere(1,1,1,ballDiv, ballDiv,
                new Material(ColorAttribute.createDiffuse(.2f,.2f,.2f,1f),
                ColorAttribute.createSpecular(.7f,.7f,.7f,1f),
                        FloatAttribute.createShininess(64f)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        btCollisionShape blackBallShape = new btSphereShape(1f/2f);
        _disposables.add(blackBallModel);

        final Model redBallModel = _modelBuilder.createSphere(1,1,1,ballDiv, ballDiv,
                new Material(ColorAttribute.createDiffuse(.8f,.2f,.2f,1f),
                        ColorAttribute.createSpecular(1f,.0f,.0f,1f),
                        FloatAttribute.createShininess(30f)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        btCollisionShape redBallShape = new btSphereShape(1f/2f);
        _disposables.add(redBallModel);

        final Model greenBallModel = _modelBuilder.createSphere(2,2,2,ballDiv, ballDiv,
                new Material(ColorAttribute.createDiffuse(.2f,.8f,.2f,1f),
                        ColorAttribute.createSpecular(0f,1f,.0f,1f),
                        FloatAttribute.createShininess(20f)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
        btCollisionShape greenBallShape = new btSphereShape(1f);
        _disposables.add(greenBallModel);

        // Add the constructors
        _bulletWorld.addConstructor("ground", new BulletConstructor(groundModel, 0f)); // mass = 0: static body
//        _bulletWorld.addConstructor("ground", new BulletConstructor(groundModel, 0f, groundShape)); // mass = 0: static body
        _bulletWorld.addConstructor("box", new BulletConstructor(boxModel, 1f)); // mass = 1kg: dynamic body
        _bulletWorld.addConstructor("staticbox", new BulletConstructor(boxModel, 0f)); // mass = 0: static body
        _bulletWorld.addConstructor("wall", new BulletConstructor(wallModel, 0f)); // mass = 0: static body
        _bulletWorld.addConstructor("black_ball", new BulletConstructor(blackBallModel, 1f, blackBallShape));
        _bulletWorld.addConstructor("red_ball", new BulletConstructor(redBallModel, 2f, redBallShape));
        _bulletWorld.addConstructor("green_ball", new BulletConstructor(greenBallModel, 1f, greenBallShape));
        _bulletWorld.addConstructor("player", new BulletConstructor(playerModel, 2f));
        G3dModelLoader loader = new G3dModelLoader(new UBJsonReader());
        Model enemyModel = loader.loadModel(Gdx.files.internal("character/head.g3db"));
        for ( Material material1 : enemyModel.materials ) {
            material1.set(new IntAttribute(IntAttribute.CullFace, GL20.GL_FRONT_FACE));
        }
        _models.put("enemy_character",enemyModel);
        Model goodModel = loader.loadModel(Gdx.files.internal("character/good_character.g3db"));
        for ( Material material1 : goodModel.materials ) {
            material1.set(new IntAttribute(IntAttribute.CullFace, GL20.GL_FRONT_FACE));
        }
        _models.put("good_character",goodModel);
        _models.put("black_ball",blackBallModel);
        _models.put("red_ball",redBallModel);
        _models.put("green_ball",greenBallModel);
    }

    public void dispose() {
        for (Disposable disposable : _disposables)
            disposable.dispose();
        _disposables.clear();
    }
}
