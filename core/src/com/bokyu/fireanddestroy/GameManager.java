package com.bokyu.fireanddestroy;

import com.bokyu.buildhouse.localdb.DbManager;

/**
 * Created by admin on 2018-08-10.
 */

public class GameManager {

    private static GameManager _instance = null;
    DbManager _dbManager = null;

    public static GameManager instance() {
        if ( _instance == null ) {
            _instance = new GameManager();
        }
        return _instance;
    }

    public void init(DbManager dbManager) {
        _dbManager = dbManager;
    }


    public void startFromLevelOne() {
        _dbManager.saveClearedLevel(0);
    }
}
