package com.bokyu.fireanddestroy.events;

public class EventLevelPhase {
    public enum PHASE {
        START,
        SHOW_AROUND_OVER
    }

    public PHASE _phase;

    public EventLevelPhase(PHASE phase) {
        _phase = phase;
    }
}
