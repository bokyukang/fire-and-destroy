package com.bokyu.fireanddestroy.events;

public class EventCommand {
    public enum COMMAND {
        GO_BACK,
        NEXT_LEVEL,
        START_FROM_BEGINNING,
        START_FROM_BEGINNING_PHASE2
    }

    public COMMAND _command;

    public EventCommand(COMMAND cmd) {
        _command = cmd;
    }
}
