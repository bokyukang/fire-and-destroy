package com.bokyu.fireanddestroy.events;

public class EventFire {

    public enum STAGES {
        START,
        END,
        DRAG
    }

    public STAGES _stage ;
    public float _x, _y;
    public int _pointer;

    public EventFire(STAGES stage, float x, float y, int pointer){
        _stage = stage;
        _x = x;
        _y = y;
        _pointer = pointer;
    }
}
