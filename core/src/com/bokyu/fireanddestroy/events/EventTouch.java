package com.bokyu.fireanddestroy.events;

/**
 * Created by admin on 2018-07-20.
 */

public class EventTouch {
    public boolean _isDown;
    public int _x , _y;
    public int _pointer;
    public EventTouch(int x, int y, int pointer, boolean isDown) {
        _isDown = isDown;
        _x = x; _y = y; _pointer = pointer;
    }
}
