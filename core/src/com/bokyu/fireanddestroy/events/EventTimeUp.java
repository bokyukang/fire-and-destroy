package com.bokyu.fireanddestroy.events;

/**
 * Created by admin on 2018-08-13.
 */

public class EventTimeUp {
    public enum CODES {
        AFTER_FIRE_TIME_UP
    }

    public CODES _code;

    public EventTimeUp(CODES code) {
        _code = code;
    }
}
