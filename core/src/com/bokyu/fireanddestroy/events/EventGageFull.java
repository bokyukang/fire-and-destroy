package com.bokyu.fireanddestroy.events;

/**
 * Created by admin on 2018-08-02.
 */

public class EventGageFull {

    static public enum STATE {
        BEGIN,
        END
    }

    public STATE _state;

    public EventGageFull(STATE state) {
        _state = state;
    }

}
