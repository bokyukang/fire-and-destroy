package com.bokyu.fireanddestroy.events;

import com.bokyu.buildhouse.glyph.Building;

/**
 * Created by admin on 2018-08-14.
 */

public class EventBuildingSaved {
    public Building _building;
    public String _name;

    public EventBuildingSaved(Building building, String name) {
        _building = building;
        _name = name;
    }
}
