package com.bokyu.fireanddestroy.events;

import com.bokyu.fireanddestroy.level.Level;

public class EventUnlockLevel {

    public Level _level;

    public EventUnlockLevel(Level level) {
        _level = level;
    }
}
