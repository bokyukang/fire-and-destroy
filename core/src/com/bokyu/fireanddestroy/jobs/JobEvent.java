package com.bokyu.fireanddestroy.jobs;

import org.greenrobot.eventbus.EventBus;

public class JobEvent extends Job {

    private Object _event;

    public JobEvent(Object event) {
        _event = event;
    }

    @Override
    public void run() {
        EventBus.getDefault().post( _event );
        finish();
    }
}
