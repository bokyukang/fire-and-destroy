package com.bokyu.fireanddestroy.jobs;

public class Job {
    protected JobManager.Token _token = null;

    public void setToken(JobManager.Token token) {
        _token = token;
    }
    public void run() {

    }

    public void finish() {
        if ( _token != null ) {
            _token.finished();
        }
    }
}
