package com.bokyu.fireanddestroy.jobs;

import com.bokyu.buildhouse.AdManager;

import static com.bokyu.fireanddestroy.jobs.JobShowAd.TYPE.INTER;
import static com.bokyu.fireanddestroy.jobs.JobShowAd.TYPE.REWARD;

public class JobShowAd extends Job {

    public enum TYPE {
        INTER,
        REWARD
    }
    private AdManager _adManager;
    private TYPE _type;

    public JobShowAd(AdManager adManager, TYPE type) {
        _adManager = adManager;
        _type = type;
    }

    @Override
    public void run() {
        if ( _type == INTER) {
            _adManager.showInterstitial(_token);
        } else if ( _type == REWARD) {
            _adManager.showReward(null);
        }
    }
}
