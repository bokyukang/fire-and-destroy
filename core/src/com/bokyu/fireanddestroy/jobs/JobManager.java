package com.bokyu.fireanddestroy.jobs;

import java.util.LinkedList;
import java.util.Queue;

public class JobManager {

    static private JobManager _instance;

    private Queue<Job> _jobs = new LinkedList();

    public class Token {
        private boolean _used = false;
        JobManager _jobManager;

        public Token(JobManager jobManager) {
            _used = false;
            _jobManager = jobManager;
        }

        public void finished() {
            if ( _used == false  ) {
                this._jobManager.consume();
                this._jobManager.next();
            }
            _used = true;
        }
    }

    static public JobManager instance() {
        if ( _instance == null ) {
            _instance = new JobManager();
        }
        return _instance;
    }

    private Job _curJob = null;
    synchronized private void next() {
        if ( _jobs.size() > 0 ) {
            _curJob = _jobs.peek();
            _curJob.setToken(new Token(this));
            _curJob.run();
        }
    }

    synchronized private void consume() {
        if ( _jobs.size() > 0 ) {
            _jobs.remove();
            _curJob = null;
        }
    }

    synchronized public JobManager addJob(Job job) {
        _jobs.add(job);
        if ( _curJob == null ) {
            next();
        }
        return this;
    }

}
