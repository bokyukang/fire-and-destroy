package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveByAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pool;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.animaters.AnimaterRemoveActor;
import com.bokyu.fireanddestroy.jobs.Job;

/**
 * Created by admin on 2018-08-02.
 */

public class TextPopup extends Label {

    static final float ACTION_DURATION_IN = 0.8f;
    static final float ACTION_DURATION_OUT = 0.8f;
    float _duration = 0;
    Job _jobWhenFinished = null;
    public enum MOVE_TYPE {
        FROM_TOP,
        FROM_TOP_SLOW,
        FROM_RIGHT
    };

    static Pool<SequenceAction> _seqActionPool = new Pool<SequenceAction>() {
        @Override
        protected SequenceAction newObject() {
            SequenceAction action = new SequenceAction();
            return action;
        }

        @Override
        protected void reset(SequenceAction object) {
            super.reset(object);
        }
    };
    static Pool<DelayAction> _delayActionPool = new Pool<DelayAction>() {
        @Override
        protected DelayAction newObject() {
            DelayAction action = new DelayAction();
            return action;
        }

        @Override
        protected void reset(DelayAction object) {
            object.finish();
            super.reset(object);
        }
    };
    static Pool<MoveByAction> _moveActionPool = new Pool<MoveByAction>() {
        @Override
        protected MoveByAction newObject() {
            MoveByAction action = new MoveByAction();
            return action;
        }

        @Override
        protected void reset(MoveByAction object) {
            object.finish();
            super.reset(object);
        }
    };

    static public void showText(String text, float duration,
                                UIManager.SKINS skinType,
                                String skinName,
                                MOVE_TYPE type, Stage stage,
                                float fontScale,
                                Job jobWhenFinished
                                ) {
        if ( stage == null ) return;
        TextPopup textPopup = new TextPopup(skinType,skinName);
        textPopup.init(duration, jobWhenFinished);
        textPopup.setFontScale(fontScale);
        textPopup.setText(text);
        stage.addActor(textPopup);
        textPopup.start(type);
    }


    public TextPopup(UIManager.SKINS skinType, String skinName) {
        super("", UIManager.instance().getSkin(skinType),skinName);
    }

    public void init(float duration, Job jobWhenfinished) {
        _duration = duration;
        _jobWhenFinished = jobWhenfinished;
    }

    public void start(MOVE_TYPE type) {
        layout();
        float width = getWidth();

        if ( type == MOVE_TYPE.FROM_TOP_SLOW ) {
            this.setAlignment(Align.center);
            setX((Gdx.graphics.getWidth() - width) / 2f);
            setY(Gdx.graphics.getHeight() + getHeight());

            SequenceAction sequenceAction = _seqActionPool.obtain();

            MoveByAction downAction = _moveActionPool.obtain();
            downAction.setAmount(0, -Gdx.graphics.getHeight() / 2f);
            float actionDuration = ACTION_DURATION_IN * 3f;
            downAction.setDuration(actionDuration);

            DelayAction delayAction = _delayActionPool.obtain();
            delayAction.setDuration(_duration - actionDuration * 2f);

            MoveByAction upAction = _moveActionPool.obtain();
            upAction.setAmount(0, -Gdx.graphics.getHeight() / 2f);
            upAction.setDuration(actionDuration);

            sequenceAction.addAction(downAction);
            sequenceAction.addAction(delayAction);
            sequenceAction.addAction(upAction);

            addAction(sequenceAction);

            AnimaterRemoveActor animater = AnimaterRemoveActor._pool.obtain();
            animater.init(_duration, this);
            animater.setJobAfter(_jobWhenFinished);
            AnimaterManager.instance().addAnimater(animater);

        }
        else if ( type == MOVE_TYPE.FROM_TOP ) {
            this.setAlignment(Align.center);
            setX((Gdx.graphics.getWidth() - width) / 2f);
            setY(Gdx.graphics.getHeight() + getHeight());

            SequenceAction sequenceAction = _seqActionPool.obtain();

            MoveByAction downAction = _moveActionPool.obtain();
            downAction.setAmount(0, -Gdx.graphics.getHeight() / 2f);
            downAction.setInterpolation(Interpolation.bounceOut);
            downAction.setDuration(ACTION_DURATION_IN);

            DelayAction delayAction = _delayActionPool.obtain();
            delayAction.setDuration(_duration - ACTION_DURATION_IN * 2f);

            MoveByAction upAction = _moveActionPool.obtain();
            upAction.setAmount(0, Gdx.graphics.getHeight() / 2f);
            upAction.setInterpolation(Interpolation.swingIn);
            upAction.setDuration(ACTION_DURATION_OUT);

            sequenceAction.addAction(downAction);
            sequenceAction.addAction(delayAction);
            sequenceAction.addAction(upAction);

            addAction(sequenceAction);

            AnimaterRemoveActor animater = AnimaterRemoveActor._pool.obtain();
            animater.init(_duration, this);
            animater.setJobAfter(_jobWhenFinished);
            AnimaterManager.instance().addAnimater(animater);
        } else if ( type == MOVE_TYPE.FROM_RIGHT )
        {
            this.setAlignment(Align.center);
            setX(Gdx.graphics.getWidth() * 1.5f);
            setY((Gdx.graphics.getHeight() + getHeight())/2f);

            SequenceAction sequenceAction = _seqActionPool.obtain();

            MoveByAction downAction = _moveActionPool.obtain();
            downAction.setDuration(ACTION_DURATION_IN);
            downAction.setAmount(-(Gdx.graphics.getWidth()), 0);
            downAction.setInterpolation(Interpolation.circleOut);

            DelayAction delayAction = _delayActionPool.obtain();
            delayAction.setDuration(_duration - ACTION_DURATION_IN * 2f);

            MoveByAction upAction = _moveActionPool.obtain();
            upAction.setAmount(-(Gdx.graphics.getWidth()  ), 0);
            upAction.setInterpolation(Interpolation.circleIn);
            upAction.setDuration(ACTION_DURATION_OUT);

            sequenceAction.addAction(downAction);
            sequenceAction.addAction(delayAction);
            sequenceAction.addAction(upAction);

            addAction(sequenceAction);

            AnimaterRemoveActor animater = AnimaterRemoveActor._pool.obtain();
            animater.init(_duration, this);
            animater.setJobAfter(_jobWhenFinished);
            AnimaterManager.instance().addAnimater(animater);
        }

    }
}
