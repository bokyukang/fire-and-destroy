package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.events.EventDescriptionVisibility;
import com.bokyu.fireanddestroy.events.EventTap;
import com.bokyu.fireanddestroy.events.EventTouch;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2018-08-09.
 */

public class Descriptions {

    static private Descriptions _instance = null;
    private TextArea _textArea = null;
    private Table _container = null;
    private List<String> _texts = new ArrayList();
    private int _textIdx = 0;
    private Stage _stage;

    static public Descriptions instance() {
        if ( _instance == null ) {
            _instance = new Descriptions();
        }
        return _instance;
    }

    private Descriptions() {
    }

    public void init(Stage stage) {
        _stage = stage;
        _container = new Table();
        _textArea = new TextArea("", UIManager.instance().getSkin(UIManager.SKINS.COMIC) );
        _textArea.setAlignment(Align.center);
        _textArea.setTouchable(Touchable.childrenOnly);
//        _container.setX(Gdx.graphics.getWidth()/2f, Align.center);
//        _container.setY(Gdx.graphics.getHeight()/2f, Align.center);
        _container.setX(Gdx.graphics.getWidth()/2f, Align.center);
        _container.setY(Gdx.graphics.getHeight()/2f, Align.center);
        _container.addListener(new ClickListener() {
//            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
//                super.touchDown(event,x,y,pointer,button);
//                return false;
//            }
            public void clicked (InputEvent event, float x, float y) {
                onClick();
            }
        });
//        _container.setDebug(true);
//        _textArea.setDebug(true);
        _container.add(_textArea).expand().fill().align(Align.center);


        _container.align(Align.center);
        _textArea.setAlignment(Align.center);

        stage.addActor(_container);
        _container.setTouchable(Touchable.enabled);

        show(false);
    }

    private void onClick() {
        _textIdx++;
        updateText();
    }

    public void showTexts(String ...text) {
        _textIdx = 0;
        _texts.clear();
        for ( int i=0; i < text.length; ++i ) {
            _texts.add(text[i]);
        }
        show(true);
    }
    private boolean _visible = true;
    public void show(boolean visible) {
        if ( _visible == visible ) return;
        _visible = visible;
        _textIdx = 0;
        if ( visible ) {
            if ( _container.getStage() == null ) {
                _stage.addActor(_container);
            }
            updateText();
            _container.toFront();
            _container.setTouchable(Touchable.enabled);
        } else {
            if ( _container.getStage() != null ) {
                _container.remove();
            }
        }
        EventBus.getDefault().post(new EventDescriptionVisibility(visible));
    }

    private void updateText() {
        if (  _texts.size() > _textIdx ) {
            String text = _texts.get(_textIdx);

            int newLineCount = 0;
            int maxLen = 0;
            int curLen = 0;
            for ( int i=0; i< text.length(); ++i ) {
                if ( text.charAt(i) == '\n' ) {
                    maxLen = Math.max(curLen, maxLen);
                    newLineCount++;
                    curLen= 0;
                } else {
                    curLen++;
                }
            }
            maxLen = Math.max(curLen, maxLen);

            int screenWidth = Gdx.graphics.getWidth();
            float scale = 1;
            if ( screenWidth < 1000 )
                scale = 1.5f/4f;
            else if ( screenWidth < 1500 )
                scale = 2.5f/4f;
            else
                scale = 1f;
            float width = maxLen * 30f * scale;
            float height = ((newLineCount+1) * 40f + 70f) ;
            _textArea.setText(text);
            _textArea.getStyle().font.getData().setScale(scale * 4f);
            _textArea.setAlignment(Align.center);
            _container.setWidth(width);
            _container.setHeight(height);
            _container.getCell(_textArea).width(width  );
            _container.getCell(_textArea).height(height );
            _container.getCell(_textArea).align(Align.center);
            _textArea.setWidth(width);
            _textArea.setHeight(height);
            _container.setX(Gdx.graphics.getWidth()/2f, Align.center);
            _container.setY(Gdx.graphics.getHeight()/2f, Align.center);
        } else {
            show(false);
        }
    }

}
