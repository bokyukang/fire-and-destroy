package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.buildhouse.etc.ResourceManager;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.SoundManager;

public class MyButton extends TextButton {


    public MyButton(String text, float width, String skinName ){
        super(text, UIManager.instance().getSkin(
                UIManager.SKINS.LGDXS
        ), skinName);
        getStyle().font.getData().setScale(width/120);
//        setWidth(width);
//        setHeight(width / 4f);
        addListener(
            new ClickListener() {
                public void clicked (InputEvent event, float x, float y) {
                    SoundManager.instance().playSound(SoundManager.SOUND_NAMES.BUTTON);
                }
            } );
    }
}
