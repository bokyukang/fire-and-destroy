package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.data.BallData;
import com.bokyu.buildhouse.events.EventBallFiringOver;
import com.bokyu.buildhouse.events.EventBallLoaded;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.ParticleManager;
import com.bokyu.fireanddestroy.SoundManager;
import com.bokyu.fireanddestroy.events.EventDescriptionVisibility;
import com.bokyu.fireanddestroy.events.EventFire;
import com.bokyu.fireanddestroy.events.EventFireEnabled;
import com.bokyu.fireanddestroy.events.EventGageFull;
import com.bokyu.fireanddestroy.events.EventLevelPhase;
import com.bokyu.fireanddestroy.events.EventTimeUp;
import com.bokyu.fireanddestroy.listeners.FireButtonListener;
import com.bokyu.fireanddestroy.listeners.MyTouchpadListener;
import com.bokyu.fireanddestroy.states.GameStatePlay;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class PlayUI {

    private final int MARGIN = 100;
    private final float BALL_SIZE = 50;
    private Stage _stage;
    private FireButtonListener _fireBtnListener;
    private boolean _firingEnabled = false;
    private ParticleEffect _particleEffect;
    private Table _fireBtnContainer;
    private Touchpad _touchpad;
    private Table _characterInfoContainer;
    private Label _enemyCountLabel, _goodCountLabel;
    private GameStatePlay _gameStatePlay;
    private Table _ballsContainer;
    private Angle _angleUI;
    PerspectiveCamera _camera;

    public PlayUI() {
        _stage = new Stage();
    }

    public void init(GameStatePlay gameStatePlay, PerspectiveCamera camera, Gage fireGage, Angle angleUI) {
        EventBus.getDefault().register(this);

        _angleUI = angleUI;
        _gameStatePlay = gameStatePlay;
        _camera = camera;

        _fireMusic = SoundManager.instance().getMusic(SoundManager.SOUND_NAMES.FIRE);

        _ballsContainer = new Table();
        _ballsContainer.align(Align.left);
        _stage.addActor(_ballsContainer);

        Table table = new Table();
        table.setTouchable(Touchable.enabled);
        table.setColor(Color.BLACK);
        table.setWidth(Gdx.graphics.getWidth() * .1f);
        table.setHeight(Gdx.graphics.getHeight() * .1f);
        _fireBtnContainer = table;
        _fireBtnListener = new FireButtonListener(table);
        table.setPosition((Gdx.graphics.getWidth() - table.getWidth()) / 2f,
                table.getHeight() + MARGIN);
        _stage.addActor(table);



        table = new Table();

        float imgWidth = 120;
        table.setWidth(imgWidth * 5f);
        table.setHeight(imgWidth);
        _enemyCountLabel = new Label("X ", UIManager.instance().getSkin(UIManager.SKINS.COMIC), "title");
        _goodCountLabel = new Label("X ", UIManager.instance().getSkin(UIManager.SKINS.COMIC), "title");
        _enemyCountLabel.getStyle().font.getData().setScale(1.5f);
        _goodCountLabel.getStyle().font.getData().setScale(1.5f);
        _enemyCountLabel.setColor(Color.WHITE);
        _goodCountLabel.setColor(Color.WHITE);

        table.add(new Image(new Texture(Gdx.files.internal("ui/enemy.png")))).width(imgWidth).height(imgWidth).expand();
        table.add(_enemyCountLabel).width(imgWidth).height(imgWidth).expand();
        table.add(new Label("", UIManager.instance().getSkin())).width(imgWidth).height(imgWidth);
        table.add(new Image(new Texture(Gdx.files.internal("ui/good_character.png")))).width(imgWidth).height(imgWidth).fill().expand();
        table.add(_goodCountLabel).fill().width(imgWidth).height(imgWidth).expand();
        _characterInfoContainer = table;
        table.setPosition(
                (Gdx.graphics.getWidth() - table.getWidth())/2f,
                Gdx.graphics.getHeight() - table.getHeight() - MARGIN/2f
        );
        _stage.addActor(table);

        setEnemyCount(5);
        setGoodCount(3);


        _touchpad = createTouchpad(camera);

        _stage.addActor(angleUI);
        _stage.addActor(fireGage);
        _stage.addActor(_touchpad);


        enableFiring(true);

        Descriptions.instance().init(_stage);

//        _stage.setDebugAll(true);
//        Descriptions.instance().showTexts("test description");

//        TextPopup.showText("test text", 10, UIManager.SKINS.CRAFTCULAR, UIManager.SKIN_NAME_TITLE, TextPopup.MOVE_TYPE.FROM_TOP, _stage);
    }

    public void setEnemyCount(int count) {
        _enemyCountLabel.setText("X" + count);
    }
    public void setGoodCount(int count) {
        _goodCountLabel.setText("X" + count);
    }

    public Stage getStage() { return _stage; }

    protected Touchpad createTouchpad(PerspectiveCamera camera) {
        /* rotate touchpad */
        int MIN_BIG_RAD = 40;
        int bigRad = 200;
        int smallRad = 30;
        Drawable bigCircle = new TextureRegionDrawable(new TextureRegion(
                createCircleTexture(Math.max(MIN_BIG_RAD, bigRad),
                        new Color(Color.rgba4444(0, 0, 0, .0f)))
        ));
        Drawable smallCircle = new TextureRegionDrawable(new TextureRegion(
                createCircleTexture(smallRad, new Color(Color.rgba8888(1, 1, 1, 1)))
        ));
        Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle(bigCircle, smallCircle);

        Touchpad touchpadRot = new Touchpad(0, touchpadStyle);
        float x = (Gdx.graphics.getWidth() - bigRad*2) / 10f;
        float y = (Gdx.graphics.getHeight() - bigRad*2) / 4f;
        touchpadRot.setX(x);
        touchpadRot.setY(y);
        touchpadRot.setTouchable(Touchable.enabled);
        touchpadRot.addListener(new MyTouchpadListener(touchpadRot, _angleUI, camera));
        return touchpadRot;
    }

    protected Texture createCircleTexture(int radius, Color color) {
        Pixmap pixmap = new Pixmap(radius * 2, radius * 2, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.rgba8888(0, 0, 0, 0));
        pixmap.fill();
        pixmap.setColor(color);
        pixmap.fillCircle(radius, radius, radius);
        return new Texture(pixmap);
    }

    public void render(float delta) {
        if ( _updateBalls ) {
            _updateBalls = false;
            _ballsContainer.clearChildren();

            for (BallData ball : _gameStatePlay._balls) {
                Image image = new Image(new Texture(Gdx.files.internal(ball.ui_name)));
                float width = BALL_SIZE * ball.size;
                _ballsContainer.add(image).width(width).height(width).fill();
                _ballsContainer.add().width(width / 2f).height((width));
            }
            _ballsContainer.setX(Gdx.graphics.getWidth() * 1f / 8f );
            _ballsContainer.setY(MARGIN);
        }
        _stage.act(delta);
        _stage.draw();
    }

    public Table getFireBtnContainer() { return _fireBtnContainer; }

    synchronized public void enableFiring(boolean enable) {
        if ( _firingEnabled != enable ) {
            _firingEnabled = enable;
            if (enable) {
                _fireBtnContainer.addListener(_fireBtnListener);
                ParticleEffect pe = ParticleManager.instance().add2DParticleEffect(ParticleManager.ARROW_UPRIGHTLEFT_PARTICLE);
                pe.setPosition(_fireBtnContainer.getX() + _fireBtnContainer.getWidth()/2f,
                        _fireBtnContainer.getY() + _fireBtnContainer.getHeight()/2f);
                _particleEffect = pe;
            } else {
                _fireBtnContainer.removeListener(_fireBtnListener);
                ParticleManager.instance().remove2DParticleEffect(ParticleManager.ARROW_UPRIGHTLEFT_PARTICLE, _particleEffect);
            }
            EventBus.getDefault().post(new EventFireEnabled(enable));
        }
    }

    private boolean _updateBalls = false;
    private Music _fireMusic = null;

    public void updateBalls() {
        _updateBalls = true;
    }

    @Subscribe
    public void onEvent(EventFire event) {

        if ( event._stage == EventFire.STAGES.END )
        {
            enableFiring(false);
            _fireMusic.stop();
        } else if ( event._stage == EventFire.STAGES.START ) {
            _fireMusic.play();
        }

    }

    @Subscribe
    public void onEvent(EventTimeUp eventTimeUp ) {
        switch (eventTimeUp._code) {
            case AFTER_FIRE_TIME_UP:
                break;
        }
    }

    @Subscribe
    public void onEvent(EventGageFull event) {
        if ( event._state == EventGageFull.STATE.END ) {
            enableFiring(false);
            new java.util.Timer().schedule(
                    new java.util.TimerTask() {
                        @Override
                        public void run() {
                            EventBus.getDefault().post(new EventBallFiringOver());
                        }
                    },
                    3000
            );
            _fireMusic.stop();
            TextPopup.showText("timed out", 4,
                    UIManager.SKINS.ARCADE,
                    UIManager.SKIN_NAME_TITLE,
                    TextPopup.MOVE_TYPE.FROM_TOP,
                    _stage, 2f, null);
        }
    }

    @Subscribe
    public void onEvent(EventBallLoaded event) {
        enableFiring(true);

        float angle = 90f - (float)Math.toDegrees(
                Math.acos(
                        _camera.direction.cpy().dot(new Vector3(0,1,0))));
        _angleUI.setAngle(angle);
    }


    @Subscribe
    public void onEvent(EventLevelPhase event) {
        switch ( event._phase )
        {
            case START:
                enableFiring(false);
                break;
            case SHOW_AROUND_OVER:
                enableFiring(true);
                break;
        }
    }

    @Subscribe
    public void onEvent(EventDescriptionVisibility event) {
        if ( event._visible ) {
            _fireBtnContainer.setTouchable(Touchable.disabled);
            _touchpad.setTouchable(Touchable.disabled);
        } else {
            _fireBtnContainer.setTouchable(Touchable.enabled);
            _touchpad.setTouchable(Touchable.enabled);
        }
    }

    public void onEnter() {
        _fireBtnContainer.toFront();
    }
}
