package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;

public class Angle extends Widget {
    ShapeRenderer _shapeRenderer;

    private float _angle = 0;

    public Angle() {
        _shapeRenderer = new ShapeRenderer();//shapeRenderer;
        _shapeRenderer.setAutoShapeType(true);
    }

    public void setAngle(float angle) {
        _angle = angle;
        invalidate();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {

        float width = getWidth();
        float height = getHeight();
        float x = getX();
        float y = getY();
        float stickWidth = 20;
        batch.end();
        _shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        _shapeRenderer.setColor(Color.BLACK);
        _shapeRenderer.identity();
        _shapeRenderer.translate(x, y, 0);
        _shapeRenderer.circle(width * .73f, 0, stickWidth * 1.5f);
        _shapeRenderer.rectLine(width * 0.73f, 0, width + stickWidth * 1.5f, -stickWidth * 1.3f, stickWidth/2);
        _shapeRenderer.translate( width - stickWidth/2f, 0, 0);
        _shapeRenderer.rotate(0,0,-1, _angle );
        _shapeRenderer.rect(-width, 0, width, stickWidth);
        _shapeRenderer.translate(-width, 0, 0);
        _shapeRenderer.rect(0,-stickWidth /4f, stickWidth, stickWidth * 1.5f);

        _shapeRenderer.end();
        batch.begin();
    }
}
