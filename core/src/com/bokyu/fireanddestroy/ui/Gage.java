package com.bokyu.fireanddestroy.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;

/**
 * Created by vince on 2015-08-07.
 */
public class Gage extends Widget {

    float GAP = 5f;
    ShapeRenderer _shapeRenderer;
    int _tickCount;
    float _percent;

    public Gage(int tickCount) {
        _tickCount = tickCount;
        _shapeRenderer = new ShapeRenderer();//shapeRenderer;
        _shapeRenderer.setAutoShapeType(true);
    }
    public void setPercent(float val) {
        _percent = val;
    }
    public float getPercent() { return _percent; }
    @Override
    public void draw(Batch batch, float parentAlpha) {

        float width = getWidth();
        float height = getHeight();
        float x = getX();
        float y = getY();
        float step = (height + GAP)/((float)_tickCount) - GAP;

        batch.end();
        _shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
//        _shapeRenderer.setColor(0,0,0,.0f);

//        _shapeRenderer.rect(
//                x, y,
//                width, height
//        );

        _shapeRenderer.setColor(Color.RED);
        float maxHeight = (height * _percent);
        int j=0;
        for (float i = 0; i < maxHeight ; ++j) {

            if (j % 2 == 0) {
                _shapeRenderer.rect(x+0, y+i, width * widthEquation(i/height), step);
                i += step;
            } else i += GAP;
        }
        _shapeRenderer.end();
        batch.begin();
    }

    private float widthEquation(float height)
    {
//        return  (1f - (float)Math.pow((1f-height),2));
        float a=2, b = 2;
        height = height * 2f;
        return (float)Math.sqrt(Math.pow(height, 3)*(a - height)/(b*b));
    }
}
