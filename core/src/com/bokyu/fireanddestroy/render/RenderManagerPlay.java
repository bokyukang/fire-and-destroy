package com.bokyu.fireanddestroy.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalShadowLight;
import com.badlogic.gdx.graphics.g3d.particles.ParticleSystem;
import com.badlogic.gdx.graphics.g3d.particles.batches.BillboardParticleBatch;
import com.badlogic.gdx.graphics.g3d.particles.batches.ModelInstanceParticleBatch;
import com.badlogic.gdx.graphics.g3d.particles.batches.PointSpriteParticleBatch;
import com.badlogic.gdx.graphics.g3d.utils.DepthShaderProvider;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.bokyu.fireanddestroy.ParticleManager;
import com.bokyu.fireanddestroy.bullet.BulletWorld;
import com.bokyu.fireanddestroy.listeners.MyTouchpadListener;
import com.bokyu.fireanddestroy.ui.Gage;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RenderManagerPlay extends RenderManager {
    boolean _initialized = false;
    boolean _shadows = false;
    ModelBatch _shadowBatch = null;
    ModelBatch _modelBatch = null;
    SpriteBatch _spriteBatch = null;
    ShapeRenderer _shapeRenderer = null;
    BulletWorld _bulletWorld;
    DirectionalLight _light;
    PerspectiveCamera _camera;
    Environment _environment;
    Color _backgroundColor = Color.WHITE;
    Color _backgroundColor2 = Color.WHITE;


    public void init(BulletWorld bulletWorld, PerspectiveCamera camera) {

        if (_initialized) return;
        _initialized = true;

        _bulletWorld = bulletWorld;
        _camera = camera;
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, .4f, .4f, .4f, 1f));
        _environment.set(new ColorAttribute(ColorAttribute.Fog, 0,0,0,0.1f));
        _light = _shadows ? new DirectionalShadowLight(1024,1024, 20f, 20f, 1f, 300f) : new DirectionalLight();
        _light.set(1f, 1f, 1f, -.5f, -1f, -.7f);
        _environment.add(_light);
        if ( _shadows )
            _environment.shadowMap = (DirectionalShadowLight)_light;

        _shadowBatch = new ModelBatch(new DepthShaderProvider());
        _modelBatch = new ModelBatch();
        _spriteBatch = new SpriteBatch();
        _shapeRenderer = new ShapeRenderer();
        _shapeRenderer.setAutoShapeType(true);


    }



    public void dispose() {
        _modelBatch.dispose();
        _modelBatch = null;

        _shadowBatch.dispose();
        _shadowBatch = null;

        if (_shadows)
            ((DirectionalShadowLight)_light).dispose();
        _light = null;
    }

    protected void beginRender () {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
//        Gdx.gl.glClearColor(_backgroundColor.r,_backgroundColor.g,_backgroundColor.b,_backgroundColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        _shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        _shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                _backgroundColor, _backgroundColor, _backgroundColor2, _backgroundColor2);
        _shapeRenderer.end();
    }

    public void setBackgroundColor(Color color, Color color2) {
        _backgroundColor = color;
        _backgroundColor2 = color2;
    }

    @Override
    public void render(float delta) {

        beginRender();

        if (_shadows) {
            ((DirectionalShadowLight)_light).begin(Vector3.Zero, _camera.direction);
            _shadowBatch.begin(((DirectionalShadowLight)_light).getCamera());
            _bulletWorld.render(_shadowBatch, null);
            _shadowBatch.end();
            ((DirectionalShadowLight)_light).end();
        }


        _modelBatch.begin(_camera);

        Gdx.gl.glCullFace(GL20.GL_FRONT_FACE);
        _bulletWorld.render(_modelBatch, _environment);

        _modelBatch.end();


        ParticleManager.instance().render(_spriteBatch, _modelBatch, delta);
    }





}
