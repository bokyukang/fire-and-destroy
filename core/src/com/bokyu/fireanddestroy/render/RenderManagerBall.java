package com.bokyu.fireanddestroy.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.bokyu.buildhouse.data.BallData;
import com.bokyu.buildhouse.events.EventBallFired;
import com.bokyu.buildhouse.events.EventBallLoaded;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.fireanddestroy.ObjectsManager;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.animaters.AnimaterRotateModel;
import com.bokyu.fireanddestroy.events.EventFireEnabled;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RenderManagerBall extends RenderManager {

    boolean _initialized = false;
    DirectionalLight _light;
    PerspectiveCamera _camera;
    public boolean _enabledFire = false;
    Environment _environment;
    ModelBatch _modelBatch = null;
    ModelInstance _modelInstance = null;
    float _width;
    Rectangle _rect;
    AnimaterRotateModel _animater;
    BallData _ballData = null;

    public RenderManagerBall() {
        EventBus.getDefault().register(this);
    }

    public void init(Table target, float width) {
        if (_initialized) return;
        _initialized = true;

        _width = width;
        _rect = new Rectangle(target.getX() + target.getWidth()/2f - width/2f,
                target.getY() + target.getHeight()/2f - width/2f,
                width, width );
        _camera = new PerspectiveCamera();
        _camera.position.set(0,0,1);
        _camera.lookAt(0,0,0);
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, .3f, .3f, .3f, 1f));
        _environment.set(new ColorAttribute(ColorAttribute.Diffuse, 1f, 1f, 1f, 1f));
        _light = new DirectionalLight();
        _light.set(1f, 1f, 1f, -.5f, -.6f, -1.7f);
        _environment.add(_light);
        _modelBatch = new ModelBatch();

        _animater = new AnimaterRotateModel(null, new Vector3(1,2,3), 400);
        _enabledFire = false;
        enableFire();
    }

    protected void beginRender () {
        Gdx.gl.glViewport((int)_rect.x, (int)_rect.y, (int)_rect.width, (int)_rect.height);
        Gdx.gl.glClearColor(0,0,0,0);
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void render(float delta) {
        beginRender();
        _modelBatch.begin(_camera);
        _lock.lock();
        try {
            if (_modelInstance != null) {
                _modelBatch.render(_modelInstance, _environment);
            }
        } finally {
            _lock.unlock();
        }
        _modelBatch.end();
    }

    public void dispose() {
        _modelBatch.dispose();
    }

    public void enableFire() {
        if ( !_enabledFire && _animater != null)
            AnimaterManager.instance().addAnimater(_animater);
        _enabledFire = true;
    }

    public void setBallData(BallData ballData) {
        _lock.lock();
        try {
            _ballData = ballData;
            _modelInstance = new ModelInstance(ObjectsManager.instance().getModel(ballData.model_name));
            _modelInstance.transform.idt();
            _animater.setModelInstance(_modelInstance);
        } finally {
            _lock.unlock();
        }
    }

    public void disableFire() {
        if ( _enabledFire && _animater != null) {
            AnimaterManager.instance().removeAnimater(_animater);
        }
        _enabledFire = false;
    }

    @Subscribe
    public void onEvent(EventFireEnabled event) {
        if ( event._enabled ) {
            enableFire();
        } else {
            disableFire();
        }
    }

    @Subscribe
    public void onEvent(EventBallLoaded event) {
        setBallData(event._ballData);
    }

    Lock _lock = new ReentrantLock();
    @Subscribe
    public void onEvent(EventBallFired event) {
        _lock.lock();
        try {
            _modelInstance = null;
        }finally {
            _lock.unlock();
        }
    }

}
