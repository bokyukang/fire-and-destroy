package com.bokyu.fireanddestroy.players;

/**
 * Created by admin on 2018-08-14.
 */

public class GoodCharacter extends Character {

    final static public String MODEL_NAME = "good_character";

    public GoodCharacter(float mass) {
        super(TYPE.GOOD_CHARACTER, MODEL_NAME, mass);
    }

    @Override
    public GoodCharacter clone() {
        GoodCharacter goodCharacter = new GoodCharacter( _mass);
        goodCharacter.setDirection(_direction);
        goodCharacter.setPosition(_position.x, _position.z);
        goodCharacter.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        goodCharacter.setElevation(_elevation);
        return goodCharacter;
    }
}
