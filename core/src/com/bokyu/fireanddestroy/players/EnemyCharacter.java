package com.bokyu.fireanddestroy.players;

/**
 * Created by admin on 2018-08-14.
 */

public class EnemyCharacter extends Character {

    final static public String MODEL_NAME = "enemy_character";

    public EnemyCharacter(float mass) {
        super(TYPE.ENEMY_CHARACTER, MODEL_NAME, mass);
    }

    @Override
    public EnemyCharacter clone() {
        EnemyCharacter enemy = new EnemyCharacter( _mass);
        enemy.setDirection(_direction);
        enemy.setPosition(_position.x, _position.z);
        enemy.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        enemy.setElevation(_elevation);
        return enemy;
    }
}
