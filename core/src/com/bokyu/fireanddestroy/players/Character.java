package com.bokyu.fireanddestroy.players;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btConeTwistConstraint;
import com.badlogic.gdx.physics.bullet.dynamics.btDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btHingeConstraint;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.dynamics.btTypedConstraint;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.fireanddestroy.ObjectsManager;
import com.bokyu.fireanddestroy.bullet.BulletWorld;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Character extends BuildingGlyph {



    public static String PART_HEAD = "head";
    public static String PART_LEFTARM = "left-arm";
    public static String PART_RIGHTARM = "right-arm";
    public static String PART_LEFTLEG = "left-leg";
    public static String PART_RIGHTLEG = "right-leg";
    public static String PART_TORSO = "torso";
    public String[] PARTS = { PART_HEAD, PART_LEFTARM, PART_RIGHTARM, PART_LEFTLEG, PART_RIGHTLEG, PART_TORSO };
    private Set<Constants.DIRECTION> _dirs = null;
    Model _model;
    private BulletWorld _bulletWorld;
    private BoundingBox _boundingBox;
    private String _modelName;
    public boolean _dead = false;
    static protected btBoxShape _shape = null;

    public Character(TYPE type, String modelName, float mass) {
        super(type, "");
        _modelName = modelName;
        Model model = ObjectsManager.instance().getModel(modelName);
        _model = model;
        _modelInstance = new ModelInstance(model);
        _mass = mass;
        _dirs = new HashSet();
        _dirs.add(Constants.DIRECTION.CENTER);

        BoundingBox boundingBox=  new BoundingBox();
        _boundingBox = boundingBox;
        _modelInstance.calculateBoundingBox(boundingBox);
        if ( _shape == null ) {
            _shape = new btBoxShape(new Vector3(boundingBox.getWidth() / 2f * .9f, boundingBox.getHeight() / 2f,
                    boundingBox.getDepth() / 2f));
            _shape.calculateLocalInertia(_mass, tmpV);
        }
        Vector3 localInertia;
        localInertia = tmpV;
        btRigidBody.btRigidBodyConstructionInfo bodyInfo
                = new btRigidBody.btRigidBodyConstructionInfo(_mass, null, _shape, localInertia);
        _btRigidBody = new btRigidBody(bodyInfo);
        _btRigidBody.setCollisionShape(_shape);
        _btRigidBody.setRestitution(0.0f);
        _btRigidBody.setFriction(1000);

        bodyInfo.dispose();
//        for ( String part : PARTS ) {
//            createGlyph(part);
//        }

//        setupContraints();
//        _btRigidBody = new btRigi
    }

    @Override
    public void init() {
        _dead = false;
    }

    @Override
    public void dispose() {
    }


    @Override
    public void onAddToWorld(BulletWorld bulletWorld ) {
        _bulletWorld = bulletWorld;
    }


    @Override
    public Character clone() {
        Character roof = new Character(_type, _modelName, _mass);
        roof.setDirection(_direction);
        roof.setPosition(_position.x, _position.z);
        roof.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        roof.setElevation(_elevation);
        return roof;
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        modelBatch.render(_modelInstance, environment);
    }
    @Override
    public void updatePosition() {
        Matrix4 matrix = new Matrix4().idt();
        setTransformPosition(matrix);
        setTransformRotate(matrix);
//        setTransformOffset(matrix);
        _btRigidBody.setWorldTransform(matrix);
        _modelInstance.transform.set(matrix);
//        for ( MyGlyph glyph : _glyphs ) {
//            glyph.getModelInstance().transform.set(matrix);
//        }
    }

    @Override
    protected void setTransformPosition(Matrix4 transform) {
        super.setTransformPosition(transform);
        transform.translate(0, _boundingBox.getHeight()/2f, 0);
    }

    @Override
    public Set<Constants.DIRECTION> getOccupyingDirs() {
        return _dirs;
    }
}
