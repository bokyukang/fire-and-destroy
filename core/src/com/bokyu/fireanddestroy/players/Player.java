package com.bokyu.fireanddestroy.players;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.bokyu.fireanddestroy.bullet.BulletEntity;
import com.bokyu.fireanddestroy.bullet.BulletWorld;

/**
 * Created by admin on 2018-07-23.
 */

public class Player {
    public BulletEntity _entity;
    public void init(BulletWorld bulletWorld) {
        _entity = bulletWorld.add("box", 0,0,0);
//        _entity.body.setCollisionFlags(_entity.body.getCollisionFlags()| btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT);
    }
}
