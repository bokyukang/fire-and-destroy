package com.bokyu.fireanddestroy.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.bokyu.buildhouse.data.BallDataContainer;
import com.bokyu.buildhouse.data.LevelData;
import com.bokyu.buildhouse.data.LevelDataContainer;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.fireanddestroy.events.EventBuildingSaved;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 2018-08-14.
 */

public class LevelManager {

    static private LevelManager _instance = null;

    static public LevelManager instance() {
        if ( _instance == null ) {
            _instance = new LevelManager();
        }
        return _instance;
    }

    List<Level> _levels = new ArrayList();
    private int _clearedLevel = 0;
    private DbManager _dbManager = null;

    private LevelManager() {
        EventBus.getDefault().register(this);
    }

    public void init(DbManager dbManager) {
        _dbManager = dbManager;

        _clearedLevel = dbManager.getClearedLevel();
//        _clearedLevel = 23;

        for ( LevelData levelData : LevelDataContainer.instance()._levelData) {
            Level level = new Level(levelData, _levels.size()+1);
            for ( String buildinName : levelData.buildings ) {
                level._buildingNames.add(buildinName);
            }
            for ( int ballIdx : levelData.balls ) {
                level._balls.add(
                        BallDataContainer.instance()._ballDataMap.get(LevelData.getBallName(ballIdx)));
            }
            _levels.add(level);
        }
    }

    public Level getLevel(int level) {
        if ( _levels.size() < level || level < 1) return null;
        return _levels.get(level-1);
    }

    public boolean setClearedLevel(int level) {
        return _dbManager.saveClearedLevel(level);
    }

    public boolean levelUp(Level curlevel) {
        _clearedLevel = curlevel._levelNo;
        _dbManager.saveClearedLevel(_clearedLevel);
        if ( _levels.size() > _clearedLevel )
            return true;
        else
            return false;
    }

    public int getClearedLevel() { return _clearedLevel; }

    @Subscribe
    public void onEvent(EventBuildingSaved event) {
        FileHandle file = Gdx.files.external("game_levels/" + event._name);
        file.parent().mkdirs();
        file.writeBytes(
                event._building.toPB().toByteArray(), false
        );
    }

    public int getTotalLevelCount() { return _levels.size(); }
}
