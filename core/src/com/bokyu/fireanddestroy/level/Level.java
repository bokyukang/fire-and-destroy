package com.bokyu.fireanddestroy.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.buildhouse.data.BallData;
import com.bokyu.buildhouse.data.LevelData;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.network.Network;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.List;


public class Level {
    public boolean _loaded = false;
    public int _levelNo = 0;
    public List<String> _buildingNames = new ArrayList();
    public List<Building> _buildings = new ArrayList();
    public int _goodCharacCount = 0;
    public int _enemyCount = 0;
    public List<BallData> _balls = new ArrayList();
    public Color _skyColor = Color.WHITE;
    public Color _skyColor2 = Color.WHITE;
    public String _floor = "grass_land";
    public float _floorUvScale = 20f;
    public LevelData _levelData;
    public Vector3 _camPosition;
    public String[] _descriptions;
    public boolean _shownDescription = false;
    public Texture _groundTexture = null;
    public TextureAttribute _groundTextureAttribute;

    public Level(LevelData levelData, int levelNo) {
        _levelData = levelData;
        _levelNo = levelNo;
    }

    public void load() {
        if ( _loaded ) return;
        _loaded = true;

        _floor = _levelData.floor;
        _floorUvScale = _levelData.floor_uv_scale;
        _skyColor = getColor( _levelData.sky_color );
        _skyColor2 = getColor( _levelData.sky_color_2 );
        _camPosition = new Vector3( 0, 3, _levelData.cam_distance);
        _descriptions = _levelData.descriptions;

        _groundTexture = new Texture(Gdx.files.internal("terrein/"+_floor));
        _groundTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        _groundTextureAttribute = TextureAttribute.createDiffuse(_groundTexture);
        _groundTextureAttribute.scaleV = _groundTextureAttribute.scaleU = _floorUvScale;

        for ( Building building : _buildings)
        {
            building.dispose();
        }
        _buildings.clear();
        _goodCharacCount = _enemyCount = 0;

        for ( String buildingName : _buildingNames ) {
            FileHandle file = Gdx.files.internal("level/" + buildingName);
            try {
                byte[] bytes = file.readBytes();
                Network.building_model pbBuilding = Network.building_model.parseFrom(bytes);
                Building building = new Building();
                building.init();
                building.parse(pbBuilding);
                _buildings.add(building);
                _goodCharacCount += building._goodCharacCount;
                _enemyCount += building._enemyCount;
            } catch ( InvalidProtocolBufferException e ) {
                e.printStackTrace();
            }

        }
    }

    public void dispose() {
        for ( Building building : _buildings)
        {
            building.dispose();
        }
        _buildings.clear();
        _loaded = false;
    }

    public enum TEST_RESULT {
        SUCCESS,
        FAIL,
        UNKNOWN
    }

    public TEST_RESULT testComplete(int goodCount, int enemyCount) {
        if ( enemyCount == _levelData.max_enemy_alive && goodCount >= _levelData.min_good_alive ) {
            return TEST_RESULT.SUCCESS;
        }
        if ( goodCount < _levelData.min_good_alive ) {
            return TEST_RESULT.FAIL;
        }
        return TEST_RESULT.UNKNOWN;
    }

    private Color getColor(float[] colorArr) {
        Color color = new Color();
        color.r = colorArr[0] / 255f;
        color.g = colorArr[1] / 255f;
        color.b = colorArr[2] / 255f;
        color.a = colorArr[3];

        return color;
    }

}
