/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.bokyu.fireanddestroy.bullet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.DebugDrawer;
import com.badlogic.gdx.physics.bullet.collision.ContactListener;
import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btManifoldPoint;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.badlogic.gdx.physics.bullet.linearmath.btIDebugDraw;
import com.badlogic.gdx.utils.PerformanceCounter;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.fireanddestroy.SoundManager;
import com.bokyu.fireanddestroy.animaters.AnimaterBlinkAndKill;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.events.EventChracterDead;
import com.bokyu.fireanddestroy.events.EventNoContact;
import com.bokyu.fireanddestroy.players.Character;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.badlogic.gdx.physics.bullet.collision.CollisionConstants.ACTIVE_TAG;


/** @author xoppa Bullet physics world that holds all bullet entities and constructors. */
public class BulletWorld extends BaseWorld<BulletEntity> {

	private final static float IMPULSE_CRITERIA = 1f;
	public final static short GROUND_FLAG = 1<<8;
	public final static short CHARACTER_FLAG = 1<<9;
	public final static short BALL_FLAG = 1<<7;
	public final static short ALL_FLAG = -1;

	public DebugDrawer debugDrawer = null;
	public boolean renderMeshes = true;

	public  btCollisionConfiguration collisionConfiguration;
	public  btCollisionDispatcher dispatcher;
	public  btBroadphaseInterface broadphase;
	public  btConstraintSolver solver;
	public  btCollisionWorld collisionWorld;
	public PerformanceCounter performanceCounter;
	public Vector3 gravity;

	public int maxSubSteps = 5;
	public float fixedTimeStep = 1f / 60f;

	ContactListener _contactListener = null;
	private List<BulletEntity> _characterEntities = new ArrayList();
	private List<Character> _characterGlyphs = new ArrayList();
	private boolean _contacted = false;
	EventNoContact _eventNoContact = new EventNoContact();
	NoContactChecker _noContactChecker = new NoContactChecker();
	Music _demolitionMusic = null;

	private class NoContactChecker extends Thread {
		@Override
		public void run() {
			try {
				while ( true ) {
					checkNoContact();
					_contacted = false;
					Thread.sleep(1000);
				}
			} catch ( InterruptedException e ) {
				e.printStackTrace();
			}
		}
	}

	public class MyContactListener extends ContactListener {

//	    @Override
//		public void onContactProcessed(btManifoldPoint cp, int userValue0, int userValue1) {
//			_contacted = true;
//			if ( userValue0 != -1 ) {
//				checkDead(userValue0);
//			}
//			if ( userValue1 != -1 ) {
//				checkDead(userValue1);
//			}
//        }

		@Override
		public void onContactProcessed(int userValue0, boolean match0, int userValue1, boolean match1) {
			{
	        if ( !_demolitionMusic.isPlaying() ) {
				_demolitionMusic.play();
			}
				_contacted = true;
			    if ( userValue0 != -1 ) {
					checkDead(userValue0);
				}
				if ( userValue1 != -1 ) {
					checkDead(userValue1);
				}

			}
        }
	}


	private void checkNoContact() {
		if ( _contacted == false ) {
			EventBus.getDefault().post(_eventNoContact);
			System.out.println("NoContactEvent");
			try {
				_demolitionMusic.stop();
			} catch ( IllegalStateException e ) {
                e.printStackTrace();}
	}
	}

	private void checkDead(int charIdx) {
		BulletEntity be = _characterEntities.get(charIdx);
		Character character = _characterGlyphs.get(charIdx);

		float[] transValues = be.modelInstance.transform.getValues();
		Vector3 upVec = new Vector3(
				transValues[Matrix4.M01]
				, transValues[Matrix4.M11]
				, transValues[Matrix4.M21]);
		if ( upVec.dot(0,1,0) < 0.7f  && !character._dead ) {
			character._dead = true;
			AnimaterBlinkAndKill animaterBlinkAndKill = new AnimaterBlinkAndKill(be, this);
			animaterBlinkAndKill.init();
			AnimaterManager.instance().addAnimater(animaterBlinkAndKill);
			EventBus.getDefault().post(
					new EventChracterDead(character.getType() == BuildingGlyph.TYPE.GOOD_CHARACTER)
			);
		}
	}

	public BulletWorld(final btCollisionConfiguration collisionConfiguration, final btCollisionDispatcher dispatcher,
                       final btBroadphaseInterface broadphase, final btConstraintSolver solver, final btCollisionWorld world, final Vector3 gravity) {
		this.collisionConfiguration = collisionConfiguration;
		this.dispatcher = dispatcher;
		this.broadphase = broadphase;
		this.solver = solver;
		this.collisionWorld = world;
		if (world instanceof btDynamicsWorld) ((btDynamicsWorld)this.collisionWorld).setGravity(gravity);
		this.gravity = gravity;
	}

	public BulletWorld(final btCollisionConfiguration collisionConfiguration, final btCollisionDispatcher dispatcher,
                       final btBroadphaseInterface broadphase, final btConstraintSolver solver, final btCollisionWorld world) {
		this(collisionConfiguration, dispatcher, broadphase, solver, world, new Vector3(0, -10, 0));
	}

	public BulletWorld(final Vector3 gravity) {
		this.gravity = gravity;
	    initVariables();
	}

	protected void initVariables() {
		collisionConfiguration = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfiguration);
		broadphase = new btDbvtBroadphase();
		solver = new btSequentialImpulseConstraintSolver();
		collisionWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
		((btDynamicsWorld)collisionWorld).setGravity(this.gravity);

	}

	Lock _lock = new ReentrantLock();
	public void refresh() {

	    _lock.lock();
	    try {
			collisionWorld.dispose();
			collisionConfiguration.dispose();
			dispatcher.dispose();
			broadphase.dispose();
			solver.dispose();

			initVariables();
		}finally {
	    	_lock.unlock();
		}
	}
	public BulletWorld() {
		this(new Vector3(0, -10, 0));
	}

	public void init() {
		_contactListener = new MyContactListener();
		_contactListener.disableOnAdded();
		_contactListener.enableOnProcessed();
		_noContactChecker.start();
		_demolitionMusic = SoundManager.instance().getMusic(SoundManager.SOUND_NAMES.DEMOLITION);
	}

	public void enableContactListener(boolean enable) {
		if ( enable ) {
			_contactListener.enable();
		} else {
			_contactListener.disable();
		}
	}

	public void stopDemolitionMusic() {
		_demolitionMusic.stop();
	}

	public BulletEntity add(final BuildingGlyph glyph) {

        BulletEntity be = new BulletEntity(glyph.getModelInstance(), glyph.getCollisionObject());
        add(be);

        if ( glyph instanceof Character) {
            be.body.setCollisionFlags(
                    be.body.getCollisionFlags()
                            | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
            be.body.setContactCallbackFlag(CHARACTER_FLAG);
            be.body.setContactCallbackFilter(ALL_FLAG);
            be.body.setUserValue(_characterEntities.size());
            _characterEntities.add(be);
            _characterGlyphs.add((Character)glyph);
        }

		glyph.onAddToWorld(this);

		return be;
	}

	@Override
	synchronized public void add (final BulletEntity entity) {
		super.add(entity);
		if (entity.body != null) {
			if (entity.body instanceof btRigidBody)
				((btDiscreteDynamicsWorld)collisionWorld).addRigidBody((btRigidBody)entity.body);
			else
				collisionWorld.addCollisionObject(entity.body);
			// Store the index of the entity in the collision object.
			entity.body.setUserValue( -1);
//			entity.body.setActivationState(ACTIVE_TAG);
		}
	}

	synchronized public void remove(final BulletEntity entity) {
		if (entity.body != null) {
//			if (entity.body instanceof btRigidBody)
//				((btDiscreteDynamicsWorld)collisionWorld).removeRigidBody((btRigidBody)entity.body);
//			else
				collisionWorld.removeCollisionObject(entity.body);
			// Store the index of the entity in the collision object.
		}

		entities.remove(entity);
//		entity.dispose();
	}

	@Override
	public void update () {
		_lock.lock();
		try {
			if (performanceCounter != null) {
				performanceCounter.tick();
				performanceCounter.start();
			}
			if (collisionWorld instanceof btDynamicsWorld) {
				((btDynamicsWorld) collisionWorld).stepSimulation(Gdx.graphics.getDeltaTime(), maxSubSteps, fixedTimeStep);
			}
			if (performanceCounter != null) performanceCounter.stop();
		} finally {
			_lock.unlock();
		}
	}

	@Override
	synchronized
	public void render (ModelBatch batch, Environment lights, Iterable<BulletEntity> entities) {
		_lock.lock();
		try {
			if (renderMeshes) {
//			super.render(batch, lights, entities);
				for (final BulletEntity e : entities) {
					if (e.modelInstance != null && e._visible) {
						batch.render(e.modelInstance, lights);
					}
				}
			}
			if (debugDrawer != null && debugDrawer.getDebugMode() > 0) {
				batch.flush();
				debugDrawer.begin(batch.getCamera());
				collisionWorld.debugDrawWorld();
				debugDrawer.end();

			}
		} finally {
			_lock.unlock();
		}
	}


	@Override
	public void dispose () {
		for ( BulletEntity be : entities) {
			if (be.body != null) {
				if (be.body instanceof btRigidBody)
					((btDynamicsWorld)collisionWorld).removeRigidBody((btRigidBody)be.body);
				else
					collisionWorld.removeCollisionObject(be.body);
			}
		}

		super.dispose();

		collisionWorld.dispose();
		if (solver != null) solver.dispose();
		if (broadphase != null) broadphase.dispose();
		if (dispatcher != null) dispatcher.dispose();
		if (collisionConfiguration != null) collisionConfiguration.dispose();

		if (_contactListener != null) _contactListener.dispose();
		_contactListener = null;
	}

	public void setDebugMode (final int mode) {
		if (mode == btIDebugDraw.DebugDrawModes.DBG_NoDebug && debugDrawer == null) return;
		if (debugDrawer == null) collisionWorld.setDebugDrawer(debugDrawer = new DebugDrawer());
		debugDrawer.setDebugMode(mode);
	}

	public int getDebugMode () {
		return (debugDrawer == null) ? 0 : debugDrawer.getDebugMode();
	}

	synchronized
	public void clearCharacters() {
		_characterGlyphs.clear();
		_characterEntities.clear();
	}

}
