package com.bokyu.fireanddestroy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffectLoader;
import com.badlogic.gdx.graphics.g3d.particles.ParticleSystem;
import com.badlogic.gdx.graphics.g3d.particles.batches.BillboardParticleBatch;
import com.badlogic.gdx.utils.Pool;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by admin on 2018-07-30.
 */

public class ParticleManager {
    static private ParticleManager _instance = null;
    private final float ALIVE_CHECK_INTERVAL = 0.3f;
    private float _aliveCheckRemaining = 0;
    private ParticleSystem _particleSystem;
    private BillboardParticleBatch _billboardBatch = null;
    static public String TARGET_PARTICLE = "target_particle.p";
    static public String STAR_PARTICLE = "star.p";
    static public String FIREWORK_PARTICLE = "firework.p";
    static public String ARROW_PARTICLE = "arrow.p";
    static public String ARROW_UPDOWN_PARTICLE = "arrow_updown.p";
    static public String ARROW_UPRIGHTLEFT_PARTICLE = "arrow_uprightleft.p";
    private Map<String, com.badlogic.gdx.graphics.g3d.particles.ParticleEffect> _3DparticleEffects = new HashMap();
    private Map<String, Pool<com.badlogic.gdx.graphics.g2d.ParticleEffect>> _2DparticleEffects = new HashMap();
    private Set<ParticleEffect> _added2DParticles = new HashSet();
    private TextureAtlas _textureAtlas;
    private Camera _camera;

    static public ParticleManager instance() {
        if ( _instance == null ) {
            _instance = new ParticleManager();
        }
        return _instance;
    }

    private ParticleManager() {
    }

    public void loadParticles(Camera camera) {
        _particleSystem = new ParticleSystem();
        _billboardBatch = new BillboardParticleBatch();
        _billboardBatch.setCamera(camera);
        _particleSystem.add(_billboardBatch);
        _textureAtlas = new TextureAtlas(Gdx.files.internal("particles/packedTextures.atlas"));
        _camera = camera;

        load3DParticle(TARGET_PARTICLE);
        load2DParticle(STAR_PARTICLE);
        load2DParticle(FIREWORK_PARTICLE);
        load2DParticle(ARROW_PARTICLE);
        load2DParticle(ARROW_UPDOWN_PARTICLE);
        load2DParticle(ARROW_UPRIGHTLEFT_PARTICLE);
    }

    private void load3DParticle(String name) {
        AssetManager assets = new AssetManager();
        ParticleEffectLoader.ParticleEffectLoadParameter loadParam = new ParticleEffectLoader.ParticleEffectLoadParameter(_particleSystem.getBatches());
        ParticleEffectLoader loader = new ParticleEffectLoader(new InternalFileHandleResolver());
        assets.setLoader(com.badlogic.gdx.graphics.g3d.particles.ParticleEffect.class, loader);
        assets.load("particles/" + name, com.badlogic.gdx.graphics.g3d.particles.ParticleEffect.class, loadParam);
        assets.finishLoading();
        com.badlogic.gdx.graphics.g3d.particles.ParticleEffect effect = assets.get("particles/" + name);
        com.badlogic.gdx.graphics.g3d.particles.ParticleEffect effect2 = effect.copy();
        effect2.init();
        _3DparticleEffects.put(name, effect2);
    }

    private void load2DParticle(final String name) {
        _2DparticleEffects.put(name,  new Pool<ParticleEffect>() {
            @Override
            protected ParticleEffect newObject() {
                ParticleEffect pe = new ParticleEffect();
                pe.load(Gdx.files.internal("particles/" + name), _textureAtlas);
                return pe;
            }
        });
    }

    public com.badlogic.gdx.graphics.g3d.particles.ParticleEffect getParticleEffect(String name) {
        return _3DparticleEffects.get(name);
    }

    public com.badlogic.gdx.graphics.g3d.particles.ParticleEffect add3DParticleEffect(String name) {
        com.badlogic.gdx.graphics.g3d.particles.ParticleEffect pe = _3DparticleEffects.get(name);
        _particleSystem.add(pe);
        return pe;
    }
    public com.badlogic.gdx.graphics.g3d.particles.ParticleEffect remove3DParticleEffect(com.badlogic.gdx.graphics.g3d.particles.ParticleEffect pe) {
        _particleSystem.remove(pe);
        return pe;
    }

    private Lock _lock = new ReentrantLock();

    public ParticleEffect add2DParticleEffect(String name) {
        ParticleEffect pe = _2DparticleEffects.get(name).obtain();
        _lock.lock();
        try {
            _added2DParticles.add(pe);
        } finally {
            _lock.unlock();
        }
        return pe;
    }

    public ParticleEffect remove2DParticleEffect(String name, ParticleEffect pe) {
        _lock.lock();
        try {
            _added2DParticles.remove(pe);
            _2DparticleEffects.get(name).free(pe);
        } finally {
            _lock.unlock();
        }
        return pe;
    }

    public ParticleSystem getParticleSystem() { return _particleSystem; }

    public void render(SpriteBatch spriteBatch, ModelBatch modelBatch, float delta) {
        _aliveCheckRemaining -= delta;

        _particleSystem.update(); // technically not necessary for rendering
        _particleSystem.begin();
        _particleSystem.draw();
        _particleSystem.end();

        modelBatch.begin(_camera);
        modelBatch.render(_particleSystem);
        modelBatch.end();

        spriteBatch.begin();
        _lock.lock();
        try {
            if ( _aliveCheckRemaining < 0 ) {
                _aliveCheckRemaining = ALIVE_CHECK_INTERVAL;
                Set<ParticleEffect> tobeRemoved = new HashSet();
                for ( ParticleEffect pe : _added2DParticles ) {
                    if ( pe.isComplete() ) {
                        tobeRemoved.add(pe);
                    }
                }
                for ( ParticleEffect pe : tobeRemoved ) {
                    _added2DParticles.remove(pe);
                }
            }
            for ( ParticleEffect pe : _added2DParticles ) {
                pe.draw(spriteBatch, delta);
            }
        } finally {
            _lock.unlock();
        }
        spriteBatch.end();
    }
    public void dispose() {
        for ( com.badlogic.gdx.graphics.g3d.particles.ParticleEffect pe : _3DparticleEffects.values() ) {
            pe.dispose();
        }
    }
}
