package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleByAction;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by admin on 2018-08-02.
 */

public class AnimaterRemoveActor extends Animater {

    static public Pool<AnimaterRemoveActor> _pool = new Pool<AnimaterRemoveActor>()
    {
        @Override
        protected AnimaterRemoveActor newObject () {
            return new AnimaterRemoveActor();
        }
    };

    public float _time = 0;
    public float _duration = 0;
    public Actor _actor;

    public AnimaterRemoveActor() {}

    public void init(float duration, Actor actor) {
        _duration = duration;
        _time = 0;
        _actor = actor;
    }

    @Override
    public boolean animate(float delta) {
        _time += delta;
        if ( _time > _duration ) {
            _actor.remove();
            return false;
        }

        return true;
    }
}
