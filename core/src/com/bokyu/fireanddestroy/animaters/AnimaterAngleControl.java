package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.bokyu.fireanddestroy.ui.Angle;

public class AnimaterAngleControl extends Animater {

    PerspectiveCamera _camera;
    Touchpad _touchpad;
    float _angleMax, _angleMin;
    Vector3 _right;
    Angle _angleUI;

    public AnimaterAngleControl(PerspectiveCamera camera, float angleMax, float angleMin, Touchpad touchpad, Angle angleUI) {
        _camera = camera;
        _angleUI = angleUI;
        _angleMax = angleMax;
        _angleMin = angleMin;
        _touchpad  = touchpad;
        _right = new Vector3(0,1,0).crs(_camera.direction).nor();
    }

    @Override
    public boolean animate(float interval) {
        float rot = -_touchpad.getKnobPercentY()*20f * interval;
        float angle = 90f - (float)Math.toDegrees(
                Math.acos(
                        _camera.direction.cpy().dot(new Vector3(0,1,0))));
        if ( angle > _angleMax && rot < 0 ) return true;
        if ( angle < _angleMin && rot > 0 ) return true;
        _camera.rotate(_right, rot);
        _camera.update();
        _angleUI.setAngle(angle);

        return true;
    }
}
