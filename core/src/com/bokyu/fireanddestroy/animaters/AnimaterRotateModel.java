package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;

public class AnimaterRotateModel extends Animater {

    ModelInstance _modelInstance;
    Vector3 _axis;
    float _speed;
    public AnimaterRotateModel(ModelInstance modelInstance, Vector3 axis, float speed) {
        _modelInstance = modelInstance;
        _axis = axis;
        _speed = speed;
    }

    @Override
    public boolean animate(float interval) {
        if ( _modelInstance != null )
            _modelInstance.transform.rotate(_axis, _speed * interval);
        return true;
    }

    public void setModelInstance(ModelInstance modelInstance ) {
        _modelInstance = modelInstance;
    }

    public void init() {
    }
}
