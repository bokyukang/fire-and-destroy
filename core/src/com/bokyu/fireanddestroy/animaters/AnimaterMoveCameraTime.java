package com.bokyu.fireanddestroy.animaters;

        import com.badlogic.gdx.graphics.PerspectiveCamera;
        import com.badlogic.gdx.math.Interpolation;
        import com.badlogic.gdx.math.Vector3;

        import org.greenrobot.eventbus.EventBus;

public class AnimaterMoveCameraTime extends Animater {
    PerspectiveCamera _camera;
    Vector3 _targetPos, _endPos;
    Vector3 _initDir, _initPos;
    float _totalTime;
    float _elapsedTime;
    Object _endEvent = null;

    public AnimaterMoveCameraTime(PerspectiveCamera camera, Vector3 targetPos, Vector3 endPos, float time) {
        _camera = camera;
        _targetPos = targetPos.nor();
        _endPos = endPos;
        _totalTime  = time;
        _initPos = _camera.position.cpy();
    }

    @Override
    public boolean animate(float interval) {
        _elapsedTime += interval;
        if ( _elapsedTime > _totalTime ){
            if ( _endEvent != null ) {
                EventBus.getDefault().post(_endEvent);
            }
            return false;
        }

        Vector3 pos = _initPos.cpy().interpolate(_endPos, _elapsedTime / _totalTime, Interpolation.smooth);
        Vector3 dir = _targetPos.cpy().sub(pos).nor();
        _camera.position.set(pos);
        _camera.direction.set(dir);
        _camera.up.set(0,1,0);
        _camera.update();
        return true;
    }

    @Override
    public void init() {
//        _totalTime = _endPos.cpy().sub(_camera.position).len() / _speed;
        _initDir = _camera.direction.cpy();
        _initPos = _camera.position.cpy();
        _elapsedTime = 0;
    }

    public void setEndPos(Vector3 endPos) {
        _endPos = endPos.cpy();
    }

    public void setEndEvent(Object e) {
        _endEvent = e;
    }

}
