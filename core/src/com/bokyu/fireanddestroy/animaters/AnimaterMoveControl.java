package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.bokyu.fireanddestroy.players.Player;

public class AnimaterMoveControl extends Animater{

    Vector3 _target;
    Touchpad _touchpad;
    PerspectiveCamera _camera;
    Player _player;

    public AnimaterMoveControl(PerspectiveCamera camera, Player player, Vector3 target, Touchpad touchpad) {
        _camera = camera;
        _target = target;
        _touchpad = touchpad;
        _player = player;
    }

    @Override
    public boolean animate(float interval) {
        Vector3 up = new Vector3(0,1,0);
        float rotX = _touchpad.getKnobPercentX()*2f;
        float moveZ = _touchpad.getKnobPercentY()/4f;

        _camera.rotate(up, -rotX);
        _camera.translate(_camera.direction.cpy().scl(moveZ));
        _camera.position.y = 2;

        _camera.up.x=up.x;
        _camera.up.y=up.y;
        _camera.up.z=up.z;
        _camera.update();
        return true;
    }

    @Override
    public void init() {
    }
}
