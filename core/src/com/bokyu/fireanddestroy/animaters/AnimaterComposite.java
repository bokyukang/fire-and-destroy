package com.bokyu.fireanddestroy.animaters;

public class AnimaterComposite extends Animater {

    Animater[] _animaters;
    int _curIdx = 0;
    boolean _initiated = false;

    public AnimaterComposite(Animater ...args) {
        _animaters = args;
    }

    @Override
    public boolean animate(float interval) {
        if ( _curIdx >= _animaters.length ) return false;

        if ( !_initiated )
        {
            _initiated = true;
            _animaters[_curIdx].init();
        }
        if (_animaters[_curIdx].animate(interval)) {
        } else {
            if ( _animaters[_curIdx].getJobAfter() != null )
                _animaters[_curIdx].getJobAfter().run();
            _initiated = false;
            _curIdx++;
        }
        return true;
    }

    @Override
    public void init() {
        _curIdx = 0;
        _initiated = false;
    }

}
