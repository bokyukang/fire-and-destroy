package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.bokyu.fireanddestroy.ParticleManager;

public class AnimaterFireworks extends Animater {

    final float DURATION = 3f;
    final float ADD_INTERVAL = 0.1f;

    float _remainingAddTime = 0;
    float _remainingTime = 0;

    public AnimaterFireworks() {
        init();
    }

    public void setRemainingTime(float time) {
        _remainingTime = time;
    }

    public boolean animate(float interval) {
        _remainingTime -= interval;
        _remainingAddTime -= interval;
        if ( _remainingAddTime < 0 ) {
            _remainingAddTime += ADD_INTERVAL;
            addFirework();
        }
        if ( _remainingTime < 0 )
            return false;
        return true;
    }
    public void init() {
        _remainingTime = DURATION;
    }

    private void addFirework() {
        ParticleEffect pe = ParticleManager.instance().add2DParticleEffect(ParticleManager.FIREWORK_PARTICLE);
        pe.setPosition((float)(Gdx.graphics.getWidth() * Math.random()), (float)(Gdx.graphics.getHeight() * Math.random()));
        pe.start();
    }
}
