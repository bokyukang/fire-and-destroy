package com.bokyu.fireanddestroy.animaters;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AnimaterManager {

    static private AnimaterManager _instance = null;
    private Set<Animater> _animaters         = new HashSet();
    private Lock          _animateLock       = new ReentrantLock();
    private Set<Animater> _animaterRemoveSet = new HashSet<Animater>();

    static public AnimaterManager instance() {
        if (_instance== null) {
            _instance = new AnimaterManager();
        }
        return _instance;
    }

    Set<Animater> _tobeAdded = new HashSet();
    Set<Animater> _tobeRemoved = new HashSet();

    public void addAnimater(Animater animater) {
        _animateLock.lock();
        try {
//            if ( !_animaters.contains(animater ) ) {
//                _animaters.add(animater);
//            }
            _tobeAdded.add(animater);
        } finally {
            _animateLock.unlock();
        }
    }
    public void removeAnimater(Animater animater) {
        _animateLock.lock();
        try {
            _tobeRemoved.add(animater);
//            if ( _animaters.contains(animater ) ) {
//                _animaters.remove(animater);
//            }
        } finally {
            _animateLock.unlock();
        }
    }

    public void animate(float interval) {
        _animateLock.lock();
        try {
            _animaters.addAll(_tobeAdded);
            _animaters.removeAll(_tobeRemoved);
            _tobeAdded.clear();
            _tobeRemoved.clear();
            for (Animater animater : _animaters) {
                if (animater.animate(interval) == false) {
                    if ( animater.getJobAfter() != null ) {
                        animater.getJobAfter().run();
                    }
                    _animaterRemoveSet.add(animater);
                }
            }
            for ( Animater animater : _animaterRemoveSet ) {
                _animaters.remove(animater);
            }
            _animaterRemoveSet.clear();
        } finally {
            _animateLock.unlock();
        }
    }

    public void init() {}
}
