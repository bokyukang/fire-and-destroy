package com.bokyu.fireanddestroy.animaters;

/**
 * Created by admin on 2018-08-13.
 */

public class AnimaterIdle extends Animater {

    public float _duration;
    private float _remainingTime;

    public AnimaterIdle(float duration) {
        _duration = duration;
    }

    @Override
    public void init() {
        _remainingTime = _duration;
    }

    @Override
    public boolean animate(float delta)
    {
        _remainingTime -= delta;
        if ( _remainingTime <= 0 )
            return false;

        return true;
    }

    public void stop() {
        _remainingTime = 0;
    }
}
