package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.fireanddestroy.events.EventTimeUp;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by admin on 2018-08-13.
 */

public class AnimaterFollowBall extends Animater {

    /* max follow time in seconds */
    final int FOLLOW_TIME_LIMIT = 20;
    private PerspectiveCamera _camera;
    private Matrix4 _targetTransform;
    private float _originDistLimit;
    private Vector3 _targetPosition = new Vector3();
    float _offsetFromBallBack, _offsetFromBallUp;
    Vector3 _lookAtPos;
    static final Vector3 _up = new Vector3(0,1,0);
    private float _lastDist = Float.MAX_VALUE;
    private float _startDist;
    private float _totalTime = 0;

    public AnimaterFollowBall(PerspectiveCamera camera, Vector3 lookAtPos, Matrix4 targetTransform,
             float originDistLimit, float offsetFromBallBack, float offsetFromBallUp)
    {
        _camera = camera;
        _lookAtPos = lookAtPos;
        _targetTransform = targetTransform;
        _originDistLimit = originDistLimit;
        _offsetFromBallBack = offsetFromBallBack;
        _offsetFromBallUp = offsetFromBallUp;
        EventBus.getDefault().register(this);
    }

    public void setTargetTransform(Matrix4 targetTransform) {
        _targetTransform = targetTransform;
    }

    @Override
    public void init() {
        super.init();
        _lastDist = Float.MAX_VALUE;
        _startDist = getDistToTarget();
        _totalTime = 0;
    }

    private float getDistToTarget() {
        return (float)(
                Math.sqrt(Math.pow(_lookAtPos.x - _camera.position.x,2) + Math.pow(_lookAtPos.z - _camera.position.z, 2)));
    }

    @Override
    public boolean animate(float interval )
    {
        _totalTime += interval;
        if ( _totalTime > FOLLOW_TIME_LIMIT ) return false;

        float xzDistP2 = getDistToTarget();

        if (xzDistP2  <= _originDistLimit ) {
            return false;
        } else if (xzDistP2 < _startDist && xzDistP2 > _lastDist) {
            return false;
        }
        _lastDist = xzDistP2;

        _targetTransform.getTranslation(_targetPosition);
        _camera.position.set(
                _targetPosition.cpy().add(
                    _targetPosition.cpy().sub(_lookAtPos).nor().scl(_offsetFromBallBack)
                    ).add(_up.cpy().scl(_offsetFromBallUp)));
        _camera.lookAt(_lookAtPos);
        _camera.up.set(_up);
        _camera.update();

        return true;
    }

    @Subscribe
    public void onEvent(EventTimeUp event) {
        switch ( event._code)
        {
            case AFTER_FIRE_TIME_UP:
                break;
        }
    }


}
