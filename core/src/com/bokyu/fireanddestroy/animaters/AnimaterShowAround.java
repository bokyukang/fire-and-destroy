package com.bokyu.fireanddestroy.animaters;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector3;

public class AnimaterShowAround extends Animater {

    PerspectiveCamera _camera;
    Vector3 _target;
    float _rotSpeed;
    Vector3 _offset;
    float _totalRotAngle;
    Vector3 _up = new Vector3(0,1,0);
    Vector3 _initPos = null;
    Interpolation _interpolation = new Interpolation.Pow(2);

    public AnimaterShowAround(PerspectiveCamera camear, Vector3 target, float rotSpeed) {
        _camera = camear;
        _target = target;
        _rotSpeed = rotSpeed;
    }


    @Override
    public boolean animate(float interval) {
        float amountToRot = interval * _rotSpeed;
        if ( _totalRotAngle > 360 ) {
            return false;
        }
//            _offset.rotate(_up, amountToRot);
        _totalRotAngle += amountToRot;
        float cur = _totalRotAngle / 360;
        float cur2 = _interpolation.apply(cur);
        _camera.position.set(_initPos.cpy().rotate(_up, cur2 * 360));
        _camera.direction.set(_target.cpy().sub(_camera.position).nor());
        _camera.up.set(_up);
        _camera.update();
        return true;
    }

    @Override
    public void init() {
        _totalRotAngle = 0;
        _initPos = _camera.position.cpy();
    }
}
