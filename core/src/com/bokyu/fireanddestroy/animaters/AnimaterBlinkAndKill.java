package com.bokyu.fireanddestroy.animaters;

import com.bokyu.fireanddestroy.bullet.BulletEntity;
import com.bokyu.fireanddestroy.bullet.BulletWorld;

/**
 * Created by admin on 2018-08-14.
 */

public class AnimaterBlinkAndKill extends Animater {

    int BLINK_COUNT = 10;
    float INTERVAL = .5f;

    boolean _visible = true;
    int _remainingCount = 0;
    float _intervalTime = 0;

    BulletEntity _bulletEntitry;
    BulletWorld _bulletWorld;

    public AnimaterBlinkAndKill(BulletEntity be, BulletWorld bulletWorld ) {
        _bulletEntitry = be;
        _bulletWorld = bulletWorld;
    }

    @Override
    public void init()
    {
        _visible = true;
        _remainingCount = BLINK_COUNT;
        _intervalTime = INTERVAL;
    }

    @Override
    public boolean animate(float interval)
    {
        _intervalTime -= interval;
        if ( _intervalTime <= 0 ) {
            if (!change()) {
                _bulletEntitry._visible = true;
                _bulletWorld.remove(_bulletEntitry);
                return false;
            }
        }
        return true;
    }

    private boolean change() {
        _intervalTime = INTERVAL;
        _remainingCount--;
        _visible = !_visible;
        _bulletEntitry._visible = _visible;
        if ( _remainingCount >= 0 ) return true;
        return false;
    }
}
