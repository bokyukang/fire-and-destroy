package com.bokyu.fireanddestroy.animaters;


import com.bokyu.fireanddestroy.events.EventGageFull;
import com.bokyu.fireanddestroy.ui.Gage;

import org.greenrobot.eventbus.EventBus;

import static com.bokyu.fireanddestroy.events.EventGageFull.STATE.BEGIN;
import static com.bokyu.fireanddestroy.events.EventGageFull.STATE.END;

public class AnimaterFireGage extends Animater{

    private enum STATE {
        INIT,
        FULL_BEGIN
    }
    final float FULL_DURATION = .2f;
    Gage _fireGage;
    float _maxTime;
    float _elapsedTime = 0;
    float _fullTime = 0;
    STATE _state = STATE.INIT;

    public AnimaterFireGage(Gage fireGage, float maxTime) {
        _fireGage = fireGage;
        _maxTime = maxTime;
        _fullTime = 0;
        _state = STATE.INIT;
    }

    @Override
    public boolean animate(float interval) {
        _elapsedTime += interval;
        _fireGage.setPercent((_elapsedTime / _maxTime) );
        if ( _elapsedTime >= _maxTime) {
            if ( _state == STATE.INIT ) {
                EventBus.getDefault().post(new EventGageFull(BEGIN));
                _fullTime = 0;
                _state = STATE.FULL_BEGIN;
            } else {
                _fullTime += interval;
                if ( _fullTime > FULL_DURATION ) {
                    EventBus.getDefault().post(new EventGageFull(END));
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void init() {
        _elapsedTime = 0;
    }

}
