package com.bokyu.fireanddestroy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;
import java.util.Map;

import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.BACKGROUND;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.BEGINNING;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.BUTTON;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.CANON;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.DEMOLITION;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.FAILED;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.FIRE;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.FIREWORKS;
import static com.bokyu.fireanddestroy.SoundManager.SOUND_NAMES.LEVELUP;

/**
 * Created by admin on 2018-08-22.
 */

public class SoundManager implements Disposable {

    public enum SOUND_NAMES {
        DEMOLITION,
        CANON,
        FIRE,
        BUTTON,
        FIREWORKS,
        LEVELUP,
        FAILED,
        BACKGROUND,
        BEGINNING
    }

    static private SoundManager _instance = null;
    private Map<SOUND_NAMES, Sound> _sounds = new HashMap();
    private Map<SOUND_NAMES, Music> _musics = new HashMap();

    static public SoundManager instance() {
        if ( _instance == null ) {
            _instance = new SoundManager();
        }
        return _instance;
    }

    public void init() {
        loadMusic(DEMOLITION, "sounds/demolition.mp3");
        loadMusic(FIRE, "sounds/fire.mp3");
        loadSound(CANON, "sounds/canon.mp3");
        loadSound(BUTTON, "sounds/button.mp3");
        loadSound(FIREWORKS, "sounds/fireworks.mp3");
        loadSound(LEVELUP, "sounds/levelup.mp3");
        loadSound(FAILED, "sounds/failed.mp3");
        loadMusic(BACKGROUND, "sounds/background.mp3");
        loadSound(BEGINNING, "sounds/beginning.mp3");
    }

    private void loadSound(SOUND_NAMES name, String path ) {
        Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
        _sounds.put(name, sound);
    }

    private void loadMusic(SOUND_NAMES name, String path ) {
        Music music = Gdx.audio.newMusic(Gdx.files.internal(path));
        _musics.put(name, music);
    }

    public void playSound(SOUND_NAMES name) {
        if ( _sounds.containsKey(name) ) {
            _sounds.get(name).play(1);
        }
    }

    public Music getMusic(SOUND_NAMES name) {
        return _musics.get(name);
    }

    public void dispose ()
    {
        for ( Sound sound : _sounds.values() ) {
            sound.dispose();
        }
        _sounds.clear();
    }

}
