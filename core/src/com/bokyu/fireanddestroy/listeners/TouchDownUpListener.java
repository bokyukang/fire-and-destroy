package com.bokyu.fireanddestroy.listeners;

public interface TouchDownUpListener {
    void touchDownUp(int x, int y, int pointer, boolean isDown);
}
