package com.bokyu.fireanddestroy.listeners;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.fireanddestroy.ParticleManager;
import com.bokyu.fireanddestroy.events.EventFire;
import com.bokyu.fireanddestroy.events.EventGageFull;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FireButtonListener extends ClickListener {

    public boolean _down = false;
    private Table _btn;
    private Map<String, Set<ParticleEffect>> _particles = new HashMap();
    private float _downX, _downY;
    private float _btnX, _btnY;

    public FireButtonListener(Table firebtn) {
        _btn = firebtn;
        _particles.put(ParticleManager.STAR_PARTICLE, new HashSet());
        _particles.put(ParticleManager.ARROW_PARTICLE, new HashSet());
        EventBus.getDefault().register(this);
    }


    @Override
    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        _down = true;
        _downX = x;
        _downY = y;
        _btnX = _btn.getX();
        _btnY = _btn.getY();
        EventBus.getDefault().post(new EventFire(EventFire.STAGES.START, x,y,pointer));
        addParticles();
        return true;
    }

    @Override
    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
        EventBus.getDefault().post(new EventFire(EventFire.STAGES.END, x,y,pointer));
        removeParticles();
    }

    private void addParticles() {
        ParticleEffect pe = ParticleManager.instance().add2DParticleEffect(ParticleManager.STAR_PARTICLE);
        pe.setPosition(_btn.getX() + _btn.getWidth()/2, _btn.getY() - _btn.getHeight()/2);
        pe.start();
        _particles.get(ParticleManager.STAR_PARTICLE).add(pe);
        pe = ParticleManager.instance().add2DParticleEffect(ParticleManager.ARROW_PARTICLE);
        pe.scaleEffect(3f);
        pe.setPosition(_btn.getX() + _btn.getWidth()/2, _btn.getY() );
        pe.start();
        _particles.get(ParticleManager.ARROW_PARTICLE).add(pe);
    }

    private void removeParticles() {
        for ( String name : _particles.keySet() ) {
            for (ParticleEffect epe : _particles.get(name)) {
                epe = ParticleManager.instance().remove2DParticleEffect(name, epe);
                epe.reset();
            }
            _particles.get(name).clear();
        }
    }

    @Override
    public void touchDragged (InputEvent event, float x, float y, int pointer) {
//        _btn.setPosition(_btnX + x - _downX, _btnY + y - _downY);
        EventBus.getDefault().post(new EventFire(EventFire.STAGES.DRAG, x,y,pointer));
    }

    @Subscribe
    public void onEvent(EventGageFull event) {
        if ( event._state == EventGageFull.STATE.END ) {
            removeParticles();
        }
    }

}
