package com.bokyu.fireanddestroy.listeners;

public interface TouchDragListener {
    void touchDragged(int x, int y, int pointer);
}
