package com.bokyu.fireanddestroy.listeners;

public interface RewardAdListener {
    void rewardSuccess();
    void rewardFailed();
}
