package com.bokyu.fireanddestroy.listeners;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.fireanddestroy.ParticleManager;
import com.bokyu.fireanddestroy.animaters.Animater;
import com.bokyu.fireanddestroy.animaters.AnimaterAngleControl;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.animaters.AnimaterMoveControl;
import com.bokyu.fireanddestroy.ui.Angle;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by admin on 2018-07-31.
 */

public class MyTouchpadListener extends ClickListener {

    private Touchpad _touchpad;
    private PerspectiveCamera _camera;
    private Animater _animMoveControl;
    private Set<ParticleEffect> _particles = new HashSet();

    public MyTouchpadListener(Touchpad touchpad, Angle angleUI, PerspectiveCamera camera) {
        _touchpad = touchpad;
//        _animMoveControl = new AnimaterMoveControl(camera, null,  new Vector3(0,0,0), touchpad);
        _animMoveControl = new AnimaterAngleControl(camera, 70, -10, touchpad, angleUI);

        ParticleEffect pe = ParticleManager.instance().add2DParticleEffect(ParticleManager.ARROW_UPDOWN_PARTICLE);
        float originX = _touchpad.getX() + _touchpad.getWidth()/2f;
        float originY = _touchpad.getY() + _touchpad.getHeight()/2f;
        pe.setPosition(originX, originY);
        pe.start();

        _particles.add(pe);
    }

    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        super.touchDown(event,x,y,pointer,button);
        AnimaterManager.instance().addAnimater(_animMoveControl);
        return true;
    }

    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
        super.touchUp(event, x, y, pointer, button);
        AnimaterManager.instance().removeAnimater(_animMoveControl);

    }

}
