package com.bokyu.fireanddestroy.listeners;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.fireanddestroy.ui.Angle;

public class FireDirectionDragListener implements TouchDragListener {

    private static float MAX_DEG = 90;
    private static float MAX_PIX = 900;
    private static float PART_DIST = 6f;

    private static Vector3 YAXIS = new Vector3(0,1,0);
    private static Vector3 XAXIS = new Vector3(1,0,0);
    private static Vector3 ZAXIS = new Vector3(0,0,1);
    private Vector3 _initialDir;
    private int _initX, _initY;
    private ParticleEffect _particleEffect;
    private Matrix4 _particleTransform;
    private PerspectiveCamera _camera;
    private Vector3 _initialPos;
    private Vector3 _dirVec;
    private boolean _initiated = false;
    private Angle _angleUI = null;

    public void init( PerspectiveCamera camera, ParticleEffect particleEffect , Vector3 dirVec, Angle angleUI) {
        _initiated = false;
        _angleUI = angleUI;
        _particleEffect = particleEffect;
        _camera = camera;
        _dirVec = dirVec;
        _initialDir = camera.direction.cpy();
        _initialPos = camera.position.cpy();
        _particleTransform = new Matrix4().translate(
                camera.position.cpy().add(camera.direction.cpy().scl(PART_DIST)));
        _particleEffect.setTransform(_particleTransform);
    }

    @Override
    public void touchDragged(int x, int y, int pointer){
        // 200px = 90 degrees
//        if ( pointer != _pointer ) return;

        if ( _initiated == false ) {
            _initX = x;
            _initY = y;
            _initiated = true;
        }

        int diffX = x - _initX;
        int diffY = y - _initY;

        float degX = Math.min(1, diffX / MAX_PIX) * MAX_DEG;
        float degY = Math.min(1, diffY / MAX_PIX) * MAX_DEG;

        Vector3 rightAxis = YAXIS.cpy().crs(_initialDir.cpy()).nor();
        Vector3 tmp = _initialDir.cpy().scl(PART_DIST).rotate(rightAxis, -degY);
        Vector3 axis2 = tmp.cpy().crs(YAXIS).crs(tmp).nor();
        Vector3 targetP = tmp.rotate(axis2, -degX);
        _dirVec.set(targetP);
        _angleUI.setAngle((float)Math.toDegrees(Math.asin(targetP.cpy().nor().y)));
        _particleEffect.setTransform(new Matrix4().setToTranslation(_initialPos.cpy().add(targetP)));
        _particleEffect.update();
    }
}
