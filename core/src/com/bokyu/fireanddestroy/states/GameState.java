package com.bokyu.fireanddestroy.states;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class GameState {
    public void render(float delta) {}
    public void init() {}
    public void dispose() {}
    public void onEnter() {}
    public void onExit() {}
    public Stage getStage() {return null;}
}
