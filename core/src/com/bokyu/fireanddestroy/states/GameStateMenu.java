package com.bokyu.fireanddestroy.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.bokyu.buildhouse.Mediater;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.ui.dialog.UIConfirm;
import com.bokyu.buildhouse.ui.dialog.UIDialogBuilding;
import com.bokyu.fireanddestroy.ObjectsManager;
import com.bokyu.fireanddestroy.SoundManager;
import com.bokyu.fireanddestroy.events.EventCommand;
import com.bokyu.fireanddestroy.ui.MyButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import static com.bokyu.buildhouse.ui.scene2d.ConstructScene2DManager.DIALOG_MARGIN;

/**
 * Created by admin on 2018-08-10.
 */

public class GameStateMenu extends GameState {

    static private int MARGIN = 50;
    static private GameStateMenu _instance = null;
    private boolean _initialized  =false;
    private float _nextBtnX = 0;
    private float _nextBtnY = 0;
    private Stage _stage;
    private ModelBatch _modelBatch = null;
    DirectionalLight _light;
    PerspectiveCamera _camera;
    Environment _environment;
    List<ModelInstance> _modelInstances = new ArrayList();
    Mediater _factory;
    private Music _background;
    static final Vector3 _up = new Vector3(0,1,0);
    private UIDialogBuilding _dialogBuilding = null;

    static public GameStateMenu instance() {
        if ( _instance == null ) {
            _instance = new GameStateMenu();
        }
        return _instance;
    }

    private GameStateMenu() {
        EventBus.getDefault().register(this);
    }

    public void init(Mediater factory) {
        if ( _initialized ) return;

        _factory = factory;
        _background = SoundManager.instance().getMusic(SoundManager.SOUND_NAMES.BACKGROUND);
        int half = (int)(Gdx.graphics.getWidth()/2f);
        int gap = 10;
        final float width = half - gap *2;
        final float height = Gdx.graphics.getHeight() - gap*2;

        _stage = new Stage();
        _camera = new PerspectiveCamera(67f, 3f, 3f * height / width);
        _camera.far = 100;
        _camera.position.set(0f, .7f, 1.7f);
        _camera.lookAt(0, 0.5f, 0);
        _camera.update();
        _modelBatch = new ModelBatch();
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, .3f, .3f, .3f, 1f));
        _light = new DirectionalLight();
        _light.set(1f, 1f, 1f, -.5f, -1f, -.7f);
        _environment.add(_light);

        _nextBtnX = Gdx.graphics.getWidth() - MARGIN;
        _nextBtnY = Gdx.graphics.getHeight() - MARGIN;

        createButton("start from beginning", EventButtonClick.BTN_NAMES.START_FROM_BEGINNING, "oval1");
        createButton("play next level", EventButtonClick.BTN_NAMES.PLAY_NEXT_LEVEL, "oval3");
//        createButton("open building editor", EventButtonClick.BTN_NAMES.OPEN_BUILDING_EDITOR, "oval2");
//        createButton("play custom building", EventButtonClick.BTN_NAMES.MENU_SCREEN_BUILDING_LIST, "oval2");
//        Label title = new Label("Fire and Destroy", UIManager.instance().getSkin(UIManager.SKINS.LGDXS), "sub-title", Color.WHITE);
//        title.setX(Gdx.graphics.getWidth()/2f);
//        title.setY(20f);
//        title.setFontScale(3f);
//        _stage.addActor(title);

        ModelInstance enemy = new ModelInstance(ObjectsManager.instance().getModel("enemy_character"));
        ModelInstance goodCharacter = new ModelInstance(ObjectsManager.instance().getModel("good_character"));
        enemy.transform.setToTranslation(-.5f,0,0);
        goodCharacter.transform.setToTranslation(.5f,0,0);
        _modelInstances.add(enemy);
        _modelInstances.add(goodCharacter);

        int marginWidth = (int)(DIALOG_MARGIN * _stage.getWidth());
        int maxHeight = (int)(_stage.getHeight() - marginWidth*2);
        int maxWidth = (int)(_stage.getWidth() - marginWidth*2);
        _dialogBuilding = new UIDialogBuilding(marginWidth,marginWidth,
                Math.min(maxWidth, 1200),
                Math.min(maxHeight, 700));
        _initialized = true;
    }

    @Override
    public Stage getStage() {
        return _stage;
    }

    private MyButton createButton(String text,final EventButtonClick.BTN_NAMES eventBusEvent, String skinName) {
        MyButton button = new MyButton(text,
                Gdx.graphics.getWidth() / 3f, skinName);
        button.addListener(new ClickListener() {
            boolean _working = false;
            public void clicked (InputEvent event, float x, float y) {
                if ( _working ) return;
                _working = true;
                Timer.schedule(
                        new Timer.Task() {
                            public void run() {
                                EventBus.getDefault().post(
                                        new EventButtonClick( eventBusEvent,
                                                EventButtonClick.TYPE.SELECT));
                                _working = false;
                            }
                        }, 0.7f
                );
            }
        });
        _stage.addActor(button);
        setupButtonPosition(button);
        return button;
    }

    private void setupButtonPosition(MyButton button) {
        button.setX(_nextBtnX, Align.right);
        button.setY(_nextBtnY, Align.top);
        _nextBtnY -= button.getHeight() + MARGIN;
        if ( _nextBtnY - button.getHeight() < MARGIN ) {
            _nextBtnX -= (button.getWidth()  + MARGIN);
            _nextBtnY = Gdx.graphics.getHeight() - MARGIN;
        }
    }

    @Override
    public void onEnter() {
        Gdx.input.setInputProcessor(_stage);
        _factory.getAdManager().setMenuAdVisibility(true, false);
        _background.setLooping(true);
        _background.play();
    }

    @Override
    public void onExit() {
        _background.setLooping(false);
        _factory.getAdManager().setMenuAdVisibility(false, false);
        _background.stop();
        _background.pause();
    }

    private void showBuildingListDialog() {
        _dialogBuilding.updateEntries();
        _dialogBuilding.setFillParent(true);
        _dialogBuilding.align(Align.right);
        _stage.addActor(_dialogBuilding);
    }

    int _rotTarget= 0;
    float _rotSpeed = 0;
    float _rotAmount = -1;
    int _rotDir = 1;

    @Override
    public void render(float delta) {

        int half = (int)(Gdx.graphics.getWidth()/2f);
        int gap = 10;
        Gdx.gl.glViewport(half + gap, gap, half - gap * 2, Gdx.graphics.getHeight() - gap * 2);
        Gdx.gl.glClearColor(255f/255f, 165f/255f, 0f/255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        if ( _rotAmount < 0 ) {
            _rotAmount = 360;
            _rotSpeed = ((float)Math.random() * 140f) + 30f;
            _rotTarget = (_rotTarget + 1 ) % _modelInstances.size();
            _rotDir = Math.random() > 0.5 ? 1 : -1;
        }
        float rotAmount = delta * _rotSpeed;
        _rotAmount -= rotAmount;

        _modelInstances.get(_rotTarget).transform.rotate(_up, rotAmount * _rotDir);

        _modelBatch.begin(_camera);
        _modelBatch.render(_modelInstances, _environment);
        _modelBatch.end();

        if ( _initialized ) {
            Gdx.gl.glViewport(0,0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            _stage.act(delta);
            _stage.draw();
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case PLAY_NEXT_LEVEL:
                break;
            case START_FROM_BEGINNING:
                UIConfirm.instance().show("are you sure?", "your previous records \n will be erased!", _stage,
                        new EventCommand(EventCommand.COMMAND.START_FROM_BEGINNING));
                UIConfirm.instance().align(Align.right);
                break;
            case MENU_SCREEN_BUILDING_LIST:
                showBuildingListDialog();
                break;
        }
    }

}
