package com.bokyu.fireanddestroy.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.bokyu.buildhouse.Mediater;
import com.bokyu.buildhouse.data.BallData;
import com.bokyu.buildhouse.events.EventBallFired;
import com.bokyu.buildhouse.events.EventBallFiringOver;
import com.bokyu.buildhouse.events.EventBallLoaded;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.scene2d.WorldScene2DManager;
import com.bokyu.fireanddestroy.ObjectsManager;
import com.bokyu.fireanddestroy.ParticleManager;
import com.bokyu.fireanddestroy.PlayController;
import com.bokyu.fireanddestroy.SoundManager;
import com.bokyu.fireanddestroy.TimeLimitManager;
import com.bokyu.fireanddestroy.animaters.Animater;
import com.bokyu.fireanddestroy.animaters.AnimaterComposite;
import com.bokyu.fireanddestroy.animaters.AnimaterFireGage;
import com.bokyu.fireanddestroy.animaters.AnimaterFireworks;
import com.bokyu.fireanddestroy.animaters.AnimaterFollowBall;
import com.bokyu.fireanddestroy.animaters.AnimaterIdle;
import com.bokyu.fireanddestroy.animaters.AnimaterManager;
import com.bokyu.fireanddestroy.animaters.AnimaterMoveCameraTime;
import com.bokyu.fireanddestroy.animaters.AnimaterShowAround;
import com.bokyu.fireanddestroy.bullet.BulletEntity;
import com.bokyu.fireanddestroy.bullet.BulletWorld;
import com.bokyu.fireanddestroy.events.EventChracterDead;
import com.bokyu.fireanddestroy.events.EventFire;
import com.bokyu.fireanddestroy.events.EventFireEnabled;
import com.bokyu.fireanddestroy.events.EventGageFull;
import com.bokyu.fireanddestroy.events.EventKeyDown;
import com.bokyu.fireanddestroy.events.EventLevelOver;
import com.bokyu.fireanddestroy.events.EventLevelPhase;
import com.bokyu.fireanddestroy.events.EventNoContact;
import com.bokyu.fireanddestroy.events.EventTimeUp;
import com.bokyu.fireanddestroy.jobs.Job;
import com.bokyu.fireanddestroy.jobs.RenderJob;
import com.bokyu.fireanddestroy.level.Level;
import com.bokyu.fireanddestroy.level.LevelManager;
import com.bokyu.fireanddestroy.listeners.FireDirectionDragListener;
import com.bokyu.fireanddestroy.listeners.TouchDownUpListener;
import com.bokyu.fireanddestroy.render.RenderManagerBall;
import com.bokyu.fireanddestroy.render.RenderManagerPlay;
import com.bokyu.fireanddestroy.ui.Angle;
import com.bokyu.fireanddestroy.ui.Descriptions;
import com.bokyu.fireanddestroy.ui.Gage;
import com.bokyu.fireanddestroy.ui.PlayUI;
import com.bokyu.fireanddestroy.ui.TextPopup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.badlogic.gdx.physics.bullet.collision.btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT;
import static com.bokyu.fireanddestroy.bullet.BulletWorld.BALL_FLAG;
import static com.bokyu.fireanddestroy.bullet.BulletWorld.CHARACTER_FLAG;
import static com.bokyu.fireanddestroy.events.EventGageFull.STATE.END;

public class GameStatePlay extends GameState implements TouchDownUpListener {

    static private GameStatePlay _instance;
    boolean _initialized = false;
    BulletWorld _bulletWorld;
    ArrayList<RenderJob> _renderJobs = new ArrayList();
    private boolean _ballFollowStopped = false;
    AnimaterIdle _animaterIdle = null;
    Set<Animater> _animaters = new HashSet();

    private PlayUI _ui = null;
    public PerspectiveCamera _camera;
    RenderManagerPlay _renderManagerPlay = null;
    RenderManagerBall _renderManagerBall = null;
    PlayController _controller = null;
    Gage _fireGage = null;
    FireDirectionDragListener _fireDirectionDragListener = new FireDirectionDragListener();
    Lock _renderLock = new ReentrantLock();
    private Angle _angleUI;
    AnimaterFireGage _animaterFireGage = null;
    BulletEntity _ball;
    private List<Building> _buildings = null;
    Set<BulletEntity> _addedEntities = new HashSet();
    AnimaterComposite _animaterShowTarget;
    AnimaterMoveCameraTime _animaterMoveCamera, _animaterMoveCamera2;
    AnimaterFollowBall _animaterFollowBall;
    Animater _animaterFollowBallComback;
    BulletEntity _ground;
    private Level _curLevel = null;
    private boolean _readyToPlay = false;
    Vector3 _fireDirVec = new Vector3();
    com.badlogic.gdx.graphics.g3d.particles.ParticleEffect _targetEffect;
    public Queue<BallData> _balls = new LinkedList<BallData>();
    private BallData _loadedBallData;
    private boolean _fireLoadStarted = false;
    private Mediater _mediater;
    Set<Timer.Task> _tasks = new HashSet();
    int _goodCharacCount = 0;
    AnimaterFireworks _animaterFireworks = new AnimaterFireworks();
    int _enemyCount = 0;

    final static Vector3 tmpV1 = new Vector3(), tmpV2 = new Vector3();

    public enum BALL_TYPE {

    }

    static public GameStatePlay instance() {
        if ( _instance == null ) _instance = new GameStatePlay();
        return _instance;
    }

    private GameStatePlay(){
        EventBus.getDefault().register(this);
    }

    public void  init(Mediater myfactory) {

        if ( _initialized ) return;

        _mediater = myfactory;

        Bullet.init();
        _bulletWorld = createBulletWorld();

        final float width = Gdx.graphics.getWidth();
        final float height = Gdx.graphics.getHeight();
//        if (width > height)
//            _camera = new PerspectiveCamera(67f, 3f * width / height, 3f);
//        else
            _camera = new PerspectiveCamera(67f, 3f, 3f * height / width);
        _camera.far = 100;
        _camera.position.set(0f, 2f, 15f);
        _camera.lookAt(0, 5, 0);
        _camera.update();

        _renderManagerPlay = new RenderManagerPlay();
        _renderManagerBall = new RenderManagerBall();
        _controller = new PlayController();
        _controller.addDownupListener(this);
        _fireGage = new Gage(30);
        _fireGage.setBounds(0,0,200, height);
        _animaterFireGage = new AnimaterFireGage(_fireGage, 2f);

        _angleUI = new Angle();
        _angleUI.setBounds(width - 200,height/2, 100,100);

        ParticleManager.instance().loadParticles(_camera);


        ObjectsManager.instance().init(_bulletWorld);

//        _bulletWorld.setDebugMode(btIDebugDraw.DebugDrawModes.DBG_MAX_DEBUG_DRAW_MODE );


        _renderManagerPlay.init(_bulletWorld, _camera);
        _ui = new PlayUI();
        _ui.init(this, _camera, _fireGage, _angleUI);
        _renderManagerBall.init(_ui.getFireBtnContainer(), width * .1f);

        _controller.init(WorldScene2DManager.instance(), _camera, _ui.getStage());

        _animaterMoveCamera = new AnimaterMoveCameraTime(_camera, new Vector3(0,1,0), new Vector3(), 3f);
        _animaterMoveCamera.setEndEvent(new EventLevelPhase(EventLevelPhase.PHASE.SHOW_AROUND_OVER));
        _animaterShowTarget = new AnimaterComposite(
                new AnimaterMoveCameraTime(_camera, new Vector3(0,1,0), new Vector3(0, 3, 10), 3f),
                new AnimaterShowAround(_camera, new Vector3(0,1,0), 70f),
                _animaterMoveCamera
        );

        _animaterFollowBall = new AnimaterFollowBall(_camera, new Vector3(0,1,0),
                null, 5f, 3f, 1.5f);
        _animaterIdle = new AnimaterIdle(25);
        _animaterMoveCamera2 = new AnimaterMoveCameraTime(_camera,
                new Vector3(0,1,0),null, 3f);
        _animaterMoveCamera2.setEndEvent(new EventBallFiringOver());
        _animaterFollowBallComback = new AnimaterComposite(_animaterFollowBall, _animaterIdle, _animaterMoveCamera2);

        _animaterFireworks.setJobAfter(
                new Job() {
                    public void run() {
                        WorldScene2DManager.instance().showSuccessContainer();
                    }
                }
        );

        _animaters.add(_animaterFireworks);
        _animaters.add(_animaterShowTarget);
        _animaters.add(_animaterFollowBallComback);
        _animaters.add(_animaterFireGage);

        _initialized = true;

    }
    @Override
    public void onEnter() {
        _controller.onEnter();
        _mediater.getAdManager().setMenuAdVisibility(false, true);
        _ui.onEnter();
    }

    @Override
    public void onExit() {
        _controller.onExit();
        for ( Timer.Task task : _tasks ) {
            task.cancel();
        }
        _tasks.clear();
        for ( Animater animater : _animaters ) {
            AnimaterManager.instance().removeAnimater(animater);
        }

        WorldScene2DManager.instance().onExit();

    }

    public void restart() {
        _initialized =  false;
        dispose();
        init();
    }

    public void start( final Vector3 startPos,final boolean showAround) {
        _ui.setEnemyCount(_enemyCount);
        _ui.setGoodCount(_goodCharacCount);

        if ( showAround ) {
            _animaterMoveCamera.setEndPos(startPos);
            _animaterShowTarget.init();
            AnimaterManager.instance().addAnimater(_animaterShowTarget);
        }
    }

    private float getRand() {
        return (float)Math.random() - .5f;
    }

    @Override
    public void dispose () {
        _bulletWorld.dispose();
        _bulletWorld = null;
        _renderManagerPlay.dispose();
        _renderManagerBall.dispose();
        ParticleManager.instance().dispose();
        ObjectsManager.instance().dispose();

        super.dispose();
    }

    @Override
    public void render(float delta) {
        if ( _initialized ) {

            _renderLock.lock();
            try {
                for ( RenderJob job : _renderJobs ) {
                    job.run();
                }
                _renderJobs.clear();
                _bulletWorld.update();
                _renderManagerPlay.render(delta);
                _renderManagerBall.render(delta);
                WorldScene2DManager.instance().render();
                _ui.render(delta);
            }finally {
                _renderLock.unlock();
            }
        }
    }


    public BulletWorld createBulletWorld () {
        BulletWorld bulletWorld = new BulletWorld();
        bulletWorld.init();
        return bulletWorld;
    }


    @Subscribe
    public void onEvent(EventKeyDown e) {
        if ( e._key == Input.Keys.SPACE ) {
            restart();
        }
    }


    public void addBuilding(List<Building> buildings) {
        removeAddedEntities();
        _renderLock.lock();
        _goodCharacCount = 0;
        _enemyCount = 0;
        try {
            _buildings = buildings;
            for ( Building building : buildings ) {
                building._position._x = 0;
                building._position._y = 0;
                building.updatePosition();
                for (MyGlyph glyph : building.getGlyphs()) {
                    glyph.init();
                    BuildingGlyph.TYPE type = ((BuildingGlyph) glyph).getType();
                    if (type == BuildingGlyph.TYPE.GOOD_CHARACTER) {
                        _goodCharacCount++;
                    } else if (type == BuildingGlyph.TYPE.ENEMY_CHARACTER) {
                        _enemyCount++;
                    }
                    _addedEntities.add(_bulletWorld.add((BuildingGlyph) glyph));
                }
            }
        } finally {
            _renderLock.unlock();
        }
    }


    public void removeAddedEntities() {
        _renderLock.lock();
        try {
            for (BulletEntity be : _addedEntities) {
                _bulletWorld.remove(be);
                be.dispose();
            }

            _addedEntities.clear();
            _bulletWorld.clearCharacters();
            _bulletWorld.refresh();
            if ( _ground != null ) {
                _ground.dispose();
            }
            BulletEntity ground = _bulletWorld.add("ground",0,0,0);
            _ground = ground;
            ground.body.setContactCallbackFlag(BulletWorld.GROUND_FLAG);
            ground.body.setContactCallbackFilter(0);
            ground.body.setCollisionFlags(ground.body.getCollisionFlags()|CF_KINEMATIC_OBJECT);
        } finally {
            _renderLock.unlock();
        }
    }

    public void setBalls(List<BallData> ballDatas) {
        _balls.clear();
        _balls.addAll(ballDatas);
    }

    private BallData consumeBall() {
        if ( _balls.size() == 0 ) {
            return null;
        }
        BallData data = _balls.remove();
        _ui.updateBalls();
        return data;
    }


    public boolean fire(float powerPercent) {
        removeBall();

        if ( _loadedBallData == null ) return false;

        Vector3 camPos = _camera.position;
        BulletEntity be = _bulletWorld.add(_loadedBallData.model_name, camPos.x, camPos.y, camPos.z);

        ((btRigidBody)be.body).applyImpulse(
                _fireDirVec.cpy().nor().scl(
                        _loadedBallData.weight * 30f * powerPercent),
                new Vector3());

        be.body.setContactCallbackFlag(BALL_FLAG);
        be.body.setContactCallbackFilter(CHARACTER_FLAG);
        _ball = be;

        _animaterFollowBall.setTargetTransform(_ball.transform);
        _animaterFollowBall.setJobAfter(
                new Job() {

                    @Override
                    public void run() {
                        _ballFollowStopped = true;
                    }
                }
        );
        _ballFollowStopped = false;
        _animaterMoveCamera2.setEndPos(_camera.position);
        _animaterFollowBallComback.init();
        AnimaterManager.instance().addAnimater(_animaterFollowBallComback);

        _removeBallTimer = TimeLimitManager.instance().startTimer(EventTimeUp.CODES.AFTER_FIRE_TIME_UP, 5000);
        EventBus.getDefault().post(new EventBallFired(_loadedBallData));
        SoundManager.instance().playSound(SoundManager.SOUND_NAMES.CANON);

        return true;
    }

    TimerTask _removeBallTimer = null;

    public boolean loadBall() {
        _loadedBallData = consumeBall();
        if ( _loadedBallData != null ) {
            EventBus.getDefault().post(new EventBallLoaded(_loadedBallData));
            return true;
        }
        return false;
    }

    synchronized private void removeBall() {
        if ( _ball != null ) {
            _bulletWorld.remove(_ball);
            _ball.dispose();
            _ball= null;
        }
    }

//    @Subscribe
//    public void onEvent(EventBuildingSelected e) {
//        List<Building> buildings = new ArrayList();
//        buildings.add(e._selectedBuildingData);
//        addBuilding(buildings);
//        start(_curLevel._camPosition, true);
//    }


    public void touchDownUp(int x, int y, int pointer, boolean isDown) {
    }

    private boolean startFireLoad() {
        if ( _fireLoadStarted ) return false;
        _fireLoadStarted = true;
        _animaterFireGage.init();
        AnimaterManager.instance().addAnimater(_animaterFireGage);
        com.badlogic.gdx.graphics.g3d.particles.ParticleEffect pe = ParticleManager.instance().add3DParticleEffect(ParticleManager.TARGET_PARTICLE);
        _targetEffect = pe;
        pe.start();
        _fireDirectionDragListener.init(_camera,pe, _fireDirVec, _angleUI);
        _controller.addDragListener(_fireDirectionDragListener);
        return true;
    }
    private boolean stopFireLoad() {
        if ( !_fireLoadStarted ) return false;

        AnimaterManager.instance().removeAnimater(_animaterFireGage);
        ParticleEffect pe = ParticleManager.instance().remove3DParticleEffect(_targetEffect);
        pe.reset();
        _controller.removeDragListener(_fireDirectionDragListener);

        _fireLoadStarted = false;

        return true;
    }

    public void addRenderJob(RenderJob renderJob) {
        _renderLock.lock();
        try {
            _renderJobs.add(renderJob);
        } finally {
            _renderLock.unlock();
        }
    }

    public void setCurLevel(Level level) {
        _curLevel = level;
    }

    @Override
    public Stage getStage() {
        return _ui.getStage();
    }

    public void startLevel(final int iLevel) {
        Level level = LevelManager.instance().getLevel(iLevel);
        startLevel(level);
    }

    boolean _disposePrev = false;
    Level _prevLevel;


    public void startLevel(final Level level) {
        if ( level == null ) return;

        try {
            Thread.sleep(1000);
        }catch (InterruptedException e ) {
            e.printStackTrace();
        }
        removeBall();
        _bulletWorld.stopDemolitionMusic();
        _readyToPlay = false;
        _bulletWorld.enableContactListener(false);

        if ( _curLevel != null && _curLevel._levelNo != level._levelNo ) {
            _prevLevel = _curLevel;
            _disposePrev = true;
        } else {
            _disposePrev = false;
        }
        _curLevel = level;



        addRenderJob(
                new RenderJob() {
                    @Override
                    public void run() {
                        level.load();

                        _enemyCount = level._enemyCount;
                        _goodCharacCount = level._goodCharacCount;
                        start( _curLevel._camPosition, true);
                        TextPopup.showText("level " + level._levelNo, 5f, UIManager.SKINS.ARCADE, "title", TextPopup.MOVE_TYPE.FROM_RIGHT, _ui.getStage(), 4f, null);
                        SoundManager.instance().playSound(SoundManager.SOUND_NAMES.BEGINNING);

                        _balls.clear();
                        _balls.addAll(level._balls);
                        _camera.position.set(_curLevel._camPosition);
                        _camera.update();
                        _renderManagerPlay.setBackgroundColor(level._skyColor, level._skyColor2);
                        addBuilding(level._buildings);
                        _ground.modelInstance.materials.get(0).set(level._groundTextureAttribute);
                        _ui.updateBalls();
                        EventBus.getDefault().post(new EventLevelPhase(EventLevelPhase.PHASE.START));

//                        if ( _disposePrev ) {
//                            _prevLevel.dispose();
//                        }
                    }
                }
        );
    }

    @Subscribe
    public void onEvent(EventFire event) {
        switch ( event._stage ) {
            case START:
                startFireLoad();
                break;

            case END:
                if (stopFireLoad() ) {
                    fire(_fireGage.getPercent());
                }
                break;

            case DRAG:
                _fireDirectionDragListener.touchDragged((int)event._x,(int)event._y,event._pointer);
                break;
        }
    }

    @Subscribe
    public void onEvent(EventGageFull event) {
        if ( event._state == END ) {
            stopFireLoad();
        }
    }

    @Subscribe
    public void onEvent(EventBallFiringOver event) {
        if ( _curLevel != null ) {
            Level.TEST_RESULT testResult = _curLevel.testComplete(_goodCharacCount, _enemyCount );
            if ( testResult == Level.TEST_RESULT.SUCCESS ) {
                EventBus.getDefault().post(new EventLevelOver(true));
            } else if ( testResult == Level.TEST_RESULT.FAIL ) {
                EventBus.getDefault().post(new EventLevelOver(false));
            } else if ( testResult == Level.TEST_RESULT.UNKNOWN ) {
                if (!loadBall()) {
                    EventBus.getDefault().post(new EventLevelOver(false));
                }
            }
        } else {
            if (!loadBall()) {
                EventBus.getDefault().post(new EventLevelOver(false));
            }
        }
    }

    @Subscribe
    public void onEvent(EventTimeUp eventTimeUp ) {
        switch ( eventTimeUp._code ) {
            case AFTER_FIRE_TIME_UP:
                removeBall();
                break;
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case TEST:
//                AnimaterManager.instance().addAnimater(new AnimaterFireworks());
//                startLevel(1);
                break;
            case REPLAY:
                _mediater.getAdManager().showInterstitial( null );
                startLevel(_curLevel);
                break;
        }
    }

    @Subscribe
    public void onEvent(EventChracterDead event) {
        if ( event._isGood ) {
            _goodCharacCount--;
            _ui.setGoodCount(_goodCharacCount);
        } else {
            _enemyCount--;
            _ui.setEnemyCount(_enemyCount);
        }
    }


    @Subscribe
    public void onEvent(EventFireEnabled event) {
        if ( event._enabled ) {
            if ( _balls.size() == 0 ) {
            }
            if ( _readyToPlay == false ) {
                _readyToPlay = true;
                _bulletWorld.enableContactListener(true);
                if ( _curLevel != null && _curLevel._descriptions.length > 0 ) {
                    if ( _curLevel._shownDescription == false ) {
                        Descriptions.instance().showTexts(_curLevel._descriptions);
                        _curLevel._shownDescription = true;
                    }
                }
            }
            if ( _removeBallTimer != null ) {
                _removeBallTimer.cancel();
                _removeBallTimer = null;
            }
        }
    }

    @Subscribe
    public void onEvent(EventLevelPhase event) {
        switch ( event._phase ) {
            case START:
                break;
            case SHOW_AROUND_OVER:
                loadBall();
                break;
        }
    }

    @Subscribe
    public void onEvent(EventLevelOver event) {
        if ( event._successful )
        {

            if ( LevelManager.instance().levelUp(_curLevel) ){
                TextPopup.showText("level cleared", 3f, UIManager.SKINS.COMIC, "title", TextPopup.MOVE_TYPE.FROM_RIGHT, _ui.getStage(), 4f,null);
                _animaterFireworks.setRemainingTime(5f);
            }
            else {
                TextPopup.showText("all level cleared", 10f, UIManager.SKINS.COMIC, "title", TextPopup.MOVE_TYPE.FROM_RIGHT, _ui.getStage(), 5f,null);
                _animaterFireworks.setRemainingTime(10f);
            }

            SoundManager.instance().playSound(SoundManager.SOUND_NAMES.FIREWORKS);
            SoundManager.instance().playSound(SoundManager.SOUND_NAMES.LEVELUP);

            AnimaterManager.instance().addAnimater(_animaterFireworks);

        } else {
            SoundManager.instance().playSound(SoundManager.SOUND_NAMES.FAILED);
            TextPopup.showText("mission failed", 7f, UIManager.SKINS.COMIC, "title", TextPopup.MOVE_TYPE.FROM_TOP_SLOW, _ui.getStage(), 4f,null);
            WorldScene2DManager.instance().showFaildContainer();
        }
    }

    @Subscribe
    public void onEvent(EventNoContact event)
    {
        if ( _ballFollowStopped ) {
            _animaterIdle.stop();
        }
    }
}
