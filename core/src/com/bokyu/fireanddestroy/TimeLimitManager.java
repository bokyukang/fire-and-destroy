package com.bokyu.fireanddestroy;

import com.bokyu.fireanddestroy.events.EventTimeUp;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by admin on 2018-08-13.
 */

public class TimeLimitManager {

    static private TimeLimitManager _instance = null;
    private Timer _timer;

    public class MyTimerTask extends TimerTask {
        public EventTimeUp.CODES _code;
        public MyTimerTask(EventTimeUp.CODES code) {
            _code = code;
        }
        @Override
        public void run() {
            EventBus.getDefault().post(new EventTimeUp(_code));
        }
    }

    static public TimeLimitManager instance() {
        if( _instance == null ) {
            _instance = new TimeLimitManager();
        }
        return _instance;
    }

    private TimeLimitManager() {
        _timer = new Timer();
    }

    /* amount : miliseconds */
    public TimerTask startTimer(EventTimeUp.CODES code, int delay) {
        TimerTask timerTask = new MyTimerTask(code);
        _timer.schedule(timerTask, delay);
        return timerTask;
    }

}
