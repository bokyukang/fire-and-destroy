package com.bokyu.buildhouse;

import com.bokyu.buildhouse.localdb.DbManager;

/**
 * Created by bk on 2017-02-22.
 */

public interface Mediater {
    DbManager getDbManager();
    AdManager getAdManager();
    void gameLoaded();
}
