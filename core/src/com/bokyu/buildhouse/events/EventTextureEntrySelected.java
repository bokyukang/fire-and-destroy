package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.TextureData;

/**
 * Created by bokyu on 2016-09-15.
 */
public class EventTextureEntrySelected {

    public int _selectedIdx = 0;
    public TextureData _selectedTextureData = null;

    public EventTextureEntrySelected(int selectedIdx, TextureData textureData) {
        _selectedIdx = selectedIdx;
        _selectedTextureData = textureData;
    }
}
