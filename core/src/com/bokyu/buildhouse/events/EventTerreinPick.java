package com.bokyu.buildhouse.events;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by bokyu on 2016-09-13.
 */
public class EventTerreinPick {

    public int _idxCrossX, _idxCrossY, _idxX, _idxY;
    public Vector3 _hitPointWorld, _hitPointLocal;


    public EventTerreinPick(int idxCrossX, int idxCrossY, int idxX, int idxY, Vector3 hitPointWorld, Vector3 hitPointLocal) {
        _idxCrossX = idxCrossX;
        _idxCrossY = idxCrossY;
        _idxX = idxX;
        _idxY = idxY;
        _hitPointWorld = hitPointWorld;
        _hitPointLocal = hitPointLocal;
    }
}
