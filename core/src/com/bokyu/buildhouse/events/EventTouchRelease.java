package com.bokyu.buildhouse.events;

/**
 * Created by bokyu on 2016-08-12.
 */
public class EventTouchRelease {
    public EventTouchRelease(int x, int y) {
        _x = x;
        _y = y;
    }
    public int _x;
    public int _y;
}
