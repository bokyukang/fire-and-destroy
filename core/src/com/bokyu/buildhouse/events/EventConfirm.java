package com.bokyu.buildhouse.events;

/**
 * Created by bk on 2017-01-11.
 */

public class EventConfirm {
    public enum CONFIRM_TYPE {
        DELETE_BUILDING_MODEL,
        DELETE_BUILDING,
        CONSTRUCT_CLEAR,
        DELETE_VILLAGE
    }

    public CONFIRM_TYPE _confirmType;

    public EventConfirm(CONFIRM_TYPE type) {
        _confirmType = type;
    }
}
