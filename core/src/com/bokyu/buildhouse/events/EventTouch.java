package com.bokyu.buildhouse.events;

/**
 * Created by bokyu on 2016-08-08.
 */
public class EventTouch {
    public EventTouch(int x, int y) {
        _x = x;
        _y = y;
    }
    public int _x;
    public int _y;
}
