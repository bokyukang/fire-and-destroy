package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.RoadData;

/**
 * Created by bk on 2017-02-01.
 */

public class EventRoadSelected {
    public EventRoadSelected(int selectedIdx, RoadData roadData) {
        _selectedData = roadData;
        _selectedIdx = selectedIdx;
    }
    public int _selectedIdx = 0;
    public RoadData _selectedData = null;
}
