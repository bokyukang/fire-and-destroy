package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.DecorationData;

/**
 * Created by bk on 2017-01-04.
 */

public class EventDecorationEntrySelected {

    public int _selectedIdx = 0;
    public DecorationData _selectedDecorationData = null;

    public EventDecorationEntrySelected(int selectedIdx, DecorationData decorationData) {
        _selectedIdx = selectedIdx;
        _selectedDecorationData = decorationData;
    }
}
