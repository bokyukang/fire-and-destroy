package com.bokyu.buildhouse.events;

/**
 * Created by bk on 2017-03-30.
 */

public class EventPurchaseConfirmed {
    public String _sku;

    public EventPurchaseConfirmed( String sku) {
        _sku = sku;
    }
}
