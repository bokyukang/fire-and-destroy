package com.bokyu.buildhouse.events;

/**
 * Created by bk on 2017-04-04.
 */

public class EventLoginSuccessful {
    public boolean _isLogin;
    public String _name;
    public EventLoginSuccessful(boolean isLogin, String name) {
        _isLogin = isLogin;
        _name = name;
    }
}
