package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.WallModelData;

/**
 * Created by bk on 2017-01-04.
 */

public class EventWallModelEntrySelected {

    public int _selectedIdx = 0;
    public WallModelData _selectedWallModelData = null;

    public EventWallModelEntrySelected(int selectedIdx, WallModelData wallModelData) {
        _selectedIdx = selectedIdx;
        _selectedWallModelData = wallModelData;
    }
}
