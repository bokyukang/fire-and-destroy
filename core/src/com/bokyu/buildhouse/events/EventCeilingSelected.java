package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.CeilingData;

/**
 * Created by bk on 2016-11-07.
 */

public class EventCeilingSelected {
    public EventCeilingSelected(int selectedIdx, CeilingData ceilingData) {
        _selectedIdx = selectedIdx;
        _selectedCeilingData = ceilingData;
    }
    public int _selectedIdx = 0;
    public CeilingData _selectedCeilingData = null;
}
