package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.glyph.Building;

/**
 * Created by bk on 2016-12-19.
 */

public class EventBuildingSelected {
    public EventBuildingSelected(int selectedIdx, Building buildingData) {
        _selectedIdx = selectedIdx;
        _selectedBuildingData = buildingData;
    }
    public int _selectedIdx = 0;
    public Building _selectedBuildingData = null;
}
