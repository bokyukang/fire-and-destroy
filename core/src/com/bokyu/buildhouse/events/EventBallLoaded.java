package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.BallData;

/**
 * Created by admin on 2018-08-16.
 */

public class EventBallLoaded {
    public BallData _ballData;
    public EventBallLoaded(BallData ballData) {
        _ballData = ballData;
    }
}
