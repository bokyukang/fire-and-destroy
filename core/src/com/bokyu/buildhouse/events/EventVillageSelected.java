package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.Village;

/**
 * Created by bk on 2017-02-03.
 */

public class EventVillageSelected {
    public EventVillageSelected(int idx, Village data) {
        _idx = idx;
        _data = data;
    }

    public int _idx;
    public Village _data;
}
