package com.bokyu.buildhouse.events;

/**
 * Created by bokyu on 2016-08-11.
 */
public class EventButtonClick {
    public enum BTN_NAMES
    {
        NO_AD,
        LOGIN_GOOGLE,
        COVER,
        COVER_LEFT,
//        COVER_UP,
//        COVER_DOWN,
//        COVER_RIGHT,
        WALL_BACK_LEFT,
//        WALL_BACK_RIGHT,
//        WALL_FRONT_LEFT,
//        WALL_FRONT_RIGHT,
        WALL_HORIZONTAL,
        DECORATION,
//        WALL_VERTICAL,
        ROOF,
        CONSTRUCT_SCREEN_CHECK,
        WORLD_SCREEN_CHECK,
        ROTATE,
        GENERATE_TERREIN,
        SAVE_TERREIN,
        SAVE_VILLAGE_NETWORK,
        CONSTRUCT_SCREEN_ORIENTATION,
        CONSTRUCT_SCREEN_TEXTURE_TYPE,
//        CONSTRUCT_SCREEN_CEILING_TYPE,
        CONSTRUCT_SCREEN_DESTROY_ALL,
        CONSTRUCT_SCREEN_SAVE_BUILDING,
        CONSTRUCT_SCREEN_BUILDING_LIST,
        CONSTRUCT_SCREEN_DECO_LIST,
        CONSTRUCT_SCREEN_ROOF_LIST,
        CONSTRUCT_SCREEN_CEILING_LIST,
        LAND_UP,
        LAND_DOWN,
        BUILDING_INSERT_MODE,
        ROAD_INSERT_MODE,
        SELECT_MODE,
        OPEN_BUILDING_EDITOR,
        SCREEN_GO_BACK,
        NETWORK_WORLD_GO_BACK,
        RECOMMENDATION,
        RECOMMENDATION_INVERSE,
        CONSTRUCT_SCREEN_WALL_TYPE,
        DESTROY,
        UPDOWN,
        ADD_VILLAGE,
        REMOVE_VILLAGE,
        RENAME_VILLAGE,
        WORLD_SCREEN_BUILDING_LIST,
        MENU_SCREEN_BUILDING_LIST,
        WORLD_SCREEN_ROAD_LIST,
        RANKING,
        HELP,
        SHARE,
        ENEMY_CHARACTER,
        GOOD_CHARACTER,
        START_FROM_BEGINNING,
        PLAY_NEXT_LEVEL,
        REPLAY,
        TEST
    }
    public enum TYPE {
        TOGGLE,
        RADIO,
        SELECT
    }


    public EventButtonClick(BTN_NAMES name , TYPE type) {
        _activated = true;
        _type = type;
        _name = name;
    }

    public BTN_NAMES _name;
    public boolean _activated = false;
    public TYPE _type;
}
