package com.bokyu.buildhouse.events;

import com.bokyu.buildhouse.data.RoofModelData;

/**
 * Created by bk on 2017-01-05.
 */

public class EventRoofModelEntrySelected {
    public int _selectedIdx = 0;
    public RoofModelData _selectedRoofModelData = null;

    public EventRoofModelEntrySelected(int selectedIdx, RoofModelData roofModelData) {
        _selectedIdx = selectedIdx;
        _selectedRoofModelData = roofModelData;
    }
}
