package com.bokyu.buildhouse.screen;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.input.CameraProcessor;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.fireanddestroy.states.GameState;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by bokyu on 2016-08-11.
 */
public class MyScreen extends GameState {

    protected Set<UIGlyph> _uiGlyphs = null;
    protected CameraProcessor _cameraInputController = null;
    protected Set<MyGlyph> _modelGlyphs = null;
    public boolean _isVisible = false;

    protected MyScreen() {
        _uiGlyphs = new HashSet();
        _modelGlyphs = new HashSet();
    }

    @Override
    public void onEnter() {
        _isVisible = true;
    }

    @Override
    public void onExit() {
        _isVisible = false;
    }

    public CameraProcessor getCameraProcessor() { return _cameraInputController; }

    public boolean getIsVisible() { return _isVisible; }

    public Stage getSceneStage() { return null; }

}
