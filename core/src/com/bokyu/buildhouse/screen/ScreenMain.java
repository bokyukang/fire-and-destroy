package com.bokyu.buildhouse.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.BHInputProcessor;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventInitialized;
import com.bokyu.buildhouse.events.EventVillageSelected;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.ui.scene2d.MainScene2DManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-02-02.
 */

public class ScreenMain extends MyScreen {
    static private ScreenMain _instance = null;
    private Terrein _terrein = null;
    protected PerspectiveCamera _perspectiveCam         = null;
    private Environment _environment            = null;
    private boolean _loaded = false;

    public static ScreenMain instance() {
         if ( _instance == null ) {
             _instance = new ScreenMain();
         }
        return _instance;
    }

    private ScreenMain() {
        super();
        EventBus.getDefault().register(this);
        MainScene2DManager.instance().init();
        _terrein = new Terrein(_perspectiveCam);
        _terrein.init();
        _modelGlyphs.add(_terrein);

        /* camera */
        _perspectiveCam = new PerspectiveCamera(67,  Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        _perspectiveCam.position.set(0f, 10f, 10f);
        _perspectiveCam.lookAt(0,0,3f);
        _perspectiveCam.near = 1f;
        _perspectiveCam.far = 300f;
        _perspectiveCam.update();

        /* environment */
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        _environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
    }

    @Override
    public void onEnter() {
        super.onEnter();
        BHInputProcessor.instance()
                .addInputProcessor(MainScene2DManager.instance().getStage());
        Configuration.instance().setCurStage(MainScene2DManager.instance().getStage());
        if ( !_loaded ) {
            _loaded = true;
            EventBus.getDefault().post(new EventInitialized());
        }
    }

    @Override
    public void onExit() {
        super.onExit();
        BHInputProcessor.instance()
                .removeInputProcessor(MainScene2DManager.instance().getStage());
    }

    @Override
    public Stage getSceneStage() {
        return MainScene2DManager.instance().getStage();
    }

    public void render(SpriteBatch spriteBatch,
                       ShapeRenderer shapeRenderer,
                       ModelBatch modelBatch
    )
    {
        long delta = RenderManager.instance().getDelta();
        _perspectiveCam.rotateAround(new Vector3(0,0,0), new Vector3(0,1,0), (float)delta / 100f);
        _perspectiveCam.update();
        modelBatch.begin(_perspectiveCam);
        for (MyGlyph glyph : _modelGlyphs) {
            glyph.render(modelBatch, _environment);
        }
        modelBatch.end();
        MainScene2DManager.instance().render();
    }

    @Subscribe
    public void onEvent(EventVillageSelected event) {
        Account.instance().setVillage(event._data);
        event._data.loadVillage(ScreenWorld.MODE.USER);
        RenderManager.instance().pushScreen(ScreenWorld.instance());
    }

}
