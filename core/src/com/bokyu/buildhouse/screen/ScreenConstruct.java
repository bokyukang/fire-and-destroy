package com.bokyu.buildhouse.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.BHInputProcessor;
import com.bokyu.buildhouse.data.DecorationData;
import com.bokyu.buildhouse.data.RoofModelData;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.data.WallModelData;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventConfirm;
import com.bokyu.buildhouse.events.EventDecorationEntrySelected;
import com.bokyu.buildhouse.events.EventDesignGroundPick;
import com.bokyu.buildhouse.events.EventRoofModelEntrySelected;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.events.EventTouchRelease;
import com.bokyu.buildhouse.events.EventWallModelEntrySelected;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.injecter.CeilingInjecter;
import com.bokyu.buildhouse.injecter.ConstructInjecter;
import com.bokyu.buildhouse.injecter.DecorationInjecter;
import com.bokyu.buildhouse.injecter.EnemyInjecter;
import com.bokyu.buildhouse.injecter.GoodCharacInjecter;
import com.bokyu.buildhouse.injecter.RoofInjecter;
import com.bokyu.buildhouse.injecter.WallInjecter;
import com.bokyu.buildhouse.input.CameraProcessor;
import com.bokyu.buildhouse.ui.UICompas;
import com.bokyu.buildhouse.ui.dialog.ISaveName;
import com.bokyu.buildhouse.ui.dialog.UIAlert;
import com.bokyu.buildhouse.ui.dialog.UIConfirm;
import com.bokyu.buildhouse.ui.dialog.UIDialogEnterName;
import com.bokyu.buildhouse.ui.scene2d.ConstructScene2DManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by bokyu on 2016-08-11.
 */
public class ScreenConstruct extends MyScreen implements ISaveName {

    static public float COMPAS_WIDTH_RATE = 0.05f;
    public int _selIdxX = 0;
    public int _selIdxY = 0;
    public Constants.ORIENTATION _orientation = Constants.ORIENTATION.HORIZONTAL;
    public Constants.DIRECTION _direction = Constants.DIRECTION.NORTH;

    private ConstructInjecter       _curInjecter            = null;
    private Map<String, ConstructInjecter>   _injecterMap            = new HashMap();
    private UICompas                _uiCompas                = null;
    protected PerspectiveCamera _perspectiveCam         = null;
    private Environment _environment            = null;
    static private ScreenConstruct _instance                = null;
    private DesignGround            _designGround           = null;
    private EventButtonClick.BTN_NAMES _selectedGlyphBtn = null;
    private Set<Building> _buildings = null;
    public boolean _deleteMode = false;
    public TextureData _selectedTextureData = null;
    public WallModelData _selectedWallModelData = null;
    public DecorationData _selectedDecorationData = null;
    public RoofModelData _selectedRoofModelData = null;

    public ModelBatch _modelBatch;
    public SpriteBatch _spriteBatch;

    static public ScreenConstruct instance() {
        if ( _instance == null ) {
            _instance = new ScreenConstruct();
        }
        return _instance;
    }


    private ScreenConstruct() {
        super();

        _modelBatch = new ModelBatch();
        _spriteBatch = new SpriteBatch();

        int width = (int)(Gdx.graphics.getWidth() * COMPAS_WIDTH_RATE);
        _uiCompas = new UICompas(Gdx.graphics.getWidth() - width - width/2,
                Gdx.graphics.getHeight() - width - width/2,
                width,
                width);
        _buildings = new HashSet<Building>();

        EventBus.getDefault().register(this);

        /* camera */
        _perspectiveCam = new PerspectiveCamera(67,  Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        _perspectiveCam.position.set(0f, 10f, 10f);
        _perspectiveCam.lookAt(0,0,0);
        _perspectiveCam.near = 1f;
        _perspectiveCam.far = 300f;
        _perspectiveCam.update();

        /* add terrein */
        _designGround = new DesignGround(_perspectiveCam);
        _designGround.init();
        _modelGlyphs.add(_designGround);

        putInjecter(new WallInjecter(_designGround));
        putInjecter(new CeilingInjecter(_designGround));
        putInjecter(new RoofInjecter(_designGround));
        putInjecter(new EnemyInjecter(_designGround));
        putInjecter(new GoodCharacInjecter(_designGround));
        putInjecter(new DecorationInjecter(_designGround));

        /* environment */
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        _environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        _environment.add(new DirectionalLight().set(0.3f, 0.3f, 0.3f, .2f, -0.8f, 1f));


        _cameraInputController = new CameraProcessor(_perspectiveCam, 100);
        BHInputProcessor.instance()
                .addInputProcessor(_cameraInputController );
        BHInputProcessor.instance()
                .addInputProcessor(ConstructScene2DManager.instance().getStage());
    }


    private void putInjecter(ConstructInjecter injecter) {
        _injecterMap.put(injecter.getName(), injecter);
    }

    public DesignGround getDesignGround() { return _designGround; }

    @Override
    public void onEnter() {
        super.onEnter();
        BHInputProcessor.instance().onEnter();
        Configuration.instance().setCurStage(ConstructScene2DManager.instance().getStage());
    }
    @Override
    public void onExit() {
        super.onExit();
        BHInputProcessor.instance().onExit();
    }

    protected void beginRender () {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClearColor(250f/255f, 206f/255f, 135f/255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void render(float delta ) {

        beginRender();

        /* 3d */
        _modelBatch.begin(_perspectiveCam);
        for ( MyGlyph glyph : _modelGlyphs) {
            glyph.render(_modelBatch, _environment);
        }
        if ( _curInjecter != null ) {
            _curInjecter.render(_modelBatch, _environment);
        }
        _modelBatch.end();

        _spriteBatch.begin();
        // compas
        Vector3 camDir = _perspectiveCam.direction;
        float camRotRad = (float)Math.atan2( camDir.x, -camDir.z);
        float camRotDeg = camRotRad * 180f / (float)Math.PI;
        _uiCompas.setRotation(camRotDeg);
        _uiCompas.render(_spriteBatch);
        _spriteBatch.end();

        ConstructScene2DManager.instance().render(delta);

    }

    protected boolean setInjecter(String name) {
        if ( _injecterMap.containsKey(name)) {
            if (_curInjecter != null) {

                if (_curInjecter.getName().equals(name)) return false;
                if (_curInjecter.getName() != name) {
                    _curInjecter.onExit();
                }
            }
            if (_injecterMap.containsKey(name)) {
                _curInjecter = _injecterMap.get(name);
            _curInjecter.onEnter();
            } else {
                _curInjecter = null;
            }
            return true;
        }
        return false;
    }

    private Constants.DIRECTION getDirection(EventButtonClick.BTN_NAMES btnName) {
        Vector3 dir = _perspectiveCam.direction.cpy();
        switch ( btnName ) {
            case COVER:
                return Constants.DIRECTION.EAST;
//                return MyHelper.getDirectionDiagonal(dir);
            case COVER_LEFT:
                dir.rotate(90 * 3, 0 , -1, 0);
                return MyHelper.getDirectionDiagonal(dir);
//            case COVER_RIGHT:
//                dir.rotate(90 , 0 , -1, 0);
//                return MyHelper.getDirectionDiagonal(dir);
//            case COVER_DOWN:
//                dir.rotate(180 , 0 , -1, 0);
//                return MyHelper.getDirectionDiagonal(dir);
//            case WALL_BACK_RIGHT:
//                dir.rotate(45, 0, -1, 0);
//                return MyHelper.getDirection(dir);
//            case WALL_FRONT_RIGHT:
//                dir.rotate(45 + 90, 0, -1, 0);
//                return MyHelper.getDirection(dir);
//            case WALL_FRONT_LEFT:
//                dir.rotate(45 + 90 * 2, 0, -1, 0);
//                return MyHelper.getDirection(dir);
            case WALL_BACK_LEFT:
                dir.rotate(45 + 90 * 3, 0, -1, 0);
                return MyHelper.getDirection(dir);
            case WALL_HORIZONTAL:
                return MyHelper.getDirectionDiagonal(dir);
//            case WALL_VERTICAL:
//                dir.rotate( 90 , 0, -1, 0);
//                return MyHelper.getDirectionDiagonal(dir);
            default:

        }
        return null;
    }

    @Subscribe
    public void onEvent(EventButtonClick event)
    {
        if ( event._type == EventButtonClick.TYPE.SELECT && event._activated == false ) return;

        switch ( event._name ) {
            case COVER:
                _selectedGlyphBtn = event._name;
                setInjecter(CeilingInjecter.class.getName());
                _direction = getDirection(_selectedGlyphBtn);
                _curInjecter.update();
                break;
            case COVER_LEFT:
//            case COVER_RIGHT:
//            case COVER_UP:
//            case COVER_DOWN:
                _selectedGlyphBtn = event._name;
                setInjecter(CeilingInjecter.class.getName());
                _direction = getDirection(_selectedGlyphBtn);
                _curInjecter.update();
                break;
            case WALL_BACK_LEFT:
//            case WALL_BACK_RIGHT:
//            case WALL_FRONT_LEFT:
//            case WALL_FRONT_RIGHT:
            case WALL_HORIZONTAL:
//            case WALL_VERTICAL:
                _selectedGlyphBtn = event._name;
                setInjecter(WallInjecter.class.getName());
                _direction = getDirection(_selectedGlyphBtn);
                _curInjecter.update();
                break;
            case DECORATION:
                _selectedGlyphBtn = event._name;
                setInjecter(DecorationInjecter.class.getName());
                _curInjecter.update();
                break;
            case ENEMY_CHARACTER:
                _selectedGlyphBtn = event._name;
                setInjecter(EnemyInjecter.class.getName());
                _direction = Constants.DIRECTION.NORTH;
                _curInjecter.update();
                break;
            case GOOD_CHARACTER:
                _selectedGlyphBtn = event._name;
                setInjecter(GoodCharacInjecter.class.getName());
                _direction = Constants.DIRECTION.NORTH;
                _curInjecter.update();
                break;
            case ROOF:
                _selectedGlyphBtn = event._name;
                setInjecter(RoofInjecter.class.getName());
                _direction = Constants.DIRECTION.NORTH;
                _curInjecter.update();
                break;
            case CONSTRUCT_SCREEN_CHECK:
                if ( _curInjecter != null ) _curInjecter.insert();
                break;
            case CONSTRUCT_SCREEN_ORIENTATION:
                _orientation = (event._activated ? Constants.ORIENTATION.VERTICAL : Constants.ORIENTATION.HORIZONTAL);
                break;
            case CONSTRUCT_SCREEN_SAVE_BUILDING:
                saveBuilding();
                break;
            case CONSTRUCT_SCREEN_DESTROY_ALL:
                destroyAll();
                break;
            case CONSTRUCT_SCREEN_WALL_TYPE:
                if ( event._activated ) {
//                    setInjecter(WallModelInjecter.class.getName());
                }
                break;
            case DESTROY:
                if ( _isVisible ) {
                    _deleteMode = event._activated;
                    if (_curInjecter != null) _curInjecter.update();
                }
                break;
            case ROTATE:
                _direction = MyHelper.getRotationDir(_direction);
                if ( _curInjecter != null ) _curInjecter.update();
                break;
            default:
        }
        if ( _curInjecter != null ) {
            _curInjecter.onButtonClick(event);
        }
        Gdx.app.debug("event", "button clicked : " + event._name);
    }

    @Subscribe
    public void onEvent(EventDesignGroundPick event) {

        _selIdxX = event._idxX;
        _selIdxY = event._idxY;

        if ( _curInjecter != null ) {
            _curInjecter.update();
        }
    }

    @Subscribe
    public void onEvent(EventTouchRelease event) {
//        if ( _selectedGlyphBtn != null ) {
//            Constants.DIRECTION newDirection = getDirection(_selectedGlyphBtn);
//            if ( newDirection != null ) {
//                _direction = newDirection;
//                _curInjecter.update();
//            }
//        }
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event) {
        _selectedTextureData = event._selectedTextureData;
    }

    @Subscribe
    public void onEvent(EventWallModelEntrySelected event)
    {
        _selectedWallModelData = event._selectedWallModelData;
    }

    @Subscribe
    public void onEvent(EventRoofModelEntrySelected event)
    {
        _selectedRoofModelData = event._selectedRoofModelData;
    }

    @Subscribe
    public void onEvent(EventDecorationEntrySelected event)
    {
        _selectedDecorationData = event._selectedDecorationData;
    }

    @Subscribe
    public void onEvent(EventConfirm event)
    {
        switch ( event._confirmType )
        {
            case CONSTRUCT_CLEAR:
                _designGround.clearAll();
                break;
        }
    }

    public void destroyAll()
    {
        UIConfirm dialog = UIConfirm.instance();
        dialog.show("confirm", "are you sure you want erase all?", ConstructScene2DManager.instance().getStage(),
                new EventConfirm(EventConfirm.CONFIRM_TYPE.CONSTRUCT_CLEAR));
    }

    public void saveBuilding() {
        UIDialogEnterName dialog = new UIDialogEnterName(this);
        dialog.setFillParent(true);
        dialog.align(Align.right);
        ConstructScene2DManager.instance().getStage().addActor(dialog);
    }

    @Override
    public Stage getSceneStage() {
        return ConstructScene2DManager.instance().getStage();
    }

    @Override
    public void saveName(String name) {
        Building building = _designGround.createBuilding();
        _designGround.clearAll();

        building.setName(name);

        if ( Configuration.instance().getFactory().getDbManager().saveBuildingModel(building) )
        {
            Gdx.app.log("saveName", "db successful");
            BuildingsManager.instance().addBuilding(building);
            BuildingsManager.instance().updateBuildingKeys();
            ConstructScene2DManager.instance().updateBuildingsDialog();
            UIAlert.instance().show("save", "building saved", ConstructScene2DManager.instance().getStage());
        } else {
            Gdx.app.log("saveName", "db failed");
        }

        /* network */
//        NetworkJob networkJob = new NetworkJobSaveVillage( building);
//        if ( NetworkManager.instance().addJob(networkJob) ) {
//            Gdx.app.log("saveName", "network successful");
//        } else {
//            Gdx.app.log("saveName", "network failed");
//        }

    }

}
