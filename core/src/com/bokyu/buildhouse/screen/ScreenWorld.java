package com.bokyu.buildhouse.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.BHInputProcessor;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventBuildingSelected;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventRoadSelected;
import com.bokyu.buildhouse.events.EventTerreinPick;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.injecter.BuildingInjecter;
import com.bokyu.buildhouse.injecter.EnemyInjecter;
import com.bokyu.buildhouse.injecter.Injecter;
import com.bokyu.buildhouse.injecter.RoadInjecter;
import com.bokyu.buildhouse.injecter.SelectInjecter;
import com.bokyu.buildhouse.injecter.WorldInjecter;
import com.bokyu.buildhouse.input.CameraProcessor;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.network.jobs.NetworkJobRecommend;
import com.bokyu.buildhouse.network.jobs.NetworkJobSaveVillage;
import com.bokyu.buildhouse.ui.scene2d.Scene2DManager;
import com.bokyu.buildhouse.ui.scene2d.WorldNetworkScene2DManager;
import com.bokyu.buildhouse.ui.scene2d.WorldScene2DManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by bokyu on 2016-09-13.
 */
public class ScreenWorld extends MyScreen {

    public enum MODE {
        USER,
        NETWORK
    }

    private MODE _curMode = MODE.USER;

    private Terrein _terrein = null;

    static private ScreenWorld _instance = null;
    protected PerspectiveCamera _perspectiveCam         = null;
    private Environment _environment            = null;
    private Injecter _curInjecter            = null;
    public int _selIdxCrossX = 0;
    public int _selIdxCrossY = 0;
    public int _selIdxX = 0;
    public int _selIdxY = 0;
    private Map<String, WorldInjecter> _injecterMap            = new HashMap();
    private Scene2DManager _scene2DManager = null;
    public boolean _deleteMode = false;
    public Building _selectedBuilding = null;
    public RoadData _selectedRoadData = null;
    public Village _curLoadedVillage = null;
    private List<Runnable> _renderJobs = new ArrayList();
    public ModelBatch _modelBatch;
    public SpriteBatch _spriteBatch;

    /* temp */
    private ModelInstance _sphere = null;

    static public ScreenWorld instance() {
        if ( _instance == null ) {
            _instance = new ScreenWorld();
        }
        return _instance;
    }

    private ScreenWorld() {
        super();

        _modelBatch = new ModelBatch();
        _spriteBatch = new SpriteBatch();

        EventBus.getDefault().register(this);
        _scene2DManager = WorldScene2DManager.instance();

        /* camera */
        _perspectiveCam = new PerspectiveCamera(67,  Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        _perspectiveCam.position.set(0f, 10f, 10f);
        _perspectiveCam.lookAt(0,0,0);
        _perspectiveCam.near = 1f;
        _perspectiveCam.far = 300f;
        _perspectiveCam.update();

        /* add terrein */
        _terrein = new Terrein(_perspectiveCam);
        _terrein.init();
        _modelGlyphs.add(_terrein);

        putInjecter(new BuildingInjecter(_terrein));
        putInjecter(new RoadInjecter(_terrein));
        putInjecter(new SelectInjecter(_terrein));

        /* environment */
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        _environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        /* temp */
        ModelBuilder modelBuilder = new ModelBuilder();
        Model sphereModel = modelBuilder.createSphere(.5f,.5f,.5f,30,30,
                new Material(new ColorAttribute(ColorAttribute.Diffuse, 1, 0, 0, 1)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal
                );
        _sphere = new ModelInstance(sphereModel);
    }

    public void init() {
    }

    public MODE setMode(MODE newMode) {
        MODE prevMode = _curMode;
        _curMode = newMode;
        Scene2DManager newManager = null;
        if ( newMode == MODE.NETWORK ) {
            newManager = WorldNetworkScene2DManager.instance();
        } else if ( newMode == MODE.USER ) {
            newManager = WorldScene2DManager.instance();
        }

        BHInputProcessor.instance()
                .removeInputProcessor(_scene2DManager.getStage());
        BHInputProcessor.instance()
                .addInputProcessor(newManager.getStage());
        _scene2DManager = newManager;
        return prevMode;
    }

    public Terrein getTerrein() { return _terrein; }

    public void loadFromDb() {
        _terrein.loadFromDb();
        Set<Building> buildings = Configuration.instance().getFactory().getDbManager().loadBuildings();
        for ( Building b : buildings ) {
            _terrein.insertBuilding(b._position._x, b._position._y, b);
        }
    }

    @Override
    public Stage getSceneStage() {
        return _scene2DManager.getStage();
    }

    @Override
    public void onEnter() {
        super.onEnter();
        _cameraInputController = new CameraProcessor(_perspectiveCam, 100);
        BHInputProcessor.instance()
                .addInputProcessor(_cameraInputController );
        BHInputProcessor.instance()
                .addInputProcessor(_scene2DManager.getStage());
        Configuration.instance().setCurStage(_scene2DManager.getStage());
    }

    @Override
    public void onExit() {
        super.onExit();
        BHInputProcessor.instance()
                .removeInputProcessor(_cameraInputController );
        BHInputProcessor.instance()
                .removeInputProcessor(_scene2DManager.getStage());
    }

    public void addRenderJob(Runnable runnable) {
        synchronized(_renderJobSync) {
            _renderJobs.add(runnable);
        }
    }

    private void putInjecter(WorldInjecter injecter) {
        _injecterMap.put(injecter.getName(), injecter);
    }

    protected boolean setInjecter(String name) {
        if ( _injecterMap.containsKey(name)) {
            if ( _curInjecter != null ) {

                if ( _curInjecter.getName().equals(name)) return false;
                else {
                    _curInjecter.onExit();
                }
            }
            if ( _injecterMap.containsKey(name )) {
                _curInjecter = _injecterMap.get(name);
                _curInjecter.onEnter();
            } else {
                _curInjecter = null;
            }
            return true;
        }
        return false;
    }

    protected Lock _lock = new ReentrantLock();

    public void lock() {
        _lock.lock();
    }

    public void unLock() {
        _lock.unlock();
    }

    Object _renderJobSync = new Object();
    private void doRenderJobs() {
        synchronized (_renderJobSync) {
            for (Runnable runnable : _renderJobs ) {
                runnable.run();
            }
            _renderJobs.clear();
        }
    }

    @Override
    public void render(float delta ) {

        lock();
        try {
            doRenderJobs();
        /* 3d */
            _modelBatch.begin(_perspectiveCam);
            for (MyGlyph glyph : _modelGlyphs) {
                glyph.render(_modelBatch, _environment);
            }
            if (_curInjecter != null) {
                _curInjecter.render(_modelBatch, _environment);
            }
            _modelBatch.render(_sphere, _environment);
            _modelBatch.end();


            _spriteBatch.begin();
            _spriteBatch.end();

            _scene2DManager.render();
        } finally {
            unLock();
        }

    }

    @Subscribe
    public void onEvent(EventTerreinPick event) {
        _selIdxCrossX = event._idxCrossX;
        _selIdxCrossY = event._idxCrossY;
        _sphere.transform.setToTranslation(
                _terrein.getWorldPosition(_selIdxCrossX, _selIdxCrossY));
        if ( _curInjecter != null ) {
            _curInjecter.update();
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event){
        switch (event._name) {
            case LAND_UP:
                _terrein.modifyLevel(1, _selIdxCrossX, _selIdxCrossY, true);
                _sphere.transform.setToTranslation(
                        _terrein.getWorldPosition(_selIdxCrossX, _selIdxCrossY));
                break;
            case LAND_DOWN:
                _terrein.modifyLevel(-1, _selIdxCrossX, _selIdxCrossY, true);
                _sphere.transform.setToTranslation(
                        _terrein.getWorldPosition(_selIdxCrossX, _selIdxCrossY));
                break;
            case OPEN_BUILDING_EDITOR:
                RenderManager.instance().pushScreen(
                        ScreenConstruct.instance()
                );
                break;
            case BUILDING_INSERT_MODE:
                if ( event._activated )
                    setInjecter(BuildingInjecter.class.getName());
                break;
            case ROAD_INSERT_MODE:
                if ( event._activated )
                    setInjecter(RoadInjecter.class.getName());
                break;
            case SELECT_MODE:
                if ( event._activated )
                {
                    setInjecter(SelectInjecter.class.getName());
                }
                break;
            case WORLD_SCREEN_CHECK:
                if ( _curInjecter != null) _curInjecter.insert();
                break;
            case SAVE_VILLAGE_NETWORK:
                NetworkJobSaveVillage nJ = new NetworkJobSaveVillage();
                NetworkManager.instance().addJob(nJ);
                break;
            case NETWORK_WORLD_GO_BACK:
                Village village = Account.instance().getVillage();
                if ( village != null ) {
                    village.loadVillage(MODE.USER);
                }
                setMode(MODE.USER);
                break;
            case RECOMMENDATION:
                if ( _curLoadedVillage != null ) {
                    NetworkManager.instance().addJob(
                            new NetworkJobRecommend(_curLoadedVillage._accountId, _curLoadedVillage._id, true)
                    );
                }  else {
                }
                break;
            case RECOMMENDATION_INVERSE:
                if ( _curLoadedVillage != null ) {
                    NetworkManager.instance().addJob(
                            new NetworkJobRecommend(_curLoadedVillage._accountId, _curLoadedVillage._id, false)
                    );
                }
                break;
        }
    }

    public Actor hit(float x, float y) {
        return _scene2DManager.hit(x,y);
    }

    @Subscribe
    public void onEvent(EventBuildingSelected event)
    {
         _selectedBuilding = event._selectedBuildingData;
    }

    @Subscribe
    public void onEvent(EventRoadSelected event) {
        _selectedRoadData = event._selectedData;
    }

}
