package com.bokyu.buildhouse;

import com.bokyu.buildhouse.data.GlyphDbData;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.data.RoadDataContainer;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.network.jobs.NetworkJobLoadVillage;
import com.bokyu.buildhouse.screen.ScreenWorld;

import java.util.Set;

/**
 * Created by bk on 2017-02-02.
 */

public class Village {
    public Village(int id , String name) {
        _id = id;
        _name = name;
    }
    public int _id = -1;
    public String _accountId = "";
    public String _accountName = "";
    public String _name = "";
    public int _recomCount = 0;


    public void loadVillage(ScreenWorld.MODE afterMode) {
        if ( _accountId == "" ) { loadFromDb(); }
        else {
            loadFromNetwork(afterMode);
        }

    }

    private void loadFromNetwork(ScreenWorld.MODE afterMode) {
        NetworkManager.instance().addJob( new NetworkJobLoadVillage(this, _accountId, _id, afterMode));
    }

    private void loadFromDb() {
        DbManager dbManager = Configuration.instance().getFactory().getDbManager();

        Terrein terrein = ScreenWorld.instance().getTerrein();
        terrein.clearAll();

        /* building models */
        BuildingsManager.instance().updateBuildingsFromDb();

        /* terrein */
        String terreinStr = dbManager.loadTerrein();
        if ( terreinStr == null ) {
            terrein.generateTerrein();
        } else {
            terrein.decodeTerrein(terreinStr);
        }

        /* buildings */
        Set<Building> buildings = dbManager.loadBuildings();
        for ( Building building : buildings ) {
            terrein.insertBuilding(building.getPosition()._x,
                    building.getPosition()._y, building);
        }

        /* glyphs */
        Set<GlyphDbData> glyphDbDatas = dbManager.loadGlyphs();
        for (GlyphDbData glyphDbData : glyphDbDatas )  {
            switch ( glyphDbData._type ) {
                case ROAD:
                    RoadData roadData = RoadDataContainer.instance()._roadDataMap.get(glyphDbData._resourceId);
                    terrein.insertRoad(glyphDbData._x, glyphDbData._y, roadData);
                    break;
                default:
                    continue;
            }
        }

    }

}
