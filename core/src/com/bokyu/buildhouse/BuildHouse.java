package com.bokyu.buildhouse;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.ResourceManager;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.screen.ScreenWorld;


public class BuildHouse extends ApplicationAdapter {

	btCollisionConfiguration _btCollitionConfiguration = null;
	btCollisionDispatcher _btCollisionDispatcher = null;
	Mediater _factory = null;

	public BuildHouse(Mediater factory) {
		_factory = factory;
	}


	@Override
	public void create () {

		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		/* bullet */
		Bullet.init();
		_btCollitionConfiguration = new btDefaultCollisionConfiguration();
		_btCollisionDispatcher = new btCollisionDispatcher(_btCollitionConfiguration);

		ResourceManager.instance().loadResources();

		DbManager dbManager = _factory.getDbManager();
		dbManager.init();
		Configuration.instance().setFactory(_factory);

		RenderManager.instance().init();
		NetworkManager.instance().init();
		ScreenWorld.instance().init();
	}

	@Override
	public void dispose () {
		NetworkManager.instance().onDestroy();
	}

	@Override
	public void render () {
        RenderManager.instance().render();
	}
}
