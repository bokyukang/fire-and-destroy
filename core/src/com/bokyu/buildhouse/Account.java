package com.bokyu.buildhouse;

/**
 * Created by bk on 2016-12-01.
 */

public class Account {
    static public String DEFAULT = "default";
    static private Account _instance = null;

    private String _accountId = DEFAULT;
    public String _accountName = "";
    private Village _selVillage = null;

    static public Account instance() {
        if ( _instance == null ) {
            _instance = new Account();
        }
        return _instance;
    }
    public String getId() {
        return _accountId;
    }

    public Village getVillage() {
        return _selVillage;
    }
    public int getVillageId() {
        return 0;//_selVillage._id;
    }

    public void setAccountId(String id) { _accountId = id; }
    public void setVillage(Village village ) { _selVillage = village; }
}
