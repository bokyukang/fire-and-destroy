package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.bokyu.buildhouse.data.CeilingData;
import com.bokyu.buildhouse.data.CeilingDataContainer;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;

import java.util.HashSet;
import java.util.Set;

import static com.bokyu.buildhouse.etc.Constants.DIRECTION.EAST;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.NORTH;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.SOUTH;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.WEST;
import static com.bokyu.buildhouse.etc.Constants.WALL_LENGTH;
import static com.bokyu.buildhouse.etc.Constants.WALL_WIDTH;

/**
 * Created by bk on 2016-11-02.
 */

public class Ceiling extends BuildingGlyph {
    public static int PIX_PER_METER = 100;
    Model _model = null;
    Model _modelPiece = null;
    ModelInstance _modelInstancePiece = null;
    Material _materialTexture = null;
    CeilingData _ceilingData;

    public Ceiling(String resourceId, String textureId, float mass) {
        super(TYPE.CEILING, textureId);
        _resourceId = resourceId;
        _ceilingData = CeilingDataContainer.instance()._ceilingDataMap.get(resourceId);
        if ( _ceilingData == null ) {
            _ceilingData = CeilingDataContainer.instance()._ceilingDataMap.get("1x1");
        }
        setTextureId(textureId);
        setDataRaw(_ceilingData);
        _height = 0;
        _mass = mass;
        init();
    }


    @Override
    public void init() {
        super.init();
        float wallLen = WALL_LENGTH * _ceilingData.xspan;
        float wallHeight = WALL_LENGTH * _ceilingData.yspan;
        float wallWidth = 0.01f;

        _texture.setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
        _materialTexture = new Material(new TextureAttribute(TextureAttribute.Diffuse, _texture));

        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        int attr = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates;
        Vector3 fbl = new Vector3(-wallLen/2f, wallWidth, wallHeight/2f);
        Vector3 fbr = new Vector3(wallLen/2f, wallWidth, wallHeight/2f);
        Vector3 ftr = new Vector3(wallLen/2f, 0, wallHeight/2f);
        Vector3 ftl = new Vector3(-wallLen/2f, 0, wallHeight/2f);
        Vector3 bbl = new Vector3(-wallLen/2f, wallWidth, -wallHeight/2f);
        Vector3 bbr = new Vector3(wallLen/2f, wallWidth, -wallHeight/2f);
        Vector3 btr = new Vector3(wallLen/2f, 0, -wallHeight/2f);
        Vector3 btl = new Vector3(-wallLen/2f, 0, -wallHeight/2f);

        MeshPartBuilder meshPartBuilder;

        // todo

        float textureRate = _texture.getWidth() / _texture.getHeight();
        meshPartBuilder = modelBuilder.part("front", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, wallWidth /  wallLen , 1);
        meshPartBuilder.rect(fbl,ftl,ftr, fbr,   new Vector3(0, 0, 1));

        meshPartBuilder = modelBuilder.part("right", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, wallWidth / wallLen, 1);
        meshPartBuilder.rect(fbr, ftr, btr,bbr,  new Vector3(1, 0, 0));

        meshPartBuilder = modelBuilder.part("left", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, wallWidth / wallLen, 1);
        meshPartBuilder.rect(bbl, btl, ftl,fbl,  new Vector3(-1, 0, 0));

        meshPartBuilder = modelBuilder.part("back", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, wallWidth / wallLen, 1);
        meshPartBuilder.rect(bbr, btr, btl,bbl,  new Vector3(0, 0, -1));

        meshPartBuilder = modelBuilder.part("top", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, 1, wallLen * textureRate);
        meshPartBuilder.rect(ftr, ftl, btl,btr,  new Vector3(0, 1, 0));

        meshPartBuilder = modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, 1, wallLen * textureRate);
        meshPartBuilder.rect(fbr,bbr, bbl,fbl,  new Vector3(0, -1, 0));

        _model = modelBuilder.end();
        _modelInstance = new ModelInstance(_model);

        btBoxShape shape = new btBoxShape(new Vector3(wallLen/2f + WALL_WIDTH/2f , .0000001f, wallHeight/2 + WALL_WIDTH/2f));

        Vector3 localInertia;
        if (_mass == 0)
            localInertia = Vector3.Zero;
        else {
            shape.calculateLocalInertia(_mass, tmpV);
            localInertia = tmpV;
        }
        btRigidBody.btRigidBodyConstructionInfo bodyInfo = new btRigidBody.btRigidBodyConstructionInfo(_mass, null, shape, localInertia);
        _btRigidBody = new btRigidBody(bodyInfo);
        _btRigidBody.setCollisionShape(shape);
        _btRigidBody.setRestitution(0);
        bodyInfo.dispose();
//        _btRigidBody.setCustomDebugColor(new Vector3(1,0,0));

        modelBuilder.begin();

        meshPartBuilder = modelBuilder.part("front", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, 1, wallWidth * textureRate);
        meshPartBuilder.rect(fbl, ftl,  ftr,fbr, new Vector3(0, 0, 1));

        meshPartBuilder = modelBuilder.part("left", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, 1, wallWidth * textureRate);
        meshPartBuilder.rect(bbl, btl, ftl,fbl,  new Vector3(-1, 0, 0));

        meshPartBuilder = modelBuilder.part("back", GL20.GL_TRIANGLES, attr, _materialTexture);
        meshPartBuilder.setUVRange(0, 0, 1 * (float)Math.sqrt(2), wallWidth * textureRate);
        meshPartBuilder.rect(fbr,ftr,  btl,bbl,  new Vector3(0, 0, -1));

        Vector3 up = new Vector3(0,1,0);
        Vector3 down = new Vector3(0,-1,0);

        meshPartBuilder = modelBuilder.part("top", GL20.GL_TRIANGLES, attr, _materialTexture);
        short index1 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(btl).setNor(up)
                .setCol(null).setUV(new Vector2(0,0)));
        short index2 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(ftl).setNor(up)
                .setCol(null).setUV( new Vector2(0,wallLen * textureRate)));
        short index3 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(ftr).setNor(up)
                .setCol(null).setUV( new Vector2(1,wallLen * textureRate)));
        meshPartBuilder.index(index1,index3,index2);

        meshPartBuilder = modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, _materialTexture);
        index1 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(fbr).setNor(down)
                .setCol(null).setUV(new Vector2(1,0) ));
        index2 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(fbl).setNor(down)
                .setCol(null).setUV( new Vector2(0,0) ));
        index3 = meshPartBuilder.vertex(new MeshPartBuilder.VertexInfo().setPos(bbl).setNor(down)
                .setCol(null).setUV( new Vector2(0,wallLen * textureRate) ));
        meshPartBuilder.setUVRange(0, 0, 1 , wallLen);
        meshPartBuilder.index(index1,index3,index2);

        _modelPiece = modelBuilder.end();
        _modelInstancePiece = new ModelInstance(_modelPiece);
        updatePosition();
    }

    public void copyProperties(Ceiling model) {
        _direction = model.getDIrection();
        _elevation = model.getElevation();
    }


    private Texture createColorTexture(int color, float wallLen) {
        int width = (int)(PIX_PER_METER * wallLen);
        Pixmap pixmap = new Pixmap(width, width, Pixmap.Format.RGBA4444);
        pixmap.setColor(color);
        pixmap.fill();
        pixmap.setColor(color);
        pixmap.fillRectangle(0,0, width, width);
        return new Texture(pixmap);
    }

    @Override
    public void updatePosition() {
        Matrix4 matrix = null;
        matrix = new Matrix4().idt();
        setTransformPosition(matrix);
        setTransformRotate(matrix);
        matrix.translate(
                WALL_LENGTH * (_ceilingData.xspan-1)/2f, 0,
                WALL_LENGTH * ( _ceilingData.yspan-1)/2f);
        _btRigidBody.setWorldTransform(matrix);
            _modelInstance.transform.set(matrix);
    }

    @Override
    public void setElevation(int elevation) {
        super.setElevation(elevation);
        _position.y += 0.01;
    }

    @Override
    public void dispose() {
        _model.dispose();
    }

    @Override
    protected void  setTransformRotate(Matrix4 transform) {
        super.setTransformRotate(transform);
    }

    @Override
    public Ceiling clone() {
        Ceiling ceiling = new Ceiling(_resourceId, _textureId, _mass);
        ceiling.setDirection(_direction);
        ceiling.setPosition(_position.x, _position.z);
        ceiling.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        ceiling.setElevation(_elevation);
        return ceiling;
    }

    @Override
    public void setDirection(Constants.DIRECTION direction) {
        super.setDirection(direction);
//        if ( _direction == Constants.DIRECTION.ALL ) {
//            _btRigidBody.setCollisionShape(Bullet.obtainStaticNodeShape(_modelInstance.nodes));
//        } else {
//            _btRigidBody.setCollisionShape(Bullet.obtainStaticNodeShape(_modelInstancePiece.nodes));
//        }
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
            modelBatch.render(_modelInstance, environment);
    }

    protected Set<Constants.DIRECTION> getConsistingDirs(Constants.DIRECTION dir ) {
        Set<Constants.DIRECTION> dirs = new HashSet();
        switch ( dir ) {
            case SW:
                dirs.add(NORTH);
                dirs.add(EAST);
                break;
            case SE:
                dirs.add(NORTH);
                dirs.add(WEST);
                break;
            case NW:
                dirs.add(SOUTH);
                dirs.add(EAST);
                break;
            case NE:
                dirs.add(SOUTH);
                dirs.add(WEST);
                break;
        }
        return dirs;
    }

    public Set<Constants.DIRECTION> getOccupyingDirs() {
        Set<Constants.DIRECTION> result = new HashSet<Constants.DIRECTION>();

            for (Constants.DIRECTION eachDir : Constants.DIRECTION.values()) {
                result.add(eachDir);
            }

//        switch ( _direction ) {
//            case SW:
//                result.add(Constants.DIRECTION.NORTH);
//                result.add(Constants.DIRECTION.EAST);
//                result.add(Constants.DIRECTION.NW);
//                result.add(Constants.DIRECTION.SE);
//                break;
//            case SE:
//                result.add(Constants.DIRECTION.NORTH);
//                result.add(Constants.DIRECTION.WEST);
//                result.add(Constants.DIRECTION.SW);
//                result.add(Constants.DIRECTION.NE);
//                break;
//            case NW:
//                result.add(Constants.DIRECTION.SOUTH);
//                result.add(Constants.DIRECTION.EAST);
//                result.add(Constants.DIRECTION.SW);
//                result.add(Constants.DIRECTION.NE);
//                break;
//            case NE:
//                result.add(Constants.DIRECTION.SOUTH);
//                result.add(Constants.DIRECTION.WEST);
//                result.add(Constants.DIRECTION.NW);
//                result.add(Constants.DIRECTION.SE);
//                break;
//            default:
//                result.add(_direction);
//        }

//        result.add(_direction);
        return result;

    }
    @Override
    protected void setTransformPosition(Matrix4 transform) {
        super.setTransformPosition(transform);
    }

    private void setDataRaw(CeilingData data ) {

        _resourceId = data.id;
        _ceilingData = data;
        setSpanX(data.xspan);
        setSpanY(data.yspan);
    }

    public void setData(CeilingData data) {
        setDataRaw(data);
        init();
    }

}
