package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.bokyu.buildhouse.data.Models;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;

/**
 * Created by bk on 2017-01-18.
 */

public class Decoration extends BuildingGlyph {

    private Model _model = null;
    public Wall _wall = null;

    public Decoration(String modelId, String textureId) {
        super(TYPE.DECORATION, modelId);
        if ( textureId.equals("") == false ) setTextureId(textureId);
        _height = 1;
        init();
    }

    @Override
    public void init() {
        super.init();
        setModel(_resourceId);
    }

    @Override
    public void updatePosition() {
        if ( _wall != null ){
            _positionIndex = _wall.getPositionIndex();
            setElevation(_wall.getElevation());
            setDirection(_wall.getDIrection());
            _isDiagonal = _wall.getIsDiagonal();
        }
        Matrix4 matrix = null;
        if ( _modelInstance != null ) {
            ModelInstance curInstance = _modelInstance;
            matrix = curInstance.transform.idt();
            setTransformPosition(matrix);
            setTransformRotate(matrix);
            setTransformOffset(matrix);
//        _btRigidBody.setWorldTransform(matrix);
            curInstance.transform.set(matrix);
        }
    }

    @Override
    protected void setTransformOffset(Matrix4 transform) {
        if ( !_isDiagonal ) {
            transform.translate(0, 0, +(Constants.WALL_LENGTH / 2 ));
        }
    }

    @Override
    protected void  setTransformRotate(Matrix4 transform) {
        float rotationAmount = getRotationAmount(_direction);
        transform.rotate(0, -1, 0, rotationAmount + 180);
    }

    public void setModel(String id) {
        if ( id == "" ) return;
        _resourceId = id;
        _model = Models.instance().getModel(id, TYPE.DECORATION);
        ModelInstance newIns = new ModelInstance(_model);
        _modelInstance = newIns;

        updatePosition();
        updateMode();
    }

//    @Override
//    public void updateMode() {
//
//    }

    @Override
    public Decoration clone() {
        Decoration decoration = new Decoration(_resourceId, _textureId);
        decoration.setDirection(_direction);
        decoration.setPosition(_position.x, _position.z);
        decoration.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        decoration.setElevation(_elevation);
        return decoration;
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        ModelInstance instance = _modelInstance;
        if ( instance != null )
            modelBatch.render(instance, environment);
    }
}
