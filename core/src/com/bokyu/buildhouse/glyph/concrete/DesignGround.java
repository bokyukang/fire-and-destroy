package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.ClosestRayResultCallback;
import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventDesignGroundPick;
import com.bokyu.buildhouse.events.EventObjectPicked;
import com.bokyu.buildhouse.events.EventTouch;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.Ground;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.ui.scene2d.ConstructScene2DManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by bokyu on 2016-07-28.
 */
public class DesignGround extends Ground {

    public enum PICK_MODE
    {
        GROUND,
        WALL
    }

    /* static */
    static public int PIXELS_PER_METER = 50;
    static public int METERS_PER_EDGE = 10;
    static public Color BACKGROUND_COLOR = Color.GOLD;
    static public Color GRID_COLOR = Color.BLACK;
    static public Color SEL_COLOR = Color.GOLDENROD;

    private Pixmap _pixMap = null;
    private Texture _texture = null;
    private Model _model = null;
    private int _selectedIndexX = 0;
    private int _selectedIndexY = 0;
    private int _pixelsPerEdge = 0;
    private PerspectiveCamera _cam = null;
    private PICK_MODE _pickMode = PICK_MODE.GROUND;

    /* bullet */
    private btCollisionShape _btShape = null;
    private btCollisionObject _btCollisionObject = null;
    private btCollisionWorld _btCollisionWorld = null;
    private btCollisionConfiguration _btCollisionConfiguration = null;
    private btCollisionDispatcher _btDispatcher = null;
    private btBroadphaseInterface _btBroadphase = null;
    private btConstraintSolver _btSolver = null;
    private ClosestRayResultCallback _btRayCallback = new ClosestRayResultCallback(Vector3.Zero, Vector3.Z);



    public DesignGround(PerspectiveCamera cam) {
        _cam = cam;
    }

    @Override
    public void init() {

        /* init variables */
        for ( int x =0; x < METERS_PER_EDGE; ++x ) {

            _glyphMap.put(x, new HashMap());

            for (int y = 0; y < METERS_PER_EDGE; ++y) {

                _glyphMap.get(x).put(y, new HashMap());

                for (Constants.DIRECTION direction : Constants.DIRECTION.values() ) {
                    _glyphMap.get(x).get(y).put(direction, new HashSet());
                }
            }
        }

        /* register eventbus */
        EventBus.getDefault().register(this);


        /* draw pixmap */
        _pixelsPerEdge = PIXELS_PER_METER * METERS_PER_EDGE + 1;
        _pixMap = new Pixmap(_pixelsPerEdge, _pixelsPerEdge, Pixmap.Format.RGBA8888);
        drawPixmap();

        // build model
        ModelBuilder modelBuilder = new ModelBuilder();
        float scale = (float)METERS_PER_EDGE / 2f;
        _model = modelBuilder.createRect(
                -scale,0, scale,
                scale, 0, scale,
                scale, 0, -scale,
                -scale, 0, -scale,
                0, 1, 0,
                new Material(new TextureAttribute(TextureAttribute.Diffuse, _texture)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates
        );
        _modelInstance = new ModelInstance(_model);

        float boxHeight = .4f;
        Model model2 = modelBuilder.createBox(METERS_PER_EDGE, boxHeight, METERS_PER_EDGE,
                new Material(ColorAttribute.createDiffuse(Color.GOLDENROD)),
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates
        );
        ModelInstance modelInstance = new ModelInstance(model2);
        modelInstance.transform.translate(0, -boxHeight/2f - .01f, 0);


        /* bullet ground */
        _btShape = Bullet.obtainStaticNodeShape(_model.nodes);
        _btCollisionObject = new btCollisionObject();
        _btCollisionObject.setCollisionShape(_btShape);
        _btCollisionObject.setWorldTransform(_modelInstance.transform);
        _btCollisionConfiguration = new btDefaultCollisionConfiguration();
        _btDispatcher = new btCollisionDispatcher(_btCollisionConfiguration);
        _btBroadphase = new btDbvtBroadphase();
        _btSolver = new btSequentialImpulseConstraintSolver();
        _btCollisionWorld = new btDiscreteDynamicsWorld(_btDispatcher, _btBroadphase,
                _btSolver, _btCollisionConfiguration);
        _btCollisionWorld.addCollisionObject(_btCollisionObject);



//        _btCollisionWorldCeilings = new btDiscreteDynamicsWorld(_btDispatcher, _btBroadphase, _btSolver, _btCollisionConfiguration);
    }

    /* returns previous mode */
    public PICK_MODE setPickMode(PICK_MODE mode ) {
        PICK_MODE prev = _pickMode;
        _pickMode = mode;
        return prev;
    }
    private void drawPixmap() {
        // clear background
        _pixMap.setColor(BACKGROUND_COLOR);
        _pixMap.fill();
        // draw pix map
        _pixMap.setColor(GRID_COLOR);
        for ( int x=0; x <= METERS_PER_EDGE; ++x ) {
            for ( int y=0; y <= METERS_PER_EDGE; ++y ) {
                int xPos = x * PIXELS_PER_METER;
                int yPos = (METERS_PER_EDGE - y) * PIXELS_PER_METER;

                //draw x line
                _pixMap.drawLine(xPos, 0, xPos, _pixelsPerEdge);
                //draw y line
                _pixMap.drawLine(0, yPos, _pixelsPerEdge, yPos);
                if ( x == _selectedIndexX && y == _selectedIndexY ) {
                    _pixMap.setColor(SEL_COLOR);
                    _pixMap.fillRectangle(xPos+1, yPos+1 - PIXELS_PER_METER, PIXELS_PER_METER - 1, PIXELS_PER_METER - 1);
                    _pixMap.setColor(GRID_COLOR);
                }
            }
        }
        if ( _texture != null ) _texture.dispose();
        _texture = new Texture(_pixMap);

    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if ( _dirty ) {
            drawPixmap();
            _modelInstance.materials.get(0).set(new TextureAttribute(TextureAttribute.Diffuse, _texture));
            _dirty = false;
        }
        modelBatch.render(_modelInstance, environment);
        for ( MyGlyph glyph : _children ) {
            glyph.render(modelBatch, environment);
        }
    }

    @Override
    public void dispose() {
        _btCollisionObject.dispose();
        _btShape.dispose();
        _model.dispose();
    }

    public int posToIdxX(float x) {
        return (int)(x + (float)METERS_PER_EDGE / 2f);
    }
    public int posToIdxY(float z) {
        return (int)-(z - (float)METERS_PER_EDGE / 2f);
    }
    public float idxToPosX(int xIdx) {
        return ((float)xIdx - (float)METERS_PER_EDGE / 2f);
    }
    public float idxToPosY(int yIdx) {
        return -((float)yIdx - (float)METERS_PER_EDGE / 2f);
    }

    /* returns y * row + x index based on screen coordinates */
    public boolean doMousePickGround(PerspectiveCamera cam, int xCoord, int yCoord) {
        Ray ray = cam.getPickRay(xCoord, yCoord);
        Vector3 vRayFrom = ray.origin;
        Vector3 vRayTo = ray.origin.cpy().add( ray.direction.cpy().scl( cam.far ));

        _btRayCallback.setCollisionObject(null);
        _btRayCallback.setRayFromWorld(vRayFrom);
        _btRayCallback.setRayToWorld(vRayTo);
        _btCollisionWorld.rayTest(vRayFrom, vRayTo, _btRayCallback);
        _btRayCallback.setClosestHitFraction(1f);

        if ( _btRayCallback.hasHit() )
        {
            Vector3 hitPointWorld = new Vector3();
            _btRayCallback.getHitPointWorld(hitPointWorld);

            Vector3 hitPointLocal = hitPointWorld.cpy().mul(
                _modelInstance.transform.cpy());
            Gdx.app.debug("buildhouse", "hit point : " +
                    hitPointLocal.toString()
            );

            _selectedIndexX = (int)posToIdxX(hitPointLocal.x);
            _selectedIndexY = (int)posToIdxY(hitPointLocal.z);
            _dirty = true;
            EventBus.getDefault().post(
                    new EventDesignGroundPick(
                            _selectedIndexX,
                            _selectedIndexY));
            return true;
        }

        return false;
    }

    public void doMousePickWall(int xCoord, int yCoord) {
        Ray ray = _cam.getPickRay(xCoord, yCoord);
        Vector3 vRayFrom = ray.origin;
        Vector3 vRayTo = ray.origin.cpy().add( ray.direction.cpy().scl( _cam.far ));

        _btRayCallback.setCollisionObject(null);
        _btRayCallback.setRayFromWorld(vRayFrom);
        _btRayCallback.setRayToWorld(vRayTo);
        _btCollisionWorldObjects.rayTest(vRayFrom, vRayTo, _btRayCallback);
        _btRayCallback.setClosestHitFraction(1f);

        if ( _btRayCallback.hasHit() )
        {
            Object userData = _btRayCallback.getCollisionObject().userData;
            if ( userData instanceof Wall ) {
                Wall wall = (Wall)userData;
                EventBus.getDefault().post(new EventObjectPicked(wall));
            }
        }
    }


    @Override
    public float getTotalWidth() {
        return METERS_PER_EDGE * Constants.WALL_LENGTH;
    }

    @Override
    public float getTotalHeight() {
        return METERS_PER_EDGE * Constants.WALL_LENGTH;
    }

    public Building createBuilding() {
        Building building = new Building();
        for ( MyGlyph glyph : _children ) {
            if ( glyph instanceof BuildingGlyph ) {
                building.addGlyph(glyph);
            }
        }

        return building;
    }

    public void clearAll() {
        for( int x : _glyphMap.keySet() ) {
            for (int y : _glyphMap.get(x).keySet()) {
                for (Constants.DIRECTION dir : _glyphMap.get(x).get(y).keySet()) {
                    for (BuildingGlyph bg : _glyphMap.get(x).get(y).get(dir) )
                    {
                        _children.remove(bg);
                    }
                    _glyphMap.get(x).get(y).get(dir).clear();
                }
            }
        }
        _children.clear();
    }


    /* events */
    @Subscribe
    public void onEvent(EventTouch event) {
        if (ConstructScene2DManager.instance().hit(event._x, event._y) == null) {
            switch ( _pickMode ) {
                case GROUND:
                    doMousePickGround(_cam, event._x, event._y);
                    break;
                case WALL:
                    doMousePickWall(event._x, event._y);
                    break;
            }
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {

        }
    }

    @Subscribe
    public void onEvent(EventObjectPicked event) {
    }

}
