package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Matrix4;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by bokyu on 2016-07-25.
 *
 * A wall which can draw itself.
 * Used while constructing a building.
 */

abstract public class Wall extends BuildingGlyph {

    public enum DIAGONAL
    {
        DIAGONAL,
        NORMAL
    }
    public enum WALL_TYPE
    {
        CUBE,
        MODEL
    }

    protected Set<Decoration> _decorations = new HashSet();


    public Wall(String resourceIdx) {
        super(TYPE.WALL, resourceIdx);
        _height = 1;
    }

    public void copyProperties(Wall model) {
        _direction = model.getDIrection();
        _isDiagonal = model.getIsDiagonal();
    }

    public boolean getIsDiagonal() { return _isDiagonal; }
    public void setDiagonal(boolean isDiagonal) {
        _isDiagonal = isDiagonal;
    }


    @Override
    public Set<Constants.DIRECTION> getOccupyingDirs() {
        Set<Constants.DIRECTION> result =new HashSet<Constants.DIRECTION>();
        if ( _isDiagonal ) {
            result.add(_direction);
        } else {
            result.add(_direction);
        }
        return result;
    }


    protected void setTransformScale(Matrix4 transform) {
        if ( _isDiagonal ) {
            transform.scale( (float)Math.sqrt(2),1, 1);
        }
    }

    public void setModel(String id, boolean temp) {
        _resourceId = id;
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
    }

    public void addDecoration(Decoration decoration) {
        decoration.setPositionIndexes(getPositionIndex());
        decoration._wall = this;
        decoration.updatePosition();
        for ( Decoration decoItr : _decorations ) {
            if ( decoItr.getResourceId() == decoration.getResourceId() ) return;
        }
        _decorations.add(decoration);
        _glyphs.add(decoration);
    }

    public void clearDecorations() {
        for ( Decoration decoration : _decorations ) {
            _glyphs.remove(decoration);
        }
        _decorations.clear();
    }

    @Override
    abstract public Wall clone();

}
