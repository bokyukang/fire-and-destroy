package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.bokyu.buildhouse.data.GlyphDbData;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.data.RoadDataContainer;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.etc.ResourceManager;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventTerreinPick;
import com.bokyu.buildhouse.events.EventTouch;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.Ground;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.localdb.DbManager;
import com.bokyu.buildhouse.screen.ScreenWorld;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.bokyu.buildhouse.data.RoadData.TYPE.FOUR;
import static com.bokyu.buildhouse.data.RoadData.TYPE.NONE;
import static com.bokyu.buildhouse.data.RoadData.TYPE.ONE;
import static com.bokyu.buildhouse.data.RoadData.TYPE.THREE;
import static com.bokyu.buildhouse.data.RoadData.TYPE.TWO_BENT;
import static com.bokyu.buildhouse.data.RoadData.TYPE.TWO_STR;

/**
 * Created by bokyu on 2016-07-12.
 */
public class Terrein extends Ground {
    static public int ITEMS_PER_EDGE = 20;
    static public float ROAD_ELEV = .1f;
    static public final float EDGE_LENGTH = 1f; // only x-z plane counts.
    static public final float LEVEL_HEIGHT = .3f;
    static private final int MAX_NEIGHBOR_DIFF = 1;
    static public final int MAX_LEVEL = 1 << 6;
    static public final int MIN_LEVEL = - ( 1 << 2 );

    public enum PICK_MODE
    {
        GROUND,
        WALL
    }

    /* <x, y, landpice>> */
    private Map<Integer, Map<Integer, Integer>> _heightMap = null;
    private Model _model = null;
    public int _selectedCrossIndexX = 0;
    public int _selectedCrossIndexY = 0;
    public int _selectedIndexX = 0;
    public int _selectedIndexY = 0;
    private PerspectiveCamera _cam = null;
    private Vector3 _hitPointWorld, _hitPointLocal;
    private ModelBuilder _modelBuilder = null;
    private MyPosition _lastHighlitedArea = null;
    private Set<MyPosition> _highlightedAreas = new HashSet();
    private PICK_MODE _pickMode = PICK_MODE.GROUND;
    private ModelInstance _waterModel = null;
    private Texture _grassTexture = null;
    private Map<Integer, RoadData> _roadDataMap = new HashMap();
    private Map<Integer, NodePart> _roadNodeMap = new HashMap();
    private Map<Integer, Integer> _nodeTriDir = new HashMap();
    private ModelInstance _modelInstanceTerrein = null;
    private ModelInstance _modelInstanceRoad = null;
    private Set<Building> _buildings = new HashSet();

    /* bullet */
    private btCollisionShape _btShape = null;
    private btCollisionObject _btCollisionObject = null;
    private btCollisionWorld _btCollisionWorld = null;
    private btCollisionConfiguration _btCollisionConfiguration = null;
    private btCollisionDispatcher _btDispatcher = null;
    private btBroadphaseInterface _btBroadphase = null;
    private btConstraintSolver _btSolver = null;



    public Terrein(PerspectiveCamera cam) { _cam = cam; }

    public void generateTerrein() {
        // setup height map
        int sum = 0;
        ArrayList<Integer> avgValues = new ArrayList();
        for ( int x=0; x <= ITEMS_PER_EDGE; ++x ) {
            Map<Integer, Integer> innerMap = _heightMap.get(x);
            _heightMap.put(x, innerMap);
            for ( int y=0; y <= ITEMS_PER_EDGE; ++y ) {
                if ( x == 0 && y == 0 ) {
                    innerMap.put(y, (int) (Math.random() * 10f));
                } else {
                    avgValues.clear();
                    int startX = Math.max(0,x-1);
                    int startY = Math.max(0,y-1);
                    for ( int itrX = startX; itrX <= x ; ++ itrX ) {
                        for ( int itrY = startY; itrY <= y; ++itrY) {
                            if ( itrX != x || itrY != y )
                                avgValues.add(_heightMap.get(itrX).get(itrY));
                        }
                    }
                    int total = 0;
                    for ( int val : avgValues ) {
                        total  += val;
                    }

                    int avg = (int)((float)total / (float)avgValues.size());
                    int newLev= avg + (int)Math.round( (Math.random()-.5f) * 2 *  MAX_NEIGHBOR_DIFF);
                    newLev = Math.max(Math.min(newLev, MAX_LEVEL), MIN_LEVEL);
                    innerMap.put(y, newLev);
                    sum += newLev;
                }
            }
        }
        int avg = (int)(sum / ((ITEMS_PER_EDGE+1)*(ITEMS_PER_EDGE+1)));
        int modification = 2-avg;
        for ( int x=0; x <= ITEMS_PER_EDGE; ++x ) {
            for (int y = 0; y <= ITEMS_PER_EDGE; ++y) {
                int origin = _heightMap.get(x).get(y);
                int newVal = origin + modification;
                _heightMap.get(x).put(y,newVal);
            }
        }

        buildTerreinVerts();

        updateElevations();
    }

    private void updateElevations() {
//        for ( Building building : _buildings ) {
//            updateBuildingPosition(building);
//        }
        for ( int idx : _roadNodeMap.keySet() )
        {
            int y = idx % ITEMS_PER_EDGE;
            int x = (idx - y) / ITEMS_PER_EDGE;
            updateRoadArea(x,y);
        }
    }

    public void buildTerreinVerts() {
        _modelInstanceTerrein.nodes.get(0).parts.clear();
        // build model
        for ( int x=0; x < ITEMS_PER_EDGE; ++x ) {
            for ( int y=0; y < ITEMS_PER_EDGE; ++y ) {
                _modelInstanceTerrein.nodes.get(0).parts.add(buildVertices(x, y, 0, 0));
            }
        }
    }

    public void init() {

        _heightMap = new HashMap();

        /* init variables */
        for ( int x =0; x <= ITEMS_PER_EDGE; ++x ) {

            Map<Integer, Integer> innerHeightMap = new HashMap<Integer, Integer>();
            _heightMap.put(x, innerHeightMap);

            _glyphMap.put(x, new HashMap());

            for (int y = 0; y <= ITEMS_PER_EDGE; ++y) {

                innerHeightMap.put(y, 0);
                _glyphMap.get(x).put(y, new HashMap());

                for (Constants.DIRECTION direction : Constants.DIRECTION.values() ) {
                    _glyphMap.get(x).get(y).put(direction, new HashSet());
                }

                int idx = x * ITEMS_PER_EDGE + y;
                _roadDataMap.put(idx , null);
                _nodeTriDir.put(idx, Math.random() > .5 ? 0 : 1);
            }
        }


        EventBus.getDefault().register(this);
        _grassTexture = new Texture("terrein/grass_land_2.gif");
        _modelInstanceTerrein = new ModelInstance(new Model());
        _modelInstanceTerrein.nodes.add(new Node());

        _modelInstanceRoad = new ModelInstance(new Model());
        _modelInstanceRoad.nodes.add(new Node());

        generateTerrein();

        // water
        Texture texture = new Texture("terrein/water.gif");
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        Material material = new Material(new TextureAttribute(TextureAttribute.Diffuse, texture));
        int attr = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates;
        Model model = MyGraphicsHelper.instance().createCube(
                ITEMS_PER_EDGE * 10,
                1,
                ITEMS_PER_EDGE * 10,
                attr, material );
        _waterModel = new ModelInstance(model);
        _waterModel.transform.translate(0,-.65f,0);


        /* bullet */
        _btShape = Bullet.obtainStaticNodeShape(_modelInstanceTerrein.nodes);
        _btCollisionObject = new btCollisionObject();
        _btCollisionObject.setCollisionShape(_btShape);
        _btCollisionObject.setWorldTransform(_modelInstanceTerrein.transform);
        _btCollisionConfiguration = new btDefaultCollisionConfiguration();
        _btDispatcher = new btCollisionDispatcher(_btCollisionConfiguration);
        _btBroadphase = new btDbvtBroadphase();
        _btSolver = new btSequentialImpulseConstraintSolver();
        _btCollisionWorld = new btDiscreteDynamicsWorld(_btDispatcher, _btBroadphase, _btSolver, _btCollisionConfiguration);
        _btCollisionWorld.addCollisionObject(_btCollisionObject);

    }

    public void clearAll() {
        ScreenWorld.instance().lock();
        try {
            for (Map<Integer,
                    Map<Constants.DIRECTION,
                            Set<BuildingGlyph>>> inner1 : _glyphMap.values()) {
                for (Map<Constants.DIRECTION,
                        Set<BuildingGlyph>> inner2 : inner1.values()) {
                    for (Set<BuildingGlyph> inner3 : inner2.values()) {
                        for (BuildingGlyph bg : inner3) {
//                            bg.dispose();
                        }
                        inner3.clear();
                    }
                }

            }

            _roadDataMap.clear();
            _roadNodeMap.clear();
            for (MyGlyph mg : _children) {
                mg.dispose();
            }
            _children.clear();
            _modelInstanceRoad.nodes.get(0).parts.clear();

//            for (Building bd : _buildings) {
//                bd.dispose();
//            }
            _buildings.clear();
        } finally {
            ScreenWorld.instance().unLock();
        }
    }

    public void loadFromDb() {
        clearAll();
        DbManager dbManager = Configuration.instance().getFactory().getDbManager();
        String encodedTerrein = dbManager.loadTerrein();
        if ( encodedTerrein.equals("")) {
        } else {
            decodeTerrein(encodedTerrein);
        }

        Set<GlyphDbData> glyphDatas = dbManager.loadGlyphs();
        for ( GlyphDbData data : glyphDatas ) {
            if ( data._type == TYPE.ROAD ) {
                insertRoad(data._x, data._y, RoadDataContainer.instance()._roadDataMap.get(data._resourceId));
            }
        }
    }

    public Set<Building> getBuildings() { return _buildings; }

    public Map<Integer, RoadData> getRoadDataMap() { return _roadDataMap; }
    public Set<Integer> getRoadKeys() { return _roadNodeMap.keySet(); }

    public void select(int x, int y) {
        if ( _lastHighlitedArea == null ) {
            _lastHighlitedArea = new MyPosition(x,y);
        }
        removeHighlight(_lastHighlitedArea);
        if ( setHighlighted(new MyPosition(x,y), Color.RED) ) {
            _lastHighlitedArea._y = y;
            _lastHighlitedArea._x = x;
        }
    }

    public void removeHighlight(MyPosition p) {
        try {
            int idx = p._x * ITEMS_PER_EDGE + p._y;
            if ( idx >= 0 ) {
                _modelInstanceTerrein.nodes.get(0).parts.get(idx).material.remove(
                        ColorAttribute.Diffuse
                );
            }
        } catch ( IndexOutOfBoundsException e ) {}
    }
    public boolean setHighlighted(MyPosition p, Color color) {
        try {
            if ( p._x < 0 || p._x >= ITEMS_PER_EDGE ) return false;
            if ( p._y < 0 || p._y >= ITEMS_PER_EDGE ) return false;
            int idx = p._x * ITEMS_PER_EDGE + p._y;
            if ( idx >= 0 ) {
                _modelInstanceTerrein.nodes.get(0).parts.get(idx).material.set(
                        ColorAttribute.createDiffuse(color)
                );
            }
        } catch ( IndexOutOfBoundsException r ) {
            return false;
        }
        return true;
    }

    public boolean updateRoadTexture(int x, int y) {
        int idx = x * ITEMS_PER_EDGE + y;
        if ( _roadDataMap.get(idx) != null ) {
            RoadData roadData = _roadDataMap.get(idx);
            RoadData.TYPE roadType = getRoadType(x, y);
            int rotAmount = getRoadRotation(x,y, roadType);
            Texture texture = ResourceManager.instance().getRoadTexture(roadData.id, roadType);

            if ( _roadNodeMap.containsKey(idx) ) {
                _modelInstanceRoad.nodes.get(0).parts.removeValue(_roadNodeMap.get(idx), false);
            }

            NodePart np = buildVertices(x,y, ROAD_ELEV, rotAmount);
            _roadNodeMap.put(idx, np);

            _modelInstanceRoad.nodes.get(0).parts.add(np);
            _roadNodeMap.get(idx).material.set(
                    TextureAttribute.createDiffuse(texture)
            );
            return true;
        }
        else if (_roadNodeMap.containsKey(idx)) {

            _modelInstanceRoad.nodes.get(0).parts.removeValue(
                    _roadNodeMap.get(idx), true
            );
            _roadNodeMap.remove(idx);
        }
        return false;
    }

    private int getRoadRotation(int x, int y, RoadData.TYPE type) {
        if ( type == FOUR || type == NONE ) return 0;

        ArrayList<MyPosition> neighbors = new ArrayList();
        neighbors.add(new MyPosition(0, -1));
        neighbors.add(new MyPosition(1, 0));
        neighbors.add(new MyPosition(0, 1));
        neighbors.add(new MyPosition(-1, 0));
        neighbors.add(new MyPosition(0, -1));

        int count = 0;
        boolean prev = false;
        for ( MyPosition p : neighbors ) {
            try {
                if (_roadDataMap.get((x + p._x) * ITEMS_PER_EDGE + y + p._y) != null) {
                    if ( type == ONE  ) {
                        return count;
                    }

                    if ( type == TWO_BENT ) {
                        if ( prev ) {
                            return count - 1;
                        }
                    }

                    if ( type == TWO_STR ) {
                        return count;
                    }

                    prev = true;
                } else {
                    if ( type == THREE ) {
                        return count + 1;
                    }
                    prev = false;
                }
            } catch (IndexOutOfBoundsException e) {}

            count++;
        }

        return 0;

    }

    private RoadData.TYPE getRoadType(int x, int y) {
        int numNeighbors = 0;
        boolean consec = false;
        boolean prev = false;
        ArrayList<MyPosition> idxs = new ArrayList();
        idxs.add(new MyPosition(0, 1));
        idxs.add(new MyPosition(1, 0));
        idxs.add(new MyPosition(0, -1));
        idxs.add(new MyPosition(-1, 0));
        idxs.add(new MyPosition(0, 1));
        int count = 0;
        for ( MyPosition idx : idxs ) {
            try {
                if (_roadDataMap.get((x + idx._x) * ITEMS_PER_EDGE + y + idx._y) != null) {
                    if ( count < idxs.size()-1 ) numNeighbors++;
                    if (prev) consec = true;
                    prev = true;
                } else {
                    prev = false;
                }
            } catch (IndexOutOfBoundsException e) {
                prev = false;
            }
            count++;
        }

        if ( numNeighbors == 4)
            return FOUR;
        if ( numNeighbors == 3 )
            return RoadData.TYPE.THREE;
        if ( numNeighbors == 1 )
            return ONE;
        if ( numNeighbors == 0 )
            return NONE;
        if ( numNeighbors == 2 )
        {
            if ( consec ) {
                return RoadData.TYPE.TWO_BENT;
            } else {
                return RoadData.TYPE.TWO_STR;
            }
        }
        return null;
    }

    private void updateRoadArea(int x, int y) {
        updateRoadTexture(x,y);

        for ( int addX = -1; addX <= 1; addX += 1 ) {
            for ( int addY = -1; addY <= 1; addY += 1 ) {
                if ( (addX == 0 || addY == 0) && !(addX == 0 && addY == 0 )) {
                    try {
                        updateRoadTexture(x + addX, y + addY);
                    } catch (NullPointerException e) {
                    }
                }
            }
        }
    }

    public boolean insertRoad(int x, int y, RoadData roadData) {
        int idx = x * ITEMS_PER_EDGE + y;

        if ( _roadDataMap.get(idx) != null )  return false;
        _roadDataMap.put(idx, roadData);

        updateRoadArea(x, y);

        return true;
    }


    public void removeRoad(int x, int y ) {
        int idx = x * ITEMS_PER_EDGE + y;
        _roadDataMap.put(idx, null);
        updateRoadArea(x,y);
    }

    public void addHighlightedSet(Set<MyPosition> ps, MyPosition criP) {
        for ( MyPosition p : ps ) addHighlightedSet(p, criP);
    }
    public void addHighlightedSet(MyPosition p, MyPosition criP) {
        try {
            int halfEdge = DesignGround.METERS_PER_EDGE / 2;
            MyPosition hP = null;
            if ( criP == null ) {
                hP = new MyPosition(p._x + _selectedCrossIndexX - halfEdge,
                        p._y + _selectedCrossIndexY - halfEdge);
            } else {
                hP = new MyPosition(p._x + criP._x - halfEdge,
                        p._y + criP._y - halfEdge);
            }
            if ( setHighlighted(hP, Color.BLACK) ) {
                _highlightedAreas.add(hP);
            }
        } catch ( IndexOutOfBoundsException e ) {}
    }
    public void clearHighlightedSet() {
        for ( MyPosition p : _highlightedAreas ) {
            removeHighlight(p);
        }
        _highlightedAreas.clear();
    }

    public void updateCollision() {
        _btShape.dispose();
        _btShape = Bullet.obtainStaticNodeShape(_modelInstanceTerrein.nodes);
        _btCollisionObject.setCollisionShape(_btShape);
    }

    public NodePart buildVertices(
            int idxX,
            int idxY,
            float elev,
            int rot
    )
    {
        // init mesh part builder
        MeshBuilder meshBuilder = new MeshBuilder();
        meshBuilder.begin(
                VertexAttributes.Usage.Position
                        | VertexAttributes.Usage.Normal
                        | VertexAttributes.Usage.TextureCoordinates
                ,
                GL20.GL_TRIANGLES
        );

        // calc actual heights
        Vector3 vCurrentNode = getWorldPosition(idxX, idxY).add(0,elev,0);//new Vector3(0, levelFrontLeft * HEIGHT_UNIT, 0);
        Vector3 vRight = getWorldPosition(idxX+1, idxY).add(0,elev,0);
        Vector3 vUp = getWorldPosition(idxX, idxY+1).add(0,elev,0);
        Vector3 vUpRight = getWorldPosition(idxX+1, idxY+1).add(0,elev,0);

        // calc normal
        Vector3 normal1, normal2;

        ArrayList<Vector3> vg1 = new ArrayList();
        ArrayList<Vector3> vg2 = new ArrayList();
        int type =_nodeTriDir.get( idxX * ITEMS_PER_EDGE + idxY );

        Vector2 uvBL = MyHelper.getUvPair(rot);
        Vector2 uvTL = MyHelper.getUvPair(rot + 1);
        Vector2 uvTR = MyHelper.getUvPair(rot + 2);
        Vector2 uvBR = MyHelper.getUvPair(rot + 3);

        ArrayList<Vector2> uvs = new ArrayList();
        if ( type == 0 ) {
            vg1.add(vCurrentNode);
            vg1.add(vRight);
            vg1.add(vUpRight);
            vg2.add(vUpRight);
            vg2.add(vUp);
            vg2.add(vCurrentNode);
            uvs.add(uvBL);
            uvs.add(uvBR);
            uvs.add(uvTR);
            uvs.add(uvTR);
            uvs.add(uvTL);
            uvs.add(uvBL);
        } else {
            vg1.add(vCurrentNode);
            vg1.add(vRight);
            vg1.add(vUp);
            vg2.add(vUp);
            vg2.add(vRight);
            vg2.add(vUpRight);
            uvs.add(uvBL);
            uvs.add(uvBR);
            uvs.add(uvTL);
            uvs.add(uvTL);
            uvs.add(uvBR);
            uvs.add(uvTR);
        }
        normal1 = vg1.get(1).cpy().sub(vg1.get(0)).crs(
                vg1.get(2).cpy().sub(vg1.get(0))
        ).nor();
        normal2 = vg2.get(1).cpy().sub(vg2.get(0)).crs(
                vg2.get(2).cpy().sub(vg2.get(0))
        ).nor();

        // create vertexes
        MeshPartBuilder.VertexInfo viT1_1 = new MeshPartBuilder.VertexInfo()
                .setPos(vg1.get(0))
                .setNor(normal1)
                .setCol(null)
                .setUV(uvs.get(0).x,uvs.get(0).y);
        MeshPartBuilder.VertexInfo viT1_2 = new MeshPartBuilder.VertexInfo()
                .setPos(vg1.get(1))
                .setNor(normal1)
                .setCol(null)
                .setUV(uvs.get(1).x, uvs.get(1).y);
        MeshPartBuilder.VertexInfo viT1_3 = new MeshPartBuilder.VertexInfo()
                .setPos(vg1.get(2))
                .setNor(normal1)
                .setCol(null)
                .setUV(uvs.get(2).x, uvs.get(2).y);
        MeshPartBuilder.VertexInfo viT2_1 = new MeshPartBuilder.VertexInfo()
                .setPos(vg2.get(0))
                .setNor(normal2)
                .setCol(null)
                .setUV(uvs.get(3).x, uvs.get(3).y);
        MeshPartBuilder.VertexInfo viT2_2 = new MeshPartBuilder.VertexInfo()
                .setPos(vg2.get(1))
                .setNor(normal2)
                .setCol(null)
                .setUV(uvs.get(4).x, uvs.get(4).y);
        MeshPartBuilder.VertexInfo viT2_3 = new MeshPartBuilder.VertexInfo()
                .setPos(vg2.get(2))
                .setNor(normal2)
                .setCol(null)
                .setUV(uvs.get(5).x, uvs.get(5).y);

        // create triangles
        meshBuilder.triangle(viT1_1, viT1_2, viT1_3);
        meshBuilder.triangle(viT2_1, viT2_2, viT2_3);

        MeshPart meshPart
                = new MeshPart("" + idxX + "," + idxY
                , meshBuilder.end()
                , 0
                , 6
                , GL20.GL_TRIANGLES
                );
        Material material = new Material(new TextureAttribute(TextureAttribute.Diffuse,
                        _grassTexture));
        material.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f));
        NodePart nodePart = new NodePart(meshPart,material);
        return nodePart;
//        MeshPartBuilder meshPartBuilder
//                = modelBuilder.part(
//                "" + idxX + "," + idxY, GL20.GL_TRIANGLES,
//                VertexAttributes.Usage.Position
//                        | VertexAttributes.Usage.Normal
//                        | VertexAttributes.Usage.TextureCoordinates,
//                new Material(new TextureAttribute(TextureAttribute.Diffuse,
//                        new Texture("terrein/grass_land.gif")))
//        );

//        // set position
//        meshPartBuilder.setVertexTransform(
//                getTransform(new Matrix4(), idxX, idxY)
//        );
    }


    public void modifyLevel(int changeAmount,int idxX, int idxY, boolean updateBullet) {
        int original = _heightMap.get(idxX).get(idxY);
        _heightMap.get(idxX).put(idxY,original + changeAmount);
        int newHeight = _heightMap.get(idxX).get(idxY);
        for ( int xitr = idxX-1; xitr <=idxX; ++xitr)
        {
            for ( int yitr = idxY -1; yitr <=idxY; ++yitr)
            {
                if ( xitr >= 0 && xitr < ITEMS_PER_EDGE
                        && yitr >= 0 && yitr < ITEMS_PER_EDGE
                        )
                {
                    int idx = xitr * ITEMS_PER_EDGE + yitr;

                    _modelInstanceTerrein.nodes.get(0).parts.set(idx,
                            buildVertices(xitr, yitr, 0, 0));
                }

            }
        }
        updateRoadArea(idxX, idxY);
        ArrayList<Integer> neighborsX = new ArrayList();
        ArrayList<Integer> neighborsY = new ArrayList();
        neighborsX.add(idxX-1); neighborsY.add(idxY);
        neighborsX.add(idxX+1); neighborsY.add(idxY);
        neighborsX.add(idxX); neighborsY.add(idxY-1);
        neighborsX.add(idxX); neighborsY.add(idxY+1);
        try {
            for (int i = 0; i < neighborsX.size(); ++i) {
                int nX = neighborsX.get(i);
                int nY = neighborsY.get(i);
                if (nX >= 0 && nX < ITEMS_PER_EDGE &&
                        nY >= 0 && nY < ITEMS_PER_EDGE) {
                    if (_heightMap.get(nX).get(nY) - newHeight > MAX_NEIGHBOR_DIFF) {
                        int nChAmt = _heightMap.get(nX).get(nY) - newHeight - MAX_NEIGHBOR_DIFF;
                        modifyLevel(-nChAmt, nX, nY, false);
                    } else if (_heightMap.get(nX).get(nY) - newHeight < -MAX_NEIGHBOR_DIFF) {
                        int nChAmt = _heightMap.get(nX).get(nY) - newHeight + MAX_NEIGHBOR_DIFF;
                        modifyLevel(-nChAmt, nX, nY, false);
                    }
                }
            }
        } catch ( IndexOutOfBoundsException e ) {
            e.printStackTrace();
        }
        if ( updateBullet ) updateCollision();

//        _btRigidBody.

    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        modelBatch.render(_modelInstanceTerrein, environment);
        for ( MyGlyph glyph : _children ) {
            glyph.render(modelBatch, environment);
        }
        modelBatch.render(_waterModel, environment);
        modelBatch.render(_modelInstanceRoad, environment);
    }

    private Matrix4 getTransform(Matrix4 mat, int idxX, int idxY) {
        return mat.idt().translate(
                idxToPosX(idxX)
                , 0
                , idxToPosY(idxY)
        );
    }

    public float idxToPosX(int idxX) {
        return EDGE_LENGTH * (idxX - (int)(ITEMS_PER_EDGE / 2));
    }
    public float idxToPosY(int idxY) {
        return  -(EDGE_LENGTH * (idxY- (int)(ITEMS_PER_EDGE / 2)));
    }

    public int posToIdxX(float x, boolean isCross) {
        if ( isCross ) return Math.round(x / EDGE_LENGTH + (ITEMS_PER_EDGE / 2));
        else return (int)(x / EDGE_LENGTH + (ITEMS_PER_EDGE / 2));
    }

    public int posToIdxY(float z, boolean isCross) {
        if ( isCross ) return Math.round(-z/EDGE_LENGTH + (int)(ITEMS_PER_EDGE / 2));
        else return (int)(-z/EDGE_LENGTH + (int)(ITEMS_PER_EDGE / 2));
    }

    /* returns y * row + x index based on screen coordinates */
    public boolean getMousePick(PerspectiveCamera cam, int xCoord, int yCoord) {

        Ray ray = cam.getPickRay(xCoord, yCoord);
        Vector3 vRayFrom = ray.origin;
        Vector3 vRayTo = ray.origin.cpy().add( ray.direction.cpy().scl( cam.far ));

        _btRayCallback.setCollisionObject(null);
        _btRayCallback.setRayFromWorld(vRayFrom);
        _btRayCallback.setRayToWorld(vRayTo);
        _btCollisionWorld.rayTest(vRayFrom, vRayTo, _btRayCallback);
        _btRayCallback.setClosestHitFraction(1f);

        if ( _btRayCallback.hasHit() )
        {
            Vector3 hitPointWorld = new Vector3();
            _btRayCallback.getHitPointWorld(hitPointWorld);

            Vector3 hitPointLocal = hitPointWorld.cpy().mul(
                    _modelInstanceTerrein.transform.cpy());

            _hitPointLocal = hitPointLocal;
            _hitPointWorld = hitPointWorld;

            _selectedCrossIndexX = posToIdxX(hitPointLocal.x, true);
            _selectedCrossIndexY = posToIdxY(hitPointLocal.z, true);
            _selectedIndexX = posToIdxX(hitPointLocal.x, false);
            _selectedIndexY = posToIdxY(hitPointLocal.z, false);
            _dirty = true;
            return true;
        }

        return false;
    }


    public Vector3 getWorldPosition(int idxX, int idxY) {
        float height = 0;
        if ( idxX == 0 || idxY == 0 || idxX == ITEMS_PER_EDGE || idxY == ITEMS_PER_EDGE ) {

        } else {
            height = _heightMap.get(idxX).get(idxY) * LEVEL_HEIGHT;
        }
        Vector3 position = new Vector3(
                idxToPosX(idxX),
                height,
                idxToPosY(idxY)
        );
        return position;
    }

    public float getTerreinElev(int x, int y) {
        return _heightMap.get(x).get(y) * LEVEL_HEIGHT;
    }

    public float getTerreinElevBuilding(int x, int y, Set<MyPosition> positions) {
        boolean minHeightSet = false;
        int minHeight = 0;
        int halfEdge = DesignGround.METERS_PER_EDGE/2;
        for ( MyPosition p : positions ) {
            for ( int xSpan =0; xSpan <=1; xSpan++ ) {
                for ( int ySpan =0; ySpan <=1; ySpan++ ) {
                    try {
                        int height = _heightMap.get(x + p._x - halfEdge + xSpan).get(y + p._y - halfEdge + ySpan);
                        if ( minHeightSet == false ) {
                            minHeight = height;
                            minHeightSet = true;
                        } else {
                            minHeight = Math.min(minHeight, height);
                        }
                    } catch ( IndexOutOfBoundsException e ) {
                    } catch ( NullPointerException e ) {}
                }
            }
        }
        return minHeight * LEVEL_HEIGHT;
    }

    public boolean insertBuilding(int x, int y, Building building) {
        if ( checkInsertPossible(x,y, building.getOccupyingPositions()) ) {

            float elevation = getTerreinElevBuilding(
                    x, y, building.getOccupyingPositions()
            );
//            addHighlightedSet(building.getOccupyingPositions(), building.getPosition());
            building._elevation = elevation;

            MyPosition position = building.getPosition();
            position._x = x;
            position._y = y;
            building.updatePosition();

            int halfEdge = DesignGround.METERS_PER_EDGE/2;
            for ( MyGlyph glyph : building.getGlyphs() ) {
                BuildingGlyph bGlyph = (BuildingGlyph)glyph;
                MyPosition glyphPosition = bGlyph.getPositionIndex();
                MyPosition newP = new MyPosition( x + glyphPosition._x - halfEdge,
                        y + glyphPosition._y - halfEdge);
                if ( bGlyph instanceof Wall ) {
                    addWall( newP._x, newP._y, (Wall)bGlyph );
                } else if ( bGlyph instanceof Roof ) {
                    addRoof( newP._x, newP._y, (Roof)bGlyph );
                } else if ( bGlyph instanceof Ceiling ) {
                    addCeiling(newP._x, newP._y, (Ceiling)bGlyph);
                } else if ( bGlyph instanceof Decoration ) {
                    _children.add(bGlyph);
                }
            }
            _buildings.add(building);
            return true;
        }
        return false;
    }

    public void removeBuilding(Building building) {
        MyPosition bP = building.getPosition();
        for ( MyGlyph glyph : building.getGlyphs() ) {
            BuildingGlyph bGlyph = (BuildingGlyph)glyph;
            MyPosition glyphPosition = bGlyph.getPositionIndex();
            if ( bGlyph instanceof Wall ) {
                removeWall(  (Wall)bGlyph , bP._x, bP._y);
            } else if ( bGlyph instanceof Roof ) {
                removeRoof(  (Roof)bGlyph , bP._x, bP._y);
            } else if ( bGlyph instanceof Ceiling ) {
                removeCeiling( (Ceiling)bGlyph, bP._x, bP._y);
            } else if ( bGlyph instanceof Decoration ) {
                _children.remove(bGlyph);
            }
        }
        _buildings.remove(building);
    }

    public boolean checkInsertPossible(int x, int y, Set<MyPosition> constructPoss) {
        for ( MyPosition p : constructPoss ) {
            int edgeHalf = DesignGround.METERS_PER_EDGE/2;
            Map<Constants.DIRECTION, Set<BuildingGlyph>> innerDs = null;
            try {
                innerDs = _glyphMap
                        .get(x + p._x - edgeHalf)
                        .get(y + p._y - edgeHalf);
                if ( innerDs == null ) {
                    return false;
                }
                for ( Constants.DIRECTION dir : innerDs.keySet() )
                {
                    Set<BuildingGlyph> dsDir = innerDs.get(dir);
                    if ( dsDir.size() > 0 ) return false;
                }

            } catch ( IndexOutOfBoundsException e ) {
                return false;
            } catch ( NullPointerException e ) { return false; }
        }
        return true;
    }

    public String encodeTerrein() {
        String result = "";
        for ( int x =0; x < _heightMap.size(); ++x ) {
            Map<Integer, Integer> innerMap = _heightMap.get(x);
            for ( int y =0; y < innerMap.size(); ++y )
            {
                int height = innerMap.get(y);
                char heightChr = (char)(height - MIN_LEVEL);
                result += heightChr;
            }
        }
        return result;
    }

    public boolean decodeTerrein(String string) {
        char[] charArr = string.toCharArray();
        int idx = 0;
        for ( int x =0; x < _heightMap.size(); ++x ) {
            Map<Integer, Integer> innerMap = _heightMap.get(x);
            for (int y = 0; y < innerMap.size(); ++y) {
                if ( charArr.length <= idx ) return false;
                int height = (int)charArr[idx] + MIN_LEVEL;
                innerMap.put(y, height);
                idx++;
            }
        }
        buildTerreinVerts();
        return true;
    }

    /* returns previous mode */
    public PICK_MODE setPickMode(PICK_MODE mode ) {
        PICK_MODE prev = _pickMode;
        _pickMode = mode;
        return prev;
    }

    /* events */
    @Subscribe
    public void onEvent(EventTouch event) {
        if (!ScreenWorld.instance().getIsVisible()) return;
        if ( ScreenWorld.instance().hit(event._x, event._y) == null ) {
            if ( _pickMode == PICK_MODE.GROUND ) {
                if (getMousePick(_cam, event._x, event._y)) {
//                select(_selectedIndexX, _selectedIndexY);
                    EventBus.getDefault().post(
                            new EventTerreinPick(
                                    _selectedCrossIndexX,
                                    _selectedCrossIndexY,
                                    _selectedIndexX,
                                    _selectedIndexY,
                                    _hitPointWorld,
                                    _hitPointLocal
                            ));
                }
            } else if ( _pickMode == PICK_MODE.WALL ) {
                doMousePickObject(_cam, event._x, event._y);
            }
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case GENERATE_TERREIN:
                generateTerrein();
                break;
            case SAVE_TERREIN:
                Configuration.instance().getFactory().getDbManager().saveTerrein(encodeTerrein());
                break;
        }
    }

}
