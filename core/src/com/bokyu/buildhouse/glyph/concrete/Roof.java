package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.collision.btCompoundShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.bokyu.buildhouse.data.Models;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;

import java.util.HashSet;
import java.util.Set;

import static com.bokyu.buildhouse.etc.Constants.WALL_HEIGHT;
import static com.bokyu.buildhouse.etc.Constants.WALL_LENGTH;
import static com.bokyu.buildhouse.etc.Constants.WALL_WIDTH;
import static com.bokyu.buildhouse.glyph.BuildingGlyph.TYPE.ROOF;

/**
 * Created by bk on 2017-01-05.
 */

public class Roof extends BuildingGlyph {
    private Model _model = null;
    private Set<Constants.DIRECTION> _dirs = null;

    public Roof(String resourceId, String textureId, float mass) {
        super(ROOF, resourceId);
        if ( !textureId.equals("")) setTextureId(textureId);
        _height = 1;
        _direction = Constants.DIRECTION.NORTH;
        _mass = mass;
        init();
    }

    @Override
    public void init() {
        super.init();
        setModel(_resourceId);

        BoundingBox box = new BoundingBox();
        _model.extendBoundingBox(box);
        btBoxShape cshape = new btBoxShape(new Vector3(WALL_LENGTH/2f , box.getHeight()/2f, WALL_LENGTH/2));
        btCompoundShape shape = new btCompoundShape();
        shape.addChildShape(new Matrix4().translate(0, box.getHeight()/2, 0), cshape);

        Vector3 localInertia;
        if (_mass == 0)
            localInertia = Vector3.Zero;
        else {
            shape.calculateLocalInertia(_mass, tmpV);
            localInertia = tmpV;
        }
        btRigidBody.btRigidBodyConstructionInfo bodyInfo = new btRigidBody.btRigidBodyConstructionInfo(_mass, null, shape, localInertia);
        _btRigidBody = new btRigidBody(bodyInfo);
        _btRigidBody.setCollisionShape(shape);
        _btRigidBody.setRestitution(0);
        bodyInfo.dispose();


        _dirs = new HashSet();
        _dirs.add(Constants.DIRECTION.ALL);
    }


    @Override
    public void updatePosition() {
        Matrix4 matrix = null;
        ModelInstance curInstance = _modelInstance;
        matrix = curInstance.transform.idt();
        setTransformPosition(matrix);
        setTransformRotate(matrix);
        _btRigidBody.setWorldTransform(matrix);
        curInstance.transform.set(matrix);
    }


    public void setModel(String id) {
        _resourceId = id;
        _model = Models.instance().getModel(id, ROOF);
        ModelInstance newIns = new ModelInstance(_model);
        setModelInstance(newIns);

        updatePosition();
        updateMode();
    }

    @Override
    public Set<Constants.DIRECTION> getOccupyingDirs() {
        return _dirs;
    }

    @Override
    public Roof clone() {
        Roof roof = new Roof(_resourceId, _textureId, _mass);
        roof.setDirection(_direction);
        roof.setPosition(_position.x, _position.z);
        roof.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        roof.setElevation(_elevation);
        return roof;
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        ModelInstance instance = _modelInstance;
        if ( instance != null )
            modelBatch.render(instance, environment);
    }
    @Override
    protected void setTransformPosition(Matrix4 transform) {
        super.setTransformPosition(transform);
    }
}
