package com.bokyu.buildhouse.glyph.concrete;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.bokyu.buildhouse.data.Models;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.fireanddestroy.ObjectsManager;

import java.util.HashMap;
import java.util.Map;

import static com.bokyu.buildhouse.etc.Constants.WALL_HEIGHT;
import static com.bokyu.buildhouse.etc.Constants.WALL_LENGTH;
import static com.bokyu.buildhouse.etc.Constants.WALL_WIDTH;
import static com.bokyu.buildhouse.glyph.concrete.Wall.WALL_TYPE.CUBE;
import static com.bokyu.buildhouse.glyph.concrete.Wall.WALL_TYPE.MODEL;

/**
 * Created by bokyu on 2016-10-06.
 */
public class WallTexture extends Wall {

    static btBoxShape shape
            = new btBoxShape(new Vector3(WALL_LENGTH/2f - WALL_WIDTH, WALL_HEIGHT/2, WALL_WIDTH/2));
    private Material _materialTexture = null;
    private Model _model;
    private Map<WALL_TYPE, ModelInstance> _modelInstancesMap = null;
    private WALL_TYPE _curWallType = MODEL;
    private boolean _mainDecoAdded = false;
    private BoundingBox _boundingBox = new BoundingBox();

    public WallTexture(String resourceId, String textureId, float mass) {
        super(resourceId);
        setTextureId(textureId);
        _mass = mass;
        init();
    }


    @Override
    public void init() {

        super.init();
        float wallLen = WALL_LENGTH;
        float wallHeight = WALL_HEIGHT;
        float wallWidth = WALL_WIDTH;
        int attr = VertexAttributes.Usage.Position
                | VertexAttributes.Usage.Normal
                | VertexAttributes.Usage.TextureCoordinates;

        setTextureId(_textureId);

        _materialTexture = new Material(_textureAttribute);
        _materialTexture.set(new IntAttribute(IntAttribute.CullFace, GL20.GL_FRONT_FACE));
        _model = createModel(wallLen, wallHeight, wallWidth, attr, _materialTexture);


        _modelInstancesMap = new HashMap();
        ModelInstance mI = new ModelInstance(_model);
        _modelInstancesMap.put(CUBE,  mI);
//        mI.transform.rotate(
//                new Vector3((float)Math.random(),(float) Math.random(),(float) Math.random())
//                        .nor(), (float)Math.random() * 100f);

//        btCollisionShape shape = Bullet.obtainStaticNodeShape(mI.nodes);
//        btCompoundShape shape = new btCompoundShape();
//        shape.addChildShape(new Matrix4().translate(0, WALL_HEIGHT/2, 0), cshape);

        Vector3 localInertia;
        if (_mass == 0)
            localInertia = Vector3.Zero;
        else {
            shape.calculateLocalInertia(_mass, tmpV);
            localInertia = tmpV;
        }
        btRigidBody.btRigidBodyConstructionInfo bodyInfo
                = new btRigidBody.btRigidBodyConstructionInfo(_mass, null, shape, localInertia);
        _btRigidBody = new btRigidBody(bodyInfo);
        _btRigidBody.setCollisionShape(shape);
        _btRigidBody.setRestitution(0.f);
        _btRigidBody.setFriction(1000);

        setModel(_resourceId, false);

        bodyInfo.dispose();
//        setCurWallType(CUBE);
//        updatePosition();
//        updateMode();
    }


    public void setCurWallType(WALL_TYPE wallType ) {
        _curWallType = wallType;
        setModelInstance( _modelInstancesMap.get(wallType));
    }
    /* removes decoration when empty string is passed in
     * when temp is set, it is rolled back when another wall is selected */
    public void setModel(String id, boolean temp) {
        if ( id.equals("") ) {
            if (!_mainDecoAdded) {
                setCurWallType( CUBE);
            }
            return;
        }

        _resourceId = id;
        _model = Models.instance().getModel(id, TYPE.WALL);
        ModelInstance newIns = new ModelInstance(_model);
        for ( Node node : newIns.nodes ) {
            node.translation.add(0, -WALL_HEIGHT / 2, 0);
            node.calculateTransforms(true);
        }
        _modelInstancesMap.put(MODEL, newIns);

        _textureAttribute = TextureAttribute.createDiffuse( _texture);
//        newIns.materials.add(new Material(_textureAttribute));
        setCurWallType( MODEL);
        if ( temp == false ) _mainDecoAdded = true;
        updatePosition();
        updateMode();
    }


    private Model createModel(float wallLen, float wallHeight, float wallWidth, int attr, Material material) {
        Model model = ObjectsManager.instance().getModel(_resourceId + _textureId);
        if (model != null ) {
            return model;
        }
        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        Vector3 fbl = new Vector3(-wallLen/2f, -wallHeight/2, wallWidth/2f);
        Vector3 fbr = new Vector3(wallLen/2f, -wallHeight/2, wallWidth/2f);
        Vector3 ftr = new Vector3(wallLen/2f, wallHeight/2, wallWidth/2f);
        Vector3 ftl = new Vector3(-wallLen/2f, wallHeight/2, wallWidth/2f);
        Vector3 bbl = new Vector3(-wallLen/2f, -wallHeight/2, -wallWidth/2f);
        Vector3 bbr = new Vector3(wallLen/2f, -wallHeight/2, -wallWidth/2f);
        Vector3 btr = new Vector3(wallLen/2f, wallHeight/2, -wallWidth/2f);
        Vector3 btl = new Vector3(-wallLen/2f, wallHeight/2, -wallWidth/2f);


        MeshPartBuilder meshPartBuilder;

        meshPartBuilder = modelBuilder.part("front", GL20.GL_TRIANGLES, attr, material);
        float texRate = _texture.getWidth() / _texture.getHeight();
        meshPartBuilder.setUVRange(0, 0, wallLen, wallHeight * texRate);
        meshPartBuilder.rect(fbl, fbr, ftr, ftl, new Vector3(0, 0, 1));

        meshPartBuilder = modelBuilder.part("right", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallWidth, wallHeight * texRate);
        meshPartBuilder.rect(fbr, bbr, btr, ftr, new Vector3(1, 0, 0));

        meshPartBuilder = modelBuilder.part("left", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallWidth, wallHeight * texRate);
        meshPartBuilder.rect(bbl,fbl,ftl,  btl,  new Vector3(-1, 0, 0));

        meshPartBuilder = modelBuilder.part("back", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallHeight * texRate);
        meshPartBuilder.rect(bbr, bbl, btl, btr, new Vector3(0, 0, -1));

        meshPartBuilder = modelBuilder.part("top", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallWidth * texRate);
        meshPartBuilder.rect(ftl, ftr, btr, btl, new Vector3(0, 0, -1));

        meshPartBuilder = modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallWidth * texRate);
        meshPartBuilder.rect(fbr,fbl, bbl, bbr, new Vector3(0, -1, 0));

        model = modelBuilder.end();
        ObjectsManager.instance().saveModel(_resourceId + _textureId, model);
        return model;
    }
    @Override
    public void updatePosition() {
        Matrix4 matrix = null;
        ModelInstance curInstance = _modelInstancesMap.get(_curWallType);
        matrix = curInstance.transform.idt();
        setTransformPosition(matrix);
        setTransformRotate(matrix);
        setTransformOffset(matrix);
        setTransformScale(matrix);
        _btRigidBody.setWorldTransform(matrix.cpy());
        curInstance.transform.set(matrix);
    }

    @Override
    protected void setTransformPosition(Matrix4 transform) {
        super.setTransformPosition(transform);
        transform.translate(0, WALL_HEIGHT/2f, 0);
    }
    @Override
    protected void setTransformOffset(Matrix4 transform) {
        if ( _isDiagonal ) {
            transform.translate(0, 0,  0/*+(Constants.WALL_WIDTH / 2)*/);
        } else {
            transform.translate(0, 0, -((Constants.WALL_LENGTH - Constants.WALL_WIDTH) / 2  ));
        }
    }

    @Override
    protected void setTransformScale(Matrix4 transform) {
        super.setTransformScale(transform);
        ModelInstance modelInstance = _modelInstancesMap.get(_curWallType);
        modelInstance.extendBoundingBox(_boundingBox);
//        float yScale = Constants.WALL_HEIGHT / _boundingBox.getHeight();
        transform.scale(1,1,1);
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        // todo:
        Gdx.gl.glLineWidth(3);
        ModelInstance instance = _modelInstancesMap.get(_curWallType);
        if ( instance != null )
        {
            modelBatch.render(instance, environment);
        }
        for ( Decoration decoration : _decorations ) {
            decoration.render(modelBatch, environment);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public Wall clone() {
        Wall wall = new WallTexture(_resourceId, _textureId, 1);
        wall.setDirection(_direction);
        wall.setDiagonal(_isDiagonal);
        wall.setPosition(_position.x, _position.z);
        wall.setPositionIndexes(_positionIndex._x, _positionIndex._y);
        wall.setElevation(_elevation);
        wall.setHeight(getHeight());
        wall.setSpanX(getSpanX());
        return wall;
    }
}
