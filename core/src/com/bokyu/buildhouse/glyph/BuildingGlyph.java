package com.bokyu.buildhouse.glyph;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.glyph.concrete.Ceiling;
import com.bokyu.buildhouse.glyph.concrete.Decoration;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.concrete.Roof;
import com.bokyu.buildhouse.glyph.concrete.WallTexture;
import com.bokyu.fireanddestroy.bullet.BulletWorld;
import com.bokyu.fireanddestroy.players.EnemyCharacter;
import com.bokyu.fireanddestroy.players.GoodCharacter;

import java.util.Set;

/**
 * Created by bokyu on 2016-07-06.
 */
public class BuildingGlyph extends CompositeGlyph {
    public enum TYPE
    {
        WALL,
        CEILING,
        ROOF,
        DECORATION,
        ENEMY_CHARACTER,
        GOOD_CHARACTER,
        ENEMY_PART
    }
    protected TYPE _type;
    protected Constants.DIRECTION _direction = Constants.DIRECTION.EAST;
    protected int _elevation = 0;
    protected int _height = 1;
    protected int _spanX = 1;
    protected int _spanY = 1;
    protected Vector3 _position = new Vector3();
    protected MyPosition _positionIndex = new MyPosition(0,0);
    public boolean _isDiagonal = false;
    public Building _building = null;
    protected final static Vector3 tmpV = new Vector3();
    protected float _mass;

    public BuildingGlyph(TYPE type, String resourceIdx) {
        _type = type;
        _resourceId = resourceIdx;
    }

    static public BuildingGlyph newInstance(
            BuildingGlyph.TYPE type,
            String textureId,
            String resourceId ,
            float weight)
    {
        switch ( type ) {
            case WALL:
                return new WallTexture(resourceId, textureId, weight);
            case CEILING:
                return new Ceiling(resourceId, textureId,weight);
            case DECORATION:
                return new Decoration(resourceId, textureId);
            case ROOF:
                return new Roof(resourceId, textureId, weight);
            case ENEMY_CHARACTER:
                return new EnemyCharacter(1);
            case  GOOD_CHARACTER:
                return new GoodCharacter(1);
        }
        return null;
    }

    public TYPE getType() { return _type; }
    public void setPosition(float x, float z)
    {
        _position.set(x,_position.y,z);
    }
    public Set<Constants.DIRECTION> getOccupyingDirs() {
        return null;
    }
    public void setElevation(int elevation) {
        _elevation = elevation;
        _position.y = _elevation * Constants.WALL_HEIGHT;
    }
    public void setDirection(Constants.DIRECTION direction) {
        _direction = direction;
    }
    public int getElevation() { return _elevation; }
    public int getHeight() { return _height; }
    public void setHeight(int height) { _height = height; }
    public void setSpanX(int span) { _spanX = span; }
    public int getSpanX() { return _spanX; }
    public void setSpanY(int span) { _spanY = span; }
    public int getSpanY() { return _spanY; }
    public void setPositionIndexes(MyPosition p) {
        _positionIndex = p;
        setPositionIndexes(p._x, p._y);
    }
    public void setPositionIndexes(int x, int y) {
        _positionIndex._x = x;
        _positionIndex._y = y;
        float halfEdge = DesignGround.METERS_PER_EDGE / 2;
        setPosition(x * Constants.WALL_LENGTH - halfEdge, -y * Constants.WALL_LENGTH + halfEdge);
    }
    public MyPosition getPositionIndex() {
        return _positionIndex;
    }
    public Constants.DIRECTION getDIrection() { return _direction; }
    protected void setTransformPosition(Matrix4 transform) {
        float halfEdge = DesignGround.METERS_PER_EDGE / 2;
        setPosition(_positionIndex._x * Constants.WALL_LENGTH - halfEdge,
                -_positionIndex._y * Constants.WALL_LENGTH + halfEdge);
        transform.translate(
                _position.cpy().add(
                        Constants.WALL_LENGTH / 2f,
                        0,
                        -Constants.WALL_LENGTH / 2f
                ));
        if ( _building != null ) {
            MyPosition buildingPos = _building.getPosition();
            if ( buildingPos._x >= 0 ) {
                transform.translate(
                        (buildingPos._x ) * Constants.WALL_LENGTH,
                        0,
                        -(buildingPos._y ) * Constants.WALL_LENGTH
                );
            }
            transform.translate(0,_building._elevation ,0);
        }
    }
    protected void setTransformOffset(Matrix4 transform) {
        if ( _isDiagonal ) {
            transform.translate(0, 0,  0/*+(Constants.WALL_WIDTH / 2)*/);
        } else {
            transform.translate(0, 0, -(Constants.WALL_LENGTH / 2 * 1.01f ));
        }
    }

    protected float getRotationAmount(Constants.DIRECTION direction ) {
        switch (direction)
        {
            case NORTH:
                return 0;
            case EAST:
                return 90;
            case SOUTH:
                return 180;
            case WEST:
                return 270;
            case NE:
                return 45;
            case SE:
                return 90 + 45;
            case SW:
                return 180 + 45;
            case NW:
                return 270 + 45;
        }
        return 0;
    }
    protected void  setTransformRotate(Matrix4 transform) {
        float rotationAmount = getRotationAmount(_direction);
        transform.rotate(0, -1, 0, rotationAmount);
    }

    public void updatePosition() {
    }

    public void rotateInBuilding() {
        int x =_positionIndex._y;
        int y = DesignGround.METERS_PER_EDGE - 1 - _positionIndex._x;
        setPositionIndexes(x,y);
        _direction = MyHelper.getRotationDir(_direction);
        updatePosition();
    }

    public BuildingGlyph clone() {
        return null;
    }

    public void onAddToWorld(BulletWorld bulletWorld) {

    }

    public void onRemoveFromWorld(BulletWorld bulletWorld ) {

    }
}
