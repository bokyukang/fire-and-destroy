package com.bokyu.buildhouse.glyph;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by bokyu on 2016-07-06.
 */
public class CompositeGlyph extends MyGlyph {


    protected Set<MyGlyph> _glyphs = new HashSet<MyGlyph>();
    public boolean _isComosite = false;

    protected CompositeGlyph() {
    }


    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        for ( MyGlyph glyph : _glyphs ) {
            glyph.render(modelBatch, environment);
        }
    }

    @Override
    public void setMode(MODE mode) {
        super.setMode(mode);
        for ( MyGlyph glyph : _glyphs ) {
            glyph.setMode(mode);
        }
    }

    @Override
    public void setColor(Color color) {
        super.setColor(color);
        for ( MyGlyph glyph : _glyphs ) {
            glyph.setColor(color);
        }
    }

    public MyGlyph addGlyph(MyGlyph glyph ) {
        _glyphs.add(glyph);
        return glyph;
    }

    public Set<MyGlyph> getGlyphs() { return _glyphs; }

    public boolean removeGlyph(MyGlyph glyph) {
        if ( _glyphs.contains(glyph )) {
            _glyphs.remove(glyph);
            return true;
        }
        return false;
    }
}
