package com.bokyu.buildhouse.glyph.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.bokyu.buildhouse.data.TextureDataContainer;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.concrete.Ceiling;
import com.bokyu.buildhouse.glyph.concrete.Decoration;
import com.bokyu.buildhouse.glyph.concrete.Roof;
import com.bokyu.buildhouse.glyph.concrete.WallTexture;
import com.bokyu.buildhouse.network.Network;
import com.bokyu.fireanddestroy.players.Character;
import com.bokyu.fireanddestroy.players.EnemyCharacter;
import com.bokyu.fireanddestroy.players.GoodCharacter;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2016-12-01.
 */

public class BuildingsManager {
    static private BuildingsManager _instance = null;
    private Map<Integer, Building> _buildings = new HashMap();
    private List<Integer> _buildingKeys = new ArrayList();



    static public BuildingsManager instance() {
        if ( _instance == null ) {
            _instance = new BuildingsManager();
        }
        return _instance;
    }

    public void init() {
        FileHandle dirHandle;
        dirHandle = Gdx.files.internal("level");
        int count = 0;
        for ( FileHandle entry : dirHandle.list() ) {
            try {
                byte[] bytes = entry.readBytes();
                Network.building_model pbBuilding = Network.building_model.parseFrom(bytes);
                Building building = new Building();
                building.init();
                building.parse(pbBuilding);
                building.setName(entry.name());
                building._modelId = 100 +count;
                addBuilding(building);
                count++;
            } catch (InvalidProtocolBufferException e ) {
                e.printStackTrace();
            }

        }
    }

    public void addBuilding(Building building) {
        _buildings.put(building._modelId, building);
    }

    public void removeBuilding(Building building) {
        _buildings.remove(building);
    }

    public  Map<Integer, Building> getBuildings() { return _buildings; }

    public void updateBuildingKeys() {
        _buildingKeys.clear();
        _buildingKeys.addAll(_buildings.keySet());
        Collections.sort(_buildingKeys);
    }

    public List<Integer> getBuildingKeys() {
        updateBuildingKeys();
        return _buildingKeys;
    }

    public void updateBuildingsFromDb() {
//        for ( Building building : _buildings.values() ) {
//            building.dispose();
//        }
//        _buildings.clear();
        for ( Building building : Configuration.instance().getFactory().getDbManager().loadBuildingModels() ) {
            addBuilding(building);
        }
        updateBuildingKeys();
    }

    public BuildingGlyph generateGlyph(BuildingGlyph.TYPE type, String resourceId, String textureId) {
        switch ( type ) {
            case WALL:
                String modelType = TextureDataContainer.instance()._textureDataMap.get(textureId).model_type;
                float mass = Float.parseFloat(TextureDataContainer.instance()._textureDataMap.get(textureId).mass);
                if ( modelType.equals("texture")) {
                    return new WallTexture(resourceId, textureId, mass);
                }
                else if ( modelType.equals("model")) {
                    return null;
                }
                break;
            case CEILING:
                return new Ceiling(resourceId, textureId,1);

            case ROOF:
                Roof roof = new Roof(resourceId, textureId, 2);
                return roof;
            case DECORATION:
                Decoration decoration = new Decoration(resourceId, textureId);
                return decoration;
            case ENEMY_CHARACTER:
                EnemyCharacter enemyCharacter = new EnemyCharacter(1);//resourceId, textureId);
                return enemyCharacter;
            case GOOD_CHARACTER:
                GoodCharacter goodCharacter = new GoodCharacter(1);//resourceId, textureId);
                return goodCharacter;

            default:
                Gdx.app.log("error", "generateGlyph");

        }
        return null;
    }


}
