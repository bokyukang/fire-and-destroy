package com.bokyu.buildhouse.glyph;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.physics.bullet.collision.btCollisionObject;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.bokyu.buildhouse.etc.ResourceManager;

import java.util.HashSet;
import java.util.Set;


/**
 * Created by bokyu on 2016-07-06.
 */
public class MyGlyph {

    public enum TYPE {
        ROAD
    }

    public enum MODE
    {
        WIREFRAME,
        COLOR,
        NORMAL
    }

    static public String FIXED_MATERIAL = "fixed";
    static public String FIXED_MATERIAL_TEXTURE = "fixed_tex";

    protected Texture _texture = null;
    protected boolean _dirty = false;
    protected int _id = -1;
    protected float _transparency = 1;
    protected btCollisionObject _btRigidBody = new btCollisionObject();
    protected Color _color = Color.GRAY;
    protected MODE _mode = MODE.NORMAL;
    protected ModelInstance _modelInstance = null;
    protected ColorAttribute _colorAttribute = null;
    protected TextureAttribute _textureAttribute = null;
    protected TextureAttribute _textureAttributeNormal = null;
    protected TextureAttribute _textureAttributeSpecular = null;
    protected TextureAttribute _textureAttributeAmbient = null;
    protected BlendingAttribute _blendingAttribute = null;
    protected String _resourceId = "";
    protected String _textureId = "";

    public MyGlyph() {
    }
    public void init() {
        setColor(Color.RED);
//        _btRigidBody.userData = this;
        _blendingAttribute = new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f);
    }

    public String getResourceId() { return _resourceId; }
    public String getTextureId() { return _textureId; }
    public int getId() { return _id; }
    public void setId(int id) { _id = id; }

    public void setTransparency(float trans) { _transparency = trans; }

    public void render(ModelBatch modelBatch, Environment environment) {}

    public void setMode(MODE mode) {
        _mode = mode;
        updateMode();
    }

    public void setColor(Color color) {
        _color = color;
        _colorAttribute = ColorAttribute.createDiffuse(_color.r, _color.g, _color.b, _color.a);
    }

    public void updateMode() {
        if ( _modelInstance != null )
            applyMode(_modelInstance);
    }

    public void setModelInstance(ModelInstance modelInstance) {
        _modelInstance = modelInstance;
        updateMode();
    }

    public void applyMode(ModelInstance modelInstance) {
        for (Node node : modelInstance.nodes) {
            for (NodePart nodePart : node.parts ) {
                if (_mode == MODE.WIREFRAME) {
                    nodePart.meshPart.primitiveType = GL20.GL_LINE_STRIP;
                } else {
                    nodePart.meshPart.primitiveType = GL20.GL_TRIANGLES;
                }
                nodePart.meshPart.update();
            }
        }
        for ( Material material : modelInstance.materials ) {
            if ( !material.id.contains(FIXED_MATERIAL))
            {
                if (_mode == MODE.COLOR) {
                    material.set(_colorAttribute);
                    if (_textureAttribute != null) material.set(_textureAttribute);
                } else if (_mode == MODE.WIREFRAME) {
                    material.set(_colorAttribute);
                    material.remove(TextureAttribute.Diffuse);
                } else if (_mode == MODE.NORMAL) {
                    material.remove(ColorAttribute.Diffuse);
                    if (_textureAttribute != null) material.set(_textureAttribute);
                    if ( _textureAttributeNormal != null ) material.set(_textureAttributeNormal);
                    if ( _textureAttributeSpecular != null ) material.set(_textureAttributeSpecular);
                    if ( _textureAttributeAmbient != null ) material.set(_textureAttributeAmbient);
                }
            } else if ( material.id.contains(FIXED_MATERIAL_TEXTURE))
            {
                material.set(_blendingAttribute);
            }
        }
    }

    public void setTextureId(String id) {
        _textureId = id;

        if ( id.equals("")) return;

        String textureName = _textureId + ".gif";
        Texture texture = ResourceManager.instance().getTexture("textures/" + textureName);
        _texture = texture;
        _textureAttribute = TextureAttribute.createDiffuse(_texture);
        _textureAttributeNormal = TextureAttribute.createNormal(_texture);
        _textureAttributeSpecular = TextureAttribute.createSpecular(_texture);
        _textureAttributeAmbient = TextureAttribute.createAmbient(_texture);
//        try {
//            Texture textureNormal = new Texture("textures/" + _resourceId + "_normal.png");
//        } catch ( GdxRuntimeException e ) {}
        updateMode();
    }

    public MODE getMode() {
        return _mode;
    }

    public void dispose() {
        if ( _btRigidBody != null ) _btRigidBody.dispose();
    }

    public btCollisionObject getCollisionObject() { return _btRigidBody; }

    public ModelInstance getModelInstance() { return _modelInstance; }

}
