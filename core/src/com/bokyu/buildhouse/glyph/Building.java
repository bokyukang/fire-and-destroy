package com.bokyu.buildhouse.glyph;

import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.network.Network;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by bk on 2016-11-23.
 */

public class Building extends CompositeGlyph {
    protected String _name = "";
    protected Set<MyPosition> _occupyingPositions = new HashSet();
    public MyPosition _position = new MyPosition(-1,-1);
    public float _elevation = 0;
    protected int _rotation = 0;
    public int _modelId = 0;
    public int _goodCharacCount = 0;
    public int _enemyCount = 0;

    public Building() {
        super();
    }

    public Building(String name, Set<MyPosition> occupyingPositions) {
        super();
        _name = name;
        for ( MyPosition p : occupyingPositions ) {
            _occupyingPositions.add(p.clone());
        }
    }
    public String getName() { return _name; }

    public void parse(Network.building_model pbBuilding) {
        _goodCharacCount = _enemyCount = 0;
//        setId(pbBuilding.getId());
        setName(pbBuilding.getName());
        _modelId = pbBuilding.getId();
        for (Network.building_model.building_glyph pbGlyph : pbBuilding.getGlyphsList() )
        {
            BuildingGlyph buildingGlyph = BuildingsManager.instance().generateGlyph(
                    BuildingGlyph.TYPE.values()[pbGlyph.getType()],
                    pbGlyph.getResourceId(),
                    pbGlyph.getTextureId()
            );
            buildingGlyph.setElevation(pbGlyph.getElevation());
            buildingGlyph.setPositionIndexes(pbGlyph.getX(), pbGlyph.getY());
            buildingGlyph.setDirection(Constants.DIRECTION.values()[pbGlyph.getDirection()]);
            addGlyph(buildingGlyph);
            if ( buildingGlyph.getType() == BuildingGlyph.TYPE.GOOD_CHARACTER ) {
                _goodCharacCount++;
            } else if ( buildingGlyph.getType() == BuildingGlyph.TYPE.ENEMY_CHARACTER ) {
                _enemyCount++;
            }
        }
        updatePosition();
    }

    public MyPosition getPosition() { return _position; }

    public void setName(String name) {
        _name = name;
    }

    @Override
    public MyGlyph addGlyph(MyGlyph glyph ) {
        if ( glyph instanceof BuildingGlyph ) {
            BuildingGlyph bGlyph = (BuildingGlyph)glyph;
            _glyphs.add(bGlyph);
            bGlyph._building = this;
            if ( !_occupyingPositions.contains( bGlyph ) ) {
                _occupyingPositions.add(
                        bGlyph.getPositionIndex().clone()
                );
            }
            if ( glyph instanceof CompositeGlyph ) {
                CompositeGlyph cG = (CompositeGlyph)glyph;
                for (MyGlyph cGlyph : cG.getGlyphs()) {
                    addGlyph(cGlyph);
                }
            }
        }
        return glyph;
    }

    public boolean removeGlyph(MyGlyph glyph) {
        if ( glyph instanceof BuildingGlyph ) {
            BuildingGlyph bGlyph = (BuildingGlyph) glyph;
            _occupyingPositions.remove(bGlyph.getPositionIndex());
            bGlyph._building = null;
        }

        return super.removeGlyph(glyph);
    }

    public Set<MyPosition> getOccupyingPositions() {
        return _occupyingPositions;
    }

    public void rotate() {
        for ( MyGlyph glyph : _glyphs ) {
            ((BuildingGlyph)glyph).rotateInBuilding();
        }
        for ( MyPosition p : _occupyingPositions ) {
            int y =p._y;
            p._y = DesignGround.METERS_PER_EDGE - 1 - p._x;
            p._x = y;
        }
        _rotation = (_rotation + 1) % 4;
    }

    public void rotate(int count) {
        for ( int i=0; i < count; ++i ) rotate();
    }

    public int getRotation() { return _rotation; }

//    @Override
//    public void setColor(Color color) {
//        super.setColor(color);
//        if ( _glyphs != null ) {
//            for (MyGlyph glyph : _glyphs) {
//                BuildingGlyph bG = (BuildingGlyph) glyph;
//                bG.setColor(color);
//            }
//        }
//    }

//    @Override
//    public void setMode(MODE mode) {
//        super.setMode(mode);
//        for ( MyGlyph glyph : _glyphs ) {
//            BuildingGlyph bG = (BuildingGlyph)glyph;
//            bG.setMode(mode);
//        }
//    }

    public Building clone() {
        Building building = new Building(getName(), _occupyingPositions);
        for ( MyGlyph glyph : _glyphs ) {
            BuildingGlyph bGlyph = (BuildingGlyph)glyph;
            BuildingGlyph newGlyph = bGlyph.clone();
            newGlyph._building = building;
            building._glyphs.add(newGlyph);
        }
        building._elevation = _elevation;
        building._position._x  = _position._x;
        building._position._y  = _position._y;
        building._id = _id;
        building._rotation = _rotation;
        building._modelId = _modelId;
        return building;
    }

    public void updatePosition() {
        for ( MyGlyph glyph : _glyphs) {
            ((BuildingGlyph)glyph).updatePosition();
        }
    }

    @Override
    public void dispose() {
        for ( MyGlyph gl : _glyphs ) {
            gl.dispose();
        }
    }

    public Network.building_model toPB() {
        Network.building_model.Builder buildingModelBuilder = Network.building_model.newBuilder();
        Network.building_model.building_glyph.Builder buildingGlyphBuilder = Network.building_model.building_glyph.newBuilder();
        buildingModelBuilder.clear();
        buildingModelBuilder.setId( getId())
                .setName(getName());
        for ( MyGlyph glyph : _glyphs ) {
            buildingGlyphBuilder.clear();
            BuildingGlyph buildingGlyph = (BuildingGlyph) glyph;
            buildingGlyphBuilder
                    .setType(buildingGlyph.getType().ordinal())
                    .setResourceId(buildingGlyph.getResourceId())
                    .setDirection(buildingGlyph.getDIrection().ordinal())
                    .setDiagonal(buildingGlyph._isDiagonal)
                    .setElevation(buildingGlyph.getElevation())
                    .setX(buildingGlyph.getPositionIndex()._x)
                    .setY(buildingGlyph.getPositionIndex()._y)
                    .setTextureId(buildingGlyph.getTextureId());

            buildingModelBuilder.addGlyphs(buildingGlyphBuilder.build());
        }
        return buildingModelBuilder.build();
    }

}
