package com.bokyu.buildhouse.glyph.graphics;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodePart;

/**
 * Created by bk on 2016-12-30.
 */

public class ModelInstanceColor extends ModelInstance {
    public ModelInstanceColor(final Model model) {
        super(model);
    }

    public Renderable getRenderable(final Renderable out, final Node node,
                                    final NodePart nodePart) {
        super.getRenderable(out, node, nodePart);

        out.material.set( new ColorAttribute(ColorAttribute.Diffuse,1,.4f,.4f,1) );
        return out;
    }
}
