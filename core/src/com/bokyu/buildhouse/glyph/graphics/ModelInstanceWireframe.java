package com.bokyu.buildhouse.glyph.graphics;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.graphics.g3d.model.NodePart;

/**
 * Created by bk on 2016-12-27.
 */

public class ModelInstanceWireframe extends ModelInstance {
    public ModelInstanceWireframe (final Model model) {
        super(model);
    }

    public Renderable getRenderable(final Renderable out, final Node node,
                                    final NodePart nodePart) {
        super.getRenderable(out, node, nodePart);
        out.meshPart.primitiveType = GL20.GL_LINE_STRIP;
        return out;
    }
}
