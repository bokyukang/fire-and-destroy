package com.bokyu.buildhouse.glyph;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.bullet.collision.ClosestRayResultCallback;
import com.badlogic.gdx.physics.bullet.collision.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.collision.btCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.collision.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.collision.btCollisionWorld;
import com.badlogic.gdx.physics.bullet.collision.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.collision.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.events.EventObjectPicked;
import com.bokyu.buildhouse.glyph.concrete.Ceiling;
import com.bokyu.buildhouse.glyph.concrete.Roof;
import com.bokyu.buildhouse.glyph.concrete.Wall;
import com.bokyu.fireanddestroy.players.Character;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by bokyu on 2016-08-18.
 */
public class Ground extends MyGlyph {
    protected Map<Integer,
            Map<Integer,
            Map<Constants.DIRECTION,
            Set<BuildingGlyph>>>> _glyphMap = new HashMap();
    protected Set<MyGlyph> _children = new HashSet();
    protected btCollisionConfiguration _btCollisionConfigurationObjects = null;
    protected btCollisionDispatcher _btDispatcherObjects = null;
    protected btBroadphaseInterface _btBroadphaseObjects = null;
    protected btConstraintSolver _btSolverObjects = null;
    protected btCollisionWorld _btCollisionWorldObjects = null;
    protected ClosestRayResultCallback _btRayCallback = new ClosestRayResultCallback(Vector3.Zero, Vector3.Z);

    public Ground() {
        _btCollisionConfigurationObjects = new btDefaultCollisionConfiguration();
        _btDispatcherObjects = new btCollisionDispatcher(_btCollisionConfigurationObjects);
        _btBroadphaseObjects = new btDbvtBroadphase();
        _btSolverObjects = new btSequentialImpulseConstraintSolver();
        _btCollisionWorldObjects = new btDiscreteDynamicsWorld(
                _btDispatcherObjects,
                _btBroadphaseObjects,
                _btSolverObjects,
                _btCollisionConfigurationObjects);
    }

    public float getTotalWidth() { return 0; }
    public float getTotalHeight() { return 0; }

    public int getElevation(int x, int y, Set<Constants.DIRECTION> directionSet, BuildingGlyph glyph) {
        int elev = 0;
        for (int spanIdx = 0; spanIdx < glyph.getSpanX(); ++ spanIdx )
        {
            MyPosition offset = MyHelper.getWallSpanDir(glyph.getDIrection(), spanIdx);

            elev = Math.max(elev,
                    getElevation(x + offset._x, y + offset._y, directionSet));
        }
        return elev;
    }

    public int getElevation(int x, int y, Set<Constants.DIRECTION> directionSet,
                            Constants.DIRECTION dir, int spanx, int spany) {
//        Constants.DIRECTION dir = ceiling.getDIrection();
        int highest = 0;

        for ( int i=0; i < spanx; ++i ) {
            for ( int j=0;j < spany; ++j ) {
                MyPosition offsetX = MyHelper.getCeilingSpanDir(dir, i);
                MyPosition offsetY = MyHelper.getCeilingSpanDir(MyHelper.getRotationDir(dir), j);
                int ele =  getElevation(x + offsetX._x  + offsetY._x,
                        y + offsetX._y + offsetY._y, directionSet) ;

                highest = Math.max(highest, ele);
            }
        }
        return highest;
    }

    /* takes care of multi set */
    public int getElevation(int x, int y, Set<Constants.DIRECTION> directionSet) {
        int max = 0;
        for (Constants.DIRECTION direction : directionSet ) {
            max = Math.max(max, getElevation(x,y, direction));
        }
        max = Math.max(max, getElevation2(x,y, Constants.DIRECTION.ALL));
        return max;
    }

    /* takes care of ALL input direction */
    public int getElevation(int x, int y, Constants.DIRECTION direction) {
        int max  = 0;
        if ( direction == Constants.DIRECTION.ALL ) {
            for (Constants.DIRECTION eachD : Constants.DIRECTION.values()) {
                if ( eachD != Constants.DIRECTION.ALL ) {
                    max = Math.max(max, getElevation(x, y, eachD));
                }
            }
            return max;
        }
        return getElevation2(x,y, direction);
    }

    /* actual checker */
    public int getElevation2(int x, int y, Constants.DIRECTION direction) {
        int max = 0;
        Set<BuildingGlyph> glyphs = null;
        try {
            glyphs = _glyphMap.get(x).get(y).get(direction);
        } catch ( Exception e ) {
            e.printStackTrace();
            return 0;
        }
        if ( glyphs == null ) return 0;

        for ( BuildingGlyph glyph : glyphs ) {
            max = Math.max(max, glyph.getElevation() + glyph.getHeight());
        }
        return max;
    }

    public <T extends BuildingGlyph> BuildingGlyph getTopGlyph(
            int x, int y, Constants.DIRECTION direction, Class<T> type) {
        Set<BuildingGlyph> glyphs = null;
        try {
            glyphs = _glyphMap.get(x).get(y).get(direction);
        } catch ( Exception e ) {
            e.printStackTrace();
            return null;
        }

        BuildingGlyph returnVal = null;
        int topLevel = -1;
        for ( BuildingGlyph glyph : glyphs ) {
            if ( type.isInstance(glyph) && glyph._elevation > topLevel ) {
                topLevel = glyph._elevation;
                returnVal = glyph;
            }
        }
        return returnVal;
    }

    public void addWall( int idxX, int idxY, Wall wall ) {
        wall.setPositionIndexes(idxX, idxY);
        _children.add(wall);
        _btCollisionWorldObjects.addCollisionObject(wall.getCollisionObject());

        for (int span = 0; span < wall.getSpanX(); ++span ) {
            addWallEachSpan(idxX, idxY, wall, span);
        }
    }

    protected void addWallEachSpan( int idxX, int idxY, Wall wall, int spanIdx)
    {
        MyPosition offset = MyHelper.getWallSpanDir(wall.getDIrection(), spanIdx);
        Set<Constants.DIRECTION> dirs = wall.getOccupyingDirs();
        for ( Constants.DIRECTION dir : dirs ) {
            _glyphMap.get(idxX + offset._x).get(idxY + offset._y).get(dir).add(wall);
        }

    }

    public void removeWall(final Wall wall, int offsetX, int offsetY) {
        MyPosition indexes = wall.getPositionIndex();
        for (int span = 0; span < wall.getSpanX(); ++span ) {
            MyPosition offset = MyHelper.getWallSpanDir(wall.getDIrection(), span);
            _glyphMap.get(indexes._x + offset._x)
                    .get(indexes._y + offset._y)
                    .get(wall.getDIrection())
                    .remove(wall);
        }

        _children.remove(wall);
        _btCollisionWorldObjects.removeCollisionObject(wall.getCollisionObject());
    }

    public void addCeiling(int x, int y, final Ceiling ceiling) {
        ceiling.setPositionIndexes(x,y);
        for ( int xspan =0; xspan < ceiling.getSpanX(); ++xspan ) {
            for ( int yspan =0; yspan < ceiling.getSpanY(); ++yspan ) {
                addCeiling(x,y, ceiling, xspan, yspan);
            }
        }
        _children.add(ceiling);
    }

    public void addCeiling(int x, int y, final Ceiling ceiling, int spanx, int spany) {

        Constants.DIRECTION dir = ceiling.getDIrection();
        MyPosition offsetX = MyHelper.getCeilingSpanDir(dir, spanx);
        MyPosition offsetY = MyHelper.getCeilingSpanDir(MyHelper.getRotationDir(dir), spany);
        for (Constants.DIRECTION direction : ceiling.getOccupyingDirs()) {
            _glyphMap.get(x + offsetX._x + offsetY._x)
                    .get(y  + offsetX._y + offsetY._y)
                    .get(direction).add(ceiling);
        }
    }

    public void removeCeiling(final Ceiling ceiling, int offsetX, int offsetY) {
        MyPosition indexes = ceiling.getPositionIndex();
        for (Constants.DIRECTION direction : ceiling.getOccupyingDirs()) {
            _glyphMap.get(indexes._x).get(indexes._y).get(direction).remove(ceiling);
        }
        _children.remove(ceiling);
    }

    public void addCharacter(int x, int y, final Character roof) {
        roof.setPositionIndexes(x,y);
        for (Constants.DIRECTION direction : roof.getOccupyingDirs()) {
            _glyphMap.get(x).get(y).get(direction).add(roof);
        }
        _children.add(roof);
    }
    public void addRoof(int x, int y, final Roof roof) {
        roof.setPositionIndexes(x,y);
//        for (Constants.DIRECTION direction : roof.getOccupyingDirs()) {
            _glyphMap.get(x).get(y).get(Constants.DIRECTION.ALL).add(roof);
//        }
        _children.add(roof);
    }

    public void removeCharacter(final Character roof, int offsetX, int offsetY) {
        MyPosition indexes = roof.getPositionIndex();
        for (Constants.DIRECTION direction : roof.getOccupyingDirs()) {
            _glyphMap.get(indexes._x).get(indexes._y).get(direction).remove(roof);
        }
        _children.remove(roof);
    }
    public void removeRoof(final Roof roof, int offsetX, int offsetY) {
        MyPosition indexes = roof.getPositionIndex();
        for (Constants.DIRECTION direction : roof.getOccupyingDirs()) {
            _glyphMap.get(indexes._x).get(indexes._y).get(direction).remove(roof);
        }
        _children.remove(roof);
    }

    public void doMousePickObject(PerspectiveCamera cam, int xCoord, int yCoord) {
        Ray ray = cam.getPickRay(xCoord, yCoord);
        Vector3 vRayFrom = ray.origin;
        Vector3 vRayTo = ray.origin.cpy().add( ray.direction.cpy().scl( cam.far ));

        _btRayCallback.setCollisionObject(null);
        _btRayCallback.setRayFromWorld(vRayFrom);
        _btRayCallback.setRayToWorld(vRayTo);
        _btCollisionWorldObjects.rayTest(vRayFrom, vRayTo, _btRayCallback);
        _btRayCallback.setClosestHitFraction(1f);

        if ( _btRayCallback.hasHit() )
        {
            Object userData = _btRayCallback.getCollisionObject().userData;
            if ( userData != null ) {
                EventBus.getDefault().post(new EventObjectPicked(userData));
            }
        }
    }

    public List<Integer> getNeighborCeilingHeights(int x, int y, Set<Constants.DIRECTION> occupyingDirs) {
        Set<Integer> resultSet = new HashSet();
        Integer[] offsets = { -1, 0, 1, 0, 0, -1, 0, 1 };
        int minHeight = getElevation(x,y, occupyingDirs);

        for ( int i=0; i < offsets.length; i += 2 ) {
            int nX = x + offsets[i];
            int nY = y + offsets[i+1];

            if ( nX < 0 || nY < 0 || nX >= _glyphMap.size() || nY >= _glyphMap.get(nX).size()) continue;

            Constants.DIRECTION occDir = MyHelper.getDirFromOffset(offsets[i], offsets[i+1]);
            if ( occupyingDirs.contains(occDir ) == false ) continue;

            Constants.DIRECTION nDir = MyHelper.getRotationDir(occDir, 2);

            for ( BuildingGlyph glyph : _glyphMap.get(nX).get(nY).get(nDir) )
            {
                resultSet.add(glyph._elevation + glyph.getHeight());
            }
        }

        List<Integer> result =  new ArrayList(resultSet);
        Collections.sort(result);

        return result;
    }
}
