package com.bokyu.buildhouse.shaders;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.utils.BaseShaderProvider;

/**
 * Created by bk on 2017-01-26.
 */

public class NormalShaderProvider extends BaseShaderProvider {
    public final NormalShader.Config config;

    public NormalShaderProvider (final NormalShader.Config config) {
        this.config = (config == null) ? new NormalShader.Config() : config;
    }

    public NormalShaderProvider (final String vertexShader, final String fragmentShader) {
        this(new NormalShader.Config(vertexShader, fragmentShader));
    }

    public NormalShaderProvider (final FileHandle vertexShader, final FileHandle fragmentShader) {
        this(vertexShader.readString(), fragmentShader.readString());
    }

    public NormalShaderProvider () {
        this(null);
    }

    @Override
    protected Shader createShader (final Renderable renderable) {
        return new NormalShader(renderable, config);
    }
}
