package com.bokyu.buildhouse.data.data_types;

import com.bokyu.buildhouse.glyph.concrete.Terrein;

/**
 * Created by bk on 2017-01-09.
 */

public class MyPosition {
    public int _x, _y;

    public MyPosition(int x, int y) {
        _x = x;
        _y = y;
    }

    public MyPosition clone() { return new MyPosition(_x, _y); }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyPosition) {
            MyPosition mObj = (MyPosition)obj;
            if ( mObj._x == _x && mObj._y == _y ) {
                return true;
            }
        }
        return false;
    }
    @Override
    public int hashCode() {
        return _x * Terrein.ITEMS_PER_EDGE +  _y;
    }
}
