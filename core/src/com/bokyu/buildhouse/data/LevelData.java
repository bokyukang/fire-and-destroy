package com.bokyu.buildhouse.data;

/**
 * Created by bk on 2016-11-07.
 */

public class LevelData extends MyData {

    public int[] balls;
    public String[] buildings;
    public float[] sky_color;
    public float[] sky_color_2;
    public String floor;
    public float floor_uv_scale;
    public int max_enemy_alive;
    public int min_good_alive;
    public float cam_distance;
    public String[] descriptions;
    public boolean needs_unlock = false;

    static public String getBallName(int ballIdx ) {
        switch ( ballIdx ) {
            case 0:
                return "black_ball";
            case 1:
                return "red_ball";
            case 2:
                return "green_ball";
        }
        return null;
    }
}
