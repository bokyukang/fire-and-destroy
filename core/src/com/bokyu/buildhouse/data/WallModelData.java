package com.bokyu.buildhouse.data;

/**
 * Created by bk on 2017-01-04.
 */

public class WallModelData {
    public String id;
    public String title;
    public String type;
    public String span;
    public String height;
}
