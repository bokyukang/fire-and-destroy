package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-01-04.
 */

public class WallModelDataContainer {
    static private WallModelDataContainer _instance = null;
    public Map<String, WallModelData> _wallModelDataMap;
    public List<String> _keyList;

    static public WallModelDataContainer instance() {
        if ( _instance == null ) _instance = new WallModelDataContainer();
        return _instance;
    }

    public void setJsonData(WallModelDataContainerJson jsonData) {
        _wallModelDataMap = new HashMap();
        for ( WallModelData wallModelData : jsonData.wallModelData) {
            _wallModelDataMap.put(wallModelData.id, wallModelData);
        }
        _keyList = new ArrayList(_wallModelDataMap.keySet());
        Collections.sort(_keyList);
    }
}
