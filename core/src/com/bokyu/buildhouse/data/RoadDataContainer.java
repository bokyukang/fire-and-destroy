package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-02-01.
 */

public class RoadDataContainer {
    static private RoadDataContainer _instance = null;
    public Map<String, RoadData> _roadDataMap;
    public List<String> _keyList = null;

    static public RoadDataContainer instance() {
        if ( _instance == null ) _instance = new RoadDataContainer();
        return _instance;
    }

    public void setJsonData(RoadDataContainerJson jsonData) {
        _roadDataMap = new HashMap();
        for ( RoadData roadData: jsonData.roadData) {
            _roadDataMap.put(roadData.id, roadData);
        }
        _keyList = new ArrayList(_roadDataMap.keySet());
        Collections.sort(_keyList);
    }
}
