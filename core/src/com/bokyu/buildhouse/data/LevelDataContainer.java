package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2016-12-22.
 */

public class LevelDataContainer {
    static private LevelDataContainer _instance = null;
    public List<LevelData> _levelData;


    static public LevelDataContainer instance() {
        if ( _instance == null ) _instance = new LevelDataContainer();
        return _instance;
    }

    public void setJsonData(LevelDataContainerJson jsonData) {
        _levelData = new ArrayList();
        for ( LevelData levelData : jsonData.levelData) {
            _levelData.add(levelData);
        }
    }
}
