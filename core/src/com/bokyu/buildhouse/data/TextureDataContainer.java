package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2016-12-22.
 */

public class TextureDataContainer {
    static private TextureDataContainer _instance = null;
    public Map<String, TextureData> _textureDataMap;
    public List<String> _keyList = null;


    static public TextureDataContainer instance() {
        if ( _instance == null ) _instance = new TextureDataContainer();
        return _instance;
    }

    public void setJsonData(TextureDataContainerJson jsonData) {
        _textureDataMap = new HashMap();
        for ( TextureData textureData : jsonData.textureData) {
            _textureDataMap.put(textureData.id, textureData);
        }
        _keyList = new ArrayList(_textureDataMap.keySet());
        Collections.sort(_keyList);
    }
}
