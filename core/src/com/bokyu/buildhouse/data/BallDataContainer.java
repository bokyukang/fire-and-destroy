package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2016-12-22.
 */

public class BallDataContainer {
    static private BallDataContainer _instance = null;
    public Map<String, BallData> _ballDataMap;
    public List<String> _keyList = null;


    static public BallDataContainer instance() {
        if ( _instance == null ) _instance = new BallDataContainer();
        return _instance;
    }

    public void setJsonData(BallDataContainerJson jsonData) {
        _ballDataMap = new HashMap();
        for ( BallData ballData : jsonData.ballData) {
            _ballDataMap.put(ballData.id, ballData);
        }
        _keyList = new ArrayList(_ballDataMap.keySet());
        Collections.sort(_keyList);
    }
}
