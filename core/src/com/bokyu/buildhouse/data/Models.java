package com.bokyu.buildhouse.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.UBJsonReader;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.BuildingGlyph;

import java.util.HashMap;
import java.util.Map;

import static com.badlogic.gdx.graphics.GL20.GL_FRONT_FACE;

/**
 * Created by bk on 2017-01-04.
 */

public class Models {

    static private Models _instance = null;
    private Map<String, Model> _models = new HashMap();
    private ModelLoader _modelLoader = null;

    static public Models instance() {
        if ( _instance == null ) {
            _instance = new Models();
        }
        return _instance;
    }

    private Models() {
        _modelLoader = new G3dModelLoader(new UBJsonReader());
    }

    public Model getModel(String id, BuildingGlyph.TYPE type) {
        try {

            if ( !_models.containsKey(id )) {
                Model model = _modelLoader.loadModel(Gdx.files.internal("models/" + id + ".g3db"));
                if ( type == BuildingGlyph.TYPE.WALL ) {
                    BoundingBox boundingBox = new BoundingBox();
                    model.nodes.get(0).calculateBoundingBox(boundingBox);
                    Vector3 min = new Vector3();
                    boundingBox.getMin(min);
                    model.nodes.get(0).translation.add(0, 0, +min.z - Constants.WALL_WIDTH/2f);
                    model.nodes.get(0).calculateTransforms(true);
                }
                for ( Material material : model.materials ) {
                    material.set(new IntAttribute(IntAttribute.CullFace, GL_FRONT_FACE));
                }
                _models.put(id, model);
            }
            return _models.get(id);
        } catch ( NullPointerException e ) {
            e.printStackTrace();
            return null;
        }
    }

}
