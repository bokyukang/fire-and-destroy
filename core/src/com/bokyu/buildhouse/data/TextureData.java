package com.bokyu.buildhouse.data;

/**
 * Created by bokyu on 2016-09-14.
 */
public class TextureData {
    public String id;
    public String title;
    public String border_type;
    public String draw_border_color;
    public String price_coin;
    public String price_gold;
    public String wall_width;
    public String model_type;
    public String wall_type;
    public String desc;
    public String mass;
}
