package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2016-12-22.
 */

public class CeilingDataContainer {
    static private CeilingDataContainer _instance = null;
    public Map<String, CeilingData> _ceilingDataMap;
    public List<String> _keyList = null;


    static public CeilingDataContainer instance() {
        if ( _instance == null ) _instance = new CeilingDataContainer();
        return _instance;
    }

    public void setJsonData(CeilingDataContainerJson jsonData) {
        _ceilingDataMap = new HashMap();
        for ( CeilingData ceilingData : jsonData.ceilingData) {
            _ceilingDataMap.put(ceilingData.id, ceilingData);
        }
        _keyList = new ArrayList(_ceilingDataMap.keySet());
        Collections.sort(_keyList);
    }
}
