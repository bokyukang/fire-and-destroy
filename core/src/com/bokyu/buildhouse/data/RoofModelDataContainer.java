package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-01-05.
 */

public class RoofModelDataContainer {
    static private RoofModelDataContainer _instance = null;
    public Map<String, RoofModelData> _roofModelDataMap;
    public List<String> _keyList = null;

    static public RoofModelDataContainer instance() {
        if ( _instance == null ) _instance = new RoofModelDataContainer();
        return _instance;
    }

    public void setJsonData(RoofModelDataContainerJson jsonData) {
        _roofModelDataMap = new HashMap();
        for ( RoofModelData roofModelData : jsonData.roofModelData) {
            _roofModelDataMap.put(roofModelData.id, roofModelData);
        }
        _keyList =new ArrayList(_roofModelDataMap.keySet());
        Collections.sort(_keyList);
    }
}
