package com.bokyu.buildhouse.data;

import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.glyph.MyGlyph;

/**
 * Created by bk on 2017-02-15.
 */

public class GlyphDbData {
    public MyGlyph.TYPE _type;
    public Constants.DIRECTION _direction = Constants.DIRECTION.ALL;
    public int _x, _y;
    public String _resourceId;
}
