package com.bokyu.buildhouse.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-01-04.
 */

public class DecorationDataContainer {
    static private DecorationDataContainer _instance = null;
    public Map<String, DecorationData> _decorationDataMap;
    public List<String> _keyList = null;

    static public DecorationDataContainer instance() {
        if ( _instance == null ) _instance = new DecorationDataContainer();
        return _instance;
    }

    public void setJsonData(DecorationDataContainerJson jsonData) {
        _decorationDataMap = new HashMap();
        for ( DecorationData decorationData : jsonData.decorationData) {
            _decorationDataMap.put(decorationData.id, decorationData);
        }

        _keyList = new ArrayList(_decorationDataMap.keySet());
        Collections.sort(_keyList);
    }
}
