package com.bokyu.buildhouse.data;

/**
 * Created by bk on 2016-11-07.
 */

public class BallData extends MyData {
    public String title;
    public float weight;
    public float size;
    public String ui_name;
    public String model_name;
}
