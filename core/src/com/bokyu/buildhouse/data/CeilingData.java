package com.bokyu.buildhouse.data;

/**
 * Created by bk on 2016-11-07.
 */

public class CeilingData extends MyData {
    public String title;
    public String border_type;
    public String draw_border_color;
    public String price_coin;
    public String price_gold;
    public String wall_width;
    public String model_type;
    public String wall_type;
    public String desc;
    public int xspan;
    public int yspan;
}
