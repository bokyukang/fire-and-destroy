package com.bokyu.buildhouse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.events.EventScreenTransition;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.screen.MyScreen;
import com.bokyu.buildhouse.screen.ScreenMain;
import com.bokyu.buildhouse.shaders.NormalShaderProvider;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.dialog.UIDialogMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Created by bokyu on 2016-07-06.
 */
public class RenderManager {
    static private RenderManager _instance = null;
    private PerspectiveCamera _perspectiveCam = null;
    private ShapeRenderer _shapeRenderer = null;
    private Environment _environment = null;
    private ModelBatch _modelBatch = null;
    private SpriteBatch _spriteBatch = null;
    private Set<UIGlyph>    _uiGlyphs = null;
    private Set<MyGlyph>    _modelGlyphs = null;
    private MyScreen        _curScreen = null;
    private long           _delta = 0;
    private long            _pTime =0, _cTime = 0;
    private Stack<MyScreen> _screenStack = null;

    static public RenderManager instance() {
        if ( _instance == null ) {
            _instance = new RenderManager();
        }
        return _instance;
    }

    private RenderManager() {
        _screenStack = new Stack<MyScreen>();
    }

    private void setCurScreen(MyScreen screen ) {
        if ( _curScreen != null ) {
            _curScreen.onExit();
            EventBus.getDefault().post(new EventScreenTransition());
        }
        _curScreen = screen;
        screen.onEnter();

    }

    public void pushScreen(MyScreen screen) {
        _screenStack.push(screen);
        setCurScreen(screen);
    }

    public boolean checkSceneObject(int x, int y) {
        try {
            if (_curScreen.getSceneStage().hit(x, y, true) != null) return true;
        } catch ( NullPointerException e ) {
        }
        return false;
    }

    public MyScreen popScreen() {
        if ( _screenStack.size() > 1 ) {
            _screenStack.pop();
            MyScreen newScreen = _screenStack.peek();
            setCurScreen(newScreen);
            return newScreen;
        }
        return null;
    }

    public void showAlertMsg(String msg) {
        if ( _curScreen == null ) return;

        if ( UIDialogMessage.instance().getParent() == null ) {
            _curScreen.getSceneStage().addActor(UIDialogMessage.instance());
        }
        UIDialogMessage.instance().setFillParent(true);
        UIDialogMessage.instance().align(Align.center);
        UIDialogMessage.instance().setMsg(msg);
        UIDialogMessage.instance().toFront();
    }

    public long getDelta() {
        return _delta;
    }

    public void init() {

        _modelGlyphs = new HashSet();
        _uiGlyphs = new HashSet();

//        pushScreen(ScreenWorld.instance());
//        ScreenWorld.instance().loadFromDb();
        pushScreen(ScreenMain.instance());

        String normalVertexShader = Gdx.files.internal("shaders/normal.vertex.glsl").readString();
        String normalFragmentShader = Gdx.files.internal("shaders/normal.fragment.glsl").readString();

        _modelBatch = new ModelBatch(new NormalShaderProvider(normalVertexShader, normalFragmentShader));
        _spriteBatch = new SpriteBatch();
        _shapeRenderer = new ShapeRenderer();

        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        _environment.add(new DirectionalLight().set(1f, 1f, 1f, -1f, -0.8f, -0.2f));
        _environment.add(new DirectionalLight().set(1f, 1f, 1f, .2f, -0.8f, 1f));
        _environment.set( new IntAttribute(IntAttribute.CullFace, 1));

        _perspectiveCam = new PerspectiveCamera(67,  Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        _perspectiveCam.position.set(0f, 10f, 10f);
        _perspectiveCam.lookAt(0,0,0);
        _perspectiveCam.near = 1f;
        _perspectiveCam.far = 300f;
        _perspectiveCam.update();


        for ( MyGlyph glyph : _modelGlyphs) {
            glyph.init();
        }
    }

    public void addGlyph(MyGlyph glyph) {
        _modelGlyphs.add(glyph);
    }

    public void removeGlyph(MyGlyph glyph) {
        _modelGlyphs.remove(glyph);
    }

    public void addGlyph(UIGlyph glyph) {
        _uiGlyphs.add(glyph);
    }

    public void removeGlyph(UIGlyph glyph) {
        _uiGlyphs.remove(glyph);
    }

    public void removeAllGlyphs() {
        _uiGlyphs.clear();
        _modelGlyphs.clear();
    }

    public void render() {
        _cTime = System.currentTimeMillis();
        if ( _pTime != 0 ) {
            _delta = _cTime - _pTime;
        }
        _pTime = _cTime;

        Gdx.gl.glClearColor(0, 0, 0, 1);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling? GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
//        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        /* 3d */
        _modelBatch.begin(_perspectiveCam);
        for ( MyGlyph glyph : _modelGlyphs) {
            glyph.render(_modelBatch, _environment);
        }
        _modelBatch.end();

        if ( _curScreen != null ) {
            _curScreen.render(_delta );
        }
    }

    public PerspectiveCamera getCamera() { return _perspectiveCam; }
}
