package com.bokyu.buildhouse.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by bokyu on 2016-08-17.
 */
public class UICompas extends UIGlyph {

    private Pixmap _pixMap = null;
    private Texture _textureCircle = null;
    private TextureRegion _textureRegionCircle = null;
    private float _rotation  = 0; // in degree

    public UICompas(int x, int y, int width, int height) {
        setX(x);
        setY(y);
        setWidth(width);
        setHeight(height);
//        _pixMap = new Pixmap(_width, _height, Pixmap.Format. RGBA8888);
//        drawPixMap();
        _textureCircle = new Texture(Gdx.files.internal("ui/compas_back.png"));
        _textureRegionCircle = new TextureRegion(
                _textureCircle,
                _textureCircle.getWidth(),
                _textureCircle.getHeight());
    }

    public void setRotation(float rot) {
        _rotation = rot;
    }

    @Override
    protected void drawPixMap() {
        int width = (int)getWidth();
        int height = (int)getHeight();
        _pixMap.setColor(Color.CLEAR);
        _pixMap.fill();
        _pixMap.setColor(Color.RED);
        _pixMap.fillTriangle(
                width/2, height/4,
                width/2 - width/10, height/2,
                width/2 + width/10 , height/2);
        _pixMap.fillTriangle(
                width/2, height - height/3,
                width/2 - width/10, height/2,
                width/2 + width/10 , height/2);
    }

    public void render(SpriteBatch batch) {
        if ( _dirty ) {
            _dirty = false;
//            drawPixMap();
        }

        batch.draw(_textureRegionCircle, getX(), getY(), getWidth()/2 , getHeight()/2, getWidth(), getHeight(), 1, 1, _rotation);
//        spriteBatch.draw(_texturePin, _x, _y, _width, _height);
    }
}
