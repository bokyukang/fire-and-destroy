package com.bokyu.buildhouse.ui;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by bokyu on 2016-08-12.
 */
public class UIRadioGroup {
    private Set<com.bokyu.buildhouse.ui.button.UIButton> _buttons = new HashSet();

    public void addButton(com.bokyu.buildhouse.ui.button.UIButton button) {
        _buttons.add(button);
    }

    public void setSelected(com.bokyu.buildhouse.ui.button.UIButton buttonIn) {
        for ( com.bokyu.buildhouse.ui.button.UIButton eachButton : _buttons ) {
            if ( eachButton != buttonIn ) {
                eachButton.setActivated(false);
            }
        }
    }
}
