package com.bokyu.buildhouse.ui.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.screen.ScreenWorld;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-03-24.
 */

public class WorldNetworkScene2DManager extends Scene2DManager {
    static public float BTN_MARGIN = .02f;
    static public int MIN_BTN_WIDTH = 30;
    static public float BTN_WIDTH_RATE = .07f;
    static public int BTN_INTERVAL = 3;

    static private WorldNetworkScene2DManager _instance = null;

    private Touchpad _touchpadRot = null;

    static public WorldNetworkScene2DManager instance()
    {
        if ( _instance == null ) {
            _instance = new WorldNetworkScene2DManager();
        }
        return _instance;
    }

    private WorldNetworkScene2DManager() {
        EventBus.getDefault().register(this);

        _stage = new Stage(new ScreenViewport());

        _touchpadRot = createTouchpad();
        _stage.addActor( _touchpadRot );

        int width = Math.max(MIN_BTN_WIDTH, (int)(_stage.getWidth() * BTN_WIDTH_RATE));
        _initialPosition = new MyPosition((int)(_stage.getWidth() - width - _stage.getWidth() * BTN_MARGIN),
                (int)( _stage.getHeight() - width - BTN_MARGIN));
        _curBtnInsX = _initialPosition._x;
        _curBtnInsY = _initialPosition._y;

        _stage.addActor(createSelectButton(width, "go_back.png"
                , EventButtonClick.BTN_NAMES.NETWORK_WORLD_GO_BACK, "go back"));
        _stage.addActor(createSelectButton(width, "recommendation.gif"
                , EventButtonClick.BTN_NAMES.RECOMMENDATION, "recommendation"));
//        _stage.addActor(createSelectButton(width, "recommendation_inverse.gif"
//                , EventButtonClick.BTN_NAMES.RECOMMENDATION_INVERSE, "recommendation inverse"));
    }

    @Override
    protected void increaseCurPos(int width) {
        _curBtnInsX -= width + BTN_INTERVAL;
        if ( _curBtnInsX < 0 ) {
            _curBtnInsY += width + BTN_INTERVAL;
            _curBtnInsX = _initialPosition._x;
        }
    }

    @Override
    public void render() {
        _stage.act(Gdx.graphics.getDeltaTime());

        float delta = RenderManager.instance().getDelta();
        if ( _touchpadRot.getKnobPercentX() != 0 || _touchpadRot.getKnobPercentY() != 0 ) {
            ScreenWorld.instance().getCameraProcessor().rotateCam(
                    -_touchpadRot.getKnobPercentX() * delta,
                    _touchpadRot.getKnobPercentY() * delta
            );
        }
        _stage.draw();
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {

    }

}
