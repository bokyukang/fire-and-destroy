package com.bokyu.buildhouse.ui.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventConfirm;
import com.bokyu.buildhouse.events.EventLoginSuccessful;
import com.bokyu.buildhouse.events.EventLogoutSuccessful;
import com.bokyu.buildhouse.events.EventPurchaseConfirmed;
import com.bokyu.buildhouse.network.Network;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.network.jobs.NetworkJobLoadAllVillages;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.dialog.ISaveName;
import com.bokyu.buildhouse.ui.dialog.UIConfirm;
import com.bokyu.buildhouse.ui.dialog.UIDialogEnterName;
import com.bokyu.buildhouse.ui.dialog.UIDialogVillages;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-02-02.
 */

public class MainScene2DManager extends Scene2DManager implements ISaveName {

    static public float BTN_MARGIN = .02f;
    static public int MIN_BTN_WIDTH = 30;
    static public float BTN_WIDTH_RATE = .07f;
    static public int BTN_INTERVAL = 3;

    static private MainScene2DManager _instance = null;

    private UIDialogVillages _uiDialogVillages = null;
    private Map<String, Village> _villageMap = null;
    private List<String> _villageKeyList = null;
    private EventButtonClick.BTN_NAMES _curBtn = null;
    private TextButton _noAdBtn = null;
    private TextButton _loginBtn = null;
    private TextButton _shareBtn = null;
    private Village _deleteTargetVillage = null;

    static public MainScene2DManager instance() {
        if (_instance == null) {
            _instance = new MainScene2DManager();
        }
        return _instance;
    }

    private MainScene2DManager() {
        _stage = new Stage(new ScreenViewport());
    }

    public void init() {
        EventBus.getDefault().register(this);
        _villageMap = new HashMap();
        _villageKeyList = new ArrayList();


        float dialogWidth = _stage.getWidth() / 2;
        float dialogHeight = _stage.getHeight() / 2;
        _uiDialogVillages = new UIDialogVillages(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2, dialogWidth,
                dialogHeight, _villageMap, _villageKeyList);
        _uiDialogVillages.setFillParent(true);
        _uiDialogVillages.align(Align.center);


        _stage.addActor(_uiDialogVillages);


        _villageMap.clear();
        _villageKeyList.clear();
//        updateVillageEntries();

        /* no ad button */
        _noAdBtn = new TextButton("remove ad", UIManager.instance().getSkin(UIManager.SKINS.ARCADE));
        _noAdBtn.addListener(
                new InputListener() {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        EventBus.getDefault().post(new EventButtonClick(EventButtonClick.BTN_NAMES.NO_AD, EventButtonClick.TYPE.SELECT));
                        return true;
                    }
                }
        );
        _noAdBtn.setX(_stage.getWidth() / 2 - _noAdBtn.getWidth());
        _noAdBtn.setY(_stage.getHeight() - _noAdBtn.getHeight() - BTN_MARGIN);
        if (Configuration.instance()._advertiseEnabled) {
            _stage.addActor(_noAdBtn);
        }

        /* login google button */
        _loginBtn = new TextButton("login to google", UIManager.instance().getSkin());
        _loginBtn.addListener(
                new InputListener() {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        EventBus.getDefault().post(new EventButtonClick(EventButtonClick.BTN_NAMES.LOGIN_GOOGLE, EventButtonClick.TYPE.SELECT));
                        return true;
                    }
                }
        );
        _loginBtn.setX(_stage.getWidth() / 2);
        _loginBtn.setY(_stage.getHeight() - _loginBtn.getHeight() - BTN_MARGIN);
        _stage.addActor(_loginBtn);

        /* share button */
        _shareBtn = new TextButton("share this app", UIManager.instance().getSkin());
        _shareBtn.addListener(
                new InputListener() {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                        EventBus.getDefault().post(new EventButtonClick(EventButtonClick.BTN_NAMES.SHARE, EventButtonClick.TYPE.SELECT));
                        return true;
                    }
                }
        );
        _shareBtn.setX(_stage.getWidth() / 2 + _loginBtn.getWidth());
        _shareBtn.setY(_stage.getHeight() - _shareBtn.getHeight() - BTN_MARGIN);
        _stage.addActor(_shareBtn);
    }

    private void updateVillageEntries() {
        List<Village> villageList
                = Configuration.instance().getFactory().getDbManager().getVillages();
        updateVillageEntriesInner(villageList);
    }

    private void updateVillageEntriesInner(List<Village> villageList) {
        for (Village village : villageList) {
            _villageMap.put(Integer.toString(village._id), village);
            _villageKeyList.add(Integer.toString(village._id));
        }
        Collections.sort(_villageKeyList);
        _uiDialogVillages.updateEntries();
    }

    public void updateVillageEntriesNetwork(String accountId, Network.all_villages villages) {
        List<Village> arVillages = new ArrayList();
        for ( Network.village_facade netVillage : villages.getVillagesList())
        {
            Village village = new Village(netVillage.getVillageId(), netVillage.getVillageName());
            village._accountId = accountId;
            arVillages.add(village);
        }
        updateVillageEntriesInner(arVillages);
    }

    public void render() {
        _stage.act(Gdx.graphics.getDeltaTime());
        _stage.draw();
    }

    public Stage getStage() {
        return _stage;
    }


    public void removeVillage(Village village) {
        if (village == null) return;
        UIConfirm.instance().show("confirm", "do you want to erase village?", getStage(),
                new EventConfirm(EventConfirm.CONFIRM_TYPE.DELETE_VILLAGE));

        _deleteTargetVillage = village;

    }

    public void saveName(String name) {
        if (_curBtn == EventButtonClick.BTN_NAMES.ADD_VILLAGE) {
            int vId = Configuration.instance().getFactory().getDbManager().addVillage(name);
            String vIdStr = Integer.toString(vId);
            Village newVillage = new Village(vId, name);
            _villageKeyList.add(vIdStr);
            _villageMap.put(vIdStr, newVillage);
            _uiDialogVillages.updateEntries();
        } else if (_curBtn == EventButtonClick.BTN_NAMES.RENAME_VILLAGE) {
            Village selVillage = _uiDialogVillages.getSelectedData();
            if (selVillage == null) return;

            Configuration.instance().getFactory().getDbManager().renameVillage(name, selVillage._id);
            selVillage._name = name;

            _uiDialogVillages.updateEntries();
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {

        _curBtn = event._name;

        switch (event._name) {
            case ADD_VILLAGE:
            case RENAME_VILLAGE:
                UIDialogEnterName d = new UIDialogEnterName(this);
                _stage.addActor(d);
                break;
            case REMOVE_VILLAGE:
                removeVillage(_uiDialogVillages.getSelectedData());
                break;
        }
    }

    @Subscribe
    public void onEvent(EventConfirm event)
    {
        switch ( event._confirmType ) {
            case DELETE_VILLAGE:
                if (Configuration.instance().getFactory()
                        .getDbManager().removeVillage(_deleteTargetVillage._id) > 0) {
                    String idStr = Integer.toString(_deleteTargetVillage._id);
                    _villageKeyList.remove(idStr);
                    _villageMap.remove(idStr);
                    _uiDialogVillages.updateEntries();
                }
                break;
        }
    }

    @Subscribe
    public void onEvent(EventPurchaseConfirmed event)
    {
        Gdx.app.debug("pay_service", "purchase confirmed " + event._sku);
        if ( event._sku.equals(Configuration.NO_AD_ITEM_NAME) ) {
            Gdx.app.debug("pay_service", "making remove add btn invisible");
            _noAdBtn.setVisible(false);
        }
    }

    @Subscribe
    public void onEvent(EventLoginSuccessful event)
    {
        if ( event._isLogin ) {
            _loginBtn.setText("logged in as " + event._name);
            _villageMap.clear();
            _villageKeyList.clear();
            updateVillageEntries();
            NetworkManager.instance().addJob(
                    new NetworkJobLoadAllVillages(Account.instance().getId())
            );
            Gdx.app.log("login", "successful!");
        } else {
            /* tmp test */
//            Account.instance().setAccountId("101423944926000041450");
//            Account.instance()._accountName = "Bokyu Kang";
//            EventBus.getDefault().post(new EventLoginSuccessful(true, "Bokyu Kang"));


            Gdx.app.log("login", "failed!");
        }
    }


    @Subscribe
    public void onEvent(EventLogoutSuccessful event)
    {
        if ( event._isLogout ) {
            _loginBtn.setText("login to google");
            _villageMap.clear();
            _villageKeyList.clear();
            updateVillageEntries();
            Gdx.app.log("login", "successful!");
        } else {
            Gdx.app.log("login", "failed!");
        }
    }
}
