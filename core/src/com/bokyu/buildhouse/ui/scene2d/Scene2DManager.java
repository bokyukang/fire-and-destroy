package com.bokyu.buildhouse.ui.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.UIRadioGroup;
import com.bokyu.buildhouse.ui.button.UIButton;

import java.util.HashSet;
import java.util.Set;

import static com.bokyu.buildhouse.ui.scene2d.ConstructScene2DManager.BTN_INTERVAL;

/**
 * Created by bk on 2017-03-08.
 */

public class Scene2DManager {
    static public float BTN_MARGIN = .02f;
    protected boolean _showingHelp = false;
    protected Set<UIButton> _btns = new HashSet();
    protected int _curBtnInsX = 0;
    protected int _curBtnInsY = 0;
    protected Stage _stage = null;
    protected MyPosition _initialPosition = new MyPosition(0, 0);

    protected void updateHelpLabel() {
        for (UIButton btn : _btns) {
            btn.setHelpLabelVisible(_showingHelp);
        }
    }

    protected Texture createCircleTexture(int radius, Color color) {
        Pixmap pixmap = new Pixmap(radius * 2, radius * 2, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.rgba8888(0, 0, 0, 0));
        pixmap.fill();
        pixmap.setColor(color);
        pixmap.fillCircle(radius, radius, radius);
        return new Texture(pixmap);
    }

    protected Texture createRectTexture(int width, int height, Color color) {
        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fill();
        return new Texture(pixmap);
    }

    protected UIButton createRadioButton(int width, String name,
                                         EventButtonClick.BTN_NAMES eventName,
                                         UIRadioGroup radioGroup, String helpText
    ) {
        return createRadioButton(width, name, eventName, radioGroup, helpText, Color.GRAY);
    }

    protected UIButton createRadioButton(int width, String name,
                                         EventButtonClick.BTN_NAMES eventName,
                                         UIRadioGroup radioGroup, String helpText,
                                         Color color) {
        increaseCurPos(width);
        UIButton button = new UIButton(width, width, name,
                new EventButtonClick(eventName, EventButtonClick.TYPE.RADIO),
                UIButton.BUTTON_TYPE.RADIO, color, "", helpText);
        button.setX(_curBtnInsX);
        button.setY(_curBtnInsY);
        button.setRadioGroup(radioGroup);
        radioGroup.addButton(button);
        _btns.add(button);
        return button;
    }

    protected UIButton createToggleButton(
            int width, String name1, String name2,
            EventButtonClick.BTN_NAMES eventName, String helpText
    ) {
        increaseCurPos(width);
        UIButton btn = new UIButton(width, width, name1,
                new EventButtonClick(eventName, EventButtonClick.TYPE.TOGGLE),
                UIButton.BUTTON_TYPE.TOGGLE_2_IMG, com.badlogic.gdx.graphics.Color.GRAY.GRAY, name2,
                helpText
        );
        btn.setX(_curBtnInsX);
        btn.setY(_curBtnInsY);
        _btns.add(btn);
        return btn;
    }

    protected UIButton createSelectButton(
            int width, String name,
            EventButtonClick.BTN_NAMES eventName, String helpText) {
        return createSelectButton(width, name, eventName, true, helpText);
    }

    protected UIButton createSelectButton(
            int width, String name,
            EventButtonClick.BTN_NAMES eventName, boolean increaseCurPos, String helpText )
    {
        if ( increaseCurPos ) {
            increaseCurPos(width);
        }

        UIButton btn = new UIButton(width, width, name,
                new EventButtonClick(eventName, EventButtonClick.TYPE.SELECT),
                UIButton.BUTTON_TYPE.CLICK, com.badlogic.gdx.graphics.Color.GRAY, "", helpText);
        btn.setX(_curBtnInsX);
        btn.setY(_curBtnInsY);
        _btns.add(btn);
        return btn;
    }
    protected void increaseCurPos(int width) {
        _curBtnInsY += width + BTN_INTERVAL;
        if ( _curBtnInsY > _stage.getHeight() - width ) {
            _curBtnInsX += width + BTN_INTERVAL;
            _curBtnInsY = _initialPosition._y;
        }
    }

    protected Touchpad createTouchpad() {
        /* rotate touchpad */
        int MIN_BIG_RAD = 40;
        float bigRadRate = .08f;
        float smallRadRate = .02f;
//        int bigRad = (int)(_stage.getWidth()*bigRadRate);
//        int smallRad = (int)(_stage.getWidth()*smallRadRate);
//        Drawable bigCircle = new TextureRegionDrawable(new TextureRegion(
//                createCircleTexture(Math.max(MIN_BIG_RAD, bigRad),
//                        new Color(Color.rgba4444(100,100,100, 255)))
//        ));
//        Drawable smallCircle = new TextureRegionDrawable(new TextureRegion(
//                createCircleTexture(smallRad, new Color(Color.rgba4444(255,255,255, 255)))
//        ));
//        Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle(bigCircle, smallCircle);

        Touchpad touchpadRot = new Touchpad(0, UIManager.instance().getSkin(UIManager.SKINS.ARCADE));//touchpadStyle);
        touchpadRot.setX(_stage.getWidth() - touchpadRot.getWidth() );
        touchpadRot.setY(0);
        touchpadRot.setTouchable(Touchable.enabled);
        return touchpadRot;
    }
    public Stage getStage() { return _stage; }

    public Actor hit(float x, float y) {
        float stageX = x * _stage.getWidth() / Gdx.graphics.getWidth();
        float stageY = _stage.getHeight() - (y * _stage.getHeight() / Gdx.graphics.getHeight());
        return _stage.hit(stageX, stageY, true);
    }
    public void render() {

    }
}
