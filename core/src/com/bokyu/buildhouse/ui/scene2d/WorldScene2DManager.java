package com.bokyu.buildhouse.ui.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.data.data_types.MyPosition;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.network.jobs.NetworkJobLoadRanking;
import com.bokyu.buildhouse.screen.ScreenWorld;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.UIRadioGroup;
import com.bokyu.buildhouse.ui.button.UIButton;
import com.bokyu.buildhouse.ui.dialog.UIDialogBuilding;
import com.bokyu.buildhouse.ui.dialog.UIDialogRanking;
import com.bokyu.buildhouse.ui.dialog.UIDialogRoad;
import com.bokyu.fireanddestroy.ui.MyButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.bokyu.buildhouse.ui.scene2d.ConstructScene2DManager.DIALOG_MARGIN;


/**
 * Created by bokyu on 2016-09-13.
 */
public class WorldScene2DManager extends Scene2DManager {

    static public int MIN_BTN_WIDTH = 30;
    static public float BTN_WIDTH_RATE = .07f;
    static public int BTN_INTERVAL = 3;

    static private WorldScene2DManager _instance = null;

//    private UIDialogBuilding _dialogBuilding = null;
    private UIDialogRoad _dialogRoad = null;
    private UIDialogRanking _dialogRanking = null;
    private UIRadioGroup _radioGroup = new UIRadioGroup();
    private Table _failedContainer = null;
    private Table _successContainer = null;


    static public WorldScene2DManager instance() {
        if (_instance == null ) {
            _instance = new WorldScene2DManager();
        }
        return _instance;
    }

    static public Button createButton(String text, final EventButtonClick.BTN_NAMES btnName, Table container) {
        Label label = new Label(text, UIManager.instance().getSkin(UIManager.SKINS.ARCADE),"title");
        label.setAlignment(Align.center);
        label.getStyle().font.getData().setScale(2f);
        label.setColor(Color.WHITE);
//        Button button = new Button(UIManager.instance().getSkin(UIManager.SKINS.LGDXS), "oval5");
        Button button = new MyButton(text, Gdx.graphics.getWidth()/3f, "oval1");
        button.addListener(
                new ClickListener() {
                    public void clicked(InputEvent event, float x, float y) {
                        EventBus.getDefault().post(new EventButtonClick(btnName, EventButtonClick.TYPE.SELECT));
                        if ( event.getTarget().getParent().getParent().getStage() != null ) {
                            event.getTarget().getParent().getParent().remove();
                        }
                    }
                });
        label.layout();
        float width = label.getPrefWidth();
        float height = label.getPrefHeight();
//        button.add(label).fill().expand();
        container.row().pad(50);
        container.add(button).width(width).height(height);
        return button;
    }
    private WorldScene2DManager() {
        EventBus.getDefault().register(this);

        _stage = new Stage(new ScreenViewport());

        /* failed container */
        _failedContainer = new Table(UIManager.instance().getSkin(UIManager.SKINS.ARCADE));
        createButton("replay", EventButtonClick.BTN_NAMES.REPLAY, _failedContainer);
//        ).width(buttonWidth).height(buttonHeight).center();
        createButton("back", EventButtonClick.BTN_NAMES.SCREEN_GO_BACK, _failedContainer);
//        ).width(buttonWidth).height(buttonHeight).center();
        _failedContainer.setX( ( Gdx.graphics.getWidth() - _failedContainer.getWidth()) / 2f );
        _failedContainer.setY( ( Gdx.graphics.getHeight() - _failedContainer.getHeight()) / 2f );

        /* success container */
        _successContainer = new Table(UIManager.instance().getSkin(UIManager.SKINS.ARCADE));
//        _successContainer.add(
        createButton("replay", EventButtonClick.BTN_NAMES.REPLAY, _successContainer);
//        ).width(buttonWidth).height(buttonHeight).center();
//        _successContainer.add(
        createButton("back", EventButtonClick.BTN_NAMES.SCREEN_GO_BACK, _successContainer);
//        ).width(buttonWidth).height(buttonHeight).center();
//        _successContainer.add(
//        createButton("next level", EventButtonClick.BTN_NAMES.PLAY_NEXT_LEVEL, _successContainer);
//        ).width(buttonWidth).height(buttonHeight).center();
        _successContainer.setX( ( Gdx.graphics.getWidth() - _successContainer.getWidth()) / 2f );
        _successContainer.setY( ( Gdx.graphics.getHeight() - _successContainer.getHeight()) / 2f );
        _successContainer.setColor(Color.WHITE);

        /* left menu */
        int margin = (int)(_stage.getWidth() * BTN_MARGIN);
        int width = Math.max(MIN_BTN_WIDTH, (int)(_stage.getWidth() * BTN_WIDTH_RATE));
        _initialPosition = new MyPosition((int)_stage.getWidth() - margin, (int)_stage.getHeight() - margin);
        _curBtnInsX = _initialPosition._x;
        _curBtnInsY = _initialPosition._y;
        _curBtnInsY -= width + BTN_INTERVAL;
//        _stage.addActor(createSelectButton( width, "building_editor.gif", EventButtonClick.BTN_NAMES.OPEN_BUILDING_EDITOR, "design building"));
//        _stage.addActor(createSelectButton( width, "building_list.gif"
//                , EventButtonClick.BTN_NAMES.WORLD_SCREEN_BUILDING_LIST, "select building"));
        UIButton btn = createSelectButton( width, "go_back.png"
                , EventButtonClick.BTN_NAMES.SCREEN_GO_BACK, "go back");
        btn.drawBack(false);
        _stage.addActor(btn);

        int marginWidth = (int)(DIALOG_MARGIN * _stage.getWidth());
        int maxHeight = (int)(_stage.getHeight() - marginWidth*2);
        int maxWidth = (int)(_stage.getWidth() - marginWidth*2);
//        _dialogBuilding = new UIDialogBuilding(marginWidth,marginWidth,
//                Math.min(maxWidth, 1200),
//                Math.min(maxHeight, 700));

    }

    public void showFaildContainer() {
        if ( _failedContainer.getStage() == null ) {
            _stage.addActor(_failedContainer);
        }
    }

    public void showSuccessContainer() {
        if ( _successContainer.getStage() == null ) {
            _stage.addActor(_successContainer);
        }
    }

    private void showRankingDialog() {
        NetworkManager.instance().addJob( new NetworkJobLoadRanking());
        _dialogRanking.setFillParent(true);
        _dialogRanking.align(Align.right);
        _stage.addActor(_dialogRanking);
    }

    private void showRoadListDialog() {
        _dialogRoad.updateEntries();
        _dialogRoad.setFillParent(true);
        _dialogRoad.align(Align.right);
        _stage.addActor(_dialogRoad);
    }

//    private void showBuildingListDialog() {
//        _dialogBuilding.updateEntries();
//        _dialogBuilding.setFillParent(true);
//        _dialogBuilding.align(Align.right);
//        _stage.addActor(_dialogBuilding);
//    }

    protected void beginRender () {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
    @Override
    protected void increaseCurPos(int width) {
//        _curBtnInsY += width + BTN_INTERVAL;
//        if ( _curBtnInsY > _stage.getHeight() - width ) {
        _curBtnInsX -= width + BTN_INTERVAL;
//            _curBtnInsY = _initialPosition._y;
//        }
    }

    @Override
    public void render() {
        beginRender();
        _stage.act(Gdx.graphics.getDeltaTime());
        _stage.draw();
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case WORLD_SCREEN_BUILDING_LIST:
//                showBuildingListDialog();
                break;
            case WORLD_SCREEN_ROAD_LIST:
                showRoadListDialog();
                break;
            case SCREEN_GO_BACK:
                if ( ScreenWorld.instance()._isVisible )
                    RenderManager.instance().popScreen();
                break;
            case RANKING:
                showRankingDialog();
                break;
            case HELP:
                _showingHelp = !_showingHelp;
                updateHelpLabel();
                break;
        }
    }

    public void onExit() {
        if ( _successContainer.getStage() != null ) {
            _successContainer.remove();
        }
        if ( _failedContainer.getStage() != null ) {
            _failedContainer.remove();
        }
    }
}
