package com.bokyu.buildhouse.ui.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.screen.ScreenConstruct;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.UIRadioGroup;
import com.bokyu.buildhouse.ui.button.UIButton;
import com.bokyu.buildhouse.ui.dialog.UIDialogBuilding;
import com.bokyu.buildhouse.ui.dialog.UIDialogCeiling;
import com.bokyu.buildhouse.ui.dialog.UIDialogDecorations;
import com.bokyu.buildhouse.ui.dialog.UIDialogRoof;
import com.bokyu.buildhouse.ui.dialog.UIDialogTexture;
import com.bokyu.buildhouse.ui.dialog.UIDialogWalls;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bokyu on 2016-08-29.
 */
public class ConstructScene2DManager extends Scene2DManager {

    static private ConstructScene2DManager _instance = null;
    static public float BTN_MARGIN = .02f;
    static public int MIN_BTN_WIDTH = 30;
    static public float BTN_WIDTH = .07f;
    static public int BTN_INTERVAL = 3;
    static public float DIALOG_MARGIN = 0.03f;

    private UIRadioGroup            _constructButtonGroup = new UIRadioGroup();
    private Touchpad _touchpad = null;
//    private UIDialogCeiling _dialogCeiling = new UIDialogCeiling(0,0,500,500);
    private UIDialogTexture _dialogTexture = null;
    private UIDialogBuilding _dialogBuilding = null;
    private UIDialogDecorations _dialogDecorations = null;
    private UIDialogWalls _dialogWall = null;
    private UIDialogRoof _dialogRoof = null;
    private UIDialogCeiling _dialogCeiling = null;

    static public ConstructScene2DManager instance() {
        if ( _instance == null ) {
            _instance = new ConstructScene2DManager();
        }
        return _instance;
    }

    private ConstructScene2DManager() {

        EventBus.getDefault().register(this);

        _stage = new Stage(new ScreenViewport());
        int width = Math.max(MIN_BTN_WIDTH, (int)(_stage.getWidth() * BTN_WIDTH));
        _initialPosition._x =  (int)(_stage.getWidth() * BTN_MARGIN);
        _initialPosition._y = 0;
        _curBtnInsX = _initialPosition._x;
        _curBtnInsY = _initialPosition._y - width - BTN_INTERVAL;
        UIButton coverAllBtn = createRadioButton( width, "cover.gif", EventButtonClick.BTN_NAMES.COVER, _constructButtonGroup, "insert ceiling", Color.GREEN);
        UIButton coverSideBtn = createRadioButton( width, "cover_left.gif", EventButtonClick.BTN_NAMES.COVER_LEFT, _constructButtonGroup, "insert half ceiling", Color.GREEN);
        _stage.addActor(coverAllBtn);
        _stage.addActor(coverSideBtn);
        _stage.addActor(createRadioButton( width, "wall_back_left.gif", EventButtonClick.BTN_NAMES.WALL_BACK_LEFT, _constructButtonGroup, "insert wall", Color.GREEN));
        _stage.addActor(createRadioButton( width, "wall_hor.gif", EventButtonClick.BTN_NAMES.WALL_HORIZONTAL, _constructButtonGroup, "insert diagonal wall", Color.GREEN));
        _stage.addActor(createRadioButton( width, "wall_deco.gif", EventButtonClick.BTN_NAMES.DECORATION, _constructButtonGroup, "insert wall decoration", Color.GREEN));
        _stage.addActor(createRadioButton(width, "roof.gif" , EventButtonClick.BTN_NAMES.ROOF, _constructButtonGroup, "insert roof", Color.GREEN));
        _stage.addActor(createRadioButton(width, "enemy.png" , EventButtonClick.BTN_NAMES.ENEMY_CHARACTER, _constructButtonGroup, "insert enemy", Color.GREEN));
        _stage.addActor(createRadioButton(width, "good_character.png" , EventButtonClick.BTN_NAMES.GOOD_CHARACTER, _constructButtonGroup, "insert good character", Color.GREEN));

        _stage.addActor(createToggleButton(width, "destroy.gif", "destroy.gif"
                , EventButtonClick.BTN_NAMES.DESTROY, "destroy"));

        _stage.addActor(createSelectButton(width, "rotation.png"
                ,EventButtonClick.BTN_NAMES.ROTATE, true , "rotate"));

        _stage.addActor(createSelectButton(width, "check.gif"
                ,EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_CHECK , true, "INSERT"));

        _stage.addActor(createSelectButton(width, "tex_hor.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_TEXTURE_TYPE, true, "select wall material"));

//        _stage.addActor(createSelectButton(width, "cover_type.gif"
//                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_CEILING_TYPE));

        _stage.addActor(createSelectButton(width, "wall_type.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_WALL_TYPE, true, "select wall shape"));

        _stage.addActor(createSelectButton(width, "wall_deco.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_DECO_LIST, true, "select decoration"));

        _stage.addActor(createSelectButton(width, "roof.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_ROOF_LIST, true, "select roof"));

        _stage.addActor(createSelectButton(width, "cover.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_CEILING_LIST, true, "select ceiling"));

        _stage.addActor(createSelectButton(width, "save_building.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_SAVE_BUILDING, true, "save building"));

        _stage.addActor(createSelectButton(width, "destroy_all.gif"
                , EventButtonClick.BTN_NAMES.CONSTRUCT_SCREEN_DESTROY_ALL, true, "destroy all"));

        UIButton btn = createSelectButton(width, "help.gif"
                , EventButtonClick.BTN_NAMES.HELP, true, "help");
//        btn.setX(_stage.getWidth()/2 + width + BTN_INTERVAL);
//        btn.setY(_stage.getHeight() - width - BTN_MARGIN);
        _stage.addActor(btn);

        btn = createSelectButton(width, "go_back.png"
                , EventButtonClick.BTN_NAMES.SCREEN_GO_BACK, false, "go back");
        btn.setX(_stage.getWidth()/2);
        btn.setY(_stage.getHeight() - width - BTN_MARGIN);
        btn.drawBack(false);
        _stage.addActor(btn);


//        _stage.addActor(createSelectButton(width,  "close.gif"
//                , EventButtonClick.BTN_NAMES.TEST, true, "test"));




        /* sub menu */
        Table vG = new Table(UIManager.instance().getSkin(UIManager.SKINS.ARCADE));
        _stage.addActor(vG);
        _curBtnInsX = 0;
        _curBtnInsY = 0;
        int padding = 10;
        vG.pad(padding);
        vG.add( new UIButton(width, width, "updown.gif",
                new EventButtonClick(EventButtonClick.BTN_NAMES.UPDOWN, EventButtonClick.TYPE.SELECT),
                UIButton.BUTTON_TYPE.CLICK, com.badlogic.gdx.graphics.Color.GRAY, "", "change elevation"))
                .width(width).height(width);
        vG.background(new TextureRegionDrawable( new TextureRegion(
            createRectTexture(width + padding * 2 ,width + padding * 2, new Color(1f,0f,0f,1f))
        )));
        /*
        Image background = new Image(createRectTexture(
                _initialPosition + _curBtnInsX + width,
                y,
                new Color(0.5f,0.5f,0.5f,.5f)
        ));
        _stage.addActor(background);
        background.toBack();
        */
        vG.setX(_stage.getWidth() - width - BTN_MARGIN);
        vG.setY(_stage.getHeight() - width  * 2 - BTN_MARGIN);
        coverAllBtn.addSubMenu(vG);
        coverSideBtn.addSubMenu(vG);



        int MIN_BIR_RAD = 40;
        float bigRad = .08f;
        float smallRad = .02f;
        Drawable bigCircle = new TextureRegionDrawable(new TextureRegion(
                createCircleTexture(Math.max(MIN_BIR_RAD, (int)(_stage.getWidth()*bigRad)), new Color(Color.rgba4444(100,100,100, 255)))
        ));
        Drawable smallCircle = new TextureRegionDrawable(new TextureRegion(
                createCircleTexture((int)(_stage.getWidth()* smallRad), new Color(Color.rgba4444(255,255,255, 255)))
        ));
        Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle(bigCircle, smallCircle);

        Touchpad touchpad = new Touchpad(0, touchpadStyle);
        touchpad.setTouchable(Touchable.enabled);
        touchpad.setX(_stage.getWidth() - touchpad.getWidth() );
        touchpad.setY(0);
        _stage.addActor( touchpad );
        _touchpad = touchpad;


        /* create dialogs */
        int marginWidth = (int)(DIALOG_MARGIN * _stage.getWidth());
        int maxHeight = (int)(_stage.getHeight() - marginWidth*2);
        int maxWidth = (int)(_stage.getWidth() - marginWidth*2);
        _dialogTexture = new UIDialogTexture(marginWidth,marginWidth,
                Math.min(maxWidth, 500),
                Math.min(maxHeight, 500));
        _dialogTexture.select(0);
        _dialogBuilding = new UIDialogBuilding(marginWidth,marginWidth,
                Math.min(maxWidth, 1200),
                Math.min(maxHeight, 700));
        _dialogDecorations = new UIDialogDecorations(marginWidth,marginWidth,
                Math.min(maxWidth, 500),
                Math.min(maxHeight, 500));
        _dialogDecorations.select(0);
        _dialogWall = new UIDialogWalls(marginWidth,marginWidth,
                Math.min(maxWidth, 1200),
                Math.min(maxHeight,700));
        _dialogWall.select(0);
        _dialogRoof = new UIDialogRoof(marginWidth,marginWidth,
                Math.min(maxWidth, 1200),
                Math.min(maxHeight, 700));
        _dialogRoof.select(0);
        _dialogCeiling = new UIDialogCeiling(marginWidth,marginWidth,
                Math.min(maxWidth, 1200),
                Math.min(maxHeight, 700));
        _dialogCeiling.select(0);

    }

    public Actor hit(float x, float y) {
        float stageX = x * _stage.getWidth() / Gdx.graphics.getWidth();
        float stageY = _stage.getHeight() - (y * _stage.getHeight() / Gdx.graphics.getHeight());
        return _stage.hit(stageX,stageY, true);
    }

//    private Texture createCircleTexture(int radius, Color color) {
//        Pixmap pixmap = new Pixmap(radius * 2, radius * 2, Pixmap.Format.RGBA4444);
//        pixmap.setColor(Color.rgba4444(0,0,0,0));
//        pixmap.fill();
//        pixmap.setColor(color);
//        pixmap.fillCircle(radius, radius,radius);
//        return new Texture(pixmap);
//    }
//
//    private Texture createRectTexture(int width, int height, Color color) {
//        Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
//        pixmap.setColor(color);
//        pixmap.fill();
//        return new Texture(pixmap);
//    }

    public void updateBuildingsDialog() {
        _dialogBuilding.updateEntries();
    }

    private void showCeilingSelectDialog() {
        _dialogCeiling.setFillParent(true);
        _dialogCeiling.align(Align.right);
        _stage.addActor(_dialogCeiling);
    }

    private void showTextureSelectDialog() {
        _dialogTexture.setFillParent(true);
        _dialogTexture.align(Align.right);
        _stage.addActor(_dialogTexture);
    }
    private void showWallSelectDialog() {
        _dialogWall.setFillParent(true);
        _dialogWall.align(Align.right);
        _stage.addActor(_dialogWall);
    }
    private void showRoofSelectDialog() {
        _dialogRoof.setFillParent(true);
        _dialogRoof.align(Align.right);
        _stage.addActor(_dialogRoof);
    }

    private void showBuildingListDialog() {
        _dialogBuilding.updateEntries();
        _dialogBuilding.setFillParent(true);
        _dialogBuilding.align(Align.right);
        _stage.addActor(_dialogBuilding);
    }

    private void showDecoListDialog() {
        _dialogDecorations.updateEntries();
        _dialogDecorations.setFillParent(true);
        _dialogDecorations.align(Align.right);
        _stage.addActor(_dialogDecorations);
    }

    public Stage getStage() {return _stage;}

//    private UIButton createRadioButton(
//            int width, String name,
//            EventButtonClick.BTN_NAMES eventName,
//            UIRadioGroup radioGroup, String helpText)
//    {
//        increaseCurPos(width);
//        UIButton button = new UIButton(width, width, name,
//                new EventButtonClick(eventName, EventButtonClick.TYPE.RADIO),
//                UIButton.BUTTON_TYPE.RADIO, com.badlogic.gdx.graphics.Color.GREEN, "", helpText);
//        button.setX(_curBtnInsX);
//        button.setY(_curBtnInsY);
//        button.setRadioGroup(radioGroup);
//        radioGroup.addButton(button);
//        _btns.add(button);
//        return button;
//    }

//    private UIButton createSelectButton(
//            int width, String name,
//            EventButtonClick.BTN_NAMES eventName , boolean increaseCurPos, String helpText)
//    {
//        if ( increaseCurPos ) {
//            increaseCurPos(width);
//        }
//
//        UIButton btn = new UIButton(width, width, name,
//                new EventButtonClick(eventName, EventButtonClick.TYPE.SELECT),
//                UIButton.BUTTON_TYPE.CLICK, com.badlogic.gdx.graphics.Color.GRAY, "", helpText);
//        btn.setX(_curBtnInsX);
//        btn.setY(_curBtnInsY);
//        _btns.add(btn);
//        return btn;
//    }
//    private UIButton createToggleButton(
//            int width, String name1, String name2,
//            EventButtonClick.BTN_NAMES eventName, String helpText
//    ) {
//        increaseCurPos(width);
//        UIButton btn = new UIButton(width, width, name1,
//                new EventButtonClick(eventName, EventButtonClick.TYPE.TOGGLE),
//                UIButton.BUTTON_TYPE.TOGGLE_2_IMG, com.badlogic.gdx.graphics.Color.BLUE, name2, helpText
//        );
//        btn.setX(_curBtnInsX);
//        btn.setY(_curBtnInsY);
//        _btns.add(btn);
//        return btn;
//    }
//
//    private void increaseCurPos(int width) {
//        _curBtnInsY += width + BTN_INTERVAL;
//        if ( _curBtnInsY > _stage.getHeight() - width ) {
//            _curBtnInsX += width + BTN_INTERVAL;
//            _curBtnInsY = _initialPosition;
//        }
//    }

    public void render(float delta) {
        if ( _touchpad.getKnobPercentX() != 0 || _touchpad.getKnobPercentY() != 0 ) {
            ScreenConstruct.instance().getCameraProcessor().rotateCam(
                    -_touchpad.getKnobPercentX() * delta,
                    _touchpad.getKnobPercentY() * delta
            );
        }
        _stage.act();
        _stage.draw();
    }

    public void dispose() {
        _stage.dispose();
    }

    @Subscribe
    public void onEvent(EventButtonClick event)
    {
        switch (event._name) {
            case CONSTRUCT_SCREEN_TEXTURE_TYPE:
                showTextureSelectDialog();
                break;
//            case CONSTRUCT_SCREEN_CEILING_TYPE:
//                showCeilingSelectDialog();
//                break;
            case CONSTRUCT_SCREEN_BUILDING_LIST:
                showBuildingListDialog();
                break;
            case CONSTRUCT_SCREEN_DECO_LIST:
                showDecoListDialog();
                break;
            case CONSTRUCT_SCREEN_WALL_TYPE:
                showWallSelectDialog();
                break;
            case SCREEN_GO_BACK:
                if ( ScreenConstruct.instance()._isVisible )
                    RenderManager.instance().popScreen();
                break;
            case CONSTRUCT_SCREEN_ROOF_LIST:
                if ( event._activated ) showRoofSelectDialog();
                break;
            case CONSTRUCT_SCREEN_CEILING_LIST:
                showCeilingSelectDialog();
                break;
            case TEST:
                Configuration.instance().getFactory().getDbManager().copyToInitialDb();
                break;
            case HELP:
                _showingHelp = !_showingHelp;
                updateHelpLabel();
        }
    }
}
