package com.bokyu.buildhouse.ui;

/**
 * Created by bokyu on 2016-08-20.
 */
public class UIVerContainer extends UIContainer {
    private int _curY = 0;
    private int _curX = 0;

    public UIVerContainer(boolean canOnlySelectOne, int padding) {
        super(canOnlySelectOne, padding);
    }

    @Override
    public void init(){
        super.init();
        _curY = _padding;
        _curX = _padding;
    }

    public void add(UIGlyph uiGlyph) {
        _uiGlyphs.add(uiGlyph);
        uiGlyph.setX(getX() + _curX);
        uiGlyph.setY(getY() + _curY);
        _curY += uiGlyph.getHeight() + _padding;
        setHeight( _curY);
        _curLineWidth = Math.max(_curLineWidth, (int)uiGlyph.getWidth());
        setWidth(_widthAccumPrevLine + _curLineWidth + _padding * 2);
    }
}
