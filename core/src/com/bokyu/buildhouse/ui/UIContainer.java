package com.bokyu.buildhouse.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bokyu on 2016-08-20.
 */
public class UIContainer extends UIGlyph {
    protected List<UIGlyph> _uiGlyphs = new ArrayList();
    protected boolean _canOnlySelectOne = false;
    protected int _padding = 0;
    protected Texture _backImage = null;
    protected int _curLine = 0;
    protected int _curLineWidth = 0;
    protected int _widthAccumPrevLine = 0;
    protected Color _color = Color.WHITE;

    public UIContainer(boolean canOnlySelectOne, int padding) {
        _canOnlySelectOne = canOnlySelectOne;
        _padding = padding;
    }
    public void init(){
        _uiGlyphs.clear();
    }
//    @Override
//    public void setX(float x) {
//        int diff = x - _x;
//        _x = x;
//
//        for ( UIGlyph uiGlyph : _uiGlyphs ) {
//            uiGlyph.setX( uiGlyph.getX() + diff );
//        }
//    }
//
//    @Override
//    public void setY(int y) {
//        int diff = y - _y;
//        _y = y;
//
//        for ( UIGlyph uiGlyph : _uiGlyphs ) {
//            uiGlyph.setY( uiGlyph.getY() + diff );
//        }
//    }

    public void setBackground(Color color) {
        _color = color;
    }

    public void setBackground(String imgName) {
        if ( _backImage != null ) {
            _backImage.dispose();
        }
        _backImage = new Texture(Gdx.files.internal("ui/" + imgName));
        _backImage.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }
}
