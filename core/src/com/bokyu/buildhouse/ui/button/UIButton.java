package com.bokyu.buildhouse.ui.button;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.buildhouse.ui.UIRadioGroup;
import com.bokyu.buildhouse.ui.scene2d.Scene2DManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashSet;
import java.util.Set;

import static com.bokyu.buildhouse.ui.button.UIButton.BUTTON_TYPE.RADIO;
import static com.bokyu.buildhouse.ui.button.UIButton.BUTTON_TYPE.TOGGLE;

public class UIButton extends UIGlyph {

    public enum BUTTON_TYPE {
        TOGGLE,
        TOGGLE_2_IMG,
        RADIO,
        CLICK
    }
    final int BORDER_WIDTH = 2;
    private BUTTON_TYPE _buttonType;
    private String  _imgName  = "", _imgName2 = "";
    private Texture _textureImage = null, _textureImage2 = null;
    private EventButtonClick _event = null;
    private UIRadioGroup _radioGroup;
    private Set<Table> _subMenus = new HashSet();
    private Label _helpLabel = null;


    public UIButton(int width, int height,
                    String imgName,
                    EventButtonClick event,
                    BUTTON_TYPE buttonType,
                    Color backgroundColor,
                    String imgName2, String helpText ) {

        _event = event;
        _buttonType = buttonType;
        setWidth(width);
        setHeight( height);
        setBackgroundColor(backgroundColor);

        _imgName = imgName;
        _imgName2 = imgName2;
        init();
        setSkin(UIManager.instance().getSkin(UIManager.SKINS.ARCADE));
        Cell<Label> labelCell = add(helpText).width(width);
        _helpLabel = labelCell.getActor();
        _helpLabel.setWrap(true);
        _helpLabel.setVisible(false);
    }
    public void setRadioGroup(UIRadioGroup radioGroup) {
        _radioGroup = radioGroup;
    }

    public void setHelpLabelVisible(boolean visible) {
        _helpLabel.setVisible(visible);
    }
    private void init() {
        _pixMap = new Pixmap((int)getWidth(), (int)getHeight(), Pixmap.Format. RGBA8888);
        _textureImage = new Texture(Gdx.files.internal("ui/" + _imgName));
        if ( _imgName2 != "" ) {
            _textureImage2 = new Texture(Gdx.files.internal("ui/" + _imgName2));
        }
        setupInputListener();
    }

    public void addSubMenu(Table subMenu) {
        _subMenus.add(subMenu);
        subMenu.setVisible(false);
        int margin = (int)(getStage().getWidth() * Scene2DManager.BTN_MARGIN);
        subMenu.setX(getStage().getWidth() - subMenu.getPrefWidth() - margin);
        subMenu.setY(getStage().getHeight() - subMenu.getPrefHeight() - margin);
    }

    public void setActivated(boolean activated) {
        _pressed = activated;
        _dirty = true;

        if ( !activated && (_buttonType == RADIO || _buttonType == TOGGLE)) {
            _event._activated = false;
            EventBus.getDefault().post(_event);
        }

        for ( Table subMenu : _subMenus ) {
            subMenu.setVisible(activated);
        }
    }

    public void dispose() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();
        batch.flush();
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        if ( _dirty ) {
            _dirty = false;
            if ( _drawBack ) {
                drawPixMap();
            }
        }
        int x = (int)getX();
        int y = (int)getY();
        int width = (int)getWidth();
        int height = (int)getHeight();

        if ( _drawBack )
            batch.draw(_textureBackground, x, y,width, height);
        if ( _buttonType == BUTTON_TYPE.TOGGLE_2_IMG && _pressed && _textureImage2 != null ) {
            batch.draw(_textureImage2, x, y, width, height);
        } else {
            batch.draw(_textureImage, x, y, width, height);
        }
        super.draw(batch,parentAlpha);
    }

    private void setupInputListener() {
        setBounds(getX(),getY(),getWidth(),getHeight());
        setTouchable(Touchable.enabled);
        setVisible(true);
        addListener(
                new InputListener() {
                    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                        return UIButton.this.touchDown(event,x,y,pointer,button);
                    }

                    public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                        UIButton.this.touchUp(event,x,y,pointer,button);
                    }
                }
        );
    }
    /* events */
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        int coordX = MyHelper.screen2CoordX((int)x);
        int coordY = MyHelper.screen2CoordY((int)y);

        {
            switch ( _buttonType ) {
                case TOGGLE:
                    setActivated(!_pressed);
                    if (_pressed) {
                        EventBus.getDefault().post(_event);
                    }
                    break;
                case RADIO:
                    if ( !_pressed) {
                        _radioGroup.setSelected(this);
                        _event._activated = true;
                        setActivated(true);
                        EventBus.getDefault().post(_event);
                    }
                    break;
                case CLICK:
                    if ( !_pressed) {
                        setActivated(true);
                    }
                    break;
                case TOGGLE_2_IMG:
                    setActivated(!_pressed);
                    _event._activated = _pressed;
                    EventBus.getDefault().post(_event);
                    break;
            }
        }
        return true;
    }

    @Subscribe
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        switch (_buttonType) {
            case TOGGLE:
                break;
            case RADIO:
                break;
            case CLICK:
                setActivated(false);
                EventBus.getDefault().post(_event);
                break;
        }
    }
}