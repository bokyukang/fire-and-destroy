package com.bokyu.buildhouse.ui;

/**
 * Created by bokyu on 2016-08-11.
 */
public class UIHorContainer extends UIContainer {

    private int _curX = 0;

    public UIHorContainer(boolean canOnlySelectOne, int padding) {
        super(canOnlySelectOne, padding);
    }

    @Override
    public void init(){
        super.init();
        _curX = 0;
    }

    public void add(UIGlyph uiGlyph) {
        _uiGlyphs.add(uiGlyph);
        uiGlyph.setX(_curX + getX());
        uiGlyph.setY(getY());
        _curX += uiGlyph.getWidth() + _padding;
    }

}
