package com.bokyu.buildhouse.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by bokyu on 2016-08-11.
 */
public class UIGlyph extends Table {
    protected boolean _dirty = true;
    protected Pixmap _pixMap = null;
    protected Color _backColorWhiteLerp = null;
    protected Color _backColorBlackLerp = null;
    protected Color _backColor = Color.CLEAR;
    protected boolean _pressed = false;
    protected Texture _textureBackground = null;

    public UIGlyph() {
        setBackgroundColor(Color.GRAY);
    }

    public void setBackgroundColor(Color color) {
        _backColor = new Color(color);
        _backColorWhiteLerp = _backColor.cpy().lerp(Color.WHITE, .5f);
        _backColorBlackLerp = _backColor.cpy().lerp(Color.BLACK, .5f);
    }

    protected void drawPixMap() {
        _pixMap = new Pixmap((int)getWidth(), (int)getHeight(), Pixmap.Format. RGBA8888);
        _pixMap.setColor(Color.CLEAR);
        _pixMap.fill();
        int width = (int)getWidth();
        int height = (int)getHeight();
        if (_drawBack) {
            _pixMap.setColor(Color.BLACK);
            _pixMap.drawRectangle(0, 0, width, height);
        }
        if (_pressed) {
            _pixMap.setColor(_backColorWhiteLerp);
            _pixMap.drawLine(width-1,1, width-1, height-1);
            _pixMap.drawLine(1,height-1, width-1, height-1);
            _pixMap.setColor(_backColorBlackLerp);
            _pixMap.drawLine(1,1, 1, height-1);
            _pixMap.drawLine(1,1, width-1, 1);
            _pixMap.setColor(_backColor);
            _pixMap.fillRectangle(2,2,width-3,height-3);
        } else {
            if (_drawBack) {
                _pixMap.setColor(_backColorBlackLerp);
                _pixMap.fillRectangle(1,1,width-2,height-2);
            } else {
                _pixMap.setColor(Color.argb8888(0,0,0,0));
                _pixMap.fillRectangle(1,1,width-2,height-2);
            }
        }
        if ( _textureBackground != null ) _textureBackground.dispose();
        _textureBackground = new Texture(_pixMap);
    }
    protected boolean _drawBack =true;
    public void drawBack(boolean enable) {
        _drawBack = enable;
        invalidate();
    }

    @Override
    public void setWidth(float width) {
        super.setWidth(width);
        _dirty = true;
    }
    @Override
    public void setHeight(float height) {
        super.setHeight(height);
        _dirty = true;
    }


}
