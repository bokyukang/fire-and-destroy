package com.bokyu.buildhouse.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;

/**
 * Created by bokyu on 2016-09-09.
 */
public class UIScrollPane extends ScrollPane {

    private ShapeRenderer _shapeRenderer = null;

    public UIScrollPane(Actor widget) {
        super(widget);
        _shapeRenderer = new ShapeRenderer();
        _shapeRenderer.setAutoShapeType(true);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();

        batch.end();

        _shapeRenderer.begin();

        _shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
        _shapeRenderer.setColor(Color.CYAN);
        _shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        float scrollPercent= getScrollPercentY();
        float scrollBarWidth = getScrollBarWidth();
        float scrollHeight = getScrollHeight();
        float scrollBarHeight = getScrollBarHeight();
        if ( Float.isNaN(scrollPercent ) ) {
            scrollPercent = 0;
        }
        _shapeRenderer.rect(getX()  , getY()  + getHeight() * scrollPercent,
                scrollBarWidth, scrollBarHeight);

        _shapeRenderer.end();

        batch.begin();
    }
}
