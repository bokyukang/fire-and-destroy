package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.etc.RankingManager;
import com.bokyu.buildhouse.events.EventRankingLoaded;
import com.bokyu.buildhouse.network.NetworkManager;
import com.bokyu.buildhouse.network.jobs.NetworkJobLoadRanking;
import com.bokyu.buildhouse.screen.ScreenWorld;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-02-27.
 */

public class UIDialogRanking extends UIDialog<Integer, Village> {

    private int _curPage = 0;

    public UIDialogRanking(float x, float y, float width, float height) {
        super(x,y,width,height, RankingManager.instance()._dataMap, RankingManager.instance()._keyList);
    }

    @Override
    protected void init() {
        super.init();
        _curPage = 0;

        TextButton loadBtn = new TextButton("load", _skin);
        TextButton nextBtn = new TextButton("next", _skin);
        TextButton prevBtn = new TextButton("prev", _skin);
        addButton(loadBtn);
        addButton(nextBtn);
        addButton(prevBtn);
        loadBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        load();
                    }
                });

        nextBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        _curPage++;
                        loadList();
                    }
                } );
        prevBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        _curPage = Math.max(0, _curPage -1);
                        loadList();
                    }
                } );


        EventBus.getDefault().register(this);
    }

    private void loadList() {
        NetworkManager.instance().addJob( new NetworkJobLoadRanking(_curPage));
    }

    private void load() {
        if ( _selectedData != null ) {
            _selectedData.loadVillage(ScreenWorld.MODE.NETWORK);
        } else {
            RenderManager.instance().showAlertMsg("please select");
        }
    }

    @Override
    protected UIDialogEntry createDialogEntry(Village village) {
        return new UIDialogEntry("[" + village._accountName + "] " + village._name + " +" + village._recomCount, 5);
    }

    @Subscribe
    public void onEvent(EventRankingLoaded e) {
        updateEntries();
    }
}
