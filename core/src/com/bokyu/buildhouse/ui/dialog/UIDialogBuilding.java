package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventBuildingSelected;
import com.bokyu.buildhouse.events.EventConfirm;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2016-12-19.
 */

public class UIDialogBuilding extends UIDialog<Integer, Building> {

    public UIDialogBuilding(float x, float y, float width, float height) {
        super(x,y,width,height,BuildingsManager.instance().getBuildings(),
                BuildingsManager.instance().getBuildingKeys());
    }

    @Override
    protected void init() {
        super.init();

        TextButton delBtn = new TextButton("delete", _skin);
        addButton(delBtn);
        delBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if ( _selectedData != null ) {
                            UIConfirm.instance().show("delete", "do you want to delete building?",
                                    getStage(), new EventConfirm(EventConfirm.CONFIRM_TYPE.DELETE_BUILDING_MODEL));
                        }
                    }
                });


        TextButton saveLevelBtn = new TextButton("save level", _skin);
        addButton(saveLevelBtn);
        saveLevelBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        UIDialogEnterLevel dialog = new UIDialogEnterLevel();
                        dialog.setBuilding(_selectedData);
                        getStage().addActor(dialog);
                        dialog.align(Align.right);
                    }
                });



        TextButton cancelBtn = new TextButton("cancel", _skin);
        addButton(cancelBtn);
        cancelBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        UIDialogBuilding.this.getParent().removeActor(UIDialogBuilding.this);
                    }
                });

        EventBus.getDefault().register(this);
    }

    @Override
    protected UIDialogEntry createDialogEntry(Building building) {
        return new UIDialogEntry(
                MyGraphicsHelper.instance().createTextureRegion( building, 300)
                , building.getName(), 300);
    }

    @Override
    public Object createEvent(int idx, Building building) {
        return null;
    }

    @Override
    protected  void onOkClick() {
        EventBus.getDefault().post(new EventBuildingSelected(0, _selectedData));
    }

    private void delete() {
        if ( Configuration.instance().getFactory().getDbManager().deleteBuildingModel(_selectedData._modelId) )
        {
            _dataMap.remove(_selectedData.getId());
            UIAlert.instance().show("deleted", "delete successful", getStage());
        }
        else
        {
            UIAlert.instance().show("failed", "delete failed", getStage());
        }
        updateEntries();
    }

    @Subscribe
    public void onEvent(EventConfirm eventConfirm) {
        switch ( eventConfirm._confirmType )
        {
            case DELETE_BUILDING_MODEL:
                delete();
                break;
            default:
                break;
        }
    }
}
