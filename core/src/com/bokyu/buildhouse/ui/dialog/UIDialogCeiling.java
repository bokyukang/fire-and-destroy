package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.CeilingData;
import com.bokyu.buildhouse.data.CeilingDataContainer;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventCeilingSelected;

/**
 * Created by bk on 2016-11-07.
 */

public class UIDialogCeiling extends UIDialog<String, CeilingData> {

    public UIDialogCeiling(float x, float y, float width, float height) {
        super(x,y,width,height, CeilingDataContainer.instance()._ceilingDataMap,
                CeilingDataContainer.instance()._keyList);
    }

    @Override
    protected UIDialogEntry createDialogEntry(CeilingData ceilingData) {
        return new UIDialogEntry( ceilingData.title, 100);
    }

    @Override
    public Object createEvent(int idx, CeilingData data) {
        return new EventCeilingSelected(idx, data);
    }
}
