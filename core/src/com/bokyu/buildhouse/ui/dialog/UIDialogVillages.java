package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventVillageSelected;
import com.bokyu.buildhouse.ui.UIManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-02-02.
 */

public class UIDialogVillages extends UIDialog<String, Village> {

    private TextButton _btnAddVillage, _btnRemoveVillage, _btnRenameVillage;

    public UIDialogVillages(float x, float y, float width, float height, Map<String, Village> data, List<String> keyList) {
        super(x,y,width,height, data, keyList);

        Skin skin = UIManager.instance().getSkin();

        _btnAddVillage = new TextButton("add", skin);
        _btnRemoveVillage = new TextButton("remove", skin);
        _btnRenameVillage = new TextButton("rename", skin);

        addButton(_btnAddVillage);
        addButton(_btnRemoveVillage);
        addButton(_btnRenameVillage);

        _btnAddVillage.addListener(
                new ClickListener() {
                    @Override
                    public void clicked (InputEvent event, float x, float y) {
                        EventBus.getDefault().post(
                                new EventButtonClick(EventButtonClick.BTN_NAMES.ADD_VILLAGE,
                                        EventButtonClick.TYPE.SELECT));
                    }
                }
        );

        _btnRemoveVillage.addListener(
                new ClickListener() {
                    @Override
                    public void clicked (InputEvent event, float x, float y ) {
                        EventBus.getDefault().post(
                                new EventButtonClick(EventButtonClick.BTN_NAMES.REMOVE_VILLAGE,
                                        EventButtonClick.TYPE.SELECT));

                    }
                }
        );

        _btnRenameVillage.addListener(
                new ClickListener() {
                    @Override
                    public void clicked (InputEvent event, float x, float y ) {
                        EventBus.getDefault().post(
                                new EventButtonClick(EventButtonClick.BTN_NAMES.RENAME_VILLAGE,
                                        EventButtonClick.TYPE.SELECT));

                    }

                }
        );

        _btnOK.clearListeners();
        _btnOK.addListener(
                new ClickListener() {
                    @Override
                    public void clicked (InputEvent event, float x, float y ) {
                        EventBus.getDefault().post(new EventVillageSelected(0, _selectedData));
                    }
                }
        );
    }

    @Override
    protected UIDialogEntry createDialogEntry(Village data) {
        return new UIDialogEntry(data._name, 25);
    }

    @Override
    public Object createEvent(int idx, Village data) {
        return null;
    }

}
