package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.UIManager;

import org.greenrobot.eventbus.EventBus;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by bk on 2017-01-04.
 */

public class UIDialog<K, T> extends UIGlyph {
    protected Window _dlBaseContainer;
    protected ScrollPane _spEntryContainer;
    protected TextArea _taDescription;
    protected Table _tbEntryContainer;
    protected Table _tbButtonContainer;
    protected Set<UIDialogEntry> _entries = new HashSet();
    protected TextButton _btnOK = null;
    protected Map<K, T> _dataMap = null;
    protected List<K> _keyList = null;
    protected UIDialogEntry _selectedEntry = null;
    protected T _selectedData = null;
    protected Skin _skin = null;


    public UIDialog(float x, float y, float width, float height, Map<K, T> dataList, List<K> keyList) {
        setWidth(width);
        setHeight(height);
        _dataMap = dataList;
        _keyList = keyList;
        init();
    }


    protected void init()
    {
        setSkin(UIManager.instance().getSkin());
        _skin = UIManager.instance().getSkin();
//        _pixMap = new Pixmap((int)getWidth(), (int)getHeight(), Pixmap.Format. RGBA8888);
//        setBackgroundColor(Color.BROWN);

        _dlBaseContainer = new Window("", _skin);
        _dlBaseContainer.setWidth(getWidth());
        _dlBaseContainer.setHeight(getHeight());
        _tbEntryContainer = new Table(_skin);
        _tbEntryContainer.pad(0).defaults().expandX().space(4);
        _spEntryContainer = new ScrollPane(_tbEntryContainer, _skin);
////        _taDescription = new TextArea()
        _tbButtonContainer = new Table(_skin);
        add(_dlBaseContainer).pad(0).width(getWidth()).height(getHeight());
        _btnOK = new TextButton("OK", _skin);


        /* add listener */
        _btnOK.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        if ( event.getType() == InputEvent.Type.touchUp
                                && UIDialog.this.getParent() != null)
                        {
                            UIDialog.this.getParent().removeActor(UIDialog.this);
                            onOkClick();
                        }
                    }
                });

        float entryHeight = getHeight() * .8f;
        _dlBaseContainer.add(_spEntryContainer).expandX().fillX().height( entryHeight );
        _dlBaseContainer.row();
        _dlBaseContainer.add(_tbButtonContainer).expand().fill();

        updateEntries();

        addButton(_btnOK);
    }

    protected void onOkClick() {

    }

    public void addButton(TextButton btn) {
        btn.setTouchable(Touchable.enabled);
        _tbButtonContainer.add(btn).minWidth(0).expand().center();
    }

    public void select(int idx) {
        try {
            UIDialogEntry entry = (UIDialogEntry) _tbEntryContainer.getChildren().get(idx);
            entry.setActivated(true);
            for ( EventListener el : entry.getListeners() )
            {
                if ( el instanceof ClickListener) {
                    ((ClickListener) el).clicked(null, 0, 0);
                }
            }
        } catch ( IndexOutOfBoundsException e ) {

        }
    }

    public void updateEntries() {
        int i =0;
        _tbEntryContainer.clearChildren();

        if ( _keyList == null ) return;

        for (final K key : _keyList) {
            final T data = _dataMap.get(key);
            _tbEntryContainer.row();

            try {
                final UIDialogEntry entry = createDialogEntry(data);
                if ( entry == null ) continue;

                _entries.add(entry);

                final int idx = i;
                entry.setTouchable(Touchable.enabled);
                _tbEntryContainer.add(entry).expandX().fillX().pad(0);
                entry.align(Align.center);
                ClickListener clickListener =
                        new ClickListener() {
                            private T _data = null;
                            private int _idx = -1;
                            public void clicked (InputEvent event, float x, float y) {
                                _data = data;
                                _idx = idx;
                                Object evt = createEvent(_idx, _data);
                                if ( evt != null ) {
                                    EventBus.getDefault().post( evt );
                                }
                                if ( _selectedEntry != null ) _selectedEntry.setActivated(false);
                                entry.setActivated(true);
                                _selectedEntry = entry;
                                _selectedData = data;
                            }
                        };
                entry.addListener( clickListener );

            } catch ( GdxRuntimeException e ) {
                e.printStackTrace();
            }
            ++i;
        }
        setLayoutEnabled(true);
        layout();
        layout();
    }

    protected UIDialogEntry createDialogEntry(T data) {
        return null;
    }
    public Object createEvent(int idx, T data) {
        return null;
    }

    public T getSelectedData() { return _selectedData; }

}
