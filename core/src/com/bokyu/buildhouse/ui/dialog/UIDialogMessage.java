package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Timer;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.UIManager;


/**
 * Created by bk on 2016-12-01.
 */

public class UIDialogMessage extends UIGlyph {

    static private UIDialogMessage _instance = null;
    private Table _tbEntryContainer;
    private Window _dlBaseContainer;
    private Label _textField;
    private Timer _timer;

    static public UIDialogMessage instance() {
        if (_instance == null ) {
            _instance = new UIDialogMessage();
        }
        return _instance;
    }

    private UIDialogMessage() {
        init();
    }
    private void init() {
        _timer = new Timer();
        Skin skin = UIManager.instance().getSkin();
        _dlBaseContainer = new Window("", skin);
        _tbEntryContainer = new Table(skin);
        _textField = new Label("",skin);

        add(_dlBaseContainer).pad(0);
        _dlBaseContainer.add(_tbEntryContainer).expandX().fill();
        _tbEntryContainer.add(_textField).fill().expand();
    }

    public void setMsg(String msg) {
        _textField.setText(msg);
        pack();

        _timer.scheduleTask(
                new Timer.Task() {
                    @Override
                    public void run() {
                        close();
                    }
                },
                5
        );
    }

    public void close() {
        if ( getParent() != null ) {
            getParent().removeActor(this);
        }
    }

}
