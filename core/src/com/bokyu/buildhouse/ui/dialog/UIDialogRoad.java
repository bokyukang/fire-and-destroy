package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.data.RoadDataContainer;
import com.bokyu.buildhouse.events.EventRoadSelected;

/**
 * Created by bk on 2017-02-01.
 */

public class UIDialogRoad extends UIDialog<String, RoadData> {

    public UIDialogRoad(float x, float y, float width, float height) {
        super( x,y, width, height, RoadDataContainer.instance()._roadDataMap,
                RoadDataContainer.instance()._keyList);
    }

    @Override
    protected UIDialogEntry createDialogEntry(RoadData roadData) {
        return new UIDialogEntry("roads/" + roadData.id + "/two_str.gif", roadData.title, 100);
    }

    @Override
    public Object createEvent(int idx, RoadData data) {
        return new EventRoadSelected(idx, data);
    }
}
