package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bokyu.buildhouse.ui.UIManager;

/**
 * Created by bk on 2017-01-11.
 */

public class UIAlert extends Dialog {

    static private UIAlert _instance = null;
    private Label _msgLabel = null;

    static public UIAlert instance() {
        if ( _instance == null ) _instance = new UIAlert();
        return _instance;
    }

    private UIAlert() {
        super("", UIManager.instance().getSkin());
        Skin skin = UIManager.instance().getSkin();
        _msgLabel = new Label("", skin);
        _msgLabel.setWrap(true);
        getContentTable().add(_msgLabel).expand().fill();
        TextButton okBtn = new TextButton("ok", skin);
        getButtonTable().add(okBtn).minWidth(0).expand().center();
        okBtn.addListener(
            new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    hide();
                }
            }
        );
    }

    public void show(String title, String msg, Stage stage) {
        getTitleLabel().setText(title);
        _msgLabel.setText(msg);
        show(stage).right();
        setX(Gdx.graphics.getWidth()/2f);
    }

    @Override
    public float getPrefWidth() {
        float prefW = super.getPrefWidth();
        return Math.min(prefW, Gdx.graphics.getWidth());
    }

}
