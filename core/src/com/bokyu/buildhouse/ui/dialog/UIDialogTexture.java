package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.data.TextureDataContainer;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;

/**
 * Created by bokyu on 2016-09-07.
 */
public class UIDialogTexture extends UIDialog<String, TextureData> {

    public UIDialogTexture(float x, float y, float width, float height) {
        super(x,y,width,height, TextureDataContainer.instance()._textureDataMap,
                TextureDataContainer.instance()._keyList);
    }

    @Override
    protected UIDialogEntry createDialogEntry(TextureData textureData) {
        if ( textureData.model_type.equals("texture")) {
            return new UIDialogEntry("textures/" + textureData.id + ".gif"
                    , textureData.title, 100);
        } else if ( textureData.model_type.equals("model")) {
            return new UIDialogEntry(
                    MyGraphicsHelper.instance().createTextureRegion(
                            "textures/" + textureData.id + ".g3db", 100,
                            "brick_1.gif"
                            )
                    , textureData.title, 100);
        } else {
            return null;
        }
    }

    @Override
    public Object createEvent(int idx, TextureData data) {
        return new EventTextureEntrySelected(idx, data);
    }


}
