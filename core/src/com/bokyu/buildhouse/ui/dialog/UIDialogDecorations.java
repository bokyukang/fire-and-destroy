package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.DecorationData;
import com.bokyu.buildhouse.data.DecorationDataContainer;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventDecorationEntrySelected;

/**
 * Created by bk on 2017-01-04.
 */

public class UIDialogDecorations extends UIDialog<String, DecorationData> {

    public UIDialogDecorations(float x, float y, float width, float height) {
        super(x, y, width, height, DecorationDataContainer.instance()._decorationDataMap,
                DecorationDataContainer.instance()._keyList);
    }


    @Override
    protected UIDialogEntry createDialogEntry(DecorationData data) {
        return new UIDialogEntry(
                MyGraphicsHelper.instance().createTextureRegion(
                        "models/" + data.id + ".g3db", 100, "brick_1.gif")
                , data.title, 100);
    }

    @Override
    public Object createEvent(int idx, DecorationData data) {
        return new EventDecorationEntrySelected(idx, data);
    }
}
