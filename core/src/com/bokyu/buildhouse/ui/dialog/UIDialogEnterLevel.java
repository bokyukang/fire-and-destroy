package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.UIManager;
import com.bokyu.fireanddestroy.events.EventBuildingSaved;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by admin on 2018-08-14.
 */

public class UIDialogEnterLevel extends UIGlyph {
    private Window _dlBaseContainer;
    private Table _tbEntryContainer;
    private Table _tbButtonContainer;
    private TextButton _okBtn = null;
    private TextButton _cancelBtn = null;
    private TextField _textField = null;
    private Building _building = null;

    public UIDialogEnterLevel() {
        setSkin(UIManager.instance().getSkin());
        setX(0);
        setY(0);
        setWidth(Math.min(Gdx.graphics.getWidth(),400));
        setHeight(Math.min(Gdx.graphics.getHeight(), 200));
        setFillParent(true);
        align(Align.right);
        init();
    }

    private void init() {
        Skin skin = UIManager.instance().getSkin();
        float entryHeight = getHeight() * .8f;

        _dlBaseContainer = new Window("please enter name", skin);

        _tbEntryContainer = new Table(skin);
        _tbEntryContainer.pad(0).defaults().expandX().space(4);
        _tbButtonContainer = new Table(skin);
        add(_dlBaseContainer).pad(0).width(getWidth()).height(getHeight());
        layout();
        _okBtn = new TextButton("OK", skin);
        _cancelBtn = new TextButton("Cancel", skin);

        _textField = new TextField("", skin);
        _tbEntryContainer.add(_textField).fillX().expandX();

        _dlBaseContainer.add(_tbEntryContainer).expandX().fillX();
        _dlBaseContainer.row();
        _dlBaseContainer.add(_tbButtonContainer).expandX().fillX();

        _dlBaseContainer.layout();
        _tbButtonContainer.layout();
        _okBtn.layout();
        _cancelBtn.layout();
        float btnWidth = getWidth()/2 - getPadLeft();
        _tbButtonContainer.add(_okBtn).width(btnWidth
        ).expandX().fillX();
        _tbButtonContainer.add(_cancelBtn).width(
                btnWidth).expandX().fillX();

        _dlBaseContainer.setTouchable(Touchable.enabled);


        _okBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        try {
                            String name =  _textField.getText();
                            UIDialogEnterLevel.this.getParent().removeActor(UIDialogEnterLevel.this);
                            EventBus.getDefault()
                                    .post(new EventBuildingSaved(_building, name));
                        } catch ( NumberFormatException e ) {
                            e.printStackTrace();
                        }
                    }
                });
        _okBtn.setTouchable(Touchable.enabled);
        _cancelBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        UIDialogEnterLevel.this.getParent().removeActor(UIDialogEnterLevel.this);
                    }
                }
        );
        _cancelBtn.setTouchable(Touchable.enabled);
    }

    public void setBuilding(Building building) {
        _building = building;
    }
}
