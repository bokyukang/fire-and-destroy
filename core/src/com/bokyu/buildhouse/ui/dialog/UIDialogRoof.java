package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.RoofModelData;
import com.bokyu.buildhouse.data.RoofModelDataContainer;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventRoofModelEntrySelected;

/**
 * Created by bk on 2017-01-05.
 */

public class UIDialogRoof extends UIDialog<String, RoofModelData> {
    public UIDialogRoof(float x, float y, float width, float height) {
        super(x,y,width,height, RoofModelDataContainer.instance()._roofModelDataMap,
                RoofModelDataContainer.instance()._keyList);
    }

    @Override
    protected UIDialogEntry createDialogEntry(RoofModelData roofData) {
        return new UIDialogEntry(
                MyGraphicsHelper.instance().createTextureRegion(
                        "models/" + roofData.id + ".g3db", 100, "brick_1.gif")
                , roofData.title, 100);
    }

    @Override
    public Object createEvent(int idx, RoofModelData data) {
        return new EventRoofModelEntrySelected(idx, data);
    }
}
