package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.bokyu.buildhouse.events.EventConfirm;
import com.bokyu.buildhouse.ui.UIManager;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by bk on 2017-01-11.
 */

public class UIConfirm extends Dialog {

    static private UIConfirm _instance = null;
    private Label _msgLabel = null;
    private Object _event = null;

    static public UIConfirm instance() {
        if ( _instance == null ) _instance = new UIConfirm();
        return _instance;
    }

    private UIConfirm() {
        super("", UIManager.instance().getSkin(UIManager.SKINS.CRAFTCULAR));
        Skin skin =UIManager.instance().getSkin(UIManager.SKINS.CRAFTCULAR);
        _msgLabel = new Label("", skin);
        getContentTable().add(_msgLabel).expand().fill().align(Align.right);
        TextButton okBtn = new TextButton("ok", skin);
        _msgLabel.setWrap(true);
        getButtonTable().add(okBtn).minWidth(0).expand().right();
        okBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        EventBus.getDefault().post(_event );
                        hide();
                    }
                }
        );
        TextButton cancelBtn = new TextButton("cancel", skin);
        getButtonTable().add(cancelBtn).minWidth(0).expand().right();
        cancelBtn.addListener(
                new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        hide();
                    }
                }
        );
        align(Align.right);
    }

    public void show(String title, String msg, Stage stage, Object event) {
        _event = event;
        getTitleLabel().setText(title);
        _msgLabel.setText(msg);
        align(Align.right);
//        stage.addActor(this);
        show(stage).align(Align.right).right();
        setX(Gdx.graphics.getWidth()/2f);
    }

    @Override
    public float getPrefWidth() {
        float prefW = super.getPrefWidth();
        return Math.min(prefW, Gdx.graphics.getWidth());
    }
}
