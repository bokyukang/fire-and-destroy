package com.bokyu.buildhouse.ui.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.bokyu.buildhouse.ui.UIGlyph;
import com.bokyu.buildhouse.ui.UIManager;

/**
 * Created by bokyu on 2016-09-07.
 */
public class UIDialogEntry extends UIGlyph {

    private Image _image;
    private Label _text;
    private Texture _texture= null;

    public UIDialogEntry(String text, int padding) {
        _text = new Label(text, UIManager.instance().getSkin());
        add(_text).expandX().fillX().pad(padding);
        _text.layout();
        this.layout();
    }
    public UIDialogEntry(String imgPath, String text, int imgWidth) {
        this(new TextureRegion(new Texture(imgPath)), text, imgWidth);
    }
    public UIDialogEntry(TextureRegion textureRegion, String text, int imgWidth) {
        _texture = textureRegion.getTexture();
        _image = new Image(textureRegion);
        _text = new Label(text, UIManager.instance().getSkin());
        int newHeight = (int) (_image.getHeight() * imgWidth / _image.getWidth());
        add(_image).width(imgWidth).height(newHeight).pad(imgWidth / 20);
        add(_text).expandX().fillX();
        _image.layout();
        _text.layout();
        this.layout();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();
        batch.flush();
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        if ( _dirty ) {
            _dirty = false;
            drawPixMap();
        }
        int x = (int)getX();
        int y = (int)getY();
        int width = (int)getWidth();
        int height = (int)getHeight();

        batch.draw(_textureBackground, x, y,width, height);
        super.draw(batch,parentAlpha);
    }

    public void setActivated(boolean activated) {
        _pressed = activated;
        _dirty = true;
    }

    public void dispose() {
        _texture.dispose();
    }
}
