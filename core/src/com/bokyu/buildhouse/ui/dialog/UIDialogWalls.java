package com.bokyu.buildhouse.ui.dialog;

import com.bokyu.buildhouse.data.WallModelData;
import com.bokyu.buildhouse.data.WallModelDataContainer;
import com.bokyu.buildhouse.etc.MyGraphicsHelper;
import com.bokyu.buildhouse.events.EventWallModelEntrySelected;

/**
 * Created by bk on 2017-01-04.
 */

public class UIDialogWalls extends UIDialog<String, WallModelData> {

    public UIDialogWalls(float x, float y, float width, float height) {
        super(x, y, width, height, WallModelDataContainer.instance()._wallModelDataMap,
                WallModelDataContainer.instance()._keyList);
    }


    @Override
    protected UIDialogEntry createDialogEntry(WallModelData data) {
        return new UIDialogEntry(
                MyGraphicsHelper.instance().createTextureRegion(
                        "models/" + data.id + ".g3db", 100, "brick_1.gif")
                , data.title, 100);
    }

    @Override
    public Object createEvent(int idx, WallModelData data) {
        return new EventWallModelEntrySelected(idx, data);
    }
}
