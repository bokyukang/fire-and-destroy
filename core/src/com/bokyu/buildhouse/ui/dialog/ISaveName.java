package com.bokyu.buildhouse.ui.dialog;

/**
 * Created by bk on 2016-11-29.
 */

public interface ISaveName {
    void saveName(String name);
}
