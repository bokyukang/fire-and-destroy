package com.bokyu.buildhouse.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by bokyu on 2016-09-12.
 */
public class UIManager {

    static private UIManager _instance = null;
    private Map<SKINS, Skin> _skins = new HashMap();
    public enum SKINS {
        CRAFTCULAR,
        ARCADE,
        COMIC,
        LGDXS
    }
    static public String SKIN_NAME_TITLE = "title";

    static public UIManager instance() {
        if ( _instance == null ) {
            _instance = new UIManager();
            _instance.init();
        }
        return _instance;
    }

    private void init() {
        _skins.put(SKINS.CRAFTCULAR, loadSkin("craftacular"));
        _skins.put(SKINS.ARCADE, loadSkin("arcade"));
        _skins.put(SKINS.COMIC, loadSkin("comic"));
        _skins.put(SKINS.LGDXS, loadSkin("lgdxs"));
    }
    private Skin loadSkin(String name) {
        FileHandle skinFile = Gdx.files.internal("skin/" + name + "/" + name + "-ui.json");
        FileHandle atlasFile = Gdx.files.internal("skin/" + name + "/" + name + "-ui.atlas");
        Skin skin = new Skin(skinFile);
        skin.addRegions(new TextureAtlas(atlasFile));
        return skin;
    }

    public Skin getSkin(SKINS name) { return _skins.get(name); }
    public Skin getSkin() { return _skins.get(SKINS.CRAFTCULAR); }
}
