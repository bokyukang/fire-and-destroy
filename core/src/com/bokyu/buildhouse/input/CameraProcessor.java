package com.bokyu.buildhouse.input;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.buildhouse.RenderManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bokyu on 2016-08-24.
 */
public class CameraProcessor  extends InputAdapter {

    float ANGLE_LIMIT_MAX = 80;
    float ANGLE_LIMIT_MIN = 10;
    float TARGET_Y = 3;
    float TARGET_DISTANCE = 10;
    Vector3 UP = new Vector3(0,1,0);
    Vector3 _target = new Vector3(0,TARGET_Y, 0);
    private Map<Integer, Vector2> _positions = new HashMap<Integer, Vector2>();
    private PerspectiveCamera _perspectiveCam = null;
    private boolean _isZoom = false;
    private float _maxBound = 0;

    public CameraProcessor(PerspectiveCamera cam, float maxBound) {
        _perspectiveCam = cam;
        _maxBound = maxBound;
    }

    public boolean touchDown (int x, int y, int pointer, int button) {
        _positions.put(pointer, new Vector2(x,y));
        if ( _positions.size() >= 2 ) _isZoom = true;
        return false;
    }

    public boolean touchUp (int x, int y, int pointer, int button) {
        _positions.remove(pointer);
        if ( _positions.size() == 0 ) _isZoom = false;
        return false;
    }

    public boolean touchDragged (int x, int y, int pointer) {
        if (RenderManager.instance().checkSceneObject(x,y)) return false;
        if ( _positions.size() == 1 && !_isZoom )
        {
            Vector2 moveVec = new Vector2(x,y).sub(_positions.get(pointer)).scl(0.7f);
            if ( moveVec.len() < 50 ) {
                moveTarget(moveVec.x, 0, moveVec.y);
            }
            _positions.get(pointer).set(x, y);
        }
        else if ( _positions.size() > 1 ) {
            /* zoom */
            float originalLen = getDistance();
            _positions.get(pointer).set(x,y);
            float afterLen = getDistance();
            float diff = afterLen - originalLen;
            moveTarget(0,0,diff);

//            Vector3 newPosition = _perspectiveCam.position.cpy().add(
//                    _perspectiveCam.direction.cpy().scl(diff / 100f));
//            float distance = newPosition.len();
//            if ( distance < MIN_ZOOM && distance > MAX_ZOOM )
//            {
//                _perspectiveCam.translate( _perspectiveCam.direction.cpy().scl(diff / 100f));
////                    _perspectiveCam.position.scl(MIN_ZOOM / distance);
//            }
        }

        _perspectiveCam.up.set(_perspectiveCam.direction.cpy().crs(UP).crs(_perspectiveCam.direction).nor());

        _perspectiveCam.update();
        return false;
    }

    public void rotateCam(float pan, float updown) {
//        Vector3 CAM_RIGHT_DIR = _perspectiveCam.direction.cpy().crs(_perspectiveCam.up);

//        Vector3 cross = _perspectiveCam.up.cpy().nor().crs(UP);
//        float angle = (float)Math.toDegrees(Math.asin(cross.len()));
//        float rotAmount = (updown / 10f);
        moveTarget(0, -updown, 0);
        _perspectiveCam.rotateAround( _target, UP, (pan / 10f));

//        if ( angle -rotAmount< ANGLE_LIMIT_MAX && angle - rotAmount> ANGLE_LIMIT_MIN ) {
//            _perspectiveCam.rotateAround( _target, CAM_RIGHT_DIR
//                    , rotAmount);
//        }
        _perspectiveCam.up.set(_perspectiveCam.direction.cpy().crs(UP).crs(_perspectiveCam.direction).nor());
        _perspectiveCam.update();

    }

    private float getDistance() {
        int count = 0;
        Vector2 v1 = null, v2 = null;
        for ( Vector2 val : _positions.values() ) {
            if ( count == 0 ) {
                v1 = val;
            } else if ( count == 1 ) {
                v2 = val;
            } else {
                break;
            }

            ++count;
        }
        try {
            return v1.cpy().sub(v2).len();
        }catch ( NullPointerException e ) {
            e.printStackTrace();
            return 0;
        }
    }

    public void moveTarget(float x, float y, float z) {
        Vector3 right = _perspectiveCam.direction.cpy().crs(UP).nor().scl(-1);
        Vector3 tmp = _perspectiveCam.direction.cpy();
        Vector3 up = new Vector3(0,1,0);
        tmp.y = 0;
        Vector3 forward = tmp.nor();
        Vector3 move = up.scl(y* 0.01f).add(forward.scl(z * 0.01f)).add(right.scl(x * 0.01f));
        _target.add(move);
        _target.y = TARGET_Y;
        _perspectiveCam.translate(move);
        _target = _target.cpy().sub(_perspectiveCam.position).nor().scl(TARGET_DISTANCE).add(_perspectiveCam.position);
        _perspectiveCam.lookAt(_target);
        _perspectiveCam.update();
    }

}

