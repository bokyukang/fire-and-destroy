package com.bokyu.buildhouse;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.bokyu.buildhouse.events.EventTouch;
import com.bokyu.buildhouse.events.EventTouchRelease;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by bokyu on 2016-08-08.
 */
public class BHInputProcessor extends InputAdapter {

    static private BHInputProcessor _instance = null;

    private InputMultiplexer _inputMultiplexer = null;

    static public BHInputProcessor instance() {
        if (_instance == null ) {
            _instance = new BHInputProcessor();
        }
        return _instance;
    }

    private BHInputProcessor() {
        _inputMultiplexer = new InputMultiplexer(this);
        Gdx.input.setInputProcessor(_inputMultiplexer);
    }

    public void addInputProcessor(InputProcessor processor) {
        _inputMultiplexer.addProcessor(processor);
    }
    public void removeInputProcessor(InputProcessor processor) {
        _inputMultiplexer.removeProcessor(processor);
    }

    public void onEnter() {
        Gdx.input.setInputProcessor(_inputMultiplexer);
    }
    public void onExit() {
    }

    public void setInputProcessor(InputProcessor inputProcessor) {
        _inputMultiplexer.clear();
        _inputMultiplexer.addProcessor(this);
        _inputMultiplexer.addProcessor(inputProcessor);
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        EventBus.getDefault().post(new EventTouch(x,y));
        return false;
    }
    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        EventBus.getDefault().post(new EventTouchRelease(x,y));
        return false;
    }
}
