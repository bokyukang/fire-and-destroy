package com.bokyu.buildhouse.localdb;

import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.data.GlyphDbData;
import com.bokyu.buildhouse.glyph.Building;

import java.util.List;
import java.util.Set;

/**
 * Created by bk on 2016-12-20.
 */

public interface DbManager {
    void init();
    boolean saveBuildingModel(Building building);
    Set<Building> loadBuildingModels();
    boolean deleteBuildingModel(int id);

    boolean saveBuilding(Building building);
    Set<Building> loadBuildings();
    boolean deleteBuilding(int id);

    boolean saveTerrein(String encodedTerrein);
    String loadTerrein();

    void renameVillage(String name, int villageId);
    int addVillage(String name);
    int removeVillage(int villageId);
    List<Village> getVillages();

    void copyToInitialDb();

    Set<GlyphDbData> loadGlyphs();
    boolean saveGlyph(GlyphDbData data);

    boolean saveClearedLevel(int level);
    int getClearedLevel();
}
