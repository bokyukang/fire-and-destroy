package com.bokyu.buildhouse;

import com.bokyu.fireanddestroy.jobs.JobManager;
import com.bokyu.fireanddestroy.listeners.RewardAdListener;

public interface AdManager {
    void reloadMenuAd();
    void showInterstitial(final JobManager.Token token);
    void showReward(RewardAdListener listener);
    void setMenuAdVisibility(boolean menuVisible, boolean playVisibile);
}
