package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.data.RoofModelData;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.events.EventRoofModelEntrySelected;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.concrete.Roof;
import com.bokyu.buildhouse.screen.ScreenConstruct;
import com.bokyu.fireanddestroy.players.Character;
import com.bokyu.fireanddestroy.players.EnemyCharacter;

import org.greenrobot.eventbus.Subscribe;

import java.util.Set;

/**
 * Created by bk on 2017-01-05.
 */

public class EnemyInjecter extends ConstructInjecter {
    protected EnemyCharacter _controllingEnemy = null;
    protected DesignGround _ground = null;
    protected EnemyCharacter _deleteTarget = null;
    protected TextureData _curTextureData = null;
    protected RoofModelData _curRoofModelData = null;
//    private Constants.DIRECTION _direction = NORTH;

    public EnemyInjecter(DesignGround ground) {
        super();
        _ground = ground;

    }

    public void setTextureType(TextureData textureData) {
        if ( textureData == null ) return;
        _curTextureData = textureData;
        _controllingEnemy.setTextureId(textureData.id);
    }

    public void setRoofModelType(RoofModelData modelData) {
        _curRoofModelData = modelData;
//        if ( _controllingRoof == null ) return;
//        _controllingRoof.setModel(modelData.id);
//        _controllingRoof.setMode(MyGlyph.MODE.WIREFRAME);
    }
    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if (!ScreenConstruct.instance()._deleteMode) _controllingEnemy.render(modelBatch, environment);
    }

    @Override
    public void update() {
//        if ( _curTextureData != null ) setTextureType(_curTextureData);
//        if ( _curRoofModelData != null ) setRoofModelType(_curRoofModelData);

        placeEnemy(_controllingEnemy);

        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
        if ( ScreenConstruct.instance()._deleteMode )
        {
            BuildingGlyph glyph = _ground.getTopGlyph(ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY, Constants.DIRECTION.ALL, Roof.class);
            if ( glyph instanceof EnemyCharacter) {
                _deleteTarget = (EnemyCharacter) glyph;
                _deleteTarget.setMode(MyGlyph.MODE.COLOR);
                _deleteTarget.updatePosition();
            }
        }

    }

    protected void placeEnemy(EnemyCharacter enemy) {
        enemy.setDirection(ScreenConstruct.instance()._direction);
        Set<Constants.DIRECTION> occupyingDirs = enemy.getOccupyingDirs();
        int elevation = _ground.getElevation(
                ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY,
                occupyingDirs, enemy);

        enemy.setElevation(elevation);
        enemy.setPositionIndexes(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY);
//        roof.setPosition(
//                ScreenConstruct.instance()._selIdxX * Constants.WALL_LENGTH - _ground.getTotalWidth() / 2f,
//                -ScreenConstruct.instance()._selIdxY * Constants.WALL_LENGTH + _ground.getTotalHeight() / 2f
//        );
        enemy.updatePosition();
    }

    @Override
    public void onEnter() {
        super.onEnter();
        if ( _controllingEnemy == null ) {
            _controllingEnemy = new EnemyCharacter(1);//ScreenConstruct.instance()._selectedRoofModelData.id, "", 2);
            _controllingEnemy.setMode(MyGlyph.MODE.WIREFRAME);
        }
        setRoofModelType(ScreenConstruct.instance()._selectedRoofModelData);
        setTextureType(ScreenConstruct.instance()._selectedTextureData);
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
    }

    @Override
    public void insert() {
        if ( ScreenConstruct.instance()._deleteMode == false ) {
            Character newRoof = _controllingEnemy.clone();
            int elevation = _ground.getElevation(
                    ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY,
                    newRoof.getOccupyingDirs(), newRoof);
            newRoof.setElevation(elevation);
            newRoof.updatePosition();
            _ground.addCharacter(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY, newRoof);
        }
        else {
            if ( _deleteTarget != null ) {
                _ground.removeCharacter(_deleteTarget, 0, 0);
                _deleteTarget = null;
            }
        }
        update();
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event)
    {
        setTextureType(event._selectedTextureData);
        update();
    }

    @Subscribe
    public void onEvent(EventRoofModelEntrySelected event)
    {
        setRoofModelType(event._selectedRoofModelData);
        update();
    }

}
