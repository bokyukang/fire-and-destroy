package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.data.CeilingData;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventCeilingSelected;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Ceiling;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.screen.ScreenConstruct;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bk on 2016-11-08.
 */

public class CeilingInjecter extends ConstructInjecter {
    protected Ceiling _controllongCeiling = null;
    protected DesignGround _ground = null;
    protected Ceiling _deleteTarget = null;
    protected TextureData _curTextureData = null;
    protected List<Integer> _neighborHeights = new ArrayList();
    protected int _neighborHeightIdx = -1;
    protected CeilingData _curCeilingData = null;

    public CeilingInjecter(DesignGround ground) {
        super();
        _ground = ground;
        _controllongCeiling = new Ceiling("1x1", "brick_1", 1);
        _controllongCeiling.setMode(MyGlyph.MODE.WIREFRAME);
    }

    public void setTextureType(TextureData textureData) {
        if ( textureData == null ) return;
        _curTextureData = textureData;
        _controllongCeiling.setTextureId(textureData.id);
    }

    public void setCeilingData(CeilingData data) {
        _curCeilingData = data;
        _controllongCeiling.setData(_curCeilingData);
        _controllongCeiling.setMode(MyGlyph.MODE.WIREFRAME);
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if ( !ScreenConstruct.instance()._deleteMode ) _controllongCeiling.render(modelBatch, environment);
    }
    @Override
    public void update() {
        placeCeiling(_controllongCeiling);

        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
        if ( ScreenConstruct.instance()._deleteMode )
        {
            BuildingGlyph glyph = _ground.getTopGlyph(ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY, ScreenConstruct.instance()._direction, Ceiling.class);
            if ( glyph instanceof Ceiling ) {
                _deleteTarget = (Ceiling)glyph;
                _deleteTarget.setMode(MyGlyph.MODE.COLOR);
                _deleteTarget.updatePosition();
            }
        }
    }

    protected void placeCeiling(Ceiling ceiling) {
        ceiling.setDirection(ScreenConstruct.instance()._direction);

        ceiling.setElevation(
                _ground.getElevation(
                        ScreenConstruct.instance()._selIdxX,
                        ScreenConstruct.instance()._selIdxY,
                        ceiling.getOccupyingDirs(),
                        ceiling.getDIrection(),
                        ceiling.getSpanX(),
                        ceiling.getSpanY()
                        ));
        ceiling.setPositionIndexes(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY);
        ceiling.setPosition(
                ScreenConstruct.instance()._selIdxX * Constants.WALL_LENGTH - _ground.getTotalWidth() / 2f,
                -ScreenConstruct.instance()._selIdxY * Constants.WALL_LENGTH + _ground.getTotalHeight() / 2f
        );

        _neighborHeights = _ground.getNeighborCeilingHeights(
                ScreenConstruct.instance()._selIdxX,
                ScreenConstruct.instance()._selIdxY,
                ceiling.getOccupyingDirs());
        _controllongCeiling.updatePosition();

        _neighborHeightIdx = -1;
    }


    @Override
    public void onEnter() {
        super.onEnter();
        setTextureType(ScreenConstruct.instance()._selectedTextureData);
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
    }

    @Override
    public void insert() {
        if ( ScreenConstruct.instance()._deleteMode == false ) {
            Ceiling newCeiling = _controllongCeiling.clone();
//            int elevation = _ground.getElevation(
//                    ScreenConstruct.instance()._selIdxX,
//                    ScreenConstruct.instance()._selIdxY,
//                    newCeiling.getOccupyingDirs(), newCeiling);
//            maxElevation = Math.max(maxElevation, elevation);
            newCeiling.updatePosition();
            _ground.addCeiling(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY, newCeiling);
        }
        else {
            if ( _deleteTarget != null ) {
                _ground.removeCeiling(_deleteTarget, 0, 0);
                _deleteTarget = null;
                update();
            }
        }
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event) {
        setTextureType(event._selectedTextureData);
        update();
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case UPDOWN:
                _neighborHeightIdx++;
                if ( _neighborHeightIdx >= _neighborHeights.size())
                {
                    placeCeiling(_controllongCeiling);
                }
                else {
                    _controllongCeiling.setElevation(_neighborHeights.get(_neighborHeightIdx));
                    _controllongCeiling.updatePosition();
                }
                break;
        }
    }

    @Subscribe
    public void onEvent(EventCeilingSelected event) {
        setCeilingData(event._selectedCeilingData);
        update();
    }
}


