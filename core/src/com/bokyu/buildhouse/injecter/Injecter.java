package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.events.EventButtonClick;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by bokyu on 2016-08-17.
 */
public class Injecter {
    public void onButtonClick(EventButtonClick btnClickEvent) {}
    public void onEnter() {
        EventBus.getDefault().register(this);
    }
    public void onExit() {
        EventBus.getDefault().unregister(this);
    }
    public void insert() {}
    public void update() {}


    public void render(ModelBatch modelBatch, Environment environment) {

    }

    public String getName() {
        return this.getClass().getName();
    }
}
