package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.data.WallModelData;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.events.EventWallModelEntrySelected;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.concrete.Wall;
import com.bokyu.buildhouse.glyph.concrete.WallTexture;
import com.bokyu.buildhouse.screen.ScreenConstruct;

import org.greenrobot.eventbus.Subscribe;

import java.util.Set;

/**
 * Created by bokyu on 2016-08-17.
 */
public class WallInjecter extends ConstructInjecter {

    protected Wall _controllingWall = null;
    protected DesignGround _ground = null;
    protected Wall _deleteTarget = null;
    protected TextureData _curTextureData = null;
    protected WallModelData _curWallModelData = null;

    public WallInjecter(DesignGround ground) {
        super();
        _ground = ground;
    }

    public void setTextureType(TextureData textureData) {
        if ( textureData == null ) return;
        _curTextureData = textureData;
        _controllingWall.setTextureId(textureData.id);
    }

    public void setWallModelType(WallModelData wallModelData) {
        _curWallModelData = wallModelData;
        if ( _controllingWall == null ) return;
        _controllingWall.setModel(wallModelData.id, false);
        _controllingWall.setMode(MyGlyph.MODE.WIREFRAME);

        if ( wallModelData.height != null ) {
            _controllingWall.setHeight(Integer.parseInt(wallModelData.height));
        } else {
            _controllingWall.setHeight(1);
        }
        if ( wallModelData.span != null ) {
            _controllingWall.setSpanX(Integer.parseInt(wallModelData.span));
        } else {
            _controllingWall.setSpanX(1);
        }
    }


    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if (!ScreenConstruct.instance()._deleteMode) _controllingWall.render(modelBatch, environment);
    }

    @Override
    public void update() {
        placeWall(_controllingWall);

        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
        if ( ScreenConstruct.instance()._deleteMode )
        {
            BuildingGlyph glyph = _ground.getTopGlyph(ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY, ScreenConstruct.instance()._direction, Wall.class);
            if ( glyph instanceof Wall ) {
                _deleteTarget = (Wall)glyph;
                _deleteTarget.setMode(MyGlyph.MODE.COLOR);
                _deleteTarget.updatePosition();
            }
        }

    }

    protected void placeWall(Wall wall) {
        wall.setDirection(ScreenConstruct.instance()._direction);
        Set<Constants.DIRECTION> occupyingDirs = wall.getOccupyingDirs();
        int elevation = _ground.getElevation(ScreenConstruct.instance()._selIdxX,
                ScreenConstruct.instance()._selIdxY, occupyingDirs, wall);
        wall.setElevation(elevation);
        wall.setPositionIndexes(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY);
//        wall.setPosition(
//                ScreenConstruct.instance()._selIdxX * Constants.WALL_LENGTH - _ground.getTotalWidth() / 2f,
//                -ScreenConstruct.instance()._selIdxY * Constants.WALL_LENGTH + _ground.getTotalHeight() / 2f
//        );
        _controllingWall.updatePosition();
    }

    @Override
    public void onEnter() {
        super.onEnter();
        if ( _controllingWall == null ) {
            _controllingWall = new WallTexture(ScreenConstruct.instance()._selectedWallModelData.id, ScreenConstruct.instance()._selectedTextureData.id, 1);
            _controllingWall.setMode(MyGlyph.MODE.WIREFRAME);
        }
        setTextureType(ScreenConstruct.instance()._selectedTextureData);
        setWallModelType(ScreenConstruct.instance()._selectedWallModelData);
        update();
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
    }

    @Override
    public void insert() {
        if ( ScreenConstruct.instance()._deleteMode == false ) {
            Wall newWall = _controllingWall.clone();
            int elevation = _ground.getElevation(
                    ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY,
                    newWall.getOccupyingDirs(), newWall);
            newWall.setElevation(elevation);
            newWall.updatePosition();
            newWall.setDirection(ScreenConstruct.instance()._direction);
            _ground.addWall(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY, newWall);
        }
        else {
            if ( _deleteTarget != null ) {
                _ground.removeWall(_deleteTarget, 0, 0);
                _deleteTarget = null;
            }
        }
        update();
    }

    @Override
    public void onButtonClick(EventButtonClick event) {

        switch ( event._name ) {
            case WALL_BACK_LEFT:
//            case WALL_BACK_RIGHT:
//            case WALL_FRONT_LEFT:
//            case WALL_FRONT_RIGHT:
                _controllingWall.setDiagonal(false);
                break;
            case WALL_HORIZONTAL:
//            case WALL_VERTICAL:
                _controllingWall.setDiagonal(true);
                break;
        }
        update();
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event)
    {
        setTextureType(event._selectedTextureData);
        update();
    }

    @Subscribe
    public void onEvent(EventWallModelEntrySelected event)
    {
        setWallModelType(event._selectedWallModelData);
        update();
    }

}
