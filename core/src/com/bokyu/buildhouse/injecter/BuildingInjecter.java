package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventBuildingSelected;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventTerreinPick;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.screen.ScreenWorld;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-01-09.
 */

public class BuildingInjecter extends WorldInjecter {

    protected Terrein _terrein = null;
    protected Building _controllingBuilding = null;
    protected Building _deleteTarget = null;
    protected boolean _drawBuilding = false;

    public BuildingInjecter(Terrein terrein) {
        super();
        _terrein = terrein;
    }

    @Override
    public void onEnter() {
        super.onEnter();
        if ( ScreenWorld.instance()._selectedBuilding != null ) {
            _controllingBuilding = ScreenWorld.instance()._selectedBuilding.clone();
        }
        update();
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
    }

    @Override
    public void update() {
        _terrein.clearHighlightedSet();
        if (_terrein.checkInsertPossible(
                _terrein._selectedCrossIndexX,
                _terrein._selectedCrossIndexY,
                _controllingBuilding.getOccupyingPositions()))
        {
            placeBuilding(_controllingBuilding);
            _drawBuilding = true;
        } else {
            _terrein.addHighlightedSet(_controllingBuilding.getOccupyingPositions(), null);
            _drawBuilding = false;
        }

        // tmp
        _terrein.addHighlightedSet(_controllingBuilding.getOccupyingPositions(), null);

        _controllingBuilding.setColor(Color.YELLOW);
        _controllingBuilding.setMode(MyGlyph.MODE.COLOR);
    }

    protected void placeBuilding(Building building) {
        float elevation = _terrein.getTerreinElevBuilding(
                _terrein._selectedCrossIndexX, _terrein._selectedCrossIndexY, building.getOccupyingPositions()
        );
        building._elevation = elevation;
        building._position._x = _terrein._selectedCrossIndexX;
        building._position._y = _terrein._selectedCrossIndexY;
        _controllingBuilding.updatePosition();
    }

    @Override
    public void insert() {
        if ( _controllingBuilding != null ) {
            _controllingBuilding.setMode(MyGlyph.MODE.NORMAL);
            _terrein.insertBuilding(
                    _terrein._selectedCrossIndexX,
                    _terrein._selectedCrossIndexY,
                    _controllingBuilding
            );
            Configuration.instance().getFactory().getDbManager().saveBuilding(_controllingBuilding);
            _controllingBuilding = null;
        }
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if (!ScreenWorld.instance()._deleteMode && _controllingBuilding != null && _drawBuilding)
            _controllingBuilding.render(modelBatch, environment);
    }

    @Subscribe
    public void onEvent(EventBuildingSelected event)
    {
        _controllingBuilding = event._selectedBuildingData.clone();
        update();
    }

    @Subscribe
    public void onEvent(EventTerreinPick event) {
        if ( _controllingBuilding != null ) {
            _controllingBuilding._position._x = event._idxX;
            _controllingBuilding._position._y = event._idxY;
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case ROTATE:
                if ( _controllingBuilding != null ) {
                    _controllingBuilding.rotate();
                    update();
                }
                break;
        }
    }
}
