package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.data.DecorationData;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.events.EventDecorationEntrySelected;
import com.bokyu.buildhouse.events.EventObjectPicked;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Decoration;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.concrete.Wall;
import com.bokyu.buildhouse.screen.ScreenConstruct;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-01-18.
 */

public class DecorationInjecter extends ConstructInjecter {
    protected Wall _selectedWall = null;
    protected DesignGround _ground = null;
    protected DecorationData _curDecorationData = null;
    protected Decoration _decoration = new Decoration("", "");
    protected DesignGround.PICK_MODE _prevMode = null;
    protected TextureData _curTextureData = null;

    public DecorationInjecter(DesignGround ground) {
        super();
        _ground = ground;
    }

    public void setTextureType(TextureData textureData) {
        if ( textureData == null ) return;
        _curTextureData = textureData;
        _decoration.setTextureId(textureData.id);
    }

    @Override
    public void onEnter() {
        super.onEnter();
        setTextureType(ScreenConstruct.instance()._selectedTextureData);
        setDecorationType(ScreenConstruct.instance()._selectedDecorationData);
        _prevMode = _ground.setPickMode(DesignGround.PICK_MODE.WALL);
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _selectedWall != null ) {
            _selectedWall.setMode(MyGlyph.MODE.NORMAL);
        }
        _ground.setPickMode(_prevMode);
    }

    public void setDecorationType(DecorationData decorationData)
    {
        if ( decorationData == null ) return;
        _curDecorationData = decorationData;
        _decoration.setModel(decorationData.id);
    }

    @Override
    public void update() {
        if ( _decoration != null ) _decoration.updatePosition();
    }

    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if (!ScreenConstruct.instance()._deleteMode) _decoration.render(modelBatch, environment);
    }

    @Override
    public void insert() {

        if ( _selectedWall == null ) return;

        if ( !ScreenConstruct.instance()._deleteMode ) {
            Decoration newDeco = _decoration.clone();
            _selectedWall.addDecoration(newDeco);
            newDeco._wall = _selectedWall;
            newDeco.updatePosition();
        }
        else {
            _selectedWall.clearDecorations();
        }
        update();
    }

    @Subscribe
    public void onEvent(EventObjectPicked event ) {
        if ( !ScreenConstruct.instance().getIsVisible() ) return;
        if ( event._object != null ) {
            if ( event._object instanceof Wall ) {
                if ( _selectedWall != null ) {
                    _selectedWall.setMode(MyGlyph.MODE.NORMAL);
                }
                Wall wall = (Wall)event._object;
                _selectedWall = wall;
                _decoration._wall = wall;
                _selectedWall.setColor(Color.RED);
                _selectedWall.setMode(MyGlyph.MODE.COLOR);
                _decoration.updatePosition();
            }
        }
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event)
    {
        setTextureType(event._selectedTextureData);
        update();
    }

    @Subscribe
    public void onEvent(EventDecorationEntrySelected event)
    {
        setDecorationType(event._selectedDecorationData);
        update();
    }
}
