package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.Color;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventConfirm;
import com.bokyu.buildhouse.events.EventObjectPicked;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.ui.dialog.UIAlert;
import com.bokyu.buildhouse.ui.dialog.UIConfirm;
import com.bokyu.buildhouse.ui.scene2d.WorldScene2DManager;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-01-11.
 */

public class SelectInjecter extends WorldInjecter {
    private Terrein _terrein = null;
    private Terrein.PICK_MODE _prevPickMode = null;
    private Building _selBuilding = null;

    public SelectInjecter(Terrein terrein) {
        _terrein = terrein;
    }

    @Override
    public void onEnter() {
        super.onEnter();
        _prevPickMode = _terrein.setPickMode(Terrein.PICK_MODE.WALL);
    }

    @Override
    public void onExit() {
        super.onExit();
        _terrein.setPickMode(_prevPickMode);
        if ( _selBuilding != null )
        {
            _selBuilding.setMode(MyGlyph.MODE.NORMAL);
            _selBuilding = null;
        }
    }

    private boolean checkBuildingSelected() {
        if ( _selBuilding == null ) {
            UIAlert.instance().show("alert","building not selected. please select a building by touching on the screen.",
                    WorldScene2DManager.instance().getStage());
            return false;
        }
        return true;
    }

    @Subscribe
    public void onEvent(EventObjectPicked event) {
        if ( event._object != null ) {
            if ( event._object instanceof BuildingGlyph ) {
                BuildingGlyph bG = (BuildingGlyph)event._object;
                if ( bG._building != null ) {
                    if ( _selBuilding != null ) {
                        _selBuilding.setMode(MyGlyph.MODE.NORMAL);
                    }
                    _selBuilding = bG._building;
                    _selBuilding.setColor(Color.RED);
                    _selBuilding.setMode(MyGlyph.MODE.COLOR);
                }
            }
        }
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case DESTROY:
                if ( checkBuildingSelected() ) {
                    UIConfirm.instance().show("confirm", "do you want to delete the selected building?",
                            WorldScene2DManager.instance().getStage(), new EventConfirm(EventConfirm.CONFIRM_TYPE.DELETE_BUILDING));
                }
                break;
        }
    }

    @Subscribe
    public void onEvent(EventConfirm event) {
        switch ( event._confirmType ) {
            case DELETE_BUILDING:
                if ( checkBuildingSelected() ) {
                    _terrein.removeBuilding(_selBuilding);
                    Configuration.instance().getFactory().getDbManager().deleteBuilding(_selBuilding.getId());
                    _selBuilding = null;
                }
                break;
        }
    }

}
