package com.bokyu.buildhouse.injecter;

import com.bokyu.buildhouse.data.GlyphDbData;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.events.EventButtonClick;
import com.bokyu.buildhouse.events.EventRoadSelected;
import com.bokyu.buildhouse.events.EventTerreinPick;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.screen.ScreenWorld;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by bk on 2017-02-01.
 */

public class RoadInjecter extends WorldInjecter {

    protected Terrein _terrein = null;
    protected RoadData _roadData = null;
    protected int _idxX, _idxY;

    public RoadInjecter(Terrein terrein) {
        super();
        _terrein = terrein;
    }

    @Override
    public void onEnter() {
        super.onEnter();
        _roadData = ScreenWorld.instance()._selectedRoadData;
        update();
    }

    @Override
    public void onExit() {
        super.onExit();
    }

    @Subscribe
    public void onEvent(EventTerreinPick event) {
        _idxX = event._idxX;
        _idxY = event._idxY;
        _terrein.select(event._idxX, event._idxY);
    }

    @Subscribe
    public void onEvent(EventButtonClick event) {
        switch ( event._name ) {
            case DESTROY:
                _terrein.removeRoad(_idxX, _idxY);
                update();
                break;
        }
    }

    @Subscribe
    public void onEvent(EventRoadSelected event) {
        _roadData = event._selectedData;
    }

    @Override
    public void insert() {
        _terrein.insertRoad(_idxX, _idxY, _roadData);
        GlyphDbData data = new GlyphDbData();
        data._resourceId = _roadData.id;
        data._x = _idxX;
        data._y = _idxY;
        data._type = MyGlyph.TYPE.ROAD;
        Configuration.instance().getFactory().getDbManager().saveGlyph(data);
        update();
    }
}
