package com.bokyu.buildhouse.injecter;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.bokyu.buildhouse.data.RoofModelData;
import com.bokyu.buildhouse.data.TextureData;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.events.EventRoofModelEntrySelected;
import com.bokyu.buildhouse.events.EventTextureEntrySelected;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.DesignGround;
import com.bokyu.buildhouse.glyph.concrete.Roof;
import com.bokyu.buildhouse.screen.ScreenConstruct;

import org.greenrobot.eventbus.Subscribe;

import java.util.Set;

/**
 * Created by bk on 2017-01-05.
 */

public class RoofInjecter extends ConstructInjecter {
    protected Roof _controllingRoof = null;
    protected DesignGround _ground = null;
    protected Roof _deleteTarget = null;
    protected TextureData _curTextureData = null;
    protected RoofModelData _curRoofModelData = null;
//    private Constants.DIRECTION _direction = NORTH;

    public RoofInjecter(DesignGround ground) {
        super();
        _ground = ground;

    }

    public void setTextureType(TextureData textureData) {
        if ( textureData == null ) return;
        _curTextureData = textureData;
        _controllingRoof.setTextureId(textureData.id);
    }

    public void setRoofModelType(RoofModelData modelData) {
        _curRoofModelData = modelData;
        if ( _controllingRoof == null ) return;
        _controllingRoof.setModel(modelData.id);
        _controllingRoof.setMode(MyGlyph.MODE.WIREFRAME);
    }
    @Override
    public void render(ModelBatch modelBatch, Environment environment) {
        if (!ScreenConstruct.instance()._deleteMode) _controllingRoof.render(modelBatch, environment);
    }

    @Override
    public void update() {
        if ( _curTextureData != null ) setTextureType(_curTextureData);
        if ( _curRoofModelData != null ) setRoofModelType(_curRoofModelData);

        placeRoof(_controllingRoof);

        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
        if ( ScreenConstruct.instance()._deleteMode )
        {
            BuildingGlyph glyph = _ground.getTopGlyph(ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY, Constants.DIRECTION.ALL, Roof.class);
            if ( glyph instanceof Roof ) {
                _deleteTarget = (Roof)glyph;
                _deleteTarget.setMode(MyGlyph.MODE.COLOR);
                _deleteTarget.updatePosition();
            }
        }

    }

    protected void placeRoof(Roof roof) {
        roof.setDirection(ScreenConstruct.instance()._direction);
        Set<Constants.DIRECTION> occupyingDirs = roof.getOccupyingDirs();
        int elevation = _ground.getElevation(
                ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY,
                occupyingDirs, roof);

        roof.setElevation(elevation);
        roof.setPositionIndexes(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY);
//        roof.setPosition(
//                ScreenConstruct.instance()._selIdxX * Constants.WALL_LENGTH - _ground.getTotalWidth() / 2f,
//                -ScreenConstruct.instance()._selIdxY * Constants.WALL_LENGTH + _ground.getTotalHeight() / 2f
//        );
        roof.updatePosition();
    }

    @Override
    public void onEnter() {
        super.onEnter();
        if ( _controllingRoof == null ) {
            _controllingRoof = new Roof(ScreenConstruct.instance()._selectedRoofModelData.id, "", 2);
            _controllingRoof.setMode(MyGlyph.MODE.WIREFRAME);
        }
        setRoofModelType(ScreenConstruct.instance()._selectedRoofModelData);
        setTextureType(ScreenConstruct.instance()._selectedTextureData);
    }

    @Override
    public void onExit() {
        super.onExit();
        if ( _deleteTarget != null ) {
            _deleteTarget.setMode(MyGlyph.MODE.NORMAL);
        }
    }

    @Override
    public void insert() {
        if ( ScreenConstruct.instance()._deleteMode == false ) {
            Roof newRoof = _controllingRoof.clone();
            int elevation = _ground.getElevation(
                    ScreenConstruct.instance()._selIdxX,
                    ScreenConstruct.instance()._selIdxY,
                    newRoof.getOccupyingDirs(), newRoof);
            newRoof.setElevation(elevation);
            newRoof.updatePosition();
            _ground.addRoof(ScreenConstruct.instance()._selIdxX, ScreenConstruct.instance()._selIdxY, newRoof);
        }
        else {
            if ( _deleteTarget != null ) {
                _ground.removeRoof(_deleteTarget, 0, 0);
                _deleteTarget = null;
            }
        }
        update();
    }

    @Subscribe
    public void onEvent(EventTextureEntrySelected event)
    {
        setTextureType(event._selectedTextureData);
        update();
    }

    @Subscribe
    public void onEvent(EventRoofModelEntrySelected event)
    {
        setRoofModelType(event._selectedRoofModelData);
        update();
    }

}
