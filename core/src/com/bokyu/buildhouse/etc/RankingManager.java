package com.bokyu.buildhouse.etc;

import com.bokyu.buildhouse.Village;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bk on 2017-02-27.
 */

public class RankingManager {

    static private RankingManager _instance = null;
    public Map<Integer, Village> _dataMap = new HashMap();
    public List<Integer> _keyList = new ArrayList();

    static public RankingManager instance() {
        if ( _instance == null ) {
            _instance = new RankingManager();
        }
        return _instance;
    }

}
