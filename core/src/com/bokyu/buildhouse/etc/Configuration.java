package com.bokyu.buildhouse.etc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;
import com.bokyu.buildhouse.Mediater;
import com.bokyu.buildhouse.events.EventPurchaseConfirmed;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


/**
 * Created by bk on 2016-11-22.
 */

public class Configuration {

    static private Configuration _instance = null;

    static public String NO_AD_ITEM_NAME = "no_ad";

    private XmlReader _xmlReader = null;
    private XmlReader.Element _root = null;
    private Mediater _factory = null;
    private Stage _curStage = null;
    public boolean _advertiseEnabled = true;


    static public Configuration instance() {
        if ( _instance == null ) {
            _instance = new Configuration();
        }
        return _instance;
    }

    private Configuration() {
        FileHandle handle = Gdx.files.internal("resources/configuration.xml");
        _xmlReader = new XmlReader();
        _root = _xmlReader.parse( handle.readString() );
        EventBus.getDefault().register(this);
//        XmlReader.Element networkServerElem = root.getChildByName("resources").getChildByName("network-server");
//        XmlReader.Element ipElem = networkServerElem.getChildByName("ip");
//        XmlReader.Element portElem = networkServerElem.getChildByName("port");
//        String ipTxt = ipElem.getText();
//        String portTxt = portElem.getText();
    }

    public void setFactory(Mediater factory) {
        _factory = factory;
    }

    public Mediater getFactory() { return _factory; }

    public String getText(String path) {
        try {
            String[] pathArr = path.split("\\.");
            XmlReader.Element elem = _root;
            for ( int i=0; i < pathArr.length; ++i ) {
                elem = _root.getChildByName(pathArr[i]);
            }
            return elem.getText();
        } catch ( GdxRuntimeException e )
        {
            e.printStackTrace();
            System.out.print(e.getMessage());
            return "";
        }
    }

    public Stage getCurStage() { return _curStage; }
    public void setCurStage(Stage stage) { _curStage = stage; }

    @Subscribe
    public void onEvent(EventPurchaseConfirmed event) {
        Gdx.app.debug("pay_service", "purchase confirmed " + event._sku);
        if ( event._sku.equals(NO_AD_ITEM_NAME) )
        {
            Gdx.app.debug("pay_service", "making remove add btn invisible");
            _advertiseEnabled = false;
        }
    }
}
