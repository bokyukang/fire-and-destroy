package com.bokyu.buildhouse.etc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.ModelLoader;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.UBJsonReader;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;

import java.util.HashSet;
import java.util.Set;

import static com.bokyu.buildhouse.glyph.MyGlyph.FIXED_MATERIAL;

/**
 * Created by bokyu on 2016-10-11.
 */
public class MyGraphicsHelper {

    static private MyGraphicsHelper _instance = null;
    private Environment _environment = null;
    private ModelBatch _modelBatch = null;
    Set<FrameBuffer> _frameBuffers = new HashSet();


    static public MyGraphicsHelper instance() {
        if ( _instance == null ) {
            _instance = new MyGraphicsHelper();
            _instance.initEnvironment();
        }
        return _instance;
    }

    /* should be called once at the beginning */
    private void initEnvironment() {
        _environment = new Environment();
        _environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        _environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
        _environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, 1f, 0.3f, -0.2f));
        _environment.set( new IntAttribute(IntAttribute.CullFace, 1));


        _modelBatch = new ModelBatch();
    }

    protected void setCamLookFrom(PerspectiveCamera cam, Vector3 from, Vector3 to) {
        cam.up.set(0,1,0);
        cam.position.set(from.x,from.y,from.z);
        cam.lookAt(to.x, to.y, to.z);
        cam.near = .01f;
        cam.far = 100f;
        cam.update();
    }

    public TextureRegion createTextureRegion(String modelPath, int imgWidth, String textureName) {

        ModelLoader modelLoader = new G3dModelLoader(new UBJsonReader());
        Model model = modelLoader.loadModel(Gdx.files.internal(modelPath));
        ModelInstance modelInstance = new ModelInstance(model);
        if ( !textureName.equals("") ) {
            Texture texture = new Texture("textures/" + textureName);

            for ( Material material : modelInstance.materials ) {
                if (!material.id.substring(0, FIXED_MATERIAL.length()).equals(FIXED_MATERIAL))
                {
                    material.set(new TextureAttribute(TextureAttribute.Diffuse, texture));
                }
            }
        }

        PerspectiveCamera cam = new PerspectiveCamera(67, imgWidth, imgWidth);
        setCamLookFrom(cam, new Vector3(0,1.2f,1.2f), new Vector3(0,.5f, 0));

        FrameBuffer frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, imgWidth, imgWidth, true);
        _frameBuffers.add(frameBuffer);
        frameBuffer.begin();

        Gdx.gl20.glClearColor(0,0,0,0);
        Gdx.gl20.glClear( GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);

        _modelBatch.begin(cam);
        _modelBatch.render(modelInstance, _environment);
        _modelBatch.end();

        frameBuffer.end();

        Texture texture = frameBuffer.getColorBufferTexture();
        TextureRegion textureRegion = new TextureRegion(texture);
        textureRegion.flip(true,true);


        return textureRegion;
    }

    public TextureRegion createTextureRegion(Building building, int imgWidth) {
//        if ( _frameBuffer != null ) {
//            _frameBuffer.dispose();
//        }


        FrameBuffer frameBuffer = new FrameBuffer(Pixmap.Format.RGB888, imgWidth, imgWidth, true);

        _frameBuffers.add(frameBuffer);
        PerspectiveCamera cam = new PerspectiveCamera(67, imgWidth, imgWidth);
        setCamLookFrom(cam, new Vector3(3,3,-3), new Vector3(0,0,0));

        frameBuffer.begin();
        Gdx.gl20.glClear( GL20.GL_DEPTH_BUFFER_BIT | GL20.GL_COLOR_BUFFER_BIT);

        _modelBatch.begin(cam);

        for (MyGlyph buildingGlyph : building.getGlyphs()) {
            ((BuildingGlyph)buildingGlyph).updatePosition();
            buildingGlyph.render(_modelBatch, _environment);
        }

        _modelBatch.end();

        frameBuffer.end();

        Texture texture = frameBuffer.getColorBufferTexture();
        TextureRegion textureRegion = new TextureRegion(texture);
        textureRegion.flip(false,true);


        return textureRegion;
    }

    public Model createCube(float wallLen, float wallHeight, float wallWidth, int attr, Material material) {
        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();
        Vector3 fbl = new Vector3(-wallLen/2f, 0, wallWidth/2f);
        Vector3 fbr = new Vector3(wallLen/2f, 0, wallWidth/2f);
        Vector3 ftr = new Vector3(wallLen/2f, wallHeight, wallWidth/2f);
        Vector3 ftl = new Vector3(-wallLen/2f, wallHeight, wallWidth/2f);
        Vector3 bbl = new Vector3(-wallLen/2f, 0, -wallWidth/2f);
        Vector3 bbr = new Vector3(wallLen/2f, 0, -wallWidth/2f);
        Vector3 btr = new Vector3(wallLen/2f, wallHeight, -wallWidth/2f);
        Vector3 btl = new Vector3(-wallLen/2f, wallHeight, -wallWidth/2f);


        MeshPartBuilder meshPartBuilder;

        meshPartBuilder = modelBuilder.part("front", GL20.GL_TRIANGLES, attr, material);
        float texRate = 1;
        meshPartBuilder.setUVRange(0, 0, wallLen, wallHeight * texRate);
        meshPartBuilder.rect(fbl, fbr, ftr, ftl, new Vector3(0, 0, 1));

        meshPartBuilder = modelBuilder.part("right", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallWidth, wallHeight * texRate);
        meshPartBuilder.rect(fbr, bbr, btr, ftr, new Vector3(1, 0, 0));

        meshPartBuilder = modelBuilder.part("left", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallWidth, wallHeight * texRate);
        meshPartBuilder.rect(bbl,fbl,ftl,  btl,  new Vector3(-1, 0, 0));

        meshPartBuilder = modelBuilder.part("back", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallHeight * texRate);
        meshPartBuilder.rect(bbr, bbl, btl, btr, new Vector3(0, 0, -1));

        meshPartBuilder = modelBuilder.part("top", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallWidth * texRate);
        meshPartBuilder.rect(ftl, ftr, btr, btl, new Vector3(0, 0, -1));

        meshPartBuilder = modelBuilder.part("bottom", GL20.GL_TRIANGLES, attr, material);
        meshPartBuilder.setUVRange(0, 0, wallLen, wallWidth * texRate);
        meshPartBuilder.rect(fbr,fbl, bbl, bbr, new Vector3(0, -1, 0));


        return modelBuilder.end();
    }
}
