package com.bokyu.buildhouse.etc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.ReflectionPool;
import com.bokyu.buildhouse.data.BallDataContainer;
import com.bokyu.buildhouse.data.BallDataContainerJson;
import com.bokyu.buildhouse.data.CeilingDataContainer;
import com.bokyu.buildhouse.data.CeilingDataContainerJson;
import com.bokyu.buildhouse.data.DecorationDataContainer;
import com.bokyu.buildhouse.data.DecorationDataContainerJson;
import com.bokyu.buildhouse.data.LevelDataContainer;
import com.bokyu.buildhouse.data.LevelDataContainerJson;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.data.RoadDataContainer;
import com.bokyu.buildhouse.data.RoadDataContainerJson;
import com.bokyu.buildhouse.data.RoofModelDataContainer;
import com.bokyu.buildhouse.data.RoofModelDataContainerJson;
import com.bokyu.buildhouse.data.TextureDataContainer;
import com.bokyu.buildhouse.data.TextureDataContainerJson;
import com.bokyu.buildhouse.data.WallModelDataContainer;
import com.bokyu.buildhouse.data.WallModelDataContainerJson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bokyu on 2016-08-18.
 */
public class ResourceManager {
    static private ResourceManager _instance = null;

    private Map<Class, Pool> _container = new HashMap<Class, Pool>();
    private Map<String, Texture> _roadTextures = new HashMap();
    private Map<String, Texture> _allTextures = new HashMap();

    static public ResourceManager instance() {
        if ( _instance == null ) {
            _instance = new ResourceManager();
        }
        return _instance;
    }

    private ResourceManager() {
    }

    public void loadResources() {


        /* walls */
        FileHandle textureResourceFile = Gdx.files.internal("resources/textures.json");
        Json json = new Json();
        json.setIgnoreUnknownFields(true);
        TextureDataContainerJson textureDataContainer = json.fromJson(TextureDataContainerJson.class, textureResourceFile);
        TextureDataContainer.instance().setJsonData(textureDataContainer);

        /* balls */
        FileHandle ballsResourceFile = Gdx.files.internal("resources/balls.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        BallDataContainerJson ballsDataContainer = json.fromJson(BallDataContainerJson.class, ballsResourceFile);
        BallDataContainer.instance().setJsonData(ballsDataContainer);

        /* levels */
        FileHandle levelsResourceFile = Gdx.files.internal("resources/levels.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        LevelDataContainerJson levelsDataContainer = json.fromJson(LevelDataContainerJson.class, levelsResourceFile);
        LevelDataContainer.instance().setJsonData(levelsDataContainer);

        /* ceilings */
        FileHandle ceilingsResourceFile = Gdx.files.internal("resources/ceilings.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        CeilingDataContainerJson ceilingsDataContainer = json.fromJson(CeilingDataContainerJson.class, ceilingsResourceFile);
        CeilingDataContainer.instance().setJsonData(ceilingsDataContainer);

        /* wall models */
        FileHandle wallModelsResourceFile = Gdx.files.internal("resources/wallModels.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        WallModelDataContainerJson wallModelsDataContainer = json.fromJson(WallModelDataContainerJson.class, wallModelsResourceFile);
        WallModelDataContainer.instance().setJsonData(wallModelsDataContainer);

        /* decoration models */
        FileHandle decorationResourceFile = Gdx.files.internal("resources/decorations.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        DecorationDataContainerJson decorationDataContainer = json.fromJson(DecorationDataContainerJson.class, decorationResourceFile);
        DecorationDataContainer.instance().setJsonData(decorationDataContainer);

        /* roofs */
        FileHandle roofModelsResourceFile = Gdx.files.internal("resources/roofModels.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        RoofModelDataContainerJson roofModelsDataContainer = json.fromJson(RoofModelDataContainerJson.class, roofModelsResourceFile);
        RoofModelDataContainer.instance().setJsonData(roofModelsDataContainer);

        /* roads */
        FileHandle roadResourceFile = Gdx.files.internal("resources/roads.json");
        json = new Json();
        json.setIgnoreUnknownFields(true);
        RoadDataContainerJson roadDataContainer = json.fromJson(RoadDataContainerJson.class, roadResourceFile);
        RoadDataContainer.instance().setJsonData(roadDataContainer);
    }

    public Texture getTexture(String path) {
        if (!_allTextures.containsKey(path)) {
            Texture texture = new Texture(path);
            texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
            _allTextures.put(path, texture);
        }
        return _allTextures.get(path);

    }

    public <T> void put(Class<T> type, T data) {
        if ( _container.containsKey(type ) == false )
        {
            _container.put( type, new ReflectionPool(type));
        }
        _container.get(type).free(data);
    }

    public Texture getRoadTexture(String id, RoadData.TYPE roadType) {
        String path = "roads/" + id + "/";
        switch ( roadType ) {
            case ONE:
                path += "one.gif";
                break;
            case NONE:
                path += "none.gif";
                break;
            case FOUR:
                path += "four.gif";
                break;
            case THREE:
                path += "three.gif";
                break;
            case TWO_BENT:
                path += "two_bent.gif";
                break;
            case TWO_STR:
                path += "two_str.gif";
                break;
        }

        if ( !_roadTextures.containsKey(path) ) {
            _roadTextures.put(path,
                    new Texture(path));
        }

        return _roadTextures.get(path);
    }


}
