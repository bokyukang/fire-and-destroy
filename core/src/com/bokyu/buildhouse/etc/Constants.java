package com.bokyu.buildhouse.etc;

/**
 * Created by bokyu on 2016-08-16.
 */
public class Constants {
    static public final float WALL_WIDTH = 0.15f;
    static public final float WALL_LENGTH = 1f;
    static public final float WALL_HEIGHT = 1f;
    static public final boolean DEBUG_MODE = true;

    static public enum DIRECTION {
        EAST,
        SOUTH,
        WEST,
        NORTH,
        SE,
        NE,
        SW,
        NW,
        CENTER,
        ALL
    }

    static public enum ORIENTATION {
        VERTICAL,
        HORIZONTAL
    }
}
