package com.bokyu.buildhouse.etc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.bokyu.buildhouse.data.data_types.MyPosition;

import static com.bokyu.buildhouse.etc.Constants.DIRECTION.EAST;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.NE;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.NORTH;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.NW;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.SE;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.SOUTH;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.SW;
import static com.bokyu.buildhouse.etc.Constants.DIRECTION.WEST;

/**
 * Created by bokyu on 2016-08-12.
 */
public class MyHelper {
    static public int screen2CoordX(int x) {
        return x;
    }
    static public int screen2CoordY(int y) {
        return Gdx.graphics.getHeight() - y;
    }

    static public String getStackTraceString(StackTraceElement[] elems) {
        String result = "";
        for ( StackTraceElement e : elems ) {
            result += e.toString() + "\n";
        }
        return result;
    }
    static public Constants.DIRECTION getDirection(Vector3 vec) {
       if (  Math.abs(vec.z) >= Math.abs(vec.x) ) {
           if ( vec.z >= 0 ) return SOUTH;
           else return NORTH;
       } else {
           if ( vec.x >= 0 ) return EAST;
           else return WEST;
       }
    }
    static public Constants.DIRECTION getDirectionDiagonal(Vector3 vec) {
        if (  vec.z >= 0 ) {
            if ( vec.x >= 0 ) return SE;
            else return SW;
        } else {
            if ( vec.x >= 0 ) return NE;
            else return NW;
        }
    }
    static public Constants.DIRECTION getRotationDir(Constants.DIRECTION dir, int count) {
        for ( int i=0; i < count; ++i ) {
            dir = getRotationDir(dir);
        }
        return dir;
    }
    static public Constants.DIRECTION getRotationDir(Constants.DIRECTION dir) {
        switch ( dir ) {
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            case NORTH:
                return EAST;
            case NE:
                return SE;
            case SE:
                return SW;
            case SW:
                return NW;
            case NW:
                return NE;
            default:
                return dir;
        }
    }

    /* rot clock wise from bottom left corner */
    static public Vector2 getUvPair(int rot) {
        rot = rot % 4;

        switch ( rot ) {
            case 0:
                return new Vector2(0f,0f);
            case 1:
                return new Vector2(0f, 1f);
            case 2:
                return new Vector2(1f, 1f);
            case 3:
                return new Vector2(1f, 0f);
        }

        return null;
    }

    /* input : (0,1), (0, -1), (1,0), (-1,0) */
    static public Constants.DIRECTION getDirFromOffset(int x, int y ) {
        if ( x == 0 ) {
            if ( y == 1 ) {
                return NORTH;
            }
            return SOUTH;
        } else {
            if ( x == 1 ) {
                return EAST;
            }
            return WEST;
        }
    }

    static public MyPosition getCeilingSpanDir(Constants.DIRECTION dir, int spanIdx) {
        int offsetX = 0;
        int offsetY = 0;
        switch (dir) {
            case EAST:
                offsetY = -1;
                break;
            case SOUTH:
                offsetX = -1;
                break;
            case WEST:
                offsetY = 1;
                break;
            case NORTH:
                offsetX = 1;
                break;
                default:
        }
        return new MyPosition(offsetX  * spanIdx, offsetY  * spanIdx);
    }

    /* when wall has larger than 1 span, this returns offset to the main position on each span index */
    static public MyPosition getWallSpanDir(Constants.DIRECTION dir, int spanIdx) {
        int offsetX = 0;
        int offsetY = 0;
        switch ( dir ) {
            case EAST:
                offsetY = -1;
                break;
            case SOUTH:
                offsetX = -1;
                break;
            case WEST:
                offsetY = 1;
                break;
            case NORTH:
                offsetX = 1;
                break;
            case NE:
                offsetY = -1;
                offsetX = 1;
                break;
            case SE:
                offsetY = 1;
                offsetX = -1;
                break;
            case SW:
                offsetY = -1;
                offsetX = -1;
                break;
            case NW:
                offsetY = 1;
                offsetX = 1;
                break;
            default:
        }

        int mult = 1;
        if ( spanIdx % 2 == 0 ) {
            mult = -1;
        } else {
            mult = 1;
        }

        if ( spanIdx == 0 ) mult = 0;
        else {
            mult *= (((int) ((spanIdx - 1) / 2)) + 1);
        }

        return new MyPosition(offsetX * mult, offsetY * mult);
    }
}
