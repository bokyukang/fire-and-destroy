package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.data.RoadData;
import com.bokyu.buildhouse.data.RoadDataContainer;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.network.Network;
import com.bokyu.buildhouse.screen.ScreenWorld;
import com.bokyu.buildhouse.ui.dialog.UIDialogMessage;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by bk on 2016-12-19.
 * <p>
 * option1 = village id
 */

public class NetworkJobLoadVillage extends NetworkJob {
    private String _accountId = "";
    private int _villageId = 0;
    private Village _village = null;
    private ScreenWorld.MODE _afterMode;

    public NetworkJobLoadVillage(Village village, String accountId, int villageId, ScreenWorld.MODE afterMode) {
        super(Network.header.HeaderEnum.LOAD);
        _afterMode = afterMode;
        _accountId = accountId;
        _villageId = villageId;
        _village = village;
        _options.add(villageId);

        build();
    }

    @Override
    public boolean build() {

        return true;
    }

    public String getAccountId() {
        return _accountId;
    }

    @Override
    public void onResponse(final Network.header header, final InputStream is) {

        final Terrein terrein = ScreenWorld.instance().getTerrein();
        terrein.clearAll();

        ScreenWorld.instance().addRenderJob(new Runnable() {
            @Override
            public void run() {

                if (header != null &&
                        header.getHeader() == Network.header.HeaderEnum.ACK) {
                    try {
                        Network.village pbVillageResult = Network.village.parseDelimitedFrom(is);
                        BuildingsManager.instance().getBuildings().clear();

                        terrein.decodeTerrein(pbVillageResult.getTerrein());

                /* load models */
                        for (Network.building_model pbBuilding : pbVillageResult.getBuildingModelsList()) {
                            Building building = new Building();
                            building.parse(pbBuilding);
                            BuildingsManager.instance().addBuilding(building);
                        }

                /* load buildings */
                        for (Network.building pbBuilding : pbVillageResult.getBuildingsList()) {
                            Building building = BuildingsManager.instance().getBuildings().get(pbBuilding.getModelId()).clone();
                            building.rotate(pbBuilding.getRotation());
                            terrein.insertBuilding(
                                    pbBuilding.getX(),
                                    pbBuilding.getY(),
                                    building
                            );
                        }

                /* load roads */
                        for (Network.glyph pbGlyph : pbVillageResult.getGlyphsList()) {
                            MyGlyph.TYPE type = MyGlyph.TYPE.values()[pbGlyph.getType()];
                            switch (type) {
                                case ROAD:
                                    RoadData roadData = RoadDataContainer.instance()._roadDataMap
                                            .get(pbGlyph.getResourceId());
                                    terrein.insertRoad(
                                            pbGlyph.getX(), pbGlyph.getY(),
                                            roadData
                                    );
                                    break;
                                default:
                                    continue;
                            }
                        }


                        UIDialogMessage.instance().setMsg("Village loaded");
                        ScreenWorld.instance().setMode(_afterMode);
                        ScreenWorld.instance()._curLoadedVillage = _village;

                    } catch (IOException e) {
                        e.printStackTrace();
                        UIDialogMessage.instance().setMsg("Village load faield!");
                    }
                }
            }
        });
    }
}
