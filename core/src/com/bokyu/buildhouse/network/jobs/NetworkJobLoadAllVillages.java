package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.network.Network;
import com.bokyu.buildhouse.ui.scene2d.MainScene2DManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by bk on 2017-11-27.
 */

public class NetworkJobLoadAllVillages extends NetworkJob {
    String _accountId = "";

    public NetworkJobLoadAllVillages(String accountId) {
        super(Network.header.HeaderEnum.LOAD_VILLAGES);

        _accountId = accountId;
        build();
    }

    @Override
    public void onResponse(Network.header header, InputStream is) {

        if ( header != null &&
                header.getHeader() == Network.header.HeaderEnum.ACK) {
            try {
                Network.all_villages pbAllVillagesResult = Network.all_villages.parseDelimitedFrom(is);
                MainScene2DManager.instance().updateVillageEntriesNetwork( _accountId, pbAllVillagesResult);

            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }

}
