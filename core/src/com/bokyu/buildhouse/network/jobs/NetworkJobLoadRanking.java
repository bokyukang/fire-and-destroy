package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.etc.RankingManager;
import com.bokyu.buildhouse.events.EventRankingLoaded;
import com.bokyu.buildhouse.network.Network;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by bk on 2017-02-27.
 */

//message rank {
//        message rank_village {
//        string account_id = 1;
//        int32 village_id = 2;
//        string village_name = 3;
//        }
//        repeated rank_village rank_data = 1;
//        }
public class NetworkJobLoadRanking extends NetworkJob {

    public NetworkJobLoadRanking(int page) {
        this();
        _options.add(page);
    }
    public NetworkJobLoadRanking() {
        super(Network.header.HeaderEnum.RANK);

        build();
    }

    @Override
    public void onResponse(Network.header header, InputStream is) {
        try {
            Network.rank pbRank = Network.rank.parseDelimitedFrom(is);
            int index = 0;
            RankingManager.instance()._dataMap.clear();
            RankingManager.instance()._keyList.clear();
            for ( Network.rank.rank_village pbRankVillage : pbRank.getRankDataList() )
            {
                Village village = new Village(pbRankVillage.getVillageId(), pbRankVillage.getVillageName());
                village._accountId = pbRankVillage.getAccountId();
                village._accountName = pbRankVillage.getAccountName();
                village._recomCount = pbRankVillage.getRecomCount();

                RankingManager.instance()._dataMap.put(index, village);
                RankingManager.instance()._keyList.add(index);
                index++;
            }
            EventBus.getDefault().post(new EventRankingLoaded());
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
