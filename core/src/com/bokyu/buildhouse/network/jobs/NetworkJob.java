package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.network.Network;
import com.google.protobuf.GeneratedMessageV3;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by bk on 2016-11-22.
 */

public class NetworkJob {
    public Network.header.HeaderEnum _header = null;
    public GeneratedMessageV3 _message = null;
    ArrayList<Integer> _options = new ArrayList();

    public NetworkJob(Network.header.HeaderEnum header) {
        _header = header;
    }
    public void setMessage(GeneratedMessageV3 msg)
    {
        _message = msg;
    }
    public boolean build() { return false; }
    public String getAccountId() { return Account.instance().getId(); }
    public String getAccountName() { return Account.instance()._accountName; }
    public void onResponse(Network.header header, InputStream is) { }
    public ArrayList<Integer> getOptions() { return _options; }
}
