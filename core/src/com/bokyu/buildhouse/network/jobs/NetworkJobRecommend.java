package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.network.Network;

import java.io.InputStream;

/**
 * Created by bk on 2017-03-24.
 */

public class NetworkJobRecommend extends NetworkJob {

    private String _accountId;
    private int _villageId;
    private boolean _isUp;

    public NetworkJobRecommend(String accId, int vilId, boolean isUp) {
        super(Network.header.HeaderEnum.RECOMMEND);
        _accountId = accId;
        _villageId = vilId;
        _isUp = isUp;

        build();
    }

    @Override
    public boolean build() {
        Network.recommend.Builder builder = Network.recommend.newBuilder();
        builder.setVillageId(_villageId);
        builder.setAccountId(_accountId);
        builder.setIsUp(_isUp);
        _message = builder.build();

        return true;
    }

    @Override
    public void onResponse(Network.header header, InputStream is) {
        if ( header != null && header.getHeader() == Network.header.HeaderEnum.ACK ) {
            if ( header.getOptions(0) == 0 ) {
                RenderManager.instance().showAlertMsg("already recommended");
            } else {
                RenderManager.instance().showAlertMsg("voted successfully!");
            }
        }
        else
        {
            RenderManager.instance().showAlertMsg("vote failed!");
        }
    }
}
