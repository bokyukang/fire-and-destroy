package com.bokyu.buildhouse.network.jobs;

import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.Village;
import com.bokyu.buildhouse.glyph.Building;
import com.bokyu.buildhouse.glyph.BuildingGlyph;
import com.bokyu.buildhouse.glyph.MyGlyph;
import com.bokyu.buildhouse.glyph.concrete.Terrein;
import com.bokyu.buildhouse.glyph.manager.BuildingsManager;
import com.bokyu.buildhouse.network.Network;
import com.bokyu.buildhouse.screen.ScreenWorld;

import java.io.InputStream;

/**
 * Created by bk on 2016-12-19.
 */

public class NetworkJobSaveVillage extends NetworkJob {

    public NetworkJobSaveVillage() {
        super(Network.header.HeaderEnum.SAVE);

        build();
    }

    @Override
    public boolean build() {
        Network.village.Builder villageBuilder = Network.village.newBuilder();

        Village village = Account.instance().getVillage();
        Terrein terrein = ScreenWorld.instance().getTerrein();
        if ( village == null ) return false;
        villageBuilder
                .setVillageId(village._id)
                .setVillageName(village._name)
                .setTerrein(terrein.encodeTerrein());

        Network.glyph.Builder glyphBuilder = Network.glyph.newBuilder();
        for ( int key : terrein.getRoadKeys() ) {
            int y = key % Terrein.ITEMS_PER_EDGE;
            int x = (key - y) / Terrein.ITEMS_PER_EDGE;

            glyphBuilder
                    .setType(MyGlyph.TYPE.ROAD.ordinal())
                    .setX(x)
                    .setY(y)
                    .setResourceId( terrein.getRoadDataMap().get(key).id)
                    .setDirection(0);

            villageBuilder.addGlyphs(glyphBuilder.build());

            glyphBuilder.clear();
        }

        Network.building.Builder buildingBuilder = Network.building.newBuilder();
        Network.building_model.Builder buildingModelBuilder = Network.building_model.newBuilder();
        Network.building_model.building_glyph.Builder buildingGlyphBuilder = Network.building_model.building_glyph.newBuilder();
        for ( Building building : terrein.getBuildings() ) {
            buildingBuilder.clear();
            buildingBuilder
                    .setBuildingId(building.getId())
                    .setX(building.getPosition()._x)
                    .setY(building.getPosition()._y)
                    .setRotation(building.getRotation())
                    .setModelId(building._modelId);



            villageBuilder.addBuildings(buildingBuilder.build());

        }

        for ( Building building : BuildingsManager.instance().getBuildings().values()) {
            buildingModelBuilder.clear();
            buildingModelBuilder.setId( building._modelId )
                    .setName(building.getName());
            for ( MyGlyph glyph : building.getGlyphs() ) {
                buildingGlyphBuilder.clear();
                BuildingGlyph buildingGlyph = (BuildingGlyph) glyph;
                buildingGlyphBuilder
                        .setType(buildingGlyph.getType().ordinal())
                        .setResourceId(buildingGlyph.getResourceId())
                        .setDirection(buildingGlyph.getDIrection().ordinal())
                        .setDiagonal(buildingGlyph._isDiagonal)
                        .setElevation(buildingGlyph.getElevation())
                        .setX(buildingGlyph.getPositionIndex()._x)
                        .setY(buildingGlyph.getPositionIndex()._y)
                        .setTextureId(buildingGlyph.getTextureId());

                buildingModelBuilder.addGlyphs(buildingGlyphBuilder.build());
            }

            villageBuilder.addBuildingModels(buildingModelBuilder.build());
        }

        _message = villageBuilder.build();

        return true;
    }

    @Override
    public void onResponse(Network.header header, InputStream is) {
        if ( header != null && header.getHeader() == Network.header.HeaderEnum.ACK ) {
            RenderManager.instance().showAlertMsg("Village saved successfully!");
        }
        else
        {
            RenderManager.instance().showAlertMsg("Village save failed!");
        }
    }
}
