package com.bokyu.buildhouse.network;

import com.badlogic.gdx.Gdx;
import com.bokyu.buildhouse.Account;
import com.bokyu.buildhouse.RenderManager;
import com.bokyu.buildhouse.etc.Configuration;
import com.bokyu.buildhouse.etc.Constants;
import com.bokyu.buildhouse.etc.MyHelper;
import com.bokyu.buildhouse.network.jobs.NetworkJob;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by bk on 2016-11-22.
 */

public class NetworkManager extends Thread {

    static private NetworkManager _instance = null;

    private Object _syncToken = new Object();
    private Queue<NetworkJob> _networkJobs = new ConcurrentLinkedQueue<NetworkJob>();
    private String _ip, _port;
    private Socket _socket = null;
    private InputStream _inputStream = null;
    private OutputStream _outputStream = null;
    private boolean _running = false;


    static public NetworkManager instance() {
        if ( _instance == null ) {
            _instance = new NetworkManager();
        }
        return _instance;
    }

    private NetworkManager() {
    }

    public boolean init() {
        _ip = Configuration.instance().getText("network-server.ip");
        _port = Configuration.instance().getText("network-server.port");
        start();
        return true;
    }


    public void stopRunning() {
        _running = false;
    }

    @Override
    public void run() {
        _running = true;

        try {
            consumeJobs();
            while (_running) {
                synchronized (_syncToken) {
                    _syncToken.wait();
                }
                consumeJobs();
            }
        } catch (InterruptedException e ) {
            e.printStackTrace();
        }

        _running = false;
    }

    public void closeConnection() {
        try {
            _socket.close();
            _socket = null;
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public boolean connect() {
        int TIME_OUT = 1000;
        if ( _socket != null && _socket.isConnected() ) return true;
        try {
            if ( _socket != null && !_socket.isClosed())  _socket.close();
            _socket = new Socket();
            _socket.connect(new InetSocketAddress(_ip, Integer.parseInt(_port)), TIME_OUT);
            _inputStream = _socket.getInputStream();
            _outputStream = _socket.getOutputStream();
        } catch (IOException e ) {
            RenderManager.instance().showAlertMsg("network connection failed.");
            Gdx.app.error("NetworkManager", e.getMessage() + "\n" + MyHelper.getStackTraceString(e.getStackTrace()));
            return false;
        }
        return true;
    }

    public void onDestroy() {
        try {
            if (_socket != null && !_socket.isClosed()) _socket.close();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    public boolean addJob(NetworkJob job) {
        if ( Account.instance().getId().equals(Account.DEFAULT) && !Constants.DEBUG_MODE ) {
            RenderManager.instance().showAlertMsg("please login to google first.");
            return false;
        }
        _networkJobs.add(job);
        synchronized (_syncToken) {
            _syncToken.notify();
        }

        return true;//consumeJobs();
    }

    public boolean writeJob(NetworkJob networkJob) {
        Network.header.Builder headerBuilder = Network.header.newBuilder();
        headerBuilder.setHeader(networkJob._header)
                     .setAccountId(networkJob.getAccountId())
                     .setAccountName(networkJob.getAccountName());
        ArrayList<Integer> options = networkJob.getOptions();
        for ( int option : options ) {
            headerBuilder.addOptions(option);
        }

        Network.header headerPacket = headerBuilder.build();
        try {
            if ( _outputStream == null ) return false;
            headerPacket.writeDelimitedTo(_outputStream);
            if ( networkJob._message != null ) {
                networkJob._message.writeDelimitedTo(_outputStream);
            }
            Network.header responseHeader = readResult();
            networkJob.onResponse(responseHeader, _inputStream);
        } catch ( IOException e ) {
            closeConnection();
            connect();
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public Network.header readResult() {
        try {
            Network.header headerPacket = Network.header.parseDelimitedFrom(_inputStream);
            return headerPacket;
        } catch ( IOException e ) {
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getInputStream() {
        return _inputStream;
    }

    synchronized private boolean consumeJobs() {

        while (true) {
            if ( Account.instance().getId().equals(Account.DEFAULT) && !Constants.DEBUG_MODE) {
                RenderManager.instance().showAlertMsg("please login to google first.");
                break;
            }
            NetworkJob networkJob = _networkJobs.poll();
            if ( networkJob == null ) break;
            if ( connect() == false ) return false;
            if ( !writeJob(networkJob) ) return false;
        }
        return true;
    }
}
